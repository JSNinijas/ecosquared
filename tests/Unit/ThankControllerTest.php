<?php

namespace Tests\Unit;

use App\Thank\Thank;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ThankControllerTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Should return thank by thank id
     */
    public function testGetThankById() {
        $thank = $this->app->make('App\Thank\Thank');
        $this->createDatabaseData();
        $thankId = $thank->orderBy('id', 'desc')->first()->id;
        $response = $this->get('/api/thank/thanks/' . $thankId);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => true
            ])
            ->assertSee('thank');
    }

    /**
     * Should delete thank from database by thank id
     */
    public function testDeleteThank() {
//        $thank = $this->app->make('App\Thank\Thank');
//        $this->createDatabaseData();
//        $thankId = $thank->orderBy('id', 'desc')->first()->id;
//
//        $response = $this->delete('/api/thank/thanks/' . $thankId);
//
//        $response
//            ->assertStatus(200)
//            ->assertJson([
//                'status' => true
//            ]);
    }

    /**
     * This method fills database table by fake testing data
     * @return array
     */
    public function createDatabaseData() {
        $user = $this->app->make('App\User');

        if(!($user->find(1) && $user->find(2))) {
            factory(User::class, 2)->create();
        }
        $thank = $this->app->make('App\Thank\Thank');
        $userId = $user->first()->id;
        $targetUserId = $user->orderBy('id', 'desc')->first()->id;

        $thank->create([
            'user_id' => $userId,
            'target_user_id' => $targetUserId,
            'value' => 100
        ]);

        return array($userId, $targetUserId);
    }
}