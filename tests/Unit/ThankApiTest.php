<?php

namespace Tests\Unit;

use App\Thank\ThankApi;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ThankApiTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Should create thank and store it in database
     */
//    public function testCreateThank() {
//        $thankApi = $this->app->make('App\Thank\ThankApi');
//        $userIds = $this->createDatabaseData();
//
//        $thank = $thankApi->createThank($userIds[0], $userIds[1], 100, 'test');
//
//        $this->assertArrayHasKey('thank', $thank);
//    }

    /**
     * Should return thank by id
     */
    public function testGetThankById() {
        $thankApi = $this->app->make('App\Thank\ThankApi');
        $thank = $this->app->make('App\Thank\Thank');
        $this->createDataBaseData();

        $thankId = $thank->orderBy('id', 'desc')->first()->id;
        $thank = $thankApi->getThankById($thankId);

        $this->assertArrayHasKey('thank', $thank);
    }

    /**
     * Should return all thanks given by user
     */
    public function testGetThanks() {
        $thankApi = $this->app->make('App\Thank\ThankApi');
        $userIds = $this->createDatabaseData();

        $thanks = $thankApi->getThanks($userIds[0]);

        $this->assertArrayHasKey('thanksFrom', $thanks);
        $this->assertArrayHasKey('thanksTo', $thanks);
    }

    /**
     * Should delete thank from database
     */
    public function testDeleteThank() {
        $thankApi =$this->app->make(ThankApi::class);
        $thank = $this->app->make('App\Thank\Thank');
        $this->createDatabaseData();

        $thankId = $thank->orderBy('id', 'desc')->first()->id;

        $thankApi->deleteThank($thankId);

        $this->assertDatabaseMissing('thanks', [
            'id' => $thankId
        ]);
    }

    /**
     * This method fills database table by fake testing data
     * @return array
     */
    public function createDatabaseData()
    {
        $user = $this->app->make('App\User');
        if(!($user->find(1) && $user->find(2))) {
            factory(User::class, 2)->create();
        }
        $thank = $this->app->make('App\Thank\Thank');
        $userId = $user->first()->id;
        $targetUserId = $user->orderBy('id', 'desc')->first()->id;
        $thank->create([
            'user_id' => $userId,
            'target_user_id' => $targetUserId,
            'value' => 100
        ]);

        return array($userId, $targetUserId);
    }
}