<?php

namespace Tests\Unit;

use App\Wallet\AccountModel;
use App\Wallet\WalletApi;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class WalletApiTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Should create an account type and store it in database
     */
    public function testCreateAccountType()
    {
        $wallet = $this->app->make('App\Wallet\WalletApi');
        $accountType = $wallet->createAccountType('test', 'test', 1);

        $this->assertArrayHasKey('accountType', $accountType);
    }

    /**
     * Should create a transaction type and store it in database
     */
    public function testCreateTransactionType()
    {
        $wallet = $this->app->make('App\Wallet\WalletApi');
        $transactionType = $wallet->createTransactionType('test', 1);

        $this->assertArrayHasKey('transactionType', $transactionType);
    }

    /**
     * Should create a user transaction
     */
    public function testCreateTransaction()
    {
        $wallet = $this->app->make('App\Wallet\WalletApi');
        $this->createTestDatabaseData();

        $transaction = $wallet->createTransaction('test', 'received',
            500,  1, 1);

        $this->assertArrayHasKey('transaction', $transaction);
    }

    /**
     * Should update a user total balance by userId and amount of credits
     */
    public function testUpdateUserTotalBalance()
    {
        $wallet = $this->app->make('App\Wallet\WalletApi');
        $userBalance = $wallet->fetchUserBalance(1)['userBalance'];

        $wallet->updateUserTotalBalance(1, 1000);

        $this->assertDatabaseHas('user_total_balance', ['total_balance' => $userBalance + 1000]);
    }

    /**
     * Should return a user total balance by user id
     */
    public function testFetchUserBalance()
    {
        $wallet = $this->app->make('App\Wallet\WalletApi');
        $userBalance = $wallet->fetchUserBalance(1);

        $this->assertArrayHasKey('userBalance', $userBalance);
    }

    /**
     * Should return all user balances
     */
    public function testFetchAllUserBalances()
    {
        $wallet = $this->app->make('App\Wallet\WalletApi');
        $this->createTestDatabaseData();

        $userBalances = $wallet->fetchAllUserBalances();

        $this->assertArrayHasKey('userBalances', $userBalances);
    }

    /**
     * Should return a user transaction by no or several params
     */
    public function testGetUserTransaction()
    {

    }

    /**
     * Should return all account types
     */
    public function testGetAccountTypes()
    {
        $wallet = $this->app->make('App\Wallet\WalletApi');
        $accountTypes = $wallet->getAccountTypes();

        $this->assertArrayHasKey('accountType', $accountTypes);
    }

    /**
     * Should return all transaction types
     */
    public function testGetTransactionTypes()
    {
        $wallet = $this->app->make('App\Wallet\WalletApi');
        $transactionTypes = $wallet->getTransactionTypes();

        $this->assertArrayHasKey('transactionType', $transactionTypes);
    }

    /**
     * Should delete a user transaction by transaction id
     */
    public function testDeleteTransaction()
    {
        $wallet = $this->app->make('App\Wallet\WalletApi');
        $wallet->deleteTransaction(1);

        $this->assertDatabaseMissing('account', [
            'id' => 1
        ]);
    }

    /**
     * Should activate (if inactive) or inactivate (if active) a user transaction
     */
    public function testUpdateTransaction()
    {
        $wallet = $this->app->make('App\Wallet\WalletApi');
        $accountModel = $this->app->make('App\Wallet\AccountModel');
        $this->createTestDatabaseData();

        $transactionStatus = $accountModel->first()->transaction_status;
        $transactionId = $accountModel->first()->id;

        if($transactionStatus == 1) {
            $wallet->updateTransaction($transactionId, 0);
            $this->assertDatabaseHas('account', [
                'id' => $transactionId,
                'transaction_status' => 0
            ]);
        } else {
            $wallet->updateTransaction($transactionId, 1);
            $this->assertDatabaseHas('account', [
                'id' => $transactionId,
                'transaction_status' => 1
            ]);
        }
    }

    /**
     * Should rebuild all total user balances dependently on account table
     */
    public function testRebuildAllUserBalances() {
        $wallet = $this->app->make('App\Wallet\WalletApi');
        $this->createTestDatabaseData();

        $userBalances = $wallet->rebuildAllUserBalances();

        $this->assertArrayHasKey('userBalances', $userBalances);
    }

    /**
     * Should rebuild user total balance dependently on account table by user id
     */
    public function testRebuildUserTotalBalance() {
        $wallet = $this->app->make('App\Wallet\WalletApi');
        $this->createTestDatabaseData();

        $userBalance = $wallet->rebuildUserTotalBalance(1);

        $this->assertArrayHasKey('userBalance', $userBalance);
    }

    /**
     * This method is used to create fake testing database data
     */
    public function createTestDatabaseData() {
        $wallet = $this->app->make('App\Wallet\WalletApi');

        $wallet->createAccountType('test', 'unique name', 1);
        $wallet->createTransactionType('received', 1);
        $wallet->createTransaction('test', 'received',
            100, 1, 1);
    }
}