<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\User;

class AdminUserControllerTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    public function testIndex() {
        $this->createTestDatabaseData();

        $response = $this->get('api/admin/users');

        $response->assertStatus(200)
            ->assertSee('id')
            ->assertSee('name');
    }

//    public function testStore() {
//        $this->createTestDatabaseData();
//
//        $response = $this->post('/api/admin/users', [
//            'name' => 'test',
//            'surname' => 'test',
//            'email' => 'test@test.test',
//            'password' => bcrypt('test'),
//        ]);
//
//        $response->assertStatus(200)
//            ->assertSee('id')
//            ->assertSee('name');
//    }

    public function testShow() {
        $this->createTestDatabaseData();

//        $response = $this->get('/');
    }

    public function testUpdate() {

    }

    public function testDestroy() {

    }

    /**
     * Should return a user total balance by user_id
     */
    public function testGetTotalBalance ()
    {
        $this->createTestDatabaseData();

        $response = $this->get('/api/admin/users/1/total-balance');

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => 'true'
            ])
            ->assertSee('userBalance');
    }

    /**
     * Should rebuild user total balance dependently on account table by user id
     */
    public function testRebuildUserTotalBalance () {

        $this->createTestDatabaseData();

        $response = $this->patch('/api/admin/users/1/total-balance');

        $response->assertStatus(200)
            ->assertJson([
                'status' => true
            ]);
    }

    /**
     * Should create thank and store it in database
     */
//    public function testCreateThank() {
//
//        $userIds = $this->createTestDatabaseData();
//
//        $response = $this->post('/api/admin/users/' . $userIds[0] . '/thanks', [
//            'targetUserId' => $userIds[1],
//            'value' => 100,
//            'message' => 'test'
//        ]);
//
//        $response
//            ->assertStatus(200)
//            ->assertJson([
//                'message' => 'test'
//            ])
//            ->assertSee('100');
//    }

    /**
     * Should return all thanks one user gave
     */
    public function testGetThanks() {
        $userIds = $this->createTestDatabaseData();

        $response = $this->get('/api/admin/users/' . $userIds[0] . '/thanks');

        $response->assertStatus(200)
            ->assertJson([
                'status' => true
            ])
            ->assertSee('thanks');
    }

    /**
     * This method is used to create fake testing database data
     */
    public function createTestDatabaseData ()
    {
        $wallet = $this->app->make('App\Wallet\WalletApi');
        $user = $this->app->make('App\User');
        $thank = $this->app->make('App\Thank\Thank');

        $wallet->createAccountType('test', 'unique string', 1);
        $wallet->createTransactionType('received', 1);
        $wallet->createTransaction('test', 'received',
            100,  1, 1);

        if(!($user->find(1) && $user->find(2))) {
            factory(User::class, 2)->create();
        }
        $userId = $user->first()->id;
        $targetUserId = $user->orderBy('id', 'desc')->first()->id;

        $thank->create([
            'user_id' => $userId,
            'target_user_id' => $targetUserId,
            'value' => 100
        ]);

        return array($userId, $targetUserId);
    }
}