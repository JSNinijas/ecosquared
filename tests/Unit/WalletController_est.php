<?php

namespace Tests\Unit;

use App\Wallet\AccountModel;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

use Illuminate\Foundation\Testing\DatabaseTransactions;

class WalletControllerTest extends TestCase
{
    use WithoutMiddleware;
    use DatabaseTransactions;

    /**
     * Should create AccountType and store it in database
     */
    public function testCreateAccountType() {

        $response = $this->post('/api/wallet/account-types', [
            'accountType' => 'test',
            'machineName' => 'test',
            'isActive' => 1
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => true,
            ])
            ->assertSee('test');
    }

    /**
     * Should create TransactionType and store it in database
     */
    public function testCreateTransactionType() {

        $response = $this->post('/api/wallet/transaction-types', [
            'transactionType' => 'test',
            'isActive' => 1
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => true
            ])
            ->assertSee('test');
    }

    /**
     * Should create a transaction and store it in database
     */
    public function testCreateUserTransaction() {

        $this->createTestDatabaseData();

        $response = $this->post('/api/wallet/transactions', [
            'accountType' => 'test',
            'transactionType' => 'received',
            'amount' => 100,
            'transactionDate' => '30.08.2017',
            'userId' => 1,
            'transactionStatus' => 1
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => true,
            ])
            ->assertSee('received');
    }

    /**
     * Should return a user transaction by no or several params
     */
    public function testGetUserTransaction() {

        $this->createTestDatabaseData();

        $response = $this->get('/api/wallet/transactions');

        $response
            ->assertStatus(200)
            ->assertSee('user_id');
    }

    /**
     * Should return all account types
     */
    public function testGetAccountTypes() {
        $response = $this->get('/api/wallet/account-types');

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => true,
            ])
            ->assertSee('accountType');
    }

    /**
     * Should return all transaction types
     */
    public function testGetTransactionTypes() {
        $response = $this->get('/api/wallet/transaction-types');

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => true
            ])
            ->assertSee('transactionType');
    }

    /**
     * Should delete transaction from database by transaction id
     */
    public function testDeleteTransaction() {

        $this->createTestDatabaseData();

        $accountModel = new AccountModel();
        $transactionId = $accountModel->orderBy('id', 'desc')->first()->id;

        $response = $this->delete('/api/wallet/transactions/' . $transactionId);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => true
            ]);
    }

    /**
     * Should activate (if inactive) or inactivate (if active) a user transaction
     */
    public function updateTransaction() {

        $this->createTestDatabaseData();

        $accountModel = $this->app->make('App\Wallet\AccountModel');
        $transactionStatus = $accountModel->first()->transaction_status;
        $transactionId = $accountModel->first()->id;
        $action = 0;

        if($transactionStatus == 0) {
            $action = 1;
        }

        $response = $this->patch('/api/wallet/transactions', [
            'transactionId' => $transactionId,
            'transactionStatus' => $action
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => true
            ]);
    }

    /**
     * This method is used to create fake testing database data
     */
    public function createTestDatabaseData()
    {
        $wallet = $this->app->make('App\Wallet\WalletApi');
        $thank = $this->app->make('App\Thank\Thank');

        $wallet->createAccountType('test', 'unique string', 1);
        $wallet->createTransactionType('received', 1);
        $wallet->createTransaction('test', 'received',
            100,  1, 1);
    }
}