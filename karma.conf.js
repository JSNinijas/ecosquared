// Karma configuration
// Generated on Tue Aug 08 2017 09:40:53 GMT+0000 (UTC)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
      'resources/assets/js/**/*.test.js'
    ],


    // list of files to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
        // add webpack as preprocessor
        'resources/assets/js/**/*.js': ['webpack', 'sourcemap']
    },

    webpack: Object.assign(
        {},
        { devtool: 'cheap-eval-source-map', watch: true },
        require('laravel-mix/setup/webpack.config.js')
    ),

    webpackMiddleware: {
       // webpack-dev-middleware configuration
       // i.e.
       noInfo: true,
       // and use stats to turn off verbose output
       stats: {
           // options i.e.
           chunks: false
       }
    },

  //  plugins: [
  //      require("karma-webpack"),
  //      require("karma-phantomjs-launcher"),
  //      require("karma-jasmine")
  //  ],


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_DISABLE,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['PhantomJS'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  })
};
