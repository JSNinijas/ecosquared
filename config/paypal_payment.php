<?php

return [
    /*
     * This part of config file made for Paypal Classic API Library
     * by Angell Eye
    */

    // Acount credentials for Classic API
    'classic' => [
        'api_username' => env('API_USERNAME', ''),
        'api_password' => env('API_PASSWORD', ''),
        'api_signature' => env('API_SIGNATURE', ''),
        'sandbox' => env('SANDBOX'),
    ],

    'settings' => [
        'print_headers' => true,
        'log_results' => false,
        'log_path' => '../log.log',
    ],

    //--------------------------------------------------------------------------------------------


    # Define your application mode here
    'mode' => env('PAYPAL_MODE', 'sandbox'),

    # Account credentials from developer portal
    'account' => [
        'client_id' => env('PAYPAL_CLIENT_ID', ''),
        'client_secret' => env('PAYPAL_CLIENT_SECRET', ''),
    ],

    # Connection Information
    'http' => [
        'connection_time_out' => 30,
        'retry' => 1,
    ],

    # Logging Information
    'log' => [
        'log_enabled' => true,

        # When using a relative path, the log file is created
        # relative to the .php file that is the entry point
        # for this request. You can also provide an absolute
        # path here
        'file_name' => '../PayPal.log',

        # Logging level can be one of FINE, INFO, WARN or ERROR
        # Logging is most verbose in the 'FINE' level and
        # decreases as you proceed towards ERROR
        'log_level' => 'FINE',
    ],
];
