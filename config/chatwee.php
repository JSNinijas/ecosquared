<?php

/**
 *  Chatwee config file created when remaking PHP SDK
 */

return [
    'account' => [
        'chatId' => env('CHAT_ID', ''),
        'clientApiKey' => env('CLIENT_API_KEY', ''),
        'sessionCookieKey' => env('SESSION_COOKIE_KEY', ''),
    ]
];