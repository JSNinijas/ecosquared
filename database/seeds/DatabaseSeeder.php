<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(Transaction_typesTableSeeder::class);
        $this->call(AccountTableSeeder::class);
        $this->call(UserRelationsTableSeeder::class);
        $this->call(ThanksTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(SharedProductsTableSeeder::class);
        $this->call(LikesTableSeeder::class);
        $this->call(AcceptedSharedProductsSeeder::class);
        $this->call(GlobalRestrictionsTableSeeder::class);
        $this->call(ProjectsTableSeeder::class);
        $this->call(SharedProjectsTableSeeder::class);
        $this->call(HappiesTableSeeder::class);
        $this->call(AcceptedSharedProjectsSeeder::class);
        $this->call(UserActivityTableSeeder::class);
        $this->call(ProductActivityTableSeeder::class);
        $this->call(EmailsTableSeeder::class);
        $this->call(ProjectActivityTableSeeder::class);
        $this->command->info('All tables seeded!');
    }
}