<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    public function run()
    {
        if(!(\App\Role::where('name', '=','admin')->first())) {
            $admin = new \App\Role();
            $admin->name = 'admin';
            $admin->display_name = 'User Administrator'; // optional
            $admin->description = 'User is allowed to manage and edit other users'; // optional
            $admin->save();
        }

        if(!(\App\Role::where('name', '=','moderator')->first())) {
            $moderator = new \App\Role();
            $moderator->name = 'moderator';
            $moderator->display_name = 'User Moderator'; // optional
            $moderator->description = 'User is allowed to manage and edit other users'; // optional
            $moderator->save();
        }
    }
}
