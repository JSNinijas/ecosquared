<?php

use Illuminate\Database\Seeder;
use App\Settings\Settings;
use App\User;

class EmailsTableSeeder extends Seeder
{
    protected $settings;
    protected $user;

    public function __construct(Settings $settings, User $user)
    {
        $this->settings = $settings;
        $this->user = $user;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = $this->user->all();
        $users->map(function ($user) {
            $this->settings->create(['user_id' => $user->id]);
        });
    }
}
