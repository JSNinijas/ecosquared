<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class HappiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $userModel = new \App\User();
        $happyModel = new \App\Happy\Happy();
        DB::table('happies')->truncate();


        $projectIds = [1 => [9, 10], 2 => [1], 3 => [3, 8], 4 => [7],
            5 => [1], 6 => [2, 4], 7 => [2], 8 => [3, 5], 9 => [6], 10 => [8], 11 => [9]];

        for($i = 1; $i <= count($projectIds); $i++) {
            $user = $userModel->find($i);
            $user->projectsHappied()->attach($projectIds[$i]);
        }

        $happies = $happyModel->All();

        foreach($happies as $happy) {
            $happy->update(['value' => $faker->numberBetween(1, 10), 'message' => $faker->realText(50)]);
        }

        $happyApi = app()->make('App\Happy\HappyApi');
        $happyApi->rebuildHappyValues();
    }
}