<?php

use Illuminate\Database\Seeder;

class GlobalRestrictionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param \App\Restriction $restriction
     * @return void
     */
    public function run(\App\Restriction $restriction)
    {
        DB::table('global_restrictions')->truncate();
        $restriction->create(['share' => 1, 'support' => 1]);
    }
}
