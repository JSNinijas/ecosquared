<?php

use Illuminate\Database\Seeder;
use App\Wallet\TransactionTypeModel;

class Transaction_typesTableSeeder extends Seeder
{
    /**
     * @var TransactionTypeModel
     */
    protected $transactionTypeModel;

    /**
     * Transaction_typesTableSeeder constructor.
     * @param TransactionTypeModel $transactionTypeModel
     */
    public function __construct(TransactionTypeModel $transactionTypeModel) {
        $this->transactionTypeModel = $transactionTypeModel;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $transactionTypes = ['received', 'shared', 'pending', 'bought'];

        $this->transactionTypeModel->truncate();

        for($i=0; $i<count($transactionTypes); $i++) {
            $this->transactionTypeModel->create([
                'code' => $transactionTypes[$i],
                'status' => 1
            ]);
        }
    }
}