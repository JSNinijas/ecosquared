<?php

use Illuminate\Database\Seeder;
use App\Wallet\AccountModel;
use App\Wallet\WalletApi;
use App\Wallet\UserTotalBalance;
use App\User;

class AccountTableSeeder extends Seeder
{
    /**
     * @var WalletApi
     * @var AccountModel
     */
    protected $walletApi;
    protected $accountModel;
    protected $balance;
    protected $user;

    /**
     * AccountTableSeeder constructor.
     * @param WalletApi $walletApi
     * @param AccountModel $accountModel
     * @param UserTotalBalance $balance
     * @param User $user
     */
    public function __construct(WalletApi $walletApi, AccountModel $accountModel, UserTotalBalance $balance, User $user) {
        $this->walletApi = $walletApi;
        $this->accountModel = $accountModel;
        $this->balance = $balance;
        $this->user = $user;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        factory(AccountModel::class, 100)->create();

        $transactions = $this->accountModel->where('transaction_status', '=', 1)->get();
        $users = $this->user->all('id');

        foreach($transactions as $transaction) {
            if($transaction->transaction_type === 'shared') {
                $transaction->amount = -$transaction->amount;
                $transaction->save();
            }
        }

        foreach($users as $user) {
            $this->walletApi->rebuildUserTotalBalance($user->id);
            $userBalance = $this->balance
                ->where('user_id', '=', $user->id)->first()->total_balance;
            if($userBalance < 0) {
                $this->walletApi->createTransaction('default',
                    'received', ($userBalance + $userBalance) * (-1), $user->id, 1);
            }
            if(!$userBalance) {
                $userTotal = $this->balance->create([
                    'user_id' => $user->id,
                    'total_balance' => 0,
                    'modify_date' => date('Y-m-d H:i:s'),
                    ]);
            }
        }
    }
}
