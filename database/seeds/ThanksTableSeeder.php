<?php

use Illuminate\Database\Seeder;
use App\Thank\Thank;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class ThanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Thank $thankModel)
    {
        DB::table('thanks')->truncate();
        $faker = Faker::create();

        $thanks = [[1, 2, 2], [1, 4, 4], [2, 1, 1],
            [2, 3, 3], [2, 4, 4], [3, 1, 1], [3, 2, 2], [3, 4, 4], [4, 1, 1], [4, 2, 2], [4, 3, 3],[1, 2, 2], [1, 4, 4], [2, 1, 1],
            [2, 3, 3], [2, 4, 4], [3, 1, 1], [3, 2, 2], [3, 4, 4], [4, 1, 1], [4, 2, 2], [4, 3, 3],[1, 2, 2], [1, 4, 4], [2, 1, 1],
            [2, 3, 3], [2, 4, 4], [3, 1, 1], [3, 2, 2], [3, 4, 4], [4, 1, 1], [4, 2, 2], [4, 3, 3],[1, 2, 2], [1, 4, 4], [2, 1, 1],
            [2, 3, 3], [2, 4, 4], [3, 1, 1], [3, 2, 2], [3, 4, 4], [4, 1, 1], [4, 2, 2], [4, 3, 3],[1, 2, 2], [1, 4, 4], [2, 1, 1],
            [2, 3, 3], [2, 4, 4], [3, 1, 1], [3, 2, 2], [3, 4, 4], [4, 1, 1], [4, 2, 2], [4, 3, 3],[1, 2, 2], [1, 4, 4], [2, 1, 1],
            [2, 3, 3], [2, 4, 4], [3, 1, 1], [3, 2, 2], [3, 4, 4], [4, 1, 1], [4, 2, 2], [4, 3, 3],[1, 2, 2], [1, 4, 4], [2, 1, 1],
            [2, 3, 3], [2, 4, 4], [3, 1, 1], [3, 2, 2], [3, 4, 4], [4, 1, 1], [4, 2, 2], [4, 3, 3]];

        for($i = 0; $i < count($thanks); $i++) {
            $thankModel->create(['user_id' => $thanks[$i][0],
                'target_user_id' => $thanks[$i][1], 'value' => $thanks[$i][2],
                'message' => $faker->realText(50)]);
        }
    }
}
