<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class UserRelationsTableSeeder extends Seeder
{
     /**
     * Run the database seeds.
     *
     * @param User $userModel
     * @return void
     */
    public function run(User $userModel)
    {
        DB::table('user_relations')->truncate();

        $friendsIds = [1 => [2, 3, 4, 5, 6, 7, 8, 9, 11], 2 => [1, 5, 6], 3 => [1, 7, 8], 4 => [1, 9, 10, 11],
            5 => [1, 2], 6 => [1, 2], 7 => [1, 3], 8 => [1, 3], 9 => [1, 4], 10 => [3, 4], 11 => [1, 4]];

        for($i = 1; $i <= count($friendsIds); $i++) {
            $user = $userModel->find($i);
            $user->users()->attach($friendsIds[$i]);
        }

//        for($i = 2; $i <= 101; $i++) {
//            $friends[] = $i;
//        }
//        shuffle($friends);
//
//        $user = $userModel->find(1);
//        $user->users()->attach($friends);

    }
}