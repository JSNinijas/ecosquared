<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Product\Product;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class SharedProductsTableSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(User $userModel, Product $productModel)
    {
        DB::table('shared_products')->truncate();

        $userModel = new \App\User();

        $productIds = [1 => [9, 10], 2 => [1], 3 => [3, 8], 4 => [6, 7], 5 => [1 , 10],
            6 => [2, 4], 7 => [2, 3], 8 => [3, 5], 9 => [6, 7] , 10 => [8], 11 => [9]];

        for($i = 1; $i <= count($productIds); $i++) {
            $user = $userModel->find($i);
            $user->productsShared()->attach($productIds[$i]);
        }

//        for($i = 1; $i <= 100; $i++) {
//            $products[] = $i;
//        }
//        shuffle($products);
//
//        $user = $userModel->find(1);
//        $user->productsShared()->attach($products);
    }
}
