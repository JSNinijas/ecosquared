<?php

use Illuminate\Database\Seeder;
use App\User;

class UserActivityTableSeeder extends Seeder
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = $this->user->all();

        $users->map(function ($user) {
            $user->users()->get()->map(function ($targetUser) use ($user) {
                $targetUser->activity()->attach($user->id, ['activity' => \Carbon\Carbon::now()]);
            });
        });
    }
}
