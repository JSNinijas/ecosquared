<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class ProjectsTableSeeder extends Seeder
{
    /**
     * @param \App\User $user
     */
    public function run(\App\User $user)
    {
        $faker = Faker::create();
        factory(App\Project\Project::class, 10)->create();

        $projectModel = new \App\Project\Project();
        $projects = $projectModel->all();
        $userIds = [1, 1, 1, 2, 1, 1, 1, 4, 4, 2];
        $usersNumber = $user->all()->count();

        foreach($projects as $project) {
            if($userIds) {
                $project->user_id = array_shift($userIds);
//                $project->user_id = 1;//$faker->numberBetween(1, $usersNumber);
                $project->save();
                continue;
            }
        }
    }
}