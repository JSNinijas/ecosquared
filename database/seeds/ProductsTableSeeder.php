<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(\App\User $user)
    {
        $faker = Faker::create();
        factory(App\Product\Product::class, 10)->create();

        $productModel = new \App\Product\Product();
        $products = $productModel->all();

        $userIds = [1, 1, 1, 2, 1, 1, 1, 4, 4, 2];
        $usersNumber = $user->all()->count();

        foreach($products as $product) {
            if($userIds) {
                $product->user_id = array_shift($userIds);

//                $product->user_id = 1; // fake

                $product->save();

                continue;
            }

            //$faker->numberBetween(1, $usersNumber);
        }
    }
}
