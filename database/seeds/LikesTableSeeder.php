<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class LikesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $userModel = new \App\User();
        $likeModel = new \App\Like\Like();
        DB::table('likes')->truncate();


        $productIds = [1 => [9, 10], 2 => [1], 3 => [3, 8], 4 => [7],
            5 => [1], 6 => [2, 4], 7 => [2], 8 => [3, 5], 9 => [6], 10 => [8], 11 => [9]];

        for($i = 1; $i <= count($productIds); $i++) {
            $user = $userModel->find($i);
            $user->productsLiked()->attach($productIds[$i]);
        }

        $likes = $likeModel->All();

        foreach($likes as $like) {
            $like->update(['value' => $faker->numberBetween(1, 10), 'message' => $faker->realText(50)]);
        }

        $likeApi = app()->make('App\Like\LikeApi');
        $likeApi->rebuildLikeValues();
    }
}
