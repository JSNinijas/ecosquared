<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{

    public function run()
    {
        if(!(\App\User::where('name','=', 'admin')->first())) {
            $admin = User::create(
                [
                    'name' => 'admin',
                    'surname' => 'admin',
                    'email' => 'admin@admin.com',
                    'password' => bcrypt('admin'),
                    'api_token' => 'adminapitoken',
                    'image' => 'https://lorempixel.com/640/480/?57559',
                    'description' => 'Description',
                    'sq' => 1,
                    'counter' => 0,
                    'active' => true,
                ]
            );

            $admin->attachRole(1);

            $admin->save();

        }

        factory(App\User::class, 10)->create();
    }
}
