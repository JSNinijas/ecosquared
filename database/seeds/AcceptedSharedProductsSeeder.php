<?php

use Illuminate\Database\Seeder;
use App\Share\AcceptedShare;

class AcceptedSharedProductsSeeder extends Seeder
{
    private $acceptedShare;

    public function __construct(AcceptedShare $acceptedShare)
    {
        $this->acceptedShare = $acceptedShare;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(1,2,1,6,1,3,3,4,1,4,6,10,1,4,7,12,1,5,1,8,1,6,2,14,
            1,7,2,28,1,8,5,2,1,9,6,18,2,5,10,12,2,6,4,16,3,7,3,10,3,8,3,2,
            4,9,7,10,4,10,8,8,4,11,9,20,5,1,10,4,10,3,8,16,11,1,9,6);

        while($data) {
            $this->acceptedShare->create([
                'user_id' => array_shift($data),
                'target_user_id' => array_shift($data),
                'product_id' => array_shift($data),
                'amount' => array_shift($data),
                'message' => 'default'
            ]);
        }
//        for($i = 2; $i<=101; $i++) {
//            $this->acceptedShare->create([
//                'user_id' => 1,
//                'target_user_id' => $i,
//                'product_id' => 1,
//                'amount' => 10,
//                'message' => 'nothing'
//            ]);
//        }
    }
}
