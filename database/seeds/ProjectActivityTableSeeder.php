<?php

use Illuminate\Database\Seeder;
use App\Project\Project;
use App\User;
use Carbon\Carbon;
use App\Share\AcceptedProjectShare;

class ProjectActivityTableSeeder extends Seeder
{
    protected $project;
    protected $user;
    protected $acceptedProjectShare;


    public function __construct(Project $project, User $user, AcceptedProjectShare $acceptedProjectShare)
    {
        $this->project = $project;
        $this->user = $user;
        $this->acceptedProjectShare = $acceptedProjectShare;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = $this->user->all();

        $users->map(function ($user) {
            $user->projects()->get()->map(function ($project) use ($user) {
                $project->activity()->attach($user->id, ['activity' => Carbon::now(), 'target_user_id' => 1]);
            });
            $shares = $this->acceptedProjectShare->where('target_user_id', '=', $user->id)->get();
            $shares->map(function ($share) use ($user) {
                $project = $this->project->find($share->project_id);
                $project->activity()->attach($user->id, ['activity' => Carbon::now(), 'target_user_id' => 1]);
            });
        });
    }
}