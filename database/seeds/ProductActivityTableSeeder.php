<?php

use Illuminate\Database\Seeder;
use App\Product\Product;
use App\User;
use Carbon\Carbon;
use App\Share\AcceptedShare;

class ProductActivityTableSeeder extends Seeder
{
    protected $product;
    protected $user;
    protected $acceptedShare;


    public function __construct(Product $product, User $user, AcceptedShare $acceptedShare)
    {
        $this->product = $product;
        $this->user = $user;
        $this->acceptedShare = $acceptedShare;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = $this->user->all();

        $users->map(function ($user) {
            $user->products()->get()->map(function ($product) use ($user) {
                $product->activity()->attach($user->id, ['activity' => Carbon::now(), 'target_user_id' => 1]);
            });
            $shares = $this->acceptedShare->where('target_user_id', '=', $user->id)->get();
            $shares->map(function ($share) use ($user) {
                $product = $this->product->find($share->product_id);
                $product->activity()->attach($user->id, ['activity' => Carbon::now(), 'target_user_id' => 1]);
            });
        });
    }
}
