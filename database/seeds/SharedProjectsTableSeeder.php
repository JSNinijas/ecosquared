<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Project\Project;
use Illuminate\Support\Facades\DB;

class SharedProjectsTableSeeder extends Seeder
{


    /**
     * @param User $userModel
     * @param Project $projectModel
     */
    public function run(User $userModel, Project $projectModel)
    {
        DB::table('shared_projects')->truncate();

        $userModel = new \App\User();

        $projectIds = [1 => [9, 10], 2 => [1], 3 => [3, 8], 4 => [6, 7], 5 => [1 , 10],
            6 => [2, 4], 7 => [2, 3], 8 => [3, 5], 9 => [6, 7] , 10 => [8], 11 => [9]];

        for($i = 1; $i <= count($projectIds); $i++) {
            $user = $userModel->find($i);
            $user->projectsShared()->attach($projectIds[$i]);
        }

//        for($i = 1; $i <= 100; $i++) {
//            $products[] = $i;
//        }
//        shuffle($products);
//
//        $user = $userModel->find(1);
//        $user->projectsShared()->attach($products);
    }
}