<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;


    return [
        'name' => $faker->firstName,
        'surname' => $faker->lastName,
        'email' => $faker->unique()->lexify('?????????@p33.org'),
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'api_token' => $faker->uuid,
        'image' => $faker->imageUrl(),
        'sq' => $faker->randomFloat(null, 0, 5),
        'description' => 'Description',
        'counter' => 0,
        'active' => true,
    ];
});

/**
 * This factory makes a fake transaction
 */
$factory->define(App\Wallet\AccountModel::class, function (Faker\Generator $faker) {

    $userModel = new \App\User();
    $userIds = $userModel->all('id')->toArray();
    $ids = array();
    $i=0;
    foreach($userIds as $id)
    {
        $ids[$i] = $id['id'];
        $i++;
    }
    $transactionTypes = ['received', 'shared', 'bought'];

    return [
        'user_id' => $faker->randomElement($ids),
        'amount' => $faker->numberBetween(1, 400),
        'account_type' => 'default',
        'transaction_type' => $faker->randomElement($transactionTypes),
        'transaction_date' => $faker->dateTimeThisMonth(),
        'transaction_status' => 1,
        'admin_id' =>1
    ];
});

/**
 * This factory makes a fake product
 */
$factory->define(App\Product\Product::class, function (Faker\Generator $faker) {

    $userModel = new \App\User();
    $userIds = $userModel->all('id')->toArray();
    $ids = array();
    $i=0;
    foreach($userIds as $id)
    {
        $ids[$i] = $id['id'];
        $i++;
    }

    return [
        'user_id' => $faker->randomElement($ids),
        'value' => random_int(1, 10),
        'title' => $faker->sentence(3),
        'description' => $faker->realText(160),
        'image' => $faker->imageUrl(),
        'link' => $faker->lexify('https://?????.?????.???'),
        'counter' => 0,
    ];
});

/**
 * This factory makes a fake project
 */
$factory->define(App\Project\Project::class, function (Faker\Generator $faker) {

    $userModel = new \App\User();
    $userIds = $userModel->all('id')->toArray();
    $ids = array();
    $i=0;
    foreach($userIds as $id)
    {
        $ids[$i] = $id['id'];
        $i++;
    }

    return [
        'user_id' => $faker->randomElement($ids),
        'value' => random_int(1, 10),
        'title' => $faker->sentence(3),
        'description' => $faker->realText(160),
        'image' => $faker->imageUrl(),
        'link' => $faker->lexify('https://?????.?????.???'),
//        'activity' => date('Y-m-d H:i:s'),
        'counter' => 0,
    ];
});