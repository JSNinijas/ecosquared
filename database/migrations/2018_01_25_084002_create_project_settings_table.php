<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->unsigned();
            $table->float('basic_balance')->unsigned()->default(1);
            $table->string('ownership_type')->default('equal');
            $table->string('sq_type')->default('closed');
            $table->float('ownership_sq_balance')->unsigned()->default(1);
            $table->string('credit_target_type')->default('support');
            $table->string('usership_type')->default('support');
            $table->float('credit_target_balance')->unsigned()->default(1);
            $table->string('vote_sq')->default('vote');
            $table->string('number_percent')->default('number');
            $table->float('ownership_usership_balance')->unsigned()->default(0);
            $table->string('editor_governance')->default('same');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_settings');
    }
}
