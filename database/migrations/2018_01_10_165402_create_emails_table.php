<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emails', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('shares')->default('instant');
            $table->string('push')->default('on');
            $table->string('activity')->default('weekly');
            $table->string('archive')->default('off');

            $table->string('thanks')->default('on');
            $table->string('thanks_remove')->default('off');
            $table->string('product_likes')->default('on');
            $table->string('product_likes_remove')->default('off');
            $table->string('project_happy')->default('on');
            $table->string('project_happy_remove')->default('off');
            $table->string('product_support')->default('on');
            $table->string('product_support_remove')->default('off');
            $table->string('project_support')->default('on');
            $table->string('project_support_remove')->default('off');
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emails');
    }
}
