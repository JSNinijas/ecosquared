<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeMessageDataTypeFieldsToText extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('likes', function (Blueprint $table) {
            $table->text('message')->nullable()->change();
        });

        Schema::table('supports', function (Blueprint $table) {
            $table->text('message')->nullable()->change();
        });

        Schema::table('project_supports', function (Blueprint $table) {
            $table->text('message')->nullable()->change();
        });

        Schema::table('pending_products', function (Blueprint $table) {
            $table->text('message')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
