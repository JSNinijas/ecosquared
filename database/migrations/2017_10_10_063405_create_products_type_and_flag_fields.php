<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTypeAndFlagFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->timestamp('creation_date')->default(\Carbon\Carbon::now());
            $table->string('type')->default('any');
            $table->integer('start')->unsigned()->nullable();
            $table->integer('end')->unsigned()->nullable();
            $table->boolean('share')->default(true);
            $table->boolean('support')->default(true);
            $table->boolean('sq_enabled')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('creation_date');
            $table->dropColumn('type');
            $table->dropColumn('start');
            $table->dropColumn('end');
            $table->dropColumn('share');
            $table->dropColumn('support');
            $table->dropColumn('sq_enabled');
        });
    }
}
