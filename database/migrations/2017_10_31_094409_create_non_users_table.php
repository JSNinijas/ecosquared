<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNonUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('non_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('card');
            $table->string('email')->unique();
            $table->integer('user_id')->unsigned();
            $table->integer('inviter_id')->unsigned();
            $table->integer('transaction_id')->unsigned();
            $table->text('message')->nullable();
            $table->integer('card_id')->unsigned();
            $table->integer('amount')->unsigned();
            $table->string('encrypted_email');
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('inviter_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('non_users');
    }
}
