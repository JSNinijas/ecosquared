<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsAdditionalFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->integer('happy')->unsigned()->nullable();
            $table->integer('value')->unsigned()->nullable();
            $table->string('image')->nullable();
            $table->timestamp('creation_date')->default(\Carbon\Carbon::now());
            $table->timestamp('completion_date')->default(\Carbon\Carbon::now()->addYear());
            $table->string('type')->default('any');
            $table->integer('start')->nullable();
            $table->integer('end')->nullable();
            $table->boolean('share')->default(true);
            $table->boolean('support')->default(true);
            $table->boolean('sq_enabled')->default(true);
            $table->timestamp('activity')->nullable();
            $table->integer('counter')->default(0);
            $table->string('author')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->dropColumn('happy');
            $table->dropColumn('value');
            $table->dropColumn('creation_date');
            $table->dropColumn('completion_date');
            $table->dropColumn('type');
            $table->dropColumn('start');
            $table->dropColumn('end');
            $table->dropColumn('share');
            $table->dropColumn('support');
            $table->dropColumn('sq_enabled');
            $table->dropColumn('activity');
            $table->dropColumn('counter');
            $table->dropColumn('author');
        });
    }
}
