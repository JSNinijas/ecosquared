<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncomingFlagFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('incoming_notifications', function (Blueprint $table) {
            $table->string('on_off')->default('on');
            $table->string('remove_on_read')->default('off');
            $table->string('read')->default('off');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('incoming_notifications', function (Blueprint $table) {
            $table->dropColumn('read');
            $table->dropColumn('remove_on_read');
            $table->dropColumn('on_off');
        });
    }
}
