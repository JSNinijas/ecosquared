<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function () {
    return auth()->user()->load('roles');
});

Route::group(
    [
        'namespace' => 'App',
        'prefix' => 'app',
        'middleware' => [
            'auth:api',
            'timeout'
        ],
        'except' => 'api/app/invite/register'
    ],
    function(){
        // UserController
//        Route::get('/users/count', 'UserController@countUsersList')->name('CountUsersList');

        Route::get('/users/{id}', 'UserController@show')->name('Show');
        Route::get('/users', 'UserController@showUsersList')->name('ShowUsersList');
        Route::get('/users/share/products/{id}', 'UserController@showUsersListToShare')->name('ShowUsersListToShare');
        Route::get('/users/share/projects/{id}', 'UserController@showUsersListToProjectShare')
            ->name('ShowUsersListToProjectsShare');
        Route::patch('/users/{id}', 'UserController@update')->name('Update');

        Route::patch('/password', 'UserController@changePassword')->name('ChangePassword');

        Route::get('/total-balance', 'UserController@getTotalBalance')->name('GetTotalBalance');
        Route::patch('/total-balance', 'UserController@rebuildUserTotalBalance')->name('RebuildUserTotalBalance');
        Route::get('/total-balance/types', 'UserController@getTotalBalanceByTypes')->name('GetTotalBalanceByTypes');


        Route::post('/thanks', 'UserController@createThank')->name('CreateThank');
        Route::get('/thanks', 'UserController@getThanks')->name('GetThanks');
        Route::post('/project-thanks', 'UserController@createProjectThank')->name('CreateProjectThank');
        Route::get('/project-thanks', 'UserController@getProjectThanks')->name('GetProjectThanks');
        Route::get('/project-thanks/projects/{id}', 'ProjectController@showProjectsListToThank')->name('ShowProjectsListToThank');
        Route::get('/project-thanks/thanks/{id}', 'ProjectController@showProjectThanks')->name('ShowProjectThanks');

        Route::get('/notifications/incoming', 'UserController@showIncomingList')->name('ShowIncomingList');
        Route::get('/notifications/incoming/count', 'UserController@countIncomingList')->name('CountIncomingList');
        Route::get('/notifications/outgoing', 'UserController@showOutgoingList')->name('ShowOutgoingList');
        Route::get('/notifications/outgoing/count', 'UserController@countOutgoingList')->name('CountOutgoingList');
        Route::get('/notifications/settings', 'UserController@getNotificationsSettings')->name('GetNotificationsSettings');
        Route::patch('/notifications/settings', 'UserController@setNotificationsSettings')->name('SetNotificationsSettings');
        Route::get('/notifications/offer-history', 'UserController@showOfferHistory')->name('ShowOfferHistory');

        Route::post('/invite/project', 'UserController@sendProjectInviteMessage')->name('SendProjectInviteMessage');
        Route::post('/invite', 'UserController@sendInviteMessage')->name('SendInviteMessage');
        Route::get('/invite/register/{param}', 'UserController@checkInvitedUser')->name('CheckInvitedUser');
        Route::post('/invite/register', 'UserController@registerUser')->name('RegisterUser');
        Route::patch('/invite/revoke/{id}', 'UserController@revokeInvite')->name('RevokeInvite');

        // Product/Project Controllers

        Route::patch('/products/revoke/{id}', 'ProductController@revokeProductProject')->name('RevokeProduct');
        Route::patch('/projects/revoke/{id}', 'ProjectController@revokeProductProject')->name('RevokeProject');
        Route::get('/products/share/{id}', 'ProductController@showProductsListToShare')->name('ShowProductsListToShare');
        Route::get('/projects/share/{id}', 'ProjectController@showProjectsListToShare')->name('ShowProjectsListToShare');

//        Route::get('/users/{id}/projects', 'ProjectsController@')->name('');

//        Route::get('/products/count', "ProductController@count");
        Route::get('/projects/count', "ProjectController@count");
        Route::get('/products/{id}/users', "UserController@showCurrentProductUsersList");
        Route::get('/projects/{id}/users', 'UserController@showCurrentProjectUsersList');
        Route::get('/projects/usership', 'ProjectController@showProjectListForUsership')->name('ShowProjectListForUsership');
        Route::get('/projects/{id}/usership', 'UserController@showProjectUsership')->name('ShowProjectUsership');
        Route::get('/projects/{id}/usership/known', 'UserController@showProjectKnownUsership')
            ->name('ShowProjectKnownUsership');
        Route::get('/projects/{id}/usership/unknown', 'UserController@showProjectUnknownUsership')
            ->name('ShowProjectUnknownUsership');

        Route::resource('products', 'ProductController');
        Route::resource('projects', 'ProjectController');


        Route::post('/likes', 'ProductController@createLike')->name('CreateLike');
        Route::post('/happies', 'ProjectController@createHappy')->name('CreateHappy');
        Route::patch('/likes/rebuild', 'ProductController@rebuildLikeValues')->name('RebuildLikeValues');

        Route::post('/products/support', 'ProductController@createSupport')->name('CreateSupport');
        Route::post('/projects/support', 'ProjectController@createSupport')->name('CreateProjectSupport');
        Route::post('/products/share', 'ProductController@createShare')->name('CreateShare');
        Route::post('/projects/share', 'ProjectController@createShare')->name('CreateProjectShare');
        Route::patch('/products/share/accept/{id}', 'ProductController@acceptShare')->name('AcceptShare');
        Route::patch('/projects/share/accept/{id}', 'ProjectController@acceptShare')->name('AcceptProjectShare');
        Route::patch('/products/share/reject/{id}', 'ProductController@rejectShare')->name('RejectShare');
        Route::patch('/projects/share/reject/{id}', 'ProjectController@rejectShare')->name('RejectProjectShare');

        // Projects part for multiple owners, editors, nested relations

        Route::post('/projects/projects', 'ProjectController@addProjectAuthor')
            ->name('AddProjectAuthor');
        Route::get('/projects/{id}/projects', 'ProjectController@getProjectAuthor')->name('GetProjectAuthor');
        Route::delete('/projects/{projectId}/projects/{authorId}', 'ProjectController@deleteProjectAuthor')
            ->name('DeleteProjectAuthor');
        Route::get('/projects/{id}/projects/add', 'ProjectController@showPossibleProjectAuthorList')
            ->name('ShowPossibleProjectAuthorList');
        Route::get('/projects/{id}/owners/add', 'UserController@showPossibleOwnerList')
            ->name('ShowPossibleOwnerList');
        Route::delete('/projects/{projectId}/owners/{ownerId}', 'ProjectController@deleteOwner')->name('DeleteOwner');
        Route::get('/projects/{id}/owners', 'ProjectController@getOwners')->name('GetOwners');
        Route::post('/projects/owners', 'ProjectController@addOwner')->name('AddOwner');
        Route::get('/projects/{id}/editors', 'ProjectController@getEditors')->name('GetEditors');
        Route::post('/projects{id}/editors', 'ProjectController@addEditor')->name('AddEditor');
        Route::get('/projects/{id}/parents', 'ProjectController@getParents')->name('GetParents');
        Route::post('/projects/parents', 'ProjectController@addParent')->name('AddParent');
        Route::get('/projects/{id}/children', 'ProjectController@getChildren')->name('GetChildren');
        Route::post('/projects/children', 'ProjectController@addChild')->name('AddChild');
        Route::get('/projects/{id}/settings', 'ProjectController@getSettings')->name('GetSettings');
        Route::post('/projects/settings', 'ProjectController@setSettings')->name('SetSettings');
    }
);

Route::get('/app/invite/register/{param}', 'App\UserController@checkInvitedUser')->name('CheckInvitedUser');
Route::post('/app/invite/register', 'App\UserController@registerUser')->name('RegisterUser');
Route::post('/invite/request', 'Admin\UserController@sendRequestInviteMessage')->name('SendRequestInviteMessage');
Route::post('/app/recovery', 'App\UserController@sendRecoveryMessage')->name('SendRecoveryMessage');
Route::get('/app/recovery/{param}', 'App\UserController@checkRecoveryUser')->name('Check');
Route::post('/app/recovery/password', 'App\UserController@setPassword')->name('SetPassword');

Route::post('/app/invite/unsubscribe','App\UserController@unsubscribeNonUser')->name('UnsubscribeNonUser');

Route::group(
    [
        'namespace' => 'PaypalPayment',
        'prefix' => 'payment',
        'middleware' => [
            'auth:api'
        ]
    ],
    function() {
//        Route::post('/express-checkout', 'PaypalPaymentController@payWithPaypal')->name('PayWithPaypal');
//        Route::post('/credit-card', 'PaypalPaymentController@payWithCreditCard')->name('PayWithCreditCard');
//        Route::get('/', 'PaypalPaymentController@index')->name('Index');
//        Route::post('/experience-profile', 'PaypalPaymentController@createExperienceProfile')->name('CreateExperienceProfile');
//        Route::get('/success/{id}', 'PaypalPaymentController@returnDataAfterSuccess')->name('ReturnDataAfterSuccess');
//        Route::get('/fail/{id}', 'PaypalPaymentController@returnDataAfterFail')->name('ReturnDataAfterSuccess');
//        Route::get('/balance', 'PaypalPaymentController@getBalance')->name('GetBalance');
    }
);

Route::group(
    [
        'namespace' => 'Admin',
        'prefix' => 'admin',
        'middleware' => [
            'auth:api', 'role:admin'
        ]
    ],
    function () {

        // UserController
        Route::post('users/{id}/assign-roles', 'UserController@assignRoles');

        Route::post('users/{id}/login', 'UserController@login');

        Route::get('users/{id}/total-balance', 'UserController@getTotalBalance')
            ->name('GetTotalBalance');
        Route::patch('users/{id}/total-balance', 'UserController@rebuildUserTotalBalance')
            ->name('RebuildUserTotalBalance');
        Route::get('total-balance','UserController@getFullBalance')->name('GetFullBalance');

        Route::post('/users/{id}/thanks', 'UserController@createThank')->name('CreateThank');
        Route::get('/users/{id}/thanks', 'UserController@getThanks')->name('GetThanks');
        Route::patch('/users/{userId}/thanks/{thankId}', 'UserController@updateThank')->name('UpdateThank');

        Route::get('/payment', 'UserController@showPaymentStats')->name('ShowPaymentStats');

        Route::get('/users/count', 'UserController@countUsers')->name('CountUsers');
        Route::resource('users', 'UserController');
        Route::resource('roles', 'RoleController');



        // Product/Project Controllers
        Route::patch('/products/share-support', 'ProductController@enableDisableShareSupport')
            ->name('EnableDisableShareSupport');
        Route::get('/products/share-support', 'ProductController@getShareSupport')->name('GetShareSupport');
        Route::get('/products/count', 'ProductController@countProducts')->name('CountProducts');
        Route::get('/projects/count', 'ProjectController@countProjects')->name('CountProjects');
        Route::resource('products', 'ProductController');
        Route::resource('projects', 'ProjectController');


    }
);

Route::group(
    [
        'namespace' => 'WalletApi',
        'prefix' => 'wallet',
        'middleware' => [
            'auth:api', 'role:admin'
        ]
    ],
    function () {
        Route::post('/account-types','WalletApiController@createAccountType')
            ->name('CreateAccountType');

        Route::post('/transaction-types', 'WalletApiController@createTransactionType')
            ->name('CreateTransactionType');

        Route::post('/transactions','WalletApiController@createTransaction')
            ->name('CreateTransaction');

        Route::get('/transactions','WalletApiController@getTransaction')
            ->name('GetTransaction');

        Route::get('/account-types', 'WalletApiController@getAccountTypes')
            ->name('GetAccountTypes');

        Route::get('/transaction-types', 'WalletApiController@getTransactionTypes')
            ->name('GetTransactionTypes');

        Route::delete('/transactions/{id}', 'WalletApiController@deleteTransaction')
            ->name('DeleteTransaction');

        Route::patch('/transactions/{id}', 'WalletApiController@updateTransaction')
            ->name('updateTransaction');

        Route::get('/transactions/count', 'WalletApiController@countTransactions')->name('CountTransaction');
    }
);

Route::group(
    [
        'namespace' => 'Thank',
        'prefix' => 'thank',
        'middleware' => [
            'auth:api', 'role:admin'
        ]
    ],
    function() {
        Route::get('/thanks', 'ThankController@index')->name('Index');
        Route::get('/thanks/{id}', 'ThankController@getThankById')->name('GetThankById');
        Route::delete('/thanks/{id}', 'ThankController@deleteThank')->name('DeleteThank');
    }
);

Route::group(
    [
        'namespace' => 'Like',
        'prefix' => 'like',
        'middleware' => [
            'auth:api', 'role:admin'
        ]
    ],
    function() {
        Route::get('/likes/{id}', 'LikeController@getLikeById')->name('GetLikeById');
        Route::delete('/likes/{id}', 'LikeController@deleteLike')->name('DeleteLike');
    }
);

Route::group(
    [
        'namespace' => 'Notification',
        'prefix' => 'notification',
        'middleware' => [
            'auth:api'
        ]
    ],
    function() {
        Route::get('/incoming/{id}', 'NotificationController@getIncoming')->name('GetIncoming');
        Route::get('/outgoing/{id}', 'NotificationController@getOutgoing')->name('GetOutgoing');
        Route::post('/outgoing', 'NotificationController@createOutgoing');
    }
);
