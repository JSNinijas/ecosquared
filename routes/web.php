<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {return view('app');});

Route::get('/update', function() {return view('update');});

Route::options('/*', function () {
    return response('ok', 200);
});

Route::any('/csrf', function () {
    return response()->json(['csrf' => csrf_token()]);
});

Route::get('/show-message', 'App\UserController@showMessage');

Route::get('/payment/fail', 'PaypalPayment\PaypalPaymentController@paymentFail')->name('PaymentFail');
Route::get('/payment/success', 'PaypalPayment\PaypalPaymentController@paymentSuccess')->name('PaymentSuccess');
Route::post('/payment/ipn', 'PaypalPayment\PaypalPaymentController@paymentIpn')->name('PaymentIpn');

Route::post('/login', "Auth\\LoginController@login")->name('login');
Route::any('/logout', "Auth\\LoginController@logout")->name('logout');


