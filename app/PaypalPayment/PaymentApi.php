<?php

namespace App\Payment;



use Anouar\Paypalpayment\Facades\PaypalPayment;
use App\PaypalPayment\Payment;
use App\User;
use App\Wallet\WalletApi;
use Ixudra\Curl\Facades\Curl;
use PayPal\Api\PaymentSummary;
use PayPal\Exception\PayPalConnectionException;
use angelleye\PayPal\PayPal;
use GuzzleHttp;

class PaymentApi
{
    protected $user;
    protected $_apiContext;
    protected $walletApi;
    protected $payment;

    public function __construct(User $user, WalletApi $walletApi, Payment $payment)
    {
        $this->user = $user;
        $this->_apiContext = PaypalPayment::apiContext(config('paypal_payment.account.client_id'),
            config('paypal_payment.account.client_secret'));
        $this->walletApi = $walletApi;
        $this->payment = $payment;
    }

    public function payWithCreditCard($userId, $credits, $card) {

        $sum =$credits / 100;

        // Credit card
        $card = PaypalPayment::creditCard();
        $card->setType('visa')
            ->setNumber('4032034366075987')
            ->setExpireMonth('12')
            ->setExpireYear('2022')
            ->setCvv2('456')
            ->setFirstName('Denis')
            ->setLastName('Mironov');

        $fi = PaypalPayment::fundingInstrument();
        $fi->setCreditCard($card);

        $payer = PaypalPayment::payer();
        $payer->setPaymentMethod('credit_card')
            ->setFundingInstruments($fi);

        $item = PaypalPayment::item();
        $item->setName('Credits Purchase')
            ->setDescription('Ecosquared Credits Purchase')
            ->setCurrency('GBP')
            ->setQuantity($credits)
            ->setPrice('0.01');

        $itemList = PaypalPayment::itemList();
        $itemList->setItems([$item]);

        $amount = Paypayment::amount();
        $amount->setCurrency('GBP')
            ->setTotal($sum);

        $transaction = PaypalPayment::transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription('Ecosquared Credits Purchase')
            ->setInvoiceNumber(uniqueid());

        $payment = PaypalPayment::payment();
        $payment->setIntent('sale')
            ->setPayer($payer)
            ->setTransactions([$transaction]);

        try {
            $payment->create($this->_apiContext);
        } catch (PayPalConnectionException $ex) {
            return array('status' => false, 'error' => $ex->getData());
        }

        $createdPayment = $this->payment->create([
            'user_id' => $userId,
            'invoice_number' => $transaction->getInvoiceNumber(),
            'credits' => $credits,
        ]);

        return array('status' => true, 'approvalLink' => $payment->getApprovalLink());
    }

    public function payWithPaypal($userId, $credits, $email) {

        $sum = $credits / 100;

        $payerInfo = PaypalPayment::payerInfo();
        $payerInfo->setEmail($email);

        $payer = PaypalPayment::payer();
        $payer->setPaymentMethod('paypal')
        ->setPayerInfo($payerInfo);

        $item = PaypalPayment::item();
        $item->setName('Credits Purchase')
            ->setDescription('Ecosquared Credits Purchase')
            ->setCurrency('GBP')
            ->setQuantity($credits)
            ->setPrice(0.01);

        $itemList = PaypalPayment::itemList();
        $itemList->setItems([$item]);

        $amount = PaypalPayment::amount();
        $amount->setCurrency('GBP')
            ->setTotal(($sum));

        $transaction = PaypalPayment::transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription('Ecosquared Credits Purchase')
            ->setInvoiceNumber(uniqid())
            ->setPaymentOptions(['allowed_payment_method' => 'IMMEDIATE_PAY']);

        $redirectUrls = PaypalPayment::redirectUrls();
        $redirectUrls->setReturnUrl(url("/payment/success"))
            ->setCancelUrl(url("/payment/fail"));


        $payment = PaypalPayment::payment();

        $payment->setIntent("sale")
            ->setExperienceProfileId(env('PAYPAL_EXPERIENCE_ID1'))
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions([$transaction]);


        try {
            $payment->create($this->_apiContext);
        } catch (PayPalConnectionException $ex) {
            return array('status' => false, 'error' => $ex->getData());
        }

        $createdPayment = $this->payment->create([
            'user_id' => $userId,
            'invoice_number' => $transaction->getInvoiceNumber(),
            'credits' => $credits,
        ]);

        return array('status' => true, 'approvalLink' => $payment->getApprovalLink());

    }

    public function executePayment($paymentId, $payerId, $token) {

        $payment = PaypalPayment::getById($paymentId, $this->_apiContext);
        $execution = PaypalPayment::PaymentExecution();
        $execution->setPayerId($payerId);

        $createdPayment = $this->payment
            ->where('invoice_number', '=', $payment->transactions[0]->invoice_number)
            ->first();
        $createdPayment->approved = true;
        $createdPayment->save();

        try {

            $result = $payment->execute($execution, $this->_apiContext);

            $executedPayment =  array($execution, $result);

            try {
                $payment = PaypalPayment::getById($paymentId, $this->_apiContext);
            } catch (PayPalConnectionException $ex) {
                return array("status" => false, 'error' => $ex->getData());
            }
        } catch (PayPalConnectionException $ex) {
            return array("status" => false, 'error' => $ex->getData());
        }

        $createdPayment->completed = true;
        $createdPayment->save();

        $this->walletApi->createTransaction('default', 'bought',
            $createdPayment->credits, $createdPayment->user_id, 1);

        return array("status" => true, 'executedPayment' => $executedPayment);
    }

    public function returnDataAfterSuccess($userId) {
        $payment = $this->payment
            ->where('user_id', '=', $userId)
            ->where('completed', '=', true)
            ->orderBy('id', 'DESC')
            ->first()->load('user');

        return $payment;
    }

    public function returnDataAfterFail($userId) {
        $payment = $this->payment
            ->where('user_id', '=', $userId)
            ->where('approved', '=', false)
            ->orderBy('id', 'DESC')
            ->first()->load('user');

        return $payment;
    }

    /**
     * @return \mixed[]
     */
    public function getBalance() {
        $PayPalConfig = array(
            'Sandbox' => config('paypal_payment.classic.sandbox'),
            'APIUsername' => config('paypal_payment.classic.api_username'),
            'APIPassword' => config('paypal_payment.classic.api_password'),
            'APISignature' => config('paypal_payment.classic.api_signature'),
            'PrintHeaders' => config('paypal_payment.settings.print_headers'),
            'LogResults' => config('paypal_payment.settings.log_results'),
            'LogPath' => config('paypal_payment.settings.log_path'),
        );

        $PayPal = new PayPal($PayPalConfig);

// Prepare request arrays
        $GBFields = array('returnallcurrencies' => '1');
        $PayPalRequestData = array('GBFields'=>$GBFields);

// Pass data into class for processing with PayPal and load the response array into $PayPalResult
        $PayPalResult = $PayPal->GetBalance($PayPalRequestData);

        return $PayPalResult;
    }
}