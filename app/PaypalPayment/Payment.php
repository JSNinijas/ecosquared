<?php

namespace App\PaypalPayment;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'user_id',
        'invoice_number',
        'credits',
        'approved',
        'completed'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }
}
