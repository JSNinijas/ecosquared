<?php

namespace App\Http\Controllers\Notification;

use App\Notification\NotificationsApi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Optimus\Bruno\LaravelController;

class NotificationController extends LaravelController
{
    protected $notificationsApi;

    public function __construct(NotificationsApi $notificationsApi)
    {
        $this->notificationsApi = $notificationsApi;
    }

    public function getIncoming($id) {
        return response()->json($this->notificationsApi->getIncoming($id));
    }

    public function getOutgoing($id) {
        return response()->json($this->notificationsApi->getOutgoing($id));
    }

    public function createOutgoing(Request $request) {
        $type = $request->get('type');
        $userId = $request->get('userId');
        $targetUserId = $request->get('targetUserId');
        $value = $request->get('value');
        $cardId = $request->get('cardId');
        $message = $request->get('message');
        $shareId = $request->get('shareId');
        return response()->json($this->notificationsApi
            ->createOutgoingNotification($type, $userId, $targetUserId, $value, $cardId, $message, $shareId));
    }

    public function getOfferHistory() {

    }
}