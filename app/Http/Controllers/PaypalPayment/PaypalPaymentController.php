<?php

namespace App\Http\Controllers\PaypalPayment;

use Anouar\Paypalpayment\Facades\PaypalPayment;
use App\Payment\PaymentApi;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use PayPal\Api\FlowConfig;
use PayPal\Api\InputFields;
use PayPal\Api\Presentation;
use PayPal\Api\WebProfile;
use PayPal\Exception\PayPalConnectionException;

class PaypalPaymentController extends Controller
{
    protected $paymentApi;
    protected $_apiContext;
    protected $user;

    public function __construct(PaymentApi $paymentApi, User $user)
    {
        $this->paymentApi = $paymentApi;
        $this->_apiContext = PaypalPayment::apiContext(config('paypal_payment.account.client_id'),
            config('paypal_payment.account.client_secret'));
        $this->user = $user;
    }

    public function payWithPaypal(Request $request) {

        $userId = Auth::user()->getAuthIdentifier();
        $credits = $request->get('credits');
        $email = $request->get('email');

        $user = $this->user->find($userId);
        if(($user->email !== $email) || ($user->email === $email && !$user->paypal)) {
            $user->paypal = $email;
            $user->save();
        }

        $response = $this->paymentApi->payWithPaypal($userId, $credits, $email);

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()
                ->json($response['approvalLink'] . '&useraction=commit', 200);
        }

    }

    public function payWithCreditCard() {

    }

    public function paymentSuccess(Request $request) {

        $paymentId = $request->get('paymentId');
        $token = $request->get('token');
        $payerId = $request->get('PayerID');

        $response = $this->paymentApi->executePayment($paymentId, $payerId, $token);

        if($response['status'] == false) {
            return redirect(URL::to('/') . '/#/app/errors/400');

        } else {
            return redirect(Url::to('/') . '/#/app/home/messages/buy-credits-success');
        }
    }

    public function paymentIpn(Request $request) {
        dd($request);
    }

    public function index() {
        $payments = Paypalpayment::getAll(['count' => 200, 'start_index' => 0], Paypalpayment::apiContext());

        return response()->json([$payments->toArray()], 200);
    }

    public function createExperienceProfile() {

        $flowConfig = new FlowConfig();
        $flowConfig->setLandingPageType("Billing");
        $flowConfig->setBankTxnPendingUrl("http://www.yeowza.com/");
        $flowConfig->setUserAction("commit");
        $flowConfig->setReturnUriHttpMethod("GET");

        $presentation = new Presentation();

        $presentation->setLogoImage("http://www.yeowza.com/favico.ico")
            ->setBrandName("Ecosquared!")
            ->setLocaleCode("GB")
            ->setReturnUrlLabel("Return")
            ->setNoteToSellerLabel("Thanks!");

        $inputFields = new InputFields();
        $inputFields->setAllowNote(true)
            ->setNoShipping(1)
            ->setAddressOverride(0);

        $webProfile = new WebProfile();

        $webProfile->setName("Ecosquared" . uniqid())
            ->setFlowConfig($flowConfig)
            ->setPresentation($presentation)
            ->setInputFields($inputFields)
            ->setTemporary(false);

        $request = clone $webProfile;

        try {
            $createProfileResponse = $webProfile->create($this->_apiContext);
        } catch (PayPalConnectionException $ex) {
            return array("Created Web Profile", "Web Profile", null, $request, $ex->getData());
        }

        return array("Created Web Profile", "Web Profile", $createProfileResponse->getId(), $request, $createProfileResponse);

    }

    public function paymentFail() {
        return redirect(Url::to('/') . '/#/app/home/messages/buy-credits-failure');
    }

    public function returnDataAfterSuccess($userId) {

        $payment = $this->paymentApi->returnDataAfterSuccess($userId);

        return response()->json($payment);
    }

    public function returnDataAfterFail($userId) {

        $payment = $this->paymentApi->returnDataAfterFail($userId);

        return response()->json($payment);
    }

    public function getBalance() {
        return response()->json($this->paymentApi->getBalance());
    }
}