<?php

namespace App\Http\Controllers\Admin;

use App\Restriction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Optimus\Bruno\EloquentBuilderTrait;
use Optimus\Bruno\LaravelController;
use App\Product\Product;

class ProductController extends LaravelController
{
    use EloquentBuilderTrait;

    protected $product;
    protected $restriction;

    public function __construct(Product $product, Restriction $restriction)
    {
        $this->product = $product;
        $this->restriction = $restriction;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resourceOptions = $this->parseResourceOptions();

        $query = Product::query()->with('user');
        $this->applyResourceOptions($query, $resourceOptions);

        $products = $query->get();
        $parsedData = $this->parseData($products, $resourceOptions);

        return $parsedData;
    }

    public function countProducts() {
        return response()->json($this->product->all()->count());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = $this->product->where('id', '=', $id)->with('user')->first();

        return response()->json($product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = $this->product->find($id);
        $title = $request->get('title');
        $description = $request->get('description');
        $link = $request->get('link');

        !$title ? : $product->title = $title;
        !$description ? : $product->description = $description;
        !$link ? : $product->link = $link;

        if($request->file('image')) {
            $path = $request->file('image')->storeAs('public/products', $id);
            Storage::setVisibility($path, 'public');
            $product->image = Storage::url($path);
        }

        $product->save();
        return response()->json($product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return response()->json($product);
    }

    public function enableDisableShareSupport(Request $request) {
        $share = $request->get('share');
        $support = $request->get('support');
        $restriction = $this->restriction->find(1);

        if($restriction) {
            $restriction->share = $share;
            $restriction->support = $support;
            $restriction->save();
        } else {
            $restriction = $this->restriction->create(['share' => $share, 'support' => $support]);
        }

        return $restriction;
    }

    public function getShareSupport() {
        $restriction = $this->restriction->find(1);

        return response()->json($restriction);
    }
}
