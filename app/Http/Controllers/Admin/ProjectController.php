<?php

namespace App\Http\Controllers\Admin;

use App\Project\Project;
use App\Restriction;
use Illuminate\Http\Request;
use Optimus\Bruno\EloquentBuilderTrait;
use Optimus\Bruno\LaravelController;


class ProjectController extends LaravelController
{
    use EloquentBuilderTrait;

    protected $project;
    protected $restriction;

    /**
     * ProjectController constructor.
     * @param Project $project
     * @param Restriction $restriction
     */
    public function __construct(Project $project, Restriction $restriction)
    {
        $this->project = $project;
        $this->restriction = $restriction;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resourceOptions = $this->parseResourceOptions();

        $query = Project::query()->with('user');
        $this->applyResourceOptions($query, $resourceOptions);

        $projects = $query->get();
        $parsedData = $this->parseData($projects, $resourceOptions);

        return $parsedData;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function countProjects() {
        return response()->json($this->project->all()->count());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = $this->project->where('id', '=', $id)->with('user')->first();

        return response()->json($project);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $project = $this->project->find($id);
        $title = $request->get('title');
        $description = $request->get('description');
        $link = $request->get('link');

        !$title ? : $project->title = $title;
        !$description ? : $project->description = $description;
        !$link ? : $project->link = $link;

        if($request->file('image')) {
            $path = $request->file('image')->storeAs('public/products', $id);
            Storage::setVisibility($path, 'public');
            $project->image = Storage::url($path);
        }

        $project->save();
        return response()->json($project);
    }

    /**
     * @param Project $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Project $project)
    {
        $project->delete();
        return response()->json($project);
    }

    public function enableDisableShareSupport(Request $request) {
        $share = $request->get('share');
        $support = $request->get('support');
        $restriction = $this->restriction->find(1);

        if($restriction) {
            $restriction->share = $share;
            $restriction->support = $support;
            $restriction->save();
        } else {
            $restriction = $this->restriction->create(['share' => $share, 'support' => $support]);
        }

        return $restriction;
    }

    public function getShareSupport() {
        $restriction = $this->restriction->find(1);

        return response()->json($restriction);
    }
}
