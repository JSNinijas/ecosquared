<?php

namespace App\Http\Controllers\Admin;

use App\Invite\InviteApi;
use App\PaypalPayment\Payment;
use App\Role;
use App\Thank\ThankApi;
use App\User;
use App\Wallet\AccountModel;
use App\Wallet\WalletApi;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Optimus\Bruno\LaravelController;
use Optimus\Bruno\EloquentBuilderTrait;

class UserController extends LaravelController
{
    use EloquentBuilderTrait;

    protected $user;
    protected $walletApi;
    protected $thankApi;
    protected $payment;
    protected $inviteApi;
    protected $account;


    /**
     * UserController constructor.
     * @param WalletApi $walletApi
     * @param ThankApi $thankApi
     * @param User $user
     * @param Payment $payment
     * @param InviteApi $inviteApi
     * @param AccountModel $account
     */
    public function __construct(WalletApi $walletApi, ThankApi $thankApi,
                                User $user, Payment $payment, InviteApi $inviteApi, AccountModel $account)
    {
        $this->walletApi = $walletApi;
        $this->thankApi = $thankApi;
        $this->user = $user;
        $this->payment = $payment;
        $this->inviteApi = $inviteApi;
        $this->account = $account;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resourceOptions = $this->parseResourceOptions();

        $query = User::query()->with('roles');
        $this->applyResourceOptions($query, $resourceOptions);

        $users = $query->get();
        $parsedData = $this->parseData($users, $resourceOptions);
        return response()->json($parsedData);
    }

    public function countUsers() {
        return response()->json($this->user->all()->count());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        $createdUser = $user->createFactory($request->all());
        $createdUser->save();

        return response()->json($createdUser);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return response()->json(User::where('id', '=', $user->id)->with('roles')->first());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $name = $request->get('name');
        $surname = $request->get('surname');
        $description = $request->get('description');
        !$name ? : $user->name = $name;
        !$surname ? : $user->surname = $surname;
        !$description ? : $user->description = $description;

        if($request->file('image')) {
            $path = $request->file('image')->storeAs('public/avatars', $user->id);
            Storage::setVisibility($path, 'public');
            $user->image = Storage::url($path);
        }

        $user->save();
        return response()->json($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return response()->json($user);
    }

    public function assignRoles(User $user, Request $request, $userId ) {

        $roles = $request->get('roles');
        $user = $user->find($userId);

        $user->roles()->detach();

        foreach ($roles as $roleId => $roleAttached) {
            $user->{$roleAttached ? 'attachRole' : 'detachRole'}($roleId);
        }

        $user->save();

        return response()->json($user->where('id', '=', $userId)->with('roles')->first());
    }

    /**
     * This method is used to get a user's total balance
     * @param int $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTotalBalance(int $userId)
    {
        return response()->json($this->walletApi
            ->fetchUserBalance($userId));
    }

    /**
     * this method is used to rebuild user balance by user id
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function rebuildUserTotalBalance($userId) {
        return response()->json($this->walletApi->rebuildUserTotalBalance($userId));
    }

    /**
     * This method is used to create thank from one user to another
     * @param Request $request
     * @param int $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function createThank(Request $request, int $userId)
    {
        $response = $this->thankApi->createThank(
            $userId,
            $request->get('targetUserId'),
            $request->get('value'),
            $request->get('message')
        );

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response['thank']);
        }
    }

    public function updateThank(Request $request, int $userId, int $thankId) {

        $response = $this->thankApi->updateThank(
            $thankId,
            $userId,
            $request->get('targetUserId'),
            $request->get('value'),
            $request->get('message')
        );

        if($response['status'] == false) {
            return $response['error'];
        } else {
            return $response['thank'];
        }
    }

    /**
     * This method is used to return all thanks one user gave and received
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getThanks($userId)
    {
        return response()->json($this->thankApi->getThanks($userId));
    }

    public function login($userId, User $user, Request $request)
    {
        Auth::guard('web')->loginUsingId($userId);
        return response()->json($user->find($userId)->load('roles'));
    }

    public function showPaymentStats() {
        $resourceOptions = $this->parseResourceOptions();

        $query = Payment::query()
            ->where('approved', '=', true)
            ->where('completed', '=', true)
            ->with('user');

        $this->applyResourceOptions($query, $resourceOptions);

        $payment = $query->get();
        $parsedData = $this->parseData($payment, $resourceOptions);
        return response()->json($parsedData);
    }

    public function sendRequestInviteMessage(Request $request) {
        $email = $request->get('email');
        $message = $request->get('message');

        $response = $this->inviteApi->sendRequestInviteMessage($email, $message);

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response);
        }
    }

    public function getFullBalance() {
        $received = $this->account
            ->where('transaction_type', '=', 'received')
            ->sum('amount');
        $shared = $this->account
            ->where('transaction_type', '=', 'shared')
            ->sum('amount');
        $bought = $this->account
            ->where('transaction_type', '=', 'bought')
            ->sum('amount');

       $fullPendingCredits = (int)-$this->account
            ->where('transaction_type', '=', 'pending')
            ->where('amount', '<', 0)
            ->sum('amount');

       $fullCredits = (int)$bought;

       $adminAddedCredits = (int)($received + $shared);

        return response()
            ->json(array(
                'fullCredits' => $fullCredits,
                'fullPendingCredits' => $fullPendingCredits,
                'adminAddedCredits' => $adminAddedCredits));
    }
}
