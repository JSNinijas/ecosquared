<?php

namespace App\Http\Controllers\Like;


use App\Like\LikeApi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LikeController extends Controller
{
    protected $likeApi;

    public function __construct(LikeApi $likeApi)
    {
        $this->likeApi = $likeApi;
    }

    public function getLikeById($likeId) {
        return response()->json($this->likeApi->getLikeById($likeId));
    }

    public function deleteLike($likeId) {
        return response()->json($this->likeApi->deleteLike($likeId));
    }
}
