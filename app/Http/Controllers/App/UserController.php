<?php

namespace App\Http\Controllers\App;

use App\Events\AuthSqChanged;
use App\Events\IncomingShareChanged;
use App\Events\OutgoingShareChanged;
use App\Invite\InviteApi;
use App\Mail\RecoveryMessage;
use App\NonUser;
use App\Notification\Incoming;
use App\Notification\NotificationsApi;
use App\Notification\Outgoing;
use App\Product\Product;
use App\Project\Project;
use App\Recovery\RecoveryApi;
use App\Restriction;
use App\Services\SQ;
use App\Settings\SettingsApi;
use App\Share\AcceptedShare;
use App\Share\Share;
use App\Thank\Thank;
use App\User;
use App\Thank\ThankApi;
use App\Wallet\WalletApi;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\LaravelController;
use App\Services\Traits\EloquentBuilderTrait;

class UserController extends LaravelController
{

    use EloquentBuilderTrait;

    protected $thankApi;
    protected $walletApi;
    protected $user;
    protected $sq;
    protected $notificationsApi;
    protected $product;
    protected $project;
    protected $inviteApi;
    protected $nonUser;
    protected $acceptedShare;
    protected $thank;
    protected $restriction;
    protected $incoming;
    protected $outgoing;
    protected $recoveryApi;
    protected $settingsApi;

    public function __construct(User $user, ThankApi $thankApi, WalletApi $walletApi, SQ $sq,
                                NotificationsApi $notificationsApi, Product $product, Project $project,
                                InviteApi $inviteApi, NonUser $nonUser, AcceptedShare $acceptedShare, Thank $thank,
                                Restriction $restriction, Incoming $incoming, Outgoing $outgoing,
                                RecoveryApi $recoveryApi, SettingsApi $settingsApi) {
        $this->user = $user;
        $this->thankApi = $thankApi;
        $this->walletApi = $walletApi;
        $this->sq = $sq;
        $this->notificationsApi = $notificationsApi;
        $this->product = $product;
        $this->inviteApi = $inviteApi;
        $this->nonUser = $nonUser;
        $this->middleware('auth:api')
            ->except([
                'checkInvitedUser',
                'registerUser',
                'sendRecoveryMessage',
                'checkRecoveryUser',
                'setPassword',
                'showMessage',
                'unsubscribeNonUser'
            ]);
        $this->acceptedShare = $acceptedShare;
        $this->thank = $thank;
        $this->restriction = $restriction;
        $this->incoming = $incoming;
        $this->outgoing = $outgoing;
        $this->project = $project;
        $this->recoveryApi = $recoveryApi;
        $this->settingsApi = $settingsApi;
    }


    /**
     * Display friends of authorized user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function showUsersList() {

        $resourceOptions = $this->parseResourceOptions();

        $this->calculateClosedGlobalSq();

        $query = $this->user->setQuery(
            DB::table('users')
                ->join('user_relations', 'users.id', '=', 'user_relations.target_user_id')
                ->where('user_relations.user_id', '=', Auth::user()->getAuthIdentifier())
            ->join('user_activity', 'users.id', '=', 'user_activity.target_user_id')
            ->where('user_activity.user_id', '=', Auth::user()->getAuthIdentifier())
        )->select('users.*', 'user_activity.activity');

        $applyResult = $this->applyResourceOptions($query, $resourceOptions);

        $users = $query->get();
        $count = $applyResult['count'];
        $filterCount = $applyResult['filterCount'];
        $limitCount = $applyResult['limitCount'];

        $users->map((function(User $user) {
            $user->thanks = $user->getTotalThanksFromUser(Auth::user());
            $user->save();
        }));

        $parsedData = $this->parseData($users, $resourceOptions, 'users');

        $parsedData['count'] = $count;
        $parsedData['filterCount'] = $filterCount;
        $parsedData['limitCount'] = $limitCount;

        return response()->json($parsedData);
    }

//    public function countUsersList() {
//        $resourceOptions = $this->parseResourceOptions();
//
//        $query = $this->user->setQuery(
//            DB::table('users')
//                ->join('user_relations', 'users.id', '=', 'user_relations.target_user_id')
//                ->where('user_id', '=', Auth::user()->getAuthIdentifier())
//        );
//
//        $count = $this->applyResourceOptions($query, $resourceOptions)->count();
//
//        return response()->json($count);
//    }

    /**
     * @param $productId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function showUsersListToShare($productId) {

        $resourceOptions = $this->parseResourceOptions();

        $product = $this->product->find($productId);

        $query = $this->user->setQuery(
            DB::table('users')
                ->join('user_relations', 'users.id', '=', 'user_relations.target_user_id')
                ->where('user_relations.user_id', '=', Auth::user()->getAuthIdentifier())
                ->where('users.id', '<>', $product->user_id)
                ->select('users.*')
                ->whereNotIn('users.id', function ($query) use ($product) {
                    $query
                        ->select('users.id')
                        ->from('users')
                        ->join('shared_products', 'users.id', '=', 'shared_products.user_id')
                        ->where('shared_products.product_id', '=', $product->id);
                })
                ->whereNotIn('users.id', function ($query) use ($product) {
                    $query
                        ->select('users.id')
                        ->from('users')
                        ->join('incoming_notifications', 'users.id', '=', 'incoming_notifications.target_user_id')
                        ->where('incoming_notifications.card_id', '=', $product->id)
                        ->where('incoming_notifications.type', '=', 'productShare');
                })
        );

        $applyResult = $this->applyResourceOptions($query, $resourceOptions);

        $users = $query->get();
        $count = $applyResult['count'];
        $filterCount = $applyResult['filterCount'];
        $limitCount = $applyResult['limitCount'];

        $users->map((function(User $user) {
            $user->thanks = $user->getTotalThanksFromUser(Auth::user());
            $user->save();
            return $user;
        }));

        $parsedData = $this->parseData($users, $resourceOptions, 'users');
        $parsedData['count'] = $count;
        $parsedData['filterCount'] = $filterCount;
        $parsedData['limitCount'] = $limitCount;

        return response()->json($parsedData);
    }

    /**
     * @param $projectId
     * @return \Illuminate\Http\JsonResponse
     */
    public function showUsersListToProjectShare($projectId) {

        $resourceOptions = $this->parseResourceOptions();

        $project = $this->project->find($projectId);

        $query = $this->user->setQuery(
            DB::table('users')
                ->join('user_relations', 'users.id', '=', 'user_relations.target_user_id')
                ->where('user_relations.user_id', '=', Auth::user()->getAuthIdentifier())
                ->where('users.id', '<>', $project->user_id)
                ->select('users.*')
                ->whereNotIn('users.id', function ($query) use ($project) {
                    $query
                        ->select('users.id')
                        ->from('users')
                        ->join('shared_projects', 'users.id', '=', 'shared_projects.user_id')
                        ->where('shared_projects.project_id', '=', $project->id);
                })
                ->whereNotIn('users.id', function ($query) use ($project) {
                    $query
                        ->select('users.id')
                        ->from('users')
                        ->join('incoming_notifications', 'users.id', '=', 'incoming_notifications.target_user_id')
                        ->where('incoming_notifications.card_id', '=', $project->id)
                        ->where('incoming_notifications.type', '=', 'projectShare');
                })
        );

        $applyResult = $this->applyResourceOptions($query, $resourceOptions);
        $users = $query->get();
        $count = $applyResult['count'];
        $filterCount = $applyResult['filterCount'];
        $limitCount = $applyResult['limitCount'];

        $users->map((function(User $user) {
            $user->thanks = $user->getTotalThanksFromUser(Auth::user());
            $user->save();
            return $user;
        }));

        $parsedData = $this->parseData($users, $resourceOptions, 'users');
        $parsedData['count'] = $count;
        $parsedData['filterCount'] = $filterCount;
        $parsedData['limitCount'] = $limitCount;

        return response()->json($parsedData);
    }

    /**
     * @param $productId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function showCurrentProductUsersList($productId){

        $resourceOptions = $this->parseResourceOptions();

        $query = $this->user->setQuery(
            DB::table('users')
                ->whereIn('users.id', function ($query) use ($productId) {
                    $query
                        ->select('users.id')
                        ->from('users')
                        ->join('accepted_product_shares', 'users.id', '=', 'accepted_product_shares.user_id')
                        ->where('accepted_product_shares.product_id', '=', $productId);
                })
                ->orWhereIn('users.id', function ($query) use ($productId) {
                    $query
                        ->select('users.id')
                        ->from('users')
                        ->join('accepted_product_shares', 'users.id', '=', 'accepted_product_shares.target_user_id')
                        ->where('accepted_product_shares.product_id', '=', $productId);
                })
                ->orWhereIn('users.id', function ($query) use ($productId) {
                    $query
                        ->select('users.id')
                        ->from('users')
                        ->join('products', 'users.id', '=', 'products.user_id')
                        ->where('products.id', '=', $productId);
                })

        )->pluck('users.id');

        $mask = rtrim(str_repeat('?, ', count($query)), ' ,');


//        $query2 = $this->user->setQuery(
//            DB::table('users')
//                ->select(DB::raw(' * '))
//                ->whereRaw('id IN ('.$mask.')', $query)
//        );
//        dd($query, $mask, $query2->get()->toArray());

//        $query = User::query();

//        dd($resourceOptions);

        $query2 = $this->user->setQuery(
            DB::table('users')->whereIn('id', $query)
        );

        $applyResult = $this->applyResourceOptions($query2, $resourceOptions);
        $count = $applyResult['count'];
        $filterCount = $applyResult['filterCount'];
        $limitCount = $applyResult['limitCount'];

//        dd($applyResult['queryBuilder']->get()->toArray(), $resourceOptions);

        $users = $applyResult['queryBuilder']->get()->each(function (User &$user) use ($productId) {
            $userLikeToProduct = $user->getLikesByProduct($this->product->find($productId));
            $userLikeToProduct
                ? $user->like = $userLikeToProduct
                : $user->like = 0;

            $userLastLikeComment = $user->getLastLikeComment($this->product->find($productId));
            $userLastLikeComment
                ? $user->lastComment = $userLastLikeComment
                : $user->lastComment = 'No comment';

            $userAdded = $user->getCreditsByProduct($this->product->find($productId), $user);
            $userAdded >= 0
                ? $user->income = $userAdded
                : $user->income = '-';
        });

        $parsedData = $this->parseData($users, $resourceOptions, 'users');
        $parsedData['count'] = $count;
        $parsedData['filterCount'] = $filterCount;
        $parsedData['limitCount'] = $limitCount;

        return response()->json($parsedData);
    }

    /**
     * @param $projectId
     * @return \Illuminate\Http\JsonResponse
     */
    public function showCurrentProjectUsersList($projectId){

        $resourceOptions = $this->parseResourceOptions();

        $query = $this->user->setQuery(
            DB::table('users')
                ->whereIn('users.id', function ($query) use ($projectId) {
                    $query
                        ->select('users.id')
                        ->from('users')
                        ->join('accepted_project_shares', 'users.id', '=', 'accepted_project_shares.user_id')
                        ->where('accepted_project_shares.project_id', '=', $projectId);
                })
                ->orWhereIn('users.id', function ($query) use ($projectId) {
                    $query
                        ->select('users.id')
                        ->from('users')
                        ->join('accepted_project_shares', 'users.id', '=', 'accepted_project_shares.target_user_id')
                        ->where('accepted_project_shares.project_id', '=', $projectId);
                })
                ->orWhereIn('users.id', function ($query) use ($projectId) {
                    $query
                        ->select('users.id')
                        ->from('users')
                        ->join('projects', 'users.id', '=', 'projects.user_id')
                        ->where('projects.id', '=', $projectId);
                })
        )->pluck('users.id');

        $query2 = $this->user->setQuery(
            DB::table('users')->whereIn('id', $query));

        $applyResult = $this->applyResourceOptions($query2, $resourceOptions);
        $count = $applyResult['count'];
        $filterCount = $applyResult['filterCount'];
        $limitCount = $applyResult['limitCount'];

        $users = $applyResult['queryBuilder']->get()->each(function (User &$user) use ($projectId) {
            $userHappyToProject = $user->getHappyByProject($this->project->find($projectId));
            $userHappyToProject
                ? $user->happy = $userHappyToProject
                : $user->happy = 0;

            $userLastHappyComment = $user->getLastHappyComment($this->project->find($projectId));
            $userLastHappyComment
                ? $user->lastComment = $userLastHappyComment
                : $user->lastComment = 'No comment';

            $userAdded = $user->getCreditsByProject($this->project->find($projectId), $user);
            $userAdded >= 0
                ? $user->income = $userAdded
                : $user->income = '-';
        });

        $parsedData = $this->parseData($users, $resourceOptions, 'users');
        $parsedData['count'] = $count;
        $parsedData['filterCount'] = $filterCount;
        $parsedData['limitCount'] = $limitCount;

        return response()->json($parsedData);
    }

    public function calculateClosedGlobalSq() {

        $userId = Auth::user()->getAuthIdentifier();

        $sqArray = $this->sq->updateUsersTable($this->sq->calculateGlobalSQ());

        $user = $this->user->find($userId);
        $user->roles = $user->roles;

        event(new AuthSqChanged($user->toArray()));

        return $sqArray;
    }

    public function calculateExclusiveClosedSq($projectId) {

        $userId = Auth::user()->getAuthIdentifier();

        $project = $this->project->find($projectId);

//        ---Get users for ExclusiveClosedSQ---

        $query = $this->user->setQuery(
            DB::table('users')
                ->whereIn('users.id', function ($query) use ($projectId, $userId) {
                    $query
                        ->select('users.id')
                        ->from('users')
                        ->join('accepted_project_shares', 'users.id', '=', 'accepted_project_shares.user_id')
                        ->where('accepted_project_shares.project_id', '=', $projectId)
                        ->whereNotIn('users.id', [$userId]);
                })
                ->orWhereIn('users.id', function ($query) use ($projectId, $userId) {
                    $query
                        ->select('users.id')
                        ->from('users')
                        ->join('accepted_project_shares', 'users.id', '=', 'accepted_project_shares.target_user_id')
                        ->where('accepted_project_shares.project_id', '=', $projectId)
                        ->whereNotIn('users.id', [$userId]);
                })
        )->pluck('users.id');

        $query2 = $this->user->setQuery(
            DB::table('users')
                ->whereIn('users.id', $query)
        )->select('users.*');

        $queryForSqIds = $query2
            ->orWhereIn('users.id', [Auth::user()->getAuthIdentifier()])
            ->pluck('users.id');

        $queryForSq = $this->user->setQuery(
            DB::table('users')
                ->whereIn('users.id', $queryForSqIds)
        );

        $sqArray = $this->sq->updateUsersTable($this->sq->calculateClosedExclusiveSq($projectId, $queryForSq));

        $user = $this->user->find($userId);
        $user->roles = $user->roles;

        event(new AuthSqChanged($user->toArray()));

        return $sqArray;
    }

    /**
     * @param $projectId
     * @return \Illuminate\Http\JsonResponse
     */
    public function showProjectUsership($projectId) {
        $resourceOptions = $this->parseResourceOptions();
        $userId = Auth::user()->getAuthIdentifier();
        $user = $this->user->find($userId);
        $project = $this->project->find($projectId);

        $this->calculateExclusiveClosedSq($projectId);

        $query = $this->user->setQuery(
            DB::table('users')
                ->whereIn('users.id', function ($query) use ($projectId, $userId) {
                    $query
                        ->select('users.id')
                        ->from('users')
                        ->join('accepted_project_shares', 'users.id', '=', 'accepted_project_shares.user_id')
                        ->where('accepted_project_shares.project_id', '=', $projectId)
                        ->whereNotIn('users.id', [$userId]);
                })
                ->orWhereIn('users.id', function ($query) use ($projectId, $userId) {
                    $query
                        ->select('users.id')
                        ->from('users')
                        ->join('accepted_project_shares', 'users.id', '=', 'accepted_project_shares.target_user_id')
                        ->where('accepted_project_shares.project_id', '=', $projectId)
                        ->whereNotIn('users.id', [$userId]);
                }))
            ->pluck('users.id');

        $query2 = $this->user->setQuery(
            DB::table('users')
//                ->where('user_activity.user_id', '=', Auth::user()->getAuthIdentifier())
                ->whereIn('users.id', $query)
        )->select('users.*');

        $applyResult = $this->applyResourceOptions($query2, $resourceOptions);

        $count = $applyResult['count'];
        $filterCount = $applyResult['filterCount'];
        $limitCount = $applyResult['limitCount'];

        $users = $applyResult['queryBuilder']->get();
        $users = $applyResult['queryBuilder']->get()->each(function (User &$user) use ($projectId, $project) {

            $user->project_thanks = $user->getTotalProjectThanksFromUser(Auth::user(),  $project);
            $user->save();

            $userHappyToProject = $user->getHappyByProject($this->project->find($projectId));
            $userHappyToProject
                ? $user->happy = $userHappyToProject
                : $user->happy = 0;

            $userLastHappyComment = $user->getLastHappyComment($this->project->find($projectId));
            $userLastHappyComment
                ? $user->lastComment = $userLastHappyComment
                : $user->lastComment = 'No comment';

            $userAdded = $user->getCreditsByProject($this->project->find($projectId), $user);
            $userAdded >= 0
                ? $user->income = $userAdded
                : $user->income = '-';
        });

        $parsedData = $this->parseData($users, $resourceOptions, 'users');
        $parsedData['count'] = $count;
        $parsedData['filterCount'] = $filterCount;
        $parsedData['limitCount'] = $limitCount;

        return response()->json($parsedData);
    }

    public function showProjectKnownUsership($projectId) {
        $resourceOptions = $this->parseResourceOptions();
        $userId = Auth::user()->getAuthIdentifier();
        $project = $this->project->find($projectId);

        $query = $this->user->setQuery(
            DB::table('users')
                ->whereIn('users.id', function ($query) use ($projectId, $userId) {
                    $query
                        ->select('users.id')
                        ->from('users')
                        ->join('accepted_project_shares', 'users.id', '=', 'accepted_project_shares.user_id')
                        ->where('accepted_project_shares.project_id', '=', $projectId)
                        ->whereNotIn('users.id', [$userId]);
                })
                ->orWhereIn('users.id', function ($query) use ($projectId, $userId) {
                    $query
                        ->select('users.id')
                        ->from('users')
                        ->join('accepted_project_shares', 'users.id', '=', 'accepted_project_shares.target_user_id')
                        ->where('accepted_project_shares.project_id', '=', $projectId)
                        ->whereNotIn('users.id', [$userId]);
                })

//                ->orWhereIn('users.id', function ($query) use ($projectId, $userId) {
//                    $query
//                        ->select('users.id')
//                        ->from('users')
//                        ->join('projects', 'users.id', '=', 'projects.user_id')
//                        ->where('projects.id', '=', $projectId)
//                        ->whereNotIn('users.id', [$userId]);
//                })
        )->pluck('users.id');

        $knownQuery = $this->user->setQuery(
            DB::table('users')
            ->whereNotIn('id', function($query) use ($projectId, $userId) {
                $query
                    ->select('users.id')
                    ->from('users')
                    ->join('project_thanks', 'users.id', '=', 'project_thanks.target_user_id')
                    ->where('project_thanks.project_id', '=', $projectId);
            })
        )->pluck('users.id');

//        dd($query, $knownQuery);

        $query2 = $this->user->setQuery(
            DB::table('users')->whereIn('id', $query)->whereNotIn('id', $knownQuery)
        );

//        dd($query2->get()->toArray());

        $applyResult = $this->applyResourceOptions($query2, $resourceOptions);
        $count = $applyResult['count'];
        $filterCount = $applyResult['filterCount'];
        $limitCount = $applyResult['limitCount'];

        $users = $applyResult['queryBuilder']->get();
//        $users->map((function(User $user) use($project){
//            $user->thanks = $user->getTotalProjectThanksFromUser(Auth::user(), $project);
//            $user->save();
//        }));

        $users = $applyResult['queryBuilder']->get()->each(function (User &$user) use ($projectId, $project) {

            $user->project_thanks = $user->getTotalProjectThanksFromUser(Auth::user(),  $project);
            $user->save();

            $userHappyToProject = $user->getHappyByProject($this->project->find($projectId));
            $userHappyToProject
                ? $user->happy = $userHappyToProject
                : $user->happy = 0;

            $userLastHappyComment = $user->getLastHappyComment($this->project->find($projectId));
            $userLastHappyComment
                ? $user->lastComment = $userLastHappyComment
                : $user->lastComment = 'No comment';

            $userAdded = $user->getCreditsByProject($this->project->find($projectId), $user);
            $userAdded >= 0
                ? $user->income = $userAdded
                : $user->income = '-';
        });

        $parsedData = $this->parseData($users, $resourceOptions, 'users');
        $parsedData['count'] = $count;
        $parsedData['filterCount'] = $filterCount;
        $parsedData['limitCount'] = $limitCount;

        return response()->json($parsedData);
    }

    public function showProjectUnknownUsership($projectId) {
        $resourceOptions = $this->parseResourceOptions();
        $userId = Auth::user()->getAuthIdentifier();
        $project = $this->project->find($projectId);

        $query = $this->user->setQuery(
            DB::table('users')
                ->whereIn('users.id', function ($query) use ($projectId, $userId) {
                    $query
                        ->select('users.id')
                        ->from('users')
                        ->join('accepted_project_shares', 'users.id', '=', 'accepted_project_shares.user_id')
                        ->where('accepted_project_shares.project_id', '=', $projectId)
                        ->whereNotIn('users.id', [$userId]);
                })
                ->orWhereIn('users.id', function ($query) use ($projectId, $userId) {
                    $query
                        ->select('users.id')
                        ->from('users')
                        ->join('accepted_project_shares', 'users.id', '=', 'accepted_project_shares.target_user_id')
                        ->where('accepted_project_shares.project_id', '=', $projectId)
                        ->whereNotIn('users.id', [$userId]);
                })
//                ->orWhereIn('users.id', function ($query) use ($projectId, $userId) {
//                    $query
//                        ->select('users.id')
//                        ->from('users')
//                        ->join('projects', 'users.id', '=', 'projects.user_id')
//                        ->where('projects.id', '=', $projectId)
//                        ->whereNotIn('users.id', [$userId]);
//                })
        )->pluck('users.id');

        $unknownQuery = $this->user->setQuery(
            DB::table('users')
                ->whereIn('id', function($query) use ($projectId, $userId) {
                    $query
                        ->select('users.id')
                        ->from('users')
                        ->join('project_thanks', 'users.id', '=', 'project_thanks.target_user_id')
                        ->where('project_thanks.project_id', '=', $projectId);
                })
        )->pluck('users.id');

        $query2 = $this->user->setQuery(
            DB::table('users')->whereIn('id', $query)->whereNotIn('id', $unknownQuery)
        );

        $applyResult = $this->applyResourceOptions($query2, $resourceOptions);
        $count = $applyResult['count'];
        $filterCount = $applyResult['filterCount'];
        $limitCount = $applyResult['limitCount'];

        $users = $applyResult['queryBuilder']->get();
//        $users->map((function(User $user) use($project){
//            $user->thanks = $user->getTotalProjectThanksFromUser(Auth::user(), $project);
//            $user->save();
//        }));

        $users = $applyResult['queryBuilder']->get()->each(function (User &$user) use ($projectId, $project) {

            $user->project_thanks = $user->getTotalProjectThanksFromUser(Auth::user(),  $project);
            $user->save();

            $userHappyToProject = $user->getHappyByProject($this->project->find($projectId));
            $userHappyToProject
                ? $user->happy = $userHappyToProject
                : $user->happy = 0;

            $userLastHappyComment = $user->getLastHappyComment($this->project->find($projectId));
            $userLastHappyComment
                ? $user->lastComment = $userLastHappyComment
                : $user->lastComment = 'No comment';

            $userAdded = $user->getCreditsByProject($this->project->find($projectId), $user);
            $userAdded >= 0
                ? $user->income = $userAdded
                : $user->income = '-';
        });

        $parsedData = $this->parseData($users, $resourceOptions, 'users');
        $parsedData['count'] = $count;
        $parsedData['filterCount'] = $filterCount;
        $parsedData['limitCount'] = $limitCount;

        return response()->json($parsedData);
    }

    /**
     * @param $projectId
     * @return \Illuminate\Http\JsonResponse
     */
    public function showPossibleOwnerList($projectId) {

        $project = $this->project->find($projectId);
        $ownerIds = $project->owners()->pluck('users.id');

        $resourceOptions = $this->parseResourceOptions();

        $query = $this->user->setQuery(
            DB::table('users')
                ->whereIn('users.id', function ($query) use ($projectId) {
                    $query
                        ->select('users.id')
                        ->from('users')
                        ->join('accepted_project_shares', 'users.id', '=', 'accepted_project_shares.user_id')
                        ->where('accepted_project_shares.project_id', '=', $projectId);
                })
                ->orWhereIn('users.id', function ($query) use ($projectId) {
                    $query
                        ->select('users.id')
                        ->from('users')
                        ->join('accepted_project_shares', 'users.id', '=', 'accepted_project_shares.target_user_id')
                        ->where('accepted_project_shares.project_id', '=', $projectId);
                })
                ->orWhereIn('users.id', function ($query) use ($projectId) {
                    $query
                        ->select('users.id')
                        ->from('users')
                        ->join('projects', 'users.id', '=', 'projects.user_id')
                        ->where('projects.id', '=', $projectId);
                })
        )->pluck('users.id');

        $query2 = $this->user->setQuery(
            DB::table('users')->whereIn('users.id', $query)->whereNotIn('users.id', $ownerIds));

        $applyResult = $this->applyResourceOptions($query2, $resourceOptions);
        $count = $applyResult['count'];
        $filterCount = $applyResult['filterCount'];
        $limitCount = $applyResult['limitCount'];

        $users = $applyResult['queryBuilder']->get();

        $parsedData = $this->parseData($users, $resourceOptions, 'users');
        $parsedData['count'] = $count;
        $parsedData['filterCount'] = $filterCount;
        $parsedData['limitCount'] = $limitCount;

        return response()->json($parsedData);
    }

    /**
     * Display the specified resource.
     *
     * @param $userId
     * @return \Illuminate\Http\Response
     */
    public function show($userId, Request $request)
    {
        $user = User::find($userId);
        $id = Auth::user()->getAuthIdentifier();
        $projectId = $request->get('projectId');
        $project = $this->project->find($projectId);

        if(!$user) {
            return response('Use a valid user identifier!', 400);
        }

        if($projectId) {
            $user->project_thanks = $user->getTotalProjectThanksFromUser(Auth::user(),  $project);
            $user->save();
        }

        $lastThank = $thank = $this->thank
            ->where('user_id', '=', $id)
            ->where('target_user_id', '=', $userId)
            ->orderBy('id', 'desc')->first();

        if($lastThank) $user->lastThank = $lastThank->value;

        $user->shareFromAuthUser = $this->acceptedShare
            ->where('user_id', '=', Auth::user()->getAuthIdentifier())
            ->where('target_user_id', '=', $userId)
            ->sum('amount');
        $user->shareToAuthUser = $this->acceptedShare
            ->where('user_id', '=', $userId)
            ->where('target_user_id', '=', Auth::user()->getAuthIdentifier())
            ->sum('amount');
        $user->thanks = $user->getTotalThanksFromUser(Auth::user());

        $user->restrictions = $this->restriction->find(1);

        return response()->json($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $userId = Auth::user()->getAuthIdentifier();
        $user = User::find($userId);

        $name = $request->get('name');
        $surname = $request->get('surname');
        $description = $request->get('description');
        !$name ? : $user->name = $name;
        !$surname ? : $user->surname = $surname;
        !$description ? : $user->description = $description;

        if($request->file('image')) {
            $path = $request->file('image')->storeAs('public/avatars', $userId);
            Storage::setVisibility($path, 'public');
            $user->image = Storage::url($path);
        }

        $user->save();
        return response()->json($user);
    }

    public function changePassword(Request $request) {
        $userId = Auth::user()->getAuthIdentifier();
        $user = User::find($userId);

        $oldPassword = $request->get('oldPassword');
        $newPassword = $request->get('newPassword');

        if(!($oldPassword && $newPassword)) {
            return response('Old and new password fields are required!', 400);
        };

        if(!Hash::check($oldPassword, $user->password)) {
            return response('Incorrect old password!', 400);
        } elseif($newPassword === $oldPassword) {
            return response('A new password is same as an old one!', 400);
        } else {
            $user->password = bcrypt($newPassword);
        }

        $user->save();
        return response()->json($user);
    }

    /**
     * This method used to return user total balance
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTotalBalance() {

        $userId = Auth::user()->getAuthIdentifier();

        return response()->json($this->walletApi
            ->fetchUserBalance($userId));
    }

    public function getTotalBalanceByTypes() {

        $userId = Auth::user()->getAuthIdentifier();

        return response()->json($this->walletApi->fetchUserBalanceByTypes($userId));
    }

    /**
     * This method is used to rebuild user total balance
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function rebuildUserTotalBalance() {

        $userId = Auth::user()->getAuthIdentifier();

        return response()->json($this->walletApi->rebuildUserTotalBalance($userId));
    }

    /**
     * This method is used to create thank
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createThank(Request $request) {

        $userId = Auth::user()->getAuthIdentifier();

        $response = $this->thankApi->createThank(
            $userId,
            $request->get('targetUserId'),
            $request->get('value'),
            $request->get('message')
        );

        if($response['status'] == false) {
            return response($response['error'],400);
        } else {
            return response()->json($response['thank']);
        }
    }

    /**
     * This method is used to get all thanks of one user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getThanks() {

        $userId = Auth::user()->getAuthIdentifier();

        return response()->json($this->thankApi->getThanks($userId));
    }

    public function createProjectThank(Request $request) {

        $userId = Auth::user()->getAuthIdentifier();

        $response = $this->thankApi->createProjectThank(
            $userId,
            $request->get('targetUserId'),
            $request->get('projectId'),
            $request->get('value'),
            $request->get('message')
        );

        if($response['status'] == false) {
            return response($response['error'],400);
        } else {
            return response()->json($response['thank']);
        }
    }

    public function getProjectThanks() {

        $userId = Auth::user()->getAuthIdentifier();

        return response()->json($this->thankApi->getProjectThanks($userId));
    }

    public function showIncomingList() {
        $userId = Auth::user()->getAuthIdentifier();

        $resourceOptions = $this->parseResourceOptions();

        $parsedData = $this->parseData($this->notificationsApi->showIncomingList($userId, $resourceOptions), $resourceOptions);

        return $parsedData;
    }

    public function countIncomingList() {

        $userId = Auth::user()->getAuthIdentifier();
        $user = $this->user->find($userId);
        $count = $this->incoming->count();

        $incomingOnOff= $this->incoming->where('on_off', '=', 'off')
            ->where('target_user_id', '=', $userId)->get();
        $unseenNotifications = $this->incoming->where('seen', '=', false)
            ->where('target_user_id', '=', $userId)->get();
        if($unseenNotifications->isEmpty() || $incomingOnOff->isNotEmpty()) {
            $unseen = false;
        } else {
            $unseen = true;
        }

        $offers = $this->incoming
            ->where('target_user_id', '=', $userId)
            ->whereIn('type', ['productShare', 'projectShare'])
            ->count();
        $outgoingOffers = $this->outgoing
            ->where('user_id', '=', $userId)
            ->whereIn('type', ['productShare', 'projectShare', 'userInvite', 'userProjectInvite'])
            ->count();

        event(new IncomingShareChanged($user, $offers));
        event(new OutgoingShareChanged($user, $outgoingOffers));

        return response()
            ->json(array('count' => $count, 'unseen' => $unseen, 'offers' => $offers, 'outgoingOffers' => $outgoingOffers));
    }

    public function showOutgoingList() {
        $userId = Auth::user()->getAuthIdentifier();

        $resourceOptions = $this->parseResourceOptions();

        $parsedData = $this->parseData($this->notificationsApi->showOutgoingList($userId, $resourceOptions), $resourceOptions);

        return $parsedData;
    }

    public function countOutgoingList() {
        return response()->json($this->outgoing->count());
    }

    public function sendInviteMessage(Request $request)
    {
        $userId = Auth::user()->getAuthIdentifier();
        $name = $request->get('name');
        $surname = $request->get('surname');
        $message = $request->get('message');
        $email = $request->get('email');
        $productId = $request->get('id');
        $amount = $request->get('amount');

        $response = $this->inviteApi
            ->sendInviteMessage($userId, $name, $surname, $message, $email, $productId, $amount);

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response['product']);
        }
    }

    public function sendProjectInviteMessage(Request $request)
    {
        $userId = Auth::user()->getAuthIdentifier();
        $name = $request->get('name');
        $surname = $request->get('surname');
        $message = $request->get('message');
        $email = $request->get('email');
        $projectId = $request->get('id');
        $amount = $request->get('amount');

        $response = $this->inviteApi
            ->sendProjectInviteMessage($userId, $name, $surname, $message, $email, $projectId, $amount);

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response['project']);
        }
    }

    public function checkInvitedUser($hash) {

        $response = $this->inviteApi->checkInvitedUser($hash);

        if($response['status'] == false) {
            return response($response['error'], 403);
        } else {
            return response()->json($response['nonUser']);
        }
    }

    public function unsubscribeNonUser(Request $request){
        if($request->has('hash')) {
            $response = $this->inviteApi->checkInvitedUser($request->get('hash'));
            if($response['status'] == false) {
                return response($response['error'], 403);
            } else {
                $nonUser = $response['nonUser'];
                $nonUser->unsubscribe = true;
                $nonUser->save();
                return response('',200);
            }
        } else {
            return response('', 403);
        }

    }

    public function registerUser(Request $request) {

        $userId = $request->get('userId');
        $card = $request->get('card');
        $name = $request->get('name');
        $surname = $request->get('surname');
        $inviterId = $request->get('inviterId');
        $transactionId = $request->get('transactionId');
        $message = $request->get('message');
        $cardId = $request->get('cardId');
        $amount = $request->get('amount');
        $password = $request->get('password');
        $email = $request->get('email');
        $description = $request->get('description');
        $file = $request->file('image');

        $response = $this->inviteApi->registerUser($card, $userId, $email, $inviterId, $transactionId, $message,
            $cardId, $amount, $name, $surname, $password, $description, $file);

        return response()->json($response);
    }

    public function revokeInvite($notificationId) {
        return response()->json($this->inviteApi->revokeInvite($notificationId));
    }

    public function sendRecoveryMessage(Request $request) {
        $email = $request->get('email');
        $response = $this->recoveryApi->sendRecoveryMessage($email);

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response['user']);
        }
    }

    public function checkRecoveryUser($hash) {
        $response = $this->recoveryApi->checkRecoveryUser($hash);

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response['user']);
        }
    }

    public function setPassword(Request $request) {
        $email = $request->get('email');
        $password = $request->get('password');

        $response = $this->recoveryApi->setPassword($email, $password);

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response['user']);
        }
    }

    public function getNotificationsSettings() {
        $userId = Auth::user()->getAuthIdentifier();
        $response = $this->settingsApi->getNotificationsSettings($userId);

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response['settings']);
        }
    }

    public function setNotificationsSettings(Request $request) {
        $userId = Auth::user()->getAuthIdentifier();
        $shares = $request->get('shareStatisticType');
        $push = $request->get('sharePushType');
        $activity = $request->get('activityStatisticType');
        $archive = $request->get('archiveActivityType');
        $thanks = $request->get('thanksStatus');
        $thanksRemove = $request->get('thanksRemove');
        $productLikes = $request->get('productLikesStatus');
        $productLikesRemove = $request->get('productLikesRemove');
        $projectHappy = $request->get('projectHappyStatus');
        $projectHappyRemove = $request->get('projectHappyRemove');
        $productSupport = $request->get('productSupportStatus');
        $productSupportRemove = $request->get('productSupportRemove');
        $projectSupport = $request->get('projectSupportStatus');
        $projectSupportRemove = $request->get('projectSupportRemove');


        $response = $this->settingsApi->setNotificationsSettings(
            $userId,
            $shares,
            $push,
            $activity,
            $archive,
            $thanks,
            $thanksRemove,
            $productLikes,
            $productLikesRemove,
            $projectHappy,
            $projectHappyRemove,
            $productSupport,
            $productSupportRemove,
            $projectSupport,
            $projectSupportRemove
        );

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response['settings']);
        }
    }

    public function showMessage() {
        return view('emails.statistics');
    }

    public function showOfferHistory() {
        $userId = Auth::user()->getAuthIdentifier();

        $resourceOptions = $this->parseResourceOptions();

        $parsedData = $this->parseData($this->notificationsApi->showOfferHistory($userId, $resourceOptions), $resourceOptions);

        return $parsedData;
    }
}