<?php

namespace App\Http\Controllers\App;

use App\Like\LikeApi;
use App\Product\Product;
use App\Product\ProductApi;
use App\Restriction;
use App\Revoke\RevokeApi;
use App\Share\ShareApi;
use App\Share\AcceptedShare;
use App\Support\SupportApi;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\LaravelController;
use App\Services\Traits\EloquentBuilderTrait;
use Illuminate\Support\Facades\Auth;

class ProductController extends LaravelController
{
    use EloquentBuilderTrait;

    /**
     * @var User
     */
    protected $productApi;
    protected $user;
    protected $product;
    protected $likeApi;
    protected $supportApi;
    protected $shareApi;
    protected $acceptedShare;
    protected $revokeApi;
    protected $restriction;

    /**
     * ProductController constructor.
     *
     * @param User $user
     * @param Product $product
     * @param LikeApi $likeApi
     * @param SupportApi $supportApi
     * @param ShareApi $shareApi
     * @param AcceptedShare $acceptedShare
     * @param RevokeApi $revokeApi
     * @param ProductApi $productApi
     * @param Restriction $restriction
     */
    public function __construct(User $user, Product $product, LikeApi $likeApi, SupportApi $supportApi,
                                ShareApi $shareApi, RevokeApi $revokeApi,
                                AcceptedShare $acceptedShare, ProductApi $productApi, Restriction $restriction)
    {
        $this->user = $user;
        $this->product = $product;
        $this->likeApi = $likeApi;
        $this->supportApi = $supportApi;
        $this->shareApi = $shareApi;
        $this->acceptedShare = $acceptedShare;
        $this->revokeApi = $revokeApi;
        $this->productApi = $productApi;
        $this->restriction = $restriction;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userId = Auth::user()->getAuthIdentifier();

        $resourceOptions = $this->parseResourceOptions();

        $applyResult = $this->productApi->showProductsList($userId, $resourceOptions);

        $parsedData = $this->parseData(
            $applyResult['products'],
            $resourceOptions,
            'products'
        );
        $parsedData['count'] = $applyResult['count'];
        $parsedData['limitCount'] = $applyResult['limitCount'];
        $parsedData['filterCount'] = $applyResult['filterCount'];

        $parsedData['products']->map(function(Product $product) {
            $product->like = $product->getLikeFromUser(Auth::user());
            $product->save();
        });

        return response()->json($parsedData);
    }

//    public function count() {
//        $userId = Auth::user()->getAuthIdentifier();
//
//        $resourceOptions = $this->parseResourceOptions();
//
//        $count = $this->productApi->showProductsList($userId, $resourceOptions)->count();
//
//        return response()->json($count);
//    }

    /**
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function showProductsListToShare($userId) {

        $authorizedUserId = Auth::user()->getAuthIdentifier();

        $resourceOptions = $this->parseResourceOptions();

        $applyResult = $this->productApi->showProductsListToShare($userId, $authorizedUserId, $resourceOptions);

        $parsedData = $this->parseData(
            $applyResult['products'],
            $resourceOptions,
            'products'
        );
        $parsedData['count'] = $applyResult['count'];
        $parsedData['limitCount'] = $applyResult['limitCount'];
        $parsedData['filterCount'] = $applyResult['filterCount'];

        $parsedData['products']->map(function(Product $product) {
            $product->like = $product->getLikeFromUser(Auth::user());
            $product->save();
        });

        return response()->json($parsedData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userId = Auth::user()->getAuthIdentifier();
        $title = $request->get('title');
        $description = $request->get('description');
        $link = $request->get('link');
        $creationDate = $request->get('creationDate');
        $type = $request->get('type');
        $start = $request->get('start');
        $end = $request->get('end');
        $share = $request->get('share');
        $support = $request->get('support');
        $sqEnabled = $request->get('sqEnabled');
        $file = $request->file('image');
        $author = $request->get('author');
        $authorStatus = $request->get('authorStatus');

        $response = $this->productApi->createProduct(
            $userId,
            $title,
            $description,
            $link,
            $creationDate,
            $type,
            $start,
            $end,
            $share,
            $support,
            $sqEnabled,
            $file,
            $author,
            $authorStatus
        );

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response['product']);
        }
    }

    /**
     * Display the specified resource with number of users liked this product
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $userId = Auth::user()->getAuthIdentifier();

        return response()->json($this->productApi->showProduct($id, $userId));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $title = $request->get('title');
        $description = $request->get('description');
        $link = $request->get('link');
        $creationDate = $request->get('creationDate');
        $type = $request->get('type');
        $start = $request->get('start');
        $end = $request->get('end');
        $share = $request->get('share');
        $support = $request->get('support');
        $sqEnabled = $request->get('sq_enabled');
        $file = $request->file('image');
        $author = $request->get('author');
        $authorStatus = $request->get('authorStatus');

        $product = $this->productApi->updateProduct(
            $id,
            $title,
            $description,
            $link,
            $creationDate,
            $type,
            $start,
            $end,
            $share,
            $support,
            $sqEnabled,
            $file,
            $author,
            $authorStatus
        );

        return response()->json($product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = $response = $this->product->where('id', '=', $id);
        $product->delete();

        return response()->json($response);
    }

    /**
     * Create a like for product
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createLike(Request $request) {

        $userId = Auth::user()->getAuthIdentifier();
        $productId = $request->get('productId');
        $value = $request->get('value');
        $message = $request->get('message');

        $response = $this->likeApi->createLike($userId, $productId, $value, $message);

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response['like']);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createSupport(Request $request) {

        $userId = Auth::user()->getAuthIdentifier();
        $targetUserId = $request->get('targetUserId');
        $productId = $request->get('productId');
        $amount = $request->get('amount');
        $message = $request->get('message');

        $response = $this->supportApi->createSupport($userId, $targetUserId, $productId, $amount, $message);

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response['support']);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createShare(Request $request) {
        $userId =Auth::user()->getAuthIdentifier();
        $targetUserId = $request->get('targetUserId');
        $productId = $request->get('productId');
        $amount = $request->get('amount');
        $message = $request->get('message');
        $flag = $request->get('entityFlag');

        $response = $this->shareApi->createShare($userId, $targetUserId, $productId, $amount, $message, $flag);

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response['share']);
        }
    }

    /**
     * @param Request $request
     * @param int $shareId
     * @return \Illuminate\Http\JsonResponse
     */
    public function acceptShare(Request $request, int $shareId) {

        $notificationId = $request->get('notificationId');
        return response()->json($this->shareApi->acceptShare($notificationId, $shareId));
    }

    /**
     * @param Request $request
     * @param int $shareId
     * @return \Illuminate\Http\JsonResponse
     */
    public function rejectShare(Request $request, int $shareId) {
        $notificationId = $request->get('notificationId');
        return response()->json($this->shareApi->rejectShare($notificationId, $shareId));
    }

    /**
     * @param Request $request
     * @param int $shareId
     * @return \Illuminate\Http\JsonResponse
     */
    public function revokeProductProject(Request $request, int $shareId) {

        $notificationId = $request->get('notificationId');
        return response()->json($this->revokeApi->revokeProductProject($notificationId, $shareId));
    }
}