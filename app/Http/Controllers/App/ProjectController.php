<?php

namespace App\Http\Controllers\App;

use App\Happy\HappyApi;
use App\Project\Project;
use App\Project\ProjectApi;
use App\Restriction;
use App\Revoke\RevokeApi;
use App\Settings\SettingsApi;
use App\Share\AcceptedProjectShare;
use App\Share\ShareApi;
use App\Support\SupportApi;
use App\Thank\ThankApi;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\LaravelController;
use App\Services\Traits\EloquentBuilderTrait;
use Illuminate\Support\Facades\Auth;

class ProjectController extends LaravelController
{
    use EloquentBuilderTrait;

    /**
     * @var User
     */
    protected $projectApi;
    protected $user;
    protected $project;
    protected $happyApi;
    protected $supportApi;
    protected $shareApi;
    protected $acceptedProjectShare;
    protected $revokeApi;
    protected $restriction;
    protected $settingsApi;
    protected $thankApi;

    /**
     * ProjectController constructor.
     * @param User $user
     * @param Project $project
     * @param HappyApi $happyApi
     * @param SupportApi $supportApi
     * @param ShareApi $shareApi
     * @param RevokeApi $revokeApi
     * @param AcceptedProjectShare $acceptedProjectShare
     * @param ProjectApi $projectApi
     * @param Restriction $restriction
     * @param SettingsApi $settingsApi
     * @param ThankApi $thankApi
     */
    public function __construct(User $user, Project $project, HappyApi $happyApi, SupportApi $supportApi,
                                ShareApi $shareApi, RevokeApi $revokeApi, AcceptedProjectShare $acceptedProjectShare,
                                ProjectApi $projectApi, Restriction $restriction, SettingsApi $settingsApi,
                                ThankApi $thankApi)
    {
        $this->user = $user;
        $this->project = $project;
        $this->happyApi = $happyApi;
        $this->supportApi = $supportApi;
        $this->shareApi = $shareApi;
        $this->acceptedProjectShare = $acceptedProjectShare;
        $this->revokeApi = $revokeApi;
        $this->projectApi = $projectApi;
        $this->restriction = $restriction;
        $this->settingsApi = $settingsApi;
        $this->thankApi = $thankApi;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userId = Auth::user()->getAuthIdentifier();

        $resourceOptions = $this->parseResourceOptions();

        $applyResult = $this->projectApi->showProjectsList($userId, $resourceOptions);

        $parsedData = $this->parseData(
            $applyResult['projects'],
            $resourceOptions,
            'projects'
        );
        $parsedData['count'] = $applyResult['count'];
        $parsedData['limitCount'] = $applyResult['limitCount'];
        $parsedData['filterCount'] = $applyResult['filterCount'];

        $parsedData['projects']->map(function(Project $project) {
            $project->happy = $project->getHappyFromUser(Auth::user());
            $project->save();

            $todayDate = Carbon::now();
            $beginDate = Carbon::parse($project->creation_date);
            $endDate = Carbon::parse($project->completion_date);

            if($todayDate->timestamp < $beginDate->timestamp) $project->date_status = 'Begins';
            if($todayDate->timestamp > $beginDate->timestamp && $todayDate->timestamp < $endDate->timestamp)
                $project->date_status = 'Till';
            if($todayDate->timestamp > $endDate->timestamp) $project->date_status = 'Ended';

            $project->save();
        });

        return response()->json($parsedData);
    }

//    public function count() {
//        $userId = Auth::user()->getAuthIdentifier();
//
//        $resourceOptions = $this->parseResourceOptions();
//
//        $count = $this->projectApi->showProjectsList($userId, $resourceOptions)->count();
//
//        return response()->json($count);
//    }

    /**
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function showProjectsListToShare($userId) {

        $authorizedUserId = Auth::user()->getAuthIdentifier();

        $resourceOptions = $this->parseResourceOptions();

        $applyResult = $this->projectApi->showProjectsListToShare($userId, $authorizedUserId, $resourceOptions);

        $parsedData = $this->parseData(
            $applyResult['projects'],
            $resourceOptions,
            'projects'
        );
        $parsedData['count'] = $applyResult['count'];
        $parsedData['limitCount'] = $applyResult['limitCount'];
        $parsedData['filterCount'] = $applyResult['filterCount'];

        $parsedData['projects']->map(function(Project $project) {
            $project->happy = $project->getHappyFromUser(Auth::user());
            $project->save();

            $todayDate = Carbon::now();
            $beginDate = Carbon::parse($project->creation_date);
            $endDate = Carbon::parse($project->completion_date);

            if($todayDate->timestamp < $beginDate->timestamp) $project->date_status = 'Begins';
            if($todayDate->timestamp > $beginDate->timestamp && $todayDate->timestamp < $endDate->timestamp)
                $project->date_status = 'Till';
            if($todayDate->timestamp > $endDate->timestamp) $project->date_status = 'Ended';

            $project->save();
        });

        return response()->json($parsedData);
    }


    /**
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function showProjectsListToThank($userId)
    {
        $resourceOptions = $this->parseResourceOptions();

        $applyResult = $this->projectApi->showProjectsListToThank($userId, $resourceOptions);

        $parsedData = $this->parseData(
            $applyResult['projects'],
            $resourceOptions,
            'projects'
        );
        $parsedData['count'] = $applyResult['count'];
        $parsedData['limitCount'] = $applyResult['limitCount'];
        $parsedData['filterCount'] = $applyResult['filterCount'];

        $parsedData['projects']->map(function(Project $project) {
            $project->happy = $project->getHappyFromUser(Auth::user());
            $project->save();

            $todayDate = Carbon::now();
            $beginDate = Carbon::parse($project->creation_date);
            $endDate = Carbon::parse($project->completion_date);

            if($todayDate->timestamp < $beginDate->timestamp) $project->date_status = 'Begins';
            if($todayDate->timestamp > $beginDate->timestamp && $todayDate->timestamp < $endDate->timestamp)
                $project->date_status = 'Till';
            if($todayDate->timestamp > $endDate->timestamp) $project->date_status = 'Ended';

            $project->save();
        });

        return response()->json($parsedData);
    }

    public function showProjectListForUsership()
    {
        $userId = Auth::user()->getAuthIdentifier();

        $resourceOptions = $this->parseResourceOptions();

        //check this later
        $applyResult = $this->projectApi->showProjectsList($userId, $resourceOptions);

        $parsedData = $this->parseData(
            $applyResult['projects'],
            $resourceOptions,
            'projects'
        );
        $parsedData['count'] = $applyResult['count'];
        $parsedData['limitCount'] = $applyResult['limitCount'];
        $parsedData['filterCount'] = $applyResult['filterCount'];

        return response()->json($parsedData);
    }


    public function showUserProjectList($userId) {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userId = Auth::user()->getAuthIdentifier();
        $title = $request->get('title');
        $description = $request->get('description');
        $link = $request->get('link');
        $creationDate = $request->get('creationDate');
        $completionDate = $request->get('completionDate');
        $type = $request->get('type');
        $start = $request->get('start');
        $end = $request->get('end');
        $share = $request->get('share');
        $support = $request->get('support');
        $sqEnabled = $request->get('sqEnabled');
        $file = $request->file('image');
        $author = $request->get('author');
        $authorStatus = $request->get('authorStatus');

        $response = $this->projectApi->createProject(
            $userId,
            $title,
            $description,
            $link,
            $creationDate,
            $completionDate,
            $type,
            $start,
            $end,
            $share,
            $support,
            $sqEnabled,
            $file,
            $author,
            $authorStatus
        );

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response['project']);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $userId = Auth::user()->getAuthIdentifier();

        return response()->json($this->projectApi->showProject($id, $userId));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $title = $request->get('title');
        $description = $request->get('description');
        $link = $request->get('link');
        $creationDate = $request->get('creationDate');
        $completionDate = $request->get('completionDate');
        $type = $request->get('type');
        $start = $request->get('start');
        $end = $request->get('end');
        $share = $request->get('share');
        $support = $request->get('support');
        $sqEnabled = $request->get('sq_enabled');
        $file = $request->file('image');
        $author = $request->get('author');
        $authorStatus = $request->get('authorStatus');

        $response = $this->projectApi->updateProject(
            $id,
            $title,
            $description,
            $link,
            $creationDate,
            $completionDate,
            $type,
            $start,
            $end,
            $share,
            $support,
            $sqEnabled,
            $file,
            $author,
            $authorStatus
        );

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response['project']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = $response = $this->project->where('id', '=', $id);
        $project->delete();

        return response()->json($response);
    }

    /**
     * Create a happy for project
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createHappy(Request $request) {

        $userId = Auth::user()->getAuthIdentifier();
        $projectId = $request->get('projectId');
        $value = $request->get('value');
        $message = $request->get('message');

        $response = $this->happyApi->createHappy($userId, $projectId, $value, $message);

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response['happy']);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createSupport(Request $request) {

        $userId = Auth::user()->getAuthIdentifier();
        $targetUserId = $request->get('targetUserId');
        $projectId = $request->get('projectId');
        $amount = $request->get('amount');
        $message = $request->get('message');

        $response = $this->supportApi->createProjectSupport($userId, $targetUserId, $projectId, $amount, $message);

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response['support']);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createShare(Request $request) {
        $userId =Auth::user()->getAuthIdentifier();
        $targetUserId = $request->get('targetUserId');
        $projectId = $request->get('projectId');
        $amount = $request->get('amount');
        $message = $request->get('message');
        $flag = $request->get('entityFlag');

        $response = $this->shareApi->createProjectShare($userId, $targetUserId, $projectId, $amount, $message, $flag);

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response['share']);
        }
    }

    /**
     * @param Request $request
     * @param int $shareId
     * @return \Illuminate\Http\JsonResponse
     */
    public function acceptShare(Request $request, int $shareId) {
        $notificationId = $request->get('notificationId');
        return response()->json($this->shareApi->acceptProjectShare($notificationId, $shareId));
    }

    /**
     * @param Request $request
     * @param int $shareId
     * @return \Illuminate\Http\JsonResponse
     */
    public function rejectShare(Request $request, int $shareId) {
        $notificationId = $request->get('notificationId');
        return response()->json($this->shareApi->rejectProjectShare($notificationId, $shareId));
    }

    /**
     * @param Request $request
     * @param int $shareId
     * @return \Illuminate\Http\JsonResponse
     */
    public function revokeProductProject(Request $request, int $shareId) {

        $notificationId = $request->get('notificationId');
        return response()->json($this->revokeApi->revokeProductProject($notificationId, $shareId));
    }

    public function showProjectThanks($projectId) {
        $userId = Auth::user()->getAuthIdentifier();

        $response = $this->thankApi->showProjectThanks($userId, $projectId);

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response['thanks']);
        }
    }

    public function getOwners(int $projectId) {
        $response = $this->projectApi->getOwners($projectId);

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response['owners']);
        }
    }

    public function addOwner(Request $request) {
        $projectId = $request->get('projectId');
        $ownersId = $request->get('ownersId');
        $authorStatus = $request->get('authorStatus');

        $response = $this->projectApi->addOwner($projectId, $ownersId, $authorStatus);

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response['owners']);
        }
    }

    public function deleteOwner($projectId, $ownerId) {
        $response = $this->projectApi->deleteOwner($projectId, $ownerId);

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response['owner']);
        }
    }

    public function addProjectAuthor(Request $request) {
        $projectId = $request->get('projectId');
        $authorId = $request->get('authorId');
        $authorStatus = $request->get('authorStatus');

        $response = $this->projectApi->addProjectAuthor($projectId, $authorId, $authorStatus);

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response['projectAuthor']);
        }
    }

    public function getProjectAuthor(int $projectId) {
        $response = $this->projectApi->getProjectAuthor($projectId);

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response['projectAuthor']);
        }
    }

    public function deleteProjectAuthor($projectId, $authorId) {
        $response = $this->projectApi->deleteProjectAuthor($projectId, $authorId);

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response['projectAuthor']);
        }
    }

    public function showPossibleProjectAuthorList($projectId) {
        $userId = Auth::user()->getAuthIdentifier();

        $resourceOptions = $this->parseResourceOptions();

        $applyResult = $this->projectApi->showPossibleProjectAuthorList($userId, $projectId, $resourceOptions);

        $parsedData = $this->parseData(
            $applyResult['projects'],
            $resourceOptions,
            'projects'
        );
        $parsedData['count'] = $applyResult['count'];
        $parsedData['limitCount'] = $applyResult['limitCount'];
        $parsedData['filterCount'] = $applyResult['filterCount'];

        return response()->json($parsedData);
    }

    public function getEditors(int $projectId) {
        $response = $this->projectApi->getEditors($projectId);

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response['editors']);
        }
    }

    public function addEditor(Request $request) {
        $projectId = $request->get('projectId');
        $userId = $request->get('userId');

        $response = $this->projectApi->addEditor($projectId, $userId);

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response['editor']);
        }
    }

    public function getParents(int $projectId) {
        $response = $this->projectApi->getParents($projectId);

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response['parents']);
        }
    }

    public function addParent(Request $request) {
        $projectId = $request->get('projectId');
        $parentProjectId = $request->get('parentProjectId');

        $response = $this->projectApi->addParent($projectId, $parentProjectId);

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response['parent']);
        }
    }

    public function getChildren(int $projectId) {
        $response = $this->projectApi->getChildren($projectId);

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response['children']);
        }
    }

    public function addChild(Request $request) {
        $projectId = $request->get('projectId');
        $childProjectId = $request->get('childProjectId');

        $response = $this->projectApi->addChild($projectId, $childProjectId);

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response['child']);
        }
    }

    public function getSettings(int $projectId) {
        $response = $this->settingsApi->getProjectSettings($projectId);

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response['settings']);
        }
    }

    public function setSettings(Request $request) {
        $projectId = $request->get('projectId');
        $basicBalance = $request->get('basicBalance');
        $ownershipType = $request->get('ownershipType');
        $sqType = $request->get('sqType');
        $ownershipSqBalance = $request->get('ownershipSqBalance');
        $creditTargetType = $request->get('creditTargetType');
        $usershipType = $request->get('usershipType');
        $creditTargetBalance = $request->get('creditTargetBalance');
        $voteSq = $request->get('voteSq');
        $numberPercent = $request->get('numberPercent');
        $ownershipUsershipBalance = $request->get('ownershipUsershipBalance');
        $editorGovernance = $request->get('editorGovernance');

        $response = $this->settingsApi->setProjectSettings(
            $projectId,
            $basicBalance,
            $ownershipType,
            $sqType,
            $ownershipSqBalance,
            $creditTargetType,
            $usershipType,
            $creditTargetBalance,
            $voteSq,
            $numberPercent,
            $ownershipUsershipBalance,
            $editorGovernance
        );

        if($response['status'] == false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response['settings']);
        }
    }
}