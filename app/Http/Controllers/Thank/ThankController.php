<?php

namespace App\Http\Controllers\Thank;

use App\Http\Controllers\Controller;
use App\Thank\Thank;
use App\Thank\ThankApi;
use Illuminate\Support\Facades\DB;
use Optimus\Bruno\LaravelController;
use Optimus\Bruno\EloquentBuilderTrait;


class ThankController extends LaravelController
{
    use EloquentBuilderTrait;

    /**
     * @var ThankApi
     */
    protected $thankModel;
    protected $thank;

    /**
     * ThankController constructor.
     *
     * @param Thank $thankModel
     * @param ThankApi $thank
     */
    public function __construct(ThankApi $thank, Thank $thankModel)
    {
        $this->thank = $thank;
        $this->thankModel = $thankModel;
    }


    public function index() {
        $resourceOptions = $this->parseResourceOptions();

        $query = $this->thankModel->setQuery(
            DB::table('thanks')
        );

        $this->applyResourceOPtions($query, $resourceOptions);
        $thanks = $query->get();
        $parsedData = $this->parseData($thanks, $resourceOptions, 'thanks');
        return response()->json($parsedData);
    }


    /**
     * This method is used to return thank by thank id
     * @param $thankId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getThankById($thankId)
    {
        return response()->json($this->thank->getThankById($thankId));
    }

    /**
     * This method is used to delete a thank by id
     * @param $thankId
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteThank($thankId) {
        return response()->json($this->thank->deleteThank($thankId));
    }
}