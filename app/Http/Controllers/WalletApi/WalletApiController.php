<?php

namespace App\Http\Controllers\WalletApi;

use App\Wallet\WalletApi;
use Illuminate\Http\Request;
use Optimus\Bruno\LaravelController;
use Optimus\Bruno\EloquentBuilderTrait;



class WalletApiController extends LaravelController
{

    use EloquentBuilderTrait;

    /**
     * @var WalletApi
     */
    private $wallet;

    /**
     * WalletApiController constructor.
     * @param WalletApi $wallet
     */
    public function __construct(WalletApi $wallet)
    {
        $this->wallet = $wallet;
    }

    /**
     * @param WalletApi $wallet
     * @return \Illuminate\Http\JsonResponse
     */
    public function countTransactions(WalletApi $wallet) {
        return $wallet->countTransactions();
    }

    /**
     * This method is used to create an account type and store it to database
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createAccountType(Request $request)
    {
        return response()->json($this->wallet->createAccountType(
            $request->get('accountType'),
            $request->get('machineName'),
            $request->get('isActive')
        ));
    }

    /**
     * This method is used to create a transaction type and store it to database
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createTransactionType(Request $request)
    {
        return response()->json($this->wallet->createTransactionType(
            $request->get('transactionType'),
            $request->get('isActive')
        ));
    }

    /**
     * This method is used to create a Transaction and store it to database
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createTransaction(Request $request)
    {
        $response = $this->wallet->createTransaction(
            'default',
            $request->get('transactionType'),
            $request->get('amount'),
            $request->get('userId'),
            $request->get('transactionStatus'),
            $request->get('adminId')
        );

        if($response['status'] = false) {
            return response($response['error'], 400);
        } else {
            return response()->json($response['transaction']);
        }
    }

    /**
     * This method is used to get a user Transaction using one or several params
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTransaction()
    {
        $resourceOptions = $this->parseResourceOptions();
        $parsedData = $this->parseData($this->wallet->getTransaction($resourceOptions),
        $resourceOptions);

        return response()->json($parsedData);
    }


    /**
     * This method is used for get account types
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAccountTypes(Request $request)
    {
        return response()->json($this->wallet->getAccountTypes($request->get('status')));
    }

    /**
     * This method is used for get transaction types
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTransactionTypes(Request $request)
    {
        return response()->json($this->wallet->getTransactionTypes($request->get('status')));
    }

    /**
     * This method is used for delete a transaction by transaction id
     * @param $transactionId
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteTransaction($transactionId)
    {
        return response()->json($this->wallet->deleteTransaction($transactionId));
    }

    /**
     * This method is used to activate or inactivate a transaction by transaction id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateTransaction(Request $request) {
        $transactionId = $request->get('transactionId');
        $transactionStatus = $request->get('transactionStatus');
        $transactionType = $request->get('transactionType');
        $accountType = $request->get('accountType');
        $amount = $request->get('amount');
        $userId = $request->get('userId');


        return response()->json($this->wallet->updateTransaction(
            $transactionId,
            $transactionStatus,
            $transactionType,
            $accountType,
            $amount,
            $userId
        ));
    }

    /**
     * This method is used to rebuild all user balances dependently on account table
     * @return \Illuminate\Http\JsonResponse
     */
    public function rebuildAllUserBalances() {
        return response()->json($this->wallet->rebuildAllUserBalances());
    }
}