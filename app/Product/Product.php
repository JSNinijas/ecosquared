<?php

namespace App\Product;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $likeModel;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->likeModel = app()->make('App\Like\Like');
    }

    protected $fillable = [
        'title',
        'description',
        'image',
        'link',
        'creationDate',
        'type',
        'start',
        'end',
        'share',
        'support',
        'like'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function activity() {
        return $this
            ->belongsToMany('App\User', 'product_activity', 'product_id', 'user_id')
            ->withPivot('activity');
    }

    public function usersLiked() {
        return $this->belongsToMany('App\User', 'likes', 'product_id', 'user_id');
    }

    public function usersSharedWith() {
        return $this->belongsToMany('App\User', 'shared_products',
            'product_id', 'user_id');
    }

    public function getLikeFromUser(User $user): int {
        $likeFromUser = $this->likeModel->where('user_id', '=', $user->id)
            ->where('product_id', '=', $this->id)
            ->orderBy('id', 'desc')->first();
        if($likeFromUser) {
            return $likeFromUser->value;
        } else {
            return 0;
        }
    }

}
