<?php

namespace App\Product;

use App\Events\ProductCreated;
use App\Events\ProductsChanged;
use App\Like\Like;
use App\Restriction;
use App\Share\AcceptedShare;
use App\Share\ShareApi;
use App\Support\Support;
use App\Support\SupportApi;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Services\Traits\EloquentBuilderTrait;

class ProductApi
{
    use EloquentBuilderTrait;

    /**
     * @var Product
     */
    protected $product;
    protected $acceptedShare;
    protected $supportApi;
    protected $user;
    protected $shareApi;
    protected $support;
    protected $like;
    protected $restriction;

    /**
     * ProductApi constructor.
     * @param Product $product
     * @param AcceptedShare $acceptedShare
     * @param SupportApi $supportApi
     * @param User $user
     * @param ShareApi $shareApi
     * @param Support $support
     * @param Like $like
     * @param Restriction $restriction
     */
    public function __construct(Product $product, AcceptedShare $acceptedShare, SupportApi $supportApi,
                                User $user, ShareApi $shareApi, Support $support, Like $like, Restriction $restriction)
    {
        $this->product = $product;
        $this->acceptedShare = $acceptedShare;
        $this->supportApi = $supportApi;
        $this->user = $user;
        $this->shareApi = $shareApi;
        $this->support = $support;
        $this->like = $like;
        $this->restriction = $restriction;
    }

    /**
     * @param $userId
     * @param $resourceOptions
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public function showProductsList($userId, $resourceOptions) {

        $initialLimit = (int)$resourceOptions['limit'];
        $resourceOptions['limit'] = -1;

        $ownProductsQuery = $this->product->setQuery(DB::table('products')
            ->where('products.user_id', '=', $userId)
            ->join('product_activity', 'products.id', 'product_activity.product_id')
            ->where('product_activity.user_id', '=', Auth::user()->getAuthIdentifier())
            ->select('products.*', 'product_activity.activity')
            );

        $applyQuery = $this->applyResourceOptions($ownProductsQuery, $resourceOptions);

        $count0 = $applyQuery['count'];
        $limitCount0 = $applyQuery['limitCount'];
        $limit = $initialLimit - $limitCount0;
        if($limit < 0) $limit = 0;

        $query = $this->product->setQuery(DB::table('products')
            ->join('shared_products', 'products.id', '=', 'shared_products.product_id')
            ->where('shared_products.user_id', '=', $userId)
            ->join('product_activity', 'products.id', 'product_activity.product_id')
            ->where('product_activity.user_id', '=', Auth::user()->getAuthIdentifier())
            ->select('products.*', 'product_activity.activity'));

        $applyQuery = $this->applyResourceOptions($query, $resourceOptions);

        $resourceOptions['limit'] = $initialLimit;
        $count = $applyQuery['count'] + $count0;
        $filterCount = $initialLimit;
        $limitCount = $applyQuery['limitCount'] + $limitCount0;

        $query = ($query->union($ownProductsQuery))->with('user');

        $resourceOptions['limit'] = $initialLimit;
        $this->applyResourceOptions($query, $resourceOptions);

//        dd($initialLimit);

        $products = $query->get();

        return array('products' => $products, 'count' => $count,
            'limitCount' => $limitCount, 'filterCount' => $filterCount);
    }

    /**
     * @param $userId
     * @param $authorizedUserId
     * @param $resourceOptions
     * @return array
     */
    public function showProductsListToShare($userId, $authorizedUserId, $resourceOptions) {

        $initialLimit = (int)$resourceOptions['limit'];
        $resourceOptions['limit'] = -1;

        $targetUserOwnProductsQuery = $this->product->setQuery(DB::table('products')
            ->select('id')
            ->where('user_id', '=', $userId));

        $userOwnProductsQuery = $this->product->setQuery(DB::table('products')
            ->where('user_id', '=', $authorizedUserId)->whereNotIn('products.id', function($query) use ($userId, $targetUserOwnProductsQuery) {
                $query
                    ->select('products.id')
                    ->from('products')
                    ->join('shared_products', 'products.id', '=', 'shared_products.product_id')
                    ->where('shared_products.user_id', '=', $userId)
                    ->union($targetUserOwnProductsQuery);
            }));

        $applyQuery = $this->applyResourceOptions($userOwnProductsQuery, $resourceOptions);

        $count0 = $applyQuery['count'];
        $limitCount0 = $applyQuery['limitCount'];
        $limit = $initialLimit - $limitCount0;
        if($limit < 0) $limit = 0;

        $query = $this->product->setQuery(
            DB::table('products')
                ->join('shared_products', 'products.id', '=', 'shared_products.product_id')
                ->where('shared_products.user_id', '=', $authorizedUserId)
                ->select('products.*')
                ->whereNotIn('products.id', function($query) use ($userId, $targetUserOwnProductsQuery) {
                    $query
                        ->select('products.id')
                        ->from('products')
                        ->join('shared_products', 'products.id', '=', 'shared_products.product_id')
                        ->where('shared_products.user_id', '=', $userId)
                        ->union($targetUserOwnProductsQuery);
                }));

        $applyQuery = $this->applyResourceOptions($query, $resourceOptions);

        $resourceOptions['limit'] = $initialLimit;
        $count = $applyQuery['count'] + $count0;
        $filterCount = $initialLimit;
        $limitCount = $applyQuery['limitCount'] + $limitCount0;

        $query = ($query->union($userOwnProductsQuery))->with('user');

        $resourceOptions['limit'] = $initialLimit;
        $this->applyResourceOptions($query, $resourceOptions);

        $products = $query->get();

        $this->applyResourceOptions($query, $resourceOptions);

        return array('products' => $products, 'count' => $count,
            'limitCount' => $limitCount, 'filterCount' => $filterCount);
    }

    /**
     * @param $userId
     * @param $title
     * @param $description
     * @param $link
     * @param $creationDate
     * @param $type
     * @param $start
     * @param $end
     * @param $share
     * @param $support
     * @param $sqEnabled
     * @param $file
     * @param $author
     * @param $authorStatus
     * @return array
     */
    public function createProduct($userId, $title, $description, $link,
                                  $creationDate, $type, $start, $end,
                                  $share, $support, $sqEnabled, $file, $author, $authorStatus) {

        if(!($title && $link)) {
            return array('status' => false, 'error' => 'Product must have title and link');
        }
        if($this->product->where('link', '=', $link)->first()) {
            return array('status' => false, 'error' => 'Not unique link!');
        }

        $product = $this->product;
        $user = $this->user->find($userId);

        $product->user_id = $userId;
        $product->title = $title;
        $product->description = $description;
        $product->link = $link;
        $product->value = 0;
        $product->like = 0;
        $authorStatus ? $product->author_status = $authorStatus : $product->author_status = 'me';
        if($creationDate) $product->creation_date = Carbon::parse($creationDate);
        if($type) $product->type = $type;
        $product->start = $start;
        $product->end = $end;
        if($share === '0' || $share === '1') $product->share = $share;
        if($support === '0' || $support == '1') $product->support = $support;
        if($sqEnabled) $product->sq_enabled = $sqEnabled;
        $product->chronology = Carbon::now();
        $product->counter =0;

        if($authorStatus === 'non-user') {
            $product->author = $author;
            $product->support = false;
        }

        $product->save();

        if($file) {
            $path = $file->storeAs('public/products', $product->id);
            Storage::setVisibility($path, 'public');
        } else {
            $path = 'public/products/default';
        }
        $product->image = Storage::url($path);

        $product->save();

        $product->activity()->attach($userId, ['activity' => Carbon::now(), 'target_user_id' => $userId]);

        event(new ProductCreated($product->load('user')));

        $product = $this->product->find($product->id);

        return array('status' => true, 'product' => $product);
    }

    /**
     * @param $id
     * @param $userId
     * @return Product
     */
    public function showProduct($id, $userId) {

        $product = $this->product->where('id', '=', $id)->with('user')->first();
        $likesSum = $product->usersLiked->unique()->count();

        $likesSum !== 0 ? : $likesSum = '0';
        $incomingShare = $this->acceptedShare
            ->where('target_user_id', '=', $userId)
            ->where('product_id', '=', $product->id)->first();
        $outgoingShare = $this->acceptedShare
            ->where('user_id', '=', $userId)
            ->where('product_id', '=', $product->id)->first();

        $lastSupport = $this->support
            ->where('user_id', '=', $userId)
            ->where('product_id', '=', $id)
            ->orderBy('id', 'desc')->first();


        if($lastSupport)$product->lastSupport = $lastSupport->amount;
        $product->liked = $likesSum;
        $product->shared = $product->usersSharedWith->count() + 1;
        // + 1 means adding author of product to show correct info when self likes enabled
        $product->supportedByUser = $this->supportApi->supportedByUser($userId, $product->id);
        $product->supportedByAllUsers = $this->supportApi->supportedByAllUsers($product->id);
        if($incomingShare) {
            $product->incomingShareUser = $this->user->find($incomingShare->user_id);
            $product->incomingShareAmount = $incomingShare->amount;
            $product->outgoingShareAmount = $this->acceptedShare
                ->where('user_id', '=', $userId)
                ->where('product_id', '=', $product->id)
                ->sum('amount');
        }
        if($outgoingShare) {
            $product->outgoingShareAmount = $this->acceptedShare
                ->where('user_id', '=', $userId)
                ->where('product_id', '=', $product->id)
                ->sum('amount');
        }
        $product->restrictions = $this->restriction->find(1);

        return $product;
    }

    /**
     * @param $id
     * @param $title
     * @param $description
     * @param $link
     * @param $creationDate
     * @param $type
     * @param $start
     * @param $end
     * @param $share
     * @param $support
     * @param $sqEnabled
     * @param $file
     * @param $author
     * @param $authorStatus
     * @return Product
     */
    public function updateProduct($id, $title, $description, $link, $creationDate,
                                  $type, $start, $end, $share, $support, $sqEnabled, $file, $author, $authorStatus) {

        $product = $this->product->find($id);
        $user = $this->user->find($product->user_id);

        !$title ? : $product->title = $title;
        !$description ? : $product->description = $description;
        !$link ? : $product->link = $link;
        !$creationDate ? : $product->creation_date = Carbon::parse($creationDate);
        !$type ? : $product->type = $type;
        !$start ? : $product->start = $start;
        !$end ? : $product->end = $end;
        !$authorStatus ? $product->author_status = 'me' : $product->author_status = $authorStatus;

        !($share === '0' || $share === '1') ? : $product->share = $share;
        !$sqEnabled ? : $product->sq_enabled = $sqEnabled;
        $product->support = $support;

        if($authorStatus === 'non-user') {
            $product->author = $author;
            $product->support = false;
        } else {
            $product->author = null;
            $product->support = $support;
        }

//        dd($product->support, $authorStatus);

        if($file) {
            $path = $file->storeAs('public/products', $id);
            Storage::setVisibility($path, 'public');
            $product->image = Storage::url($path);
        }
//        dd($support);
        $product->save();
        $product->authorStatus = $authorStatus;
        event(new ProductsChanged($user, $product));
        return $product;
    }
}