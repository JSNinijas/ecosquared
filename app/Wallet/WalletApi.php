<?php

namespace App\Wallet;

use App\Share\Share;
use App\User;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;
use Optimus\Bruno\LaravelController;
use Optimus\Bruno\EloquentBuilderTrait;
use App\Events\TransactionCreated;

class WalletApi extends LaravelController
{
    use EloquentBuilderTrait;

    protected $accountTypeModel;
    protected $transactionTypeModel;
    protected $accountModel;
    protected $userTotalBalance;
    protected $share;
    protected $user;

    public function __construct(AccountTypeModel $accountTypeModel,
                                TransactionTypeModel $transactionTypeModel,
                                AccountModel $accountModel,
                                UserTotalBalance $userTotalBalance,
                                Share $share, User $user ) {

        $this->accountTypeModel = $accountTypeModel;
        $this->transactionTypeModel = $transactionTypeModel;
        $this->accountModel = $accountModel;
        $this->userTotalBalance = $userTotalBalance;
        $this->share = $share;
        $this->user = $user;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function countTransactions() {
        return response()->json(AccountModel::count());
    }

    /**
     * This method is used to create an account type and store it to database (possible future use)
     * @param $name
     * @param $code
     * @param $status
     * @return array
     */
    public function createAccountType($name, $code, $status) {
        try {
            if($this->accountTypeModel->where('code', '=', $code)->first()) {
                return array('status' => false, 'error' => 'Code must be unique!');
            }
            $this->accountTypeModel->create([
                'name' => $name,
                'code' => $code,
                'status' => $status
            ]);
            return array('status' => true, 'accountType' => $this->accountTypeModel
                ->orderBy('id', 'desc')->first());
        }
        catch(Exception $e) {
            return array('status' => false, 'error' => 'Something wrong!');
        }
    }

    /**
     * This method is used to create transaction type and store it to database
     * @param $code
     * @param $status
     * @return array
     */
    public function createTransactionType($code, $status) {
        try {
            if($this->transactionTypeModel->where('code', '=', $code)->first()) {
                return array('status' => false, 'error' => 'Transaction type must be unique!');
            }
            $this->transactionTypeModel->create([
                'code' => $code,
                'status' => $status
            ]);
            return array('status' => true, 'transactionType' => $this->transactionTypeModel
                ->orderBy('id', 'desc')->first());
        }
        catch(Exception $e) {
            return array('status' => false, 'error' => 'Something wrong!');
        }
    }

    /**
     * This method is used to create a transaction
     * @param string $accountType is account type id (possible future usage)
     * @param $transactionType string transaction type ('received', 'shared', 'pending', 'bought')
     * @param $amount int transaction amount it could be positive or negative depends on demand
     * @param $userId int user id from transaction
     * @param int $transactionStatus is transaction status will be 0 => Inactive or 1 => Active
     * @return array
     */

    public function createTransaction($accountType = 'default', $transactionType, $amount, $userId, $transactionStatus = 1, $adminId = null)
    {
        try {
            if(!$this->transactionTypeModel->where('code', '=', $transactionType)->first()) {
                return array('status' => false, 'error' => 'Use a valid transaction type!');
            }

            $this->accountModel->create([
                'user_id' => $userId,
                'amount' => $amount,
                'account_type' => $accountType,
                'transaction_type' => $transactionType,
                'transaction_date' => date('Y-m-d H:i:s'),
                'transaction_status' => $transactionStatus,
                'admin_id' => $adminId
            ]);

            $sum = $this->accountModel->where('user_id', $userId)
                                ->where('transaction_type', 'bought')->sum('amount');

            $user = $this->user->where('id', $userId)->first();
            if($sum >= 1000) {
                $user->create_feature = true;
                $user->save();
            }

            $transaction = $this->accountModel->orderBy('id', 'desc')->first();

            event(new TransactionCreated($transaction));

            return array('status' => true, 'transaction' => $transaction);
        } catch (Exception $e) {
            return array('status' => false, 'error' => 'Something wrong!');
        }
    }

    /**
     * This method is used to update user total balance (database search was fixed)
     * @param $userId
     * @param $amount
     * @return $userTotal
     */
    public function updateUserTotalBalance($userId, $amount)
    {
        $userTotal = $this->userTotalBalance->where('user_id', '=', $userId)->first();

        if (!empty($userTotal)) {
            $userTotal->total_balance += $amount;
            $userTotal->save();
        } else {
            $userTotal = $this->userTotalBalance->create([
                'user_id' => $userId,
                'total_balance' => $amount,
                'modify_date' => date('Y-m-d H:i:s'),
            ]);
        }

        return $userTotal;
    }

    /**
     * This method is used to return user total balance
     * @param $userId
     * @return array
     */
    public function fetchUserBalance($userId) {
        $balance = 0;
        $userTotal = $this->userTotalBalance->where('user_id', '=', $userId)->get();

        if(!empty($userTotal) || $userTotal[0]['total_balance'] === 0) {
            $balance = $userTotal[0]['total_balance'];
        }
        return array('status' => true, 'userBalance' => $balance);
    }

    public function fetchUserBalanceByTypes($userId)
    {
        $userBalance = $this->userTotalBalance->where('user_id', '=', $userId);
        if ($userBalance) {
            $userBalance->pendingFromUser = -(int)$this->accountModel->setQuery(DB::table('account')
                ->where('user_id', '=', $userId)->whereIn('account.id', function ($query) use ($userId) {
                    $query
                        ->select('account.id')
                        ->from('account')
                        ->join('pending_products', 'account.id', '=', 'pending_products.user_transaction_id')
                        ->where('pending_products.user_id', '=', $userId)
                        ->where('account.transaction_status', '=', 1);
                })
               ->orWhereIn('account.id', function ($query) use($userId) {
                   $query
                       ->select('account.id')
                       ->from('account')
                       ->join('non_users', 'account.id', '=', 'non_users.transaction_id')
                       ->where('non_users.inviter_id', '=', $userId);
               }))->sum('amount');
            $userBalance->pendingToUser = (int)$this->accountModel->setQuery(DB::table('account')
                ->where('user_id', '=', $userId)->whereIn('account.id', function ($query) use ($userId) {
                    $query
                        ->select('account.id')
                        ->from('account')
                        ->join('pending_products', 'account.id', '=', 'pending_products.target_user_transaction_id')
                        ->where('pending_products.target_user_id', '=', $userId);
                }))->sum('amount');
            $userBalance->shared = (int)-$this->accountModel
                ->where('transaction_type', '=', 'shared')
                ->where('user_id', '=', $userId)->sum('amount');
            $userBalance->received = (int)$this->accountModel
                ->where('transaction_type', '=', 'received')
                ->where('user_id', '=', $userId)->sum('amount');
            $userBalance->bought = (int)$this->accountModel
                ->where('transaction_type', '=', 'bought')
                ->where('user_id', '=', $userId)->sum('amount');
        }

        return $userBalance;
    }

    /**
     * This method is used to return all total balances
     * @return array
     */
    public function fetchAllUserBalances() {
        $userBalances = $this->userTotalBalance->all()->toArray();
        if(!empty($userBalances)) {
            return array('status' => true, 'userBalances' => $userBalances);
        }
        else {
            return array('status' =>false, 'error' => 'User balances table is empty');
        }
    }

    /**
     * This method is used to get a user transaction using one or several params
     * @param $resourceOptions
     * @return array
     */
    public function getTransaction($resourceOptions) {

        $query = AccountModel::query();
        $this->applyResourceOptions($query, $resourceOptions);
        $transactions = $query->get();

        $transactions->map(function($transaction) {
            $transaction->admin = $this->user->find($transaction->admin_id);
        });

        return $transactions;
    }

    public function getTransactionById($transactionId) {
        return $this->accountModel->find($transactionId);
    }

    /**
     * This method is used for get account types
     * @param null $status
     * @return array
     */
    public function getAccountTypes($status = null) {
        $accountType = $this->accountTypeModel->where(function ($query) use ($status) {
            if($status != null)
                return $query->where('status','=',$status);
        })->get()->toArray();
        return array('status' => true, 'accountType' => $accountType);
    }

    /**
     * This method is used for get transaction types
     * @param null $status
     * @return array
     */
    public function getTransactionTypes($status = null) {
        $transactionType = $this->transactionTypeModel->where(function ($query) use ($status) {
            if($status != null)
                return $query->where('status','=',$status);
        })->get()->toArray();
        return array('status' => true, 'transactionType' => $transactionType);
    }

    /**
     * @param $transactionId
     * @return array
     * @throws \Exception
     */
    public function deleteTransaction($transactionId)
    {
        if (!$this->accountModel->find($transactionId)) {
            return array('status' => false, 'error' => 'Please enter a valid transaction id!');
        } else {
            $transaction = $this->accountModel->where('id', '=', $transactionId)->get()->toArray();
            $transaction = array_shift($transaction);
            $deleteTransaction = $this->accountModel->find($transactionId);
            $deleteTransaction->delete();
            if ($transaction['transaction_status'] == 1) {
                $userTotal = $this->userTotalBalance->where('user_id', '=', $transaction['user_id'])->first();
                if (!empty($userTotal)) {
                    $userTotal->total_balance -= $transaction['amount'];
                    $userTotal->save();
                }
            }
            return array('status' => true);
        }
    }

    /**
     * @param int $transactionId
     * @param int|null $transactionStatus
     * @param null $transactionType
     * @param null $accountType
     * @param null $amount
     * @param $userId
     * @return array
     */
    public function updateTransaction(
        int $transactionId,
        int $transactionStatus = null,
        $transactionType = null,
        $accountType = null,
        $amount = null,
        $userId
    ) {

        if(!$this->accountModel->find($transactionId)) {
            return array('status' => false, 'error' => 'Please enter a valid transaction id');
        }
        else if(!($transactionStatus == 0 || $transactionStatus == 1)) {
            return array('status' =>false, 'error' => 'Please enter a valid status');
        }
        else {
            $transaction = $this->accountModel->where('id','=',$transactionId)->get()->toArray();
            $transaction = array_shift($transaction);
            $saveTransaction = $this->accountModel->find($transactionId);

            if($transactionStatus === 0 || $transactionStatus === 1) {
                $saveTransaction->transaction_status = $transactionStatus;
            }
            if($transactionType) {
                $saveTransaction->transaction_type = $transactionType;
            }
            if($accountType) {
                $saveTransaction->account_type = $accountType;
            }
            if($amount) {
                $saveTransaction->amount = $amount;
            }
            if($userId) {
                $saveTransaction->user_id = $userId;
            }
            $saveTransaction->save();
        }
        return array('status' => true, 'transaction' => $saveTransaction);
    }

    /**
     * This method is used to rebuild user_total_balance table dependently on account_table
     * @return array
     */
    public function rebuildAllUserBalances() {
        $transactions = $this->accountModel->all();
        $userBalances = $this->userTotalBalance->all();
        foreach($userBalances as $balance) {
            $balance->total_balance = $transactions
                ->where('user_id', '=', $balance->user_id)
                ->where('transaction_status', '=', 1)
                ->sum('amount');
            $balance->save();
        }
        return array('status' => true, 'userBalances' => $userBalances);
    }

    /**
     * This method is used to rebuild user balance dependently on account_table by user id
     * TODO: REFACTOR THIS SH
     * @param $userId
     * @return array
     */
    public function rebuildUserTotalBalance($userId) {

        $userBalance = $this->userTotalBalance->where('user_id', '=', $userId)->first();
        $balance = $this->accountModel->all()
            ->where('user_id', '=', $userId)
            ->where('transaction_status', '=', 1)
            ->sum('amount');
        if($userBalance) {
            $userBalance->total_balance = $balance;
            $userBalance->save();
        } else {
            $this->userTotalBalance->create([
                'user_id' => $userId,
                'modify_date' => date('Y-m-d H:i:s'),
                'total_balance' => $balance,
            ]);
        }
        return array(
            'status' => true,
            'userBalance' => $this->userTotalBalance->where('user_id', '=', $userId)->first()
        );
    }
}