<?php

namespace App\Invite;


use App\Chatwee\Chatwee;
use App\Events\OutgoingNotificationDeleted;
use App\Events\OutgoingShareChanged;
use App\Events\UserBalanceUpdated;
use App\Events\UserInvited;
use App\Mail\InviteMessage;
use App\Mail\RequestInviteMessage;
use App\NonUser;
use App\Notification\NotificationsApi;
use App\Notification\Outgoing;
use App\Product\Product;
use App\Project\Project;
use App\Settings\Settings;
use App\Share\AcceptedProjectShare;
use App\Share\AcceptedShare;
use App\Share\Share;
use App\Share\ShareApi;
use App\User;
use App\Wallet\UserTotalBalance;
use App\Wallet\WalletApi;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;

class InviteApi
{
    protected $user;
    protected $product;
    protected $project;
    protected $nonUser;
    protected $walletApi;
    protected $acceptedShare;
    protected $acceptedProjectShare;
    protected $notificationsApi;
    protected $shareApi;
    protected $balance;
    protected $outgoing;
    protected $settings;
    protected $chatweeApi;

    public function __construct(User $user, Product $product, Project $project, NonUser $nonUser,
                                WalletApi $walletApi, AcceptedShare $acceptedShare,
                                AcceptedProjectShare $acceptedProjectShare,
                                NotificationsApi $notificationsApi,
                                ShareApi $shareApi, UserTotalBalance $balance, Outgoing $outgoing,
                                Settings $settings, Chatwee $chatweeApi)
    {
        $this->user = $user;
        $this->product = $product;
        $this->project = $project;
        $this->nonUser = $nonUser;
        $this->walletApi = $walletApi;
        $this->acceptedShare = $acceptedShare;
        $this->acceptedProjectShare = $acceptedProjectShare;
        $this->notificationsApi = $notificationsApi;
        $this->shareApi = $shareApi;
        $this->balance = $balance;
        $this->outgoing = $outgoing;
        $this->settings = $settings;
        $this->chatweeApi = $chatweeApi;
    }

    public function sendInviteMessage($userId, $name = null, $surname = null,
                                      $message = null, $email, $productId, $amount)
    {

        $isUnsubscribe = NonUser::where('email', $email)
                ->where('unsubscribe', true)
                ->count() != 0;

        if ($isUnsubscribe) {
            return array('status' => false, 'error' => 'User denied sending invitation!');
        }

        if (!$email) {
            return array('status' => false, 'error' => 'Email field is required!');
        }

        if (!$message) {
            return array('status' => false, 'error' => 'Message field is required!');
        }

        if ($this->walletApi->fetchUserBalance($userId)['userBalance'] < $amount) {
            return array('status' => false, 'error' => 'Not enough credits to invite!');
        }

        if ($this->user->where('email', '=', $email)->first()) {
            $targetUser = $this->user->where('email', '=', $email)->first();

            $duplicateShare = $this->acceptedShare
                ->where('target_user_id', '=', $targetUser->id)
                ->where('product_id', '=', $productId)->get();

            if ($duplicateShare->isEmpty()) {
                $duplicateProduct = $targetUser->products()->find($productId);
            } else {
                $duplicateProduct = $this->product->find($duplicateShare->first()->product_id);
            }
            if ($duplicateProduct) {
                return array('status' => false, 'error' => 'Target User already has this product!');
            }

            $flag = 'users';
            $this->shareApi->createShare(
                $userId,
                $targetUser->id,
                $productId,
                $amount,
                $message,
                $flag
            );
            return array('status' => true, 'product' => $this->product->find($productId));
        }

        $user = $this->user->find($userId);
        $product = $this->product->find($productId);
        $productTitle = $product->title;
        $author = $this->user->find($product->user_id);
        if ($product->author !== null) {
            $author = $product->author;
        } else {
            $author = $author->name . ' ' . $author->surname;
        }

        $duplicateUserCheck = $this->nonUser->where('email', '=', $email)->get();

        if ($duplicateUserCheck->isEmpty()) {

            $fields = array();

            $fields['name'] = 'unknown';
            $fields['surname'] = 'unknown';
            $fields['email'] = uniqid();
            $fields['password'] = 'new';
            $fields['active'] = 0;

            $newUser = $this->user->createFactory($fields);
        } else {

            $newUserId = $this->nonUser->where('email', '=', $email)->first()->user_id;
            $newUser = $this->user->find($newUserId);
        }

        $token = md5($email);

        $scheme = URL::to('/');
        $url = URL::to('/') . "/#/registration/" . $token;
        $urlUnsubscribe = URL::to('/') . "/#/unsubscribe/" . $token;

//        $url = route('SendInviteMessage');
//        $url = substr($url, 0, -15) . "/#/registration/" . $token;

//        $url = $url . parse_url($currentUrl, PHP_URL_PORT) . "/#/registration/" . $token;

        Mail::to($email)->send(new InviteMessage($user->name, $user->surname,
            $name, $surname, $message, $productTitle, $amount, $url, $scheme, $author, $urlUnsubscribe));

        $transaction = $this->walletApi
            ->createTransaction('default', 'pending', -$amount, $userId, 1);

        $nonUser = $this->nonUser->create([
            'user_id' => $newUser->id,
            'inviter_id' => $userId,
            'transaction_id' => $transaction['transaction']->id,
            'message' => $message,
            'card' => 'product',
            'card_id' => $product->id,
            'amount' => $amount,
            'encrypted_email' => $token,
            'email' => $email,
        ]);
        event(new UserInvited($nonUser));

        $outgoingOffers = $this->outgoing
            ->where('user_id', '=', $userId)
            ->whereIn('type', ['userInvite', 'productShare', 'projectShare'])
            ->count();

        event(new OutgoingShareChanged($user, $outgoingOffers));

        return array('status' => true, 'product' => $product);
    }

    public function sendProjectInviteMessage($userId, $name = null, $surname = null,
                                             $message = null, $email, $projectId, $amount)
    {

        $isUnsubscribe = NonUser::where('email', $email)
                ->where('unsubscribe', true)
                ->count() != 0;

        if ($isUnsubscribe) {
            return array('status' => false, 'error' => 'User denied sending invitation!');
        }

        if (!$email) {
            return array('status' => false, 'error' => 'Email field is required!');
        }

        if (!$message) {
            return array('status' => false, 'error' => 'Message field is required!');
        }

        if ($this->walletApi->fetchUserBalance($userId)['userBalance'] < $amount) {
            return array('status' => false, 'error' => 'Not enough credits to invite!');
        }

        if ($this->user->where('email', '=', $email)->first()) {
            $targetUser = $this->user->where('email', '=', $email)->first();

            $duplicateProjectShare = $this->acceptedProjectShare
                ->where('target_user_id', '=', $targetUser->id)
                ->where('project_id', '=', $projectId)->get();

            if ($duplicateProjectShare->isEmpty()) {
                $duplicateProject = $targetUser->projects()->find($projectId);
            } else {
                $duplicateProject = $this->project->find($duplicateProjectShare->first()->project_id);
            }
            if ($duplicateProject) {
                return array('status' => false, 'error' => 'Target User already has this project!');
            }

            $flag = 'users';
            $this->shareApi->createProjectShare(
                $userId,
                $targetUser->id,
                $projectId,
                $amount,
                $message,
                $flag
            );
            return array('status' => true, 'project' => $this->project->find($projectId));
        }

        $user = $this->user->find($userId);
        $project = $this->project->find($projectId);
        $projectTitle = $project->title;
        $author = $this->user->find($project->user_id);
        $author = $author->name . ' ' . $author->surname;

        $duplicateUserCheck = $this->nonUser->where('email', '=', $email)->get();

        if ($duplicateUserCheck->isEmpty()) {

            $fields = array();

            $fields['name'] = 'unknown';
            $fields['surname'] = 'unknown';
            $fields['email'] = uniqid();
            $fields['password'] = 'new';
            $fields['active'] = 0;

            $newUser = $this->user->createFactory($fields);
        } else {

            $newUserId = $this->nonUser->where('email', '=', $email)->first()->user_id;
            $newUser = $this->user->find($newUserId);
        }

        $token = md5($email);

        $scheme = URL::to('/');
        $url = URL::to('/') . "/#/registration/" . $token;
        $urlUnsubscribe = URL::to('/') . "/#/unsubscribe/" . $token;

//        $url = route('SendInviteMessage');
//        $url = substr($url, 0, -15) . "/#/registration/" . $token;


//        $url = $url . parse_url($currentUrl, PHP_URL_PORT) . "/#/registration/" . $token;

        Mail::to($email)->send(new InviteMessage($user->name, $user->surname,
            $name, $surname, $message, $projectTitle, $amount, $url, $scheme, $author, $urlUnsubscribe));

        $transaction = $this->walletApi
            ->createTransaction('default', 'pending', -$amount, $userId, 1);

        $nonUser = $this->nonUser->create([
            'user_id' => $newUser->id,
            'inviter_id' => $userId,
            'transaction_id' => $transaction['transaction']->id,
            'message' => $message,
            'card' => 'project',
            'card_id' => $project->id,
            'amount' => $amount,
            'encrypted_email' => $token,
            'email' => $email,
        ]);
        event(new UserInvited($nonUser));

        $outgoingOffers = $this->outgoing
            ->where('user_id', '=', $userId)
            ->whereIn('type', ['userInvite', 'productShare', 'projectShare', 'userProjectInvite'])
            ->count();

        event(new OutgoingShareChanged($user, $outgoingOffers));

        return array('status' => true, 'project' => $project);
    }

    public function checkInvitedUser($hash)
    {
        $nonUser = $this->nonUser->where('encrypted_email', '=', $hash)->first();

        if ($nonUser) {
            return array('status' => true, 'nonUser' => $nonUser);
        } else {
            return array('status' => false, 'error' => 'You can not register on this service!!!');
        }
    }

    public function registerUser($card, $userId, $email, $inviterId, $transactionId, $message, $cardId,
                                 $amount, $name, $surname, $password, $description = null, $file = null)
    {


        $user = $this->user->find($userId);

        $user->name = $name;
        $user->surname = $surname;
        $description ? $user->description = $description : $user->description = 'Description';
        $user->sq = 1;
        $user->active = true;
        $user->password = bcrypt($password);
        $user->email = $this->nonUser->where('user_id', '=', $userId)->first()->email;

        if ($file) {
            $path = $file->storeAs('public/avatars', $userId);
            Storage::setVisibility($path, 'public');
            $user->image = Storage::url($path);
        }
        $user->save();

//        $chatweeUserId = $this->chatweeApi->registerUser($user->with('roles')->first());
//
//
//        $user->chatwee_id = $chatweeUserId;
//        $user->save();

        $this->balance->create([
            'user_id' => $user->id,
            'total_balance' => 0,
            'modify_date' => date('Y-m-d H:i:s'),
        ]);

        $notifications = $this->outgoing
            ->where('target_user_id', '=', $userId)
            ->get();
        $nonUsers = $this->nonUser->where('user_id', '=', $userId)->get();

        foreach ($notifications as $notification) {
            event(new OutgoingNotificationDeleted($notification));
            $notification->delete();
        }

        foreach ($nonUsers as $nonUser) {
            if ($nonUser->card == 'product') {
                $this->shareApi->createShare(
                    $nonUser->inviter_id,
                    $nonUser->user_id,
                    $nonUser->card_id,
                    $nonUser->amount,
                    $nonUser->message,
                    'users'
                );
            } else {
                $this->shareApi->createProjectShare(
                    $nonUser->inviter_id,
                    $nonUser->user_id,
                    $nonUser->card_id,
                    $nonUser->amount,
                    $nonUser->message,
                    'users'
                );
            }

            $transaction = $this->walletApi
                ->createTransaction('default', 'pending',
                    $nonUser->amount, $nonUser->inviter_id, 1);
            $this->walletApi->deleteTransaction($nonUser->transaction_id);
            $this->walletApi->deleteTransaction($transaction['transaction']->id);

            $nonUser->delete();
        }

        $this->settings->create(['user_id' => $userId]);


        return $user;
    }

    /**
     * @param $notificationId
     * @return mixed|static
     */
    public function revokeInvite($notificationId)
    {

        $notification = $this->notificationsApi->getOutgoing($notificationId);
        $nonUser = $this->nonUser->find($notification->share_id);
        $transaction = $this->walletApi->getTransactionById($nonUser->transaction_id);
        if ($notification->type == 'productShare') {
            $card = $this->product->find($nonUser->card_id);
        } else {
            $card = $this->project->find($nonUser->card_id);
        }
        $user = $this->user->find($nonUser->user_id);
        $inviter = $this->user->find($nonUser->inviter_id);

        $this->walletApi->updateTransaction($transaction->id, 1, 'received',
            'default', $nonUser->amount, $nonUser->inviter_id);

        $this->walletApi->deleteTransaction($transaction->id);
        event(new OutgoingNotificationDeleted($notification));
        event(new UserBalanceUpdated($this->walletApi->rebuildUserTotalBalance($nonUser->inviter_id)['userBalance']));

        $notification->delete();
        if ($this->nonUser->where('user_id', '=', $user->id)->get()->isEmpty()) {
            $user->delete();
        }

        $outgoingOffers = $this->outgoing
            ->where('user_id', '=', $inviter->id)
            ->whereIn('type', ['userInvite', 'userProjectInvite', 'productShare', 'projectShare'])
            ->count();

        event(new OutgoingShareChanged($inviter, $outgoingOffers));

        $nonUser->delete();
        return $card;
    }

    public function revokeProjectInvite($notificationId)
    {

        $notification = $this->notificationsApi->getOutgoing($notificationId);
        $nonUser = $this->nonUser->find($notification->share_id);
        $transaction = $this->walletApi->getTransactionById($nonUser->transaction_id);
        $card = $this->project->find($nonUser->card_id);
        $user = $this->user->find($nonUser->user_id);
        $inviter = $this->user->find($nonUser->inviter_id);

        $this->walletApi->updateTransaction($transaction->id, 1, 'received',
            'default', $nonUser->amount, $nonUser->inviter_id);

        $this->walletApi->deleteTransaction($transaction->id);
        event(new OutgoingNotificationDeleted($notification));
        event(new UserBalanceUpdated($this->walletApi->rebuildUserTotalBalance($nonUser->inviter_id)['userBalance']));

        $notification->delete();
        if ($this->nonUser->where('user_id', '=', $user->id)->get()->isEmpty()) {
            $user->delete();
        }

        $outgoingOffers = $this->outgoing
            ->where('user_id', '=', $inviter->id)
            ->whereIn('type', ['userInvite', 'userProjectInvite', 'productShare', 'projectShare'])
            ->count();

        event(new OutgoingShareChanged($inviter, $outgoingOffers));

        $nonUser->delete();
        return $card;
    }

    public function sendRequestInviteMessage($email, $userMessage)
    {

        if (!$email) {
            return array('status' => false, 'error' => 'An error occurred while sending an email!');
        }

        if (!$userMessage) {
            return array('status' => false, 'error' => "Message can't be empty!");
        }

        Mail::to('request@ecosquared.co.uk')->send(new RequestInviteMessage($email, $userMessage));

        return array('status' => true, 'email' => $email, 'userMessage' => $userMessage);
    }
}