<?php

namespace App\Notification;

use App\Events\FetchNewIncoming;
use App\Events\IncomingNotificationChanged;
use App\Events\IncomingNotificationCreated;
use App\Events\IncomingNotificationDeleted;
use App\Events\OutgoingNotificationCreated;
use App\Happy\Happy;
use App\Like\Like;
use App\NonUser;
use App\Product\Product;
use App\Project\Project;
use App\Settings\Settings;
use App\Share\AcceptedProjectShare;
use App\Share\AcceptedShare;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Services\Traits\EloquentBuilderTrait;

class NotificationsApi
{
    use EloquentBuilderTrait;

    protected $user;
    protected $product;
    protected $project;
    protected $incoming;
    protected $outgoing;
    protected $acceptedShare;
    protected $acceptedProjectShare;
    protected $nonUser;
    protected $like;
    protected $happy;
    protected $settings;

    public function __construct(Incoming $incoming, Outgoing $outgoing, Project $project,
                                AcceptedProjectShare $acceptedProjectShare, User $user, Product $product,
                                AcceptedShare $acceptedShare, NonUser $nonUser, Like $like, Happy $happy,
                                Settings $settings ) {
        $this->incoming = $incoming;
        $this->outgoing = $outgoing;
        $this->user = $user;
        $this->product = $product;
        $this->project = $project;
        $this->acceptedShare = $acceptedShare;
        $this->acceptedProjectShare = $acceptedProjectShare;
        $this->nonUser = $nonUser;
        $this->like = $like;
        $this->happy = $happy;
        $this->settings = $settings;
    }

    public function showIncomingList($userId, $resourceOptions) {

        $card = null;
        $query = Incoming::query()->where('target_user_id', '=', $userId);
        $applyResult = $this->applyResourceOptions($query, $resourceOptions);
        $user = $this->user->find($userId);

        $notifications = $query->orderBy('created_at','DESC')->get();

        $notifications->map(function($notification) use($card){
            $user = $this->user->find($notification->user_id);
            if($notification->type == 'productShare' || $notification->type == 'userInvite' ||
                $notification->type == 'productLike' || $notification->type == 'productSupported') {
                $card = $this->product->find($notification->card_id);
            } elseif($notification->type == 'projectShare' || $notification->type == 'userProjectInvite' ||
                $notification->type == 'projectHappy' || $notification->type == 'projectSupported'  ||
                $notification->type == 'projectThank') {
                $card = $this->project->find($notification->card_id);
            }
            $notification->seen = true;
            $notification->save();

            $notification->user = $user;
            if($card) {
                $notification->card = $card;
                $notification->card->user = $this->user->find($card->user_id);
            }

            switch($notification->type) {
                case 'productShare':
                case 'projectShare':
                case 'userInvite':
                case 'userProjectInvite':
                    $notification->status = 'offered';
                    break;
                case 'thank':
                    $notification->status = 'thanked';
                    break;
                case 'productLike':
                    $notification->status = 'liked';
                    break;
                case 'productSupported':
                case 'projectSupported':
                    $notification->status = 'supported';
                    break;
                case 'productRevoke':
                    $notification->status = 'revoked';
            }
        });

        $count = $applyResult['count'];
        $filterCount = $applyResult['filterCount'];
        $limitCount = $applyResult['limitCount'];

        $incomingOnOff = $this->incoming->where('on_off', '=', 'off')
            ->where('target_user_id', '=', $userId)->get();
        $unseenNotifications = $this->incoming->where('seen', '=', false)
            ->where('target_user_id', '=', $userId)->get();
        if ($unseenNotifications->isEmpty() || $incomingOnOff->isNotEmpty()) {
            $unseen = false;
        } else {
            $unseen = true;
        }

        if (!$notifications->isEmpty() && !$incomingOnOff->isNotEmpty()) {
            $notification = $notifications->first();
            event(new IncomingNotificationChanged($notification));
            event(new FetchNewIncoming($user, array('count' => $count, 'unseen' => $unseen)));
        }

        return array(
            'notifications' => $notifications,
            'count' => $count,
            'filterCount' => $filterCount,
            'limitCount' => $limitCount
        );
    }

    public function showOutgoingList($userId, $resourceOptions) {

        $card = null;
        $query = Outgoing::query()->where('user_id', '=', $userId);
        $applyResult = $this->applyResourceOptions($query, $resourceOptions);

        $notifications = $query->orderBy('created_at','DESC')->get();

        $notifications->map(function($notification) use($card) {
            $user = $this->user->find($notification->target_user_id);
            if($notification->type == 'productShare' || $notification->type == 'userInvite' ||
                $notification->type == 'productLike' || $notification->type == 'productSupported') {
                $card = $this->product->find($notification->card_id);
            } elseif($notification->type == 'projectShare' || $notification->type == 'userProjectInvite' ||
                $notification->type == 'projectHappy' || $notification->type == 'projectSupported') {
                $card = $this->project->find($notification->card_id);
            }
            $notification = $this->addCorrectInvitedUserEmail($notification);
            $notification->user = $user;
            if($card) {
                $notification->card = $card;
                $notification->card->user = $this->user->find($card->user_id);
            }

            switch($notification->type) {
                case 'productShare':
                case 'projectShare':
                case 'userInvite':
                case 'userProjectInvite':
                    $notification->status = 'offered';
                    break;
                case 'thank':
                    $notification->status = 'thanked';
                    break;
                case 'productLike':
                    $notification->status = 'liked';
                    break;
                case 'projectHappy':
                    $notification->status = 'happied';
                    break;
                case 'productSupported':
                case 'projectSupported':
                    $notification->status = 'supported';
                    break;
                case 'productRevoke':
                case 'projectRevoke':
                    $notification->status = 'revoked';
                    break;
            }
        });

        $count = $applyResult['count'];
        $filterCount = $applyResult['filterCount'];
        $limitCount = $applyResult['limitCount'];

        return array(
            'notifications' => $notifications,
            'count' => $count,
            'filterCount' => $filterCount,
            'limitCount' => $limitCount
        );
    }

    public function createIncomingNotification(string $type, int $userId, int $targetUserId,
                                       int $value, int $cardId = null, string $message = null, int $shareId = null) {

        $incoming = $this->incoming->create([
            'type' => $type,
            'user_id' => $userId,
            'target_user_id' => $targetUserId,
            'card_id' => $cardId,
            'value' => $value,
            'message' => $message,
            'share_id' => $shareId,
            'seen' => 0
        ])->load('user', 'card.user');

        $settings = $this->settings->where('user_id', '=', $targetUserId)->first();

        switch($incoming->type) {
            case 'thank':
                $incoming->on_off = $settings->thanks;
                $incoming->remove_on_read = $settings->thanks_remove;
                break;
            case 'productLike':
                $incoming->on_off = $settings->product_likes;
                $incoming->remove_on_read = $settings->product_likes_remove;
                break;
            case 'projectHappy':
                $incoming->on_off = $settings->project_happy;
                $incoming->remove_on_read = $settings->project_happy_remove;
                break;
            case 'productSupported':
                $incoming->on_off = $settings->product_support;
                $incoming->remove_on_read = $settings->product_support_remove;
                break;
            case 'projectSupported':
                $incoming->on_off = $settings->project_support;
                $incoming->remove_on_read = $settings->project_support_remove;
                break;
            case 'projectThank':
                $incoming->on_off = $settings->thanks;
                $incoming->remove_on_read = $settings->thanks_remove;
                break;
        }
        $incoming->save();
        $incomingOnOff = $this->incoming->where('on_off', '=', 'off')
            ->where('target_user_id', '=', $targetUserId)->get();
        if ($incomingOnOff->isEmpty()) {
            event(new IncomingNotificationCreated($incoming));
        }
        return $incoming;
    }

    public function createOutgoingNotification(string $type, int $userId, int $targetUserId,
                                           int $value, int $cardId = null, string $message = null, $shareId = null) {

        $outgoing = $this->outgoing->create([
            'type' => $type,
            'user_id' => $userId,
            'target_user_id' => $targetUserId,
            'card_id' => $cardId,
            'value' => $value,
            'message' => $message,
            'share_id' => $shareId,
        ])->load('user', 'card.user', 'targetUser');

        $outgoing = $this->addCorrectInvitedUserEmail($outgoing);

        event(new OutgoingNotificationCreated($outgoing));
        return $outgoing;
    }

    public function getIncoming($notificationId) {

        $card = null;
        $notification = $this->incoming->find($notificationId);
        $notification->update(['seen' => true]);
        if($notification->remove_on_read == 'off') {
            $notification->read = 'on';
            $notification->save();
        }
        $user = $this->user->find($notification->user_id);
        if($notification->type == 'productShare' || $notification->type == 'userInvite' ||
            $notification->type == 'productSupported' || $notification->type == 'productLike') {
            $card = $this->product->find($notification->card_id);
        } elseif($notification->type == 'projectShare' || $notification->type == 'projectSupported' ||
            $notification->type == 'userProjectInvite' || $notification->type == 'projectHappy') {
            $card = $this->project->find($notification->card_id);
        } elseif($notification->type == 'thank' || $notification->type == 'productLike' ||
            $notification->type == 'projectHappy') {
        } elseif($notification->type == 'projectThank') {
            $card = $this->project->find($notification->card_id);
        }

        $notification->shareFromAuthUser = $this->acceptedShare
            ->where('user_id', '=', Auth::user()->getAuthIdentifier())
            ->where('target_user_id', '=', $user->id)
            ->sum('amount');
        $notification->shareToAuthUser = $this->acceptedShare
            ->where('user_id', '=', $user->id)
            ->where('target_user_id', '=', Auth::user()->getAuthIdentifier())
            ->sum('amount');


        $notification->user = $user;
        if($card) {
            $notification->card = $card;
            $notification->card->user = $this->user->find($card->user_id);
            $notification->card->shared = $card->usersSharedWith->count() + 1;
            // +1 means adding author of product to count

            if($notification->type == 'productShare' ||
                $notification->type == 'userInvite' ||
                $notification->type == 'productSupported') {
                $notification->card->liked = $card->usersLiked->unique()->count();
//                $notification->card->like = $this->like
//                    ->where('user_id', '=', $notification->target_user_id)
//                    ->where('product_id', '=', $card->id)
//                    ->orderBy('id', 'desc')->first('value');
            } elseif($notification->type == 'projectShare' ||
                    $notification->type == 'projectSupported' ||
                    $notification->type == 'userProjectInvite') {
                $notification->card->happied = $card->usersHappy->unique()->count();
//                $notification->card->happy = $this->happy
//                    ->where('user_id', '=', $notification->target_user_id)
//                    ->where('project_id', '=', $card->id)
//                    ->orderBy('id', 'desc')->first('value');
            }
        }

        if($notification->remove_on_read == 'on') {
            event(new IncomingNotificationDeleted($notification));
            event(new IncomingNotificationChanged($notification));
            $notification->delete();
        } else {
            event(new IncomingNotificationChanged($notification));
        }

        return $notification;
    }

    public function getOutgoing($notificationId) {

        $card = null;
        $notification = $this->outgoing->find($notificationId);
        $notification->update(['seen' => true]);

        $targetUser = $this->user->find($notification->target_user_id);
        if ($notification->type == 'productShare' || $notification->type == 'userInvite' ||
            $notification->type == 'productSupported') {
            $card = $this->product->find($notification->card_id);
        } elseif($notification->type == 'projectShare' || $notification->type == 'projectSupported' ||
            $notification->type == 'userProjectInvite') {
            $card = $this->project->find($notification->card_id);
        }
        $notification->user = $targetUser;

        if($card) {
            $notification->card = $card;
            $notification->card->user = $this->user->find($card->user_id);

            $notification->card->shared = $card->usersSharedWith->count() + 1;
            // +1 means adding author of product to count

            if($notification->type == 'productShare' ||
                $notification->type == 'userInvite' ||
                $notification->type == 'productSupported') {
                $notification->card->liked = $card->usersLiked->unique()->count();
//                $notification->card->userLike = $this->like
//                    ->where('user_id', '=', $notification->user_id)
//                    ->where('product_id', '=', $card->id)
//                    ->orderBy('id', 'desc');//->first('value');
                $incomingShare = $this->acceptedShare
                    ->where('target_user_id', '=', $notification->user_id)
                    ->where('product_id', '=', $card->id)->first();
            } elseif($notification->type == 'projectShare' ||
                $notification->type == 'projectSupported' ||
                $notification->type == 'userProjectInvite') {
                $notification->card->happied = $card->usersHappy->unique()->count();
//                $notification->card->happy = $this->happy
//                    ->where('user_id', '=', $notification->user_id)
//                    ->where('project_id', '=', $card->id)
//                    ->orderBy('id', 'desc')->first('value');
                $incomingShare = $this->acceptedProjectShare
                    ->where('target_user_id', '=', $notification->user_id)
                    ->where('project_id', '=', $card->id)->first();
            }

            if($incomingShare) {
                $notification->incomingShareUser = $this->user->find($incomingShare->user_id);
                $notification->incomingShareAmount = $incomingShare->amount;
            }
        }

        $notification = $this->addCorrectInvitedUserEmail($notification);

        return $notification;
    }

    public function getIncomingByIds($type, $userId, $targetUserId, $cardId) {
        $notification = $this->incoming
            ->where('type', '=', $type)
            ->where('user_id', '=', $userId)
            ->where('target_user_id', '=', $targetUserId)
            ->where('card_id', '=', $cardId);

        return $notification;
    }

    public function getOutgoingByIds($type, $userId, $targetUserId, $cardId) {
        $notification = $this->outgoing
            ->where('type', '=', $type)
            ->where('user_id', '=', $userId)
            ->where('target_user_id', '=', $targetUserId)
            ->where('card_id', '=', $cardId)->first();

        return $notification;
    }

    public function addCorrectInvitedUserEmail($outgoing) {

        $nonUser = $this->nonUser->where('inviter_id', '=', $outgoing->user_id)
            ->where('user_id', '=', $outgoing->target_user_id)->get();

        if(!$nonUser->isEmpty()) {
            $outgoing->targetUser->email = $nonUser->first()->email;
        };

//        dd($outgoing);

        return $outgoing;
    }

    public function showOfferHistory($userId, $resourceOptions) {
        $card = null;
        $projects = AcceptedProjectShare::query()->where('target_user_id', '=', $userId);
        $products = AcceptedShare::query()->where('target_user_id', '=', $userId);


        $projectsResult = $this->applyResourceOptions($projects, $resourceOptions);
        $productsResult = $this->applyResourceOptions($products, $resourceOptions);

        $user = $this->user->find($userId);

        $projectsOffer = $projectsResult['queryBuilder']->orderBy('created_at', 'DESC')->get();
        $productsOffer = $productsResult['queryBuilder']->orderBy('created_at', 'DESC')->get();

        $projectsOffer->map(function($item) {
            $user = $this->user->find($item->user_id);

            $item->user = $user;
            if($item->project_id) {
                $item->card = $this->project->where('id', '=', $item->project_id)->first();
                $item->card->user = $user = $this->user->find($item->user_id);
            }
        });

        $productsOffer->map(function($item) {
            $user = $this->user->find($item->user_id);

            $item->user = $user;
            if($item->product_id) {
                $item->card = $this->product->where('id', '=', $item->product_id)->first();
                $item->card->user = $user = $this->user->find($item->user_id);
            }
        });

        $count = $projectsResult['count'] + $productsResult['count'];
        $filterCount = $projectsResult['filterCount'] + $productsResult['filterCount'];
        $limitCount = $projectsResult['limitCount'] + $productsResult['limitCount'];

        return array(
            'notifications' => [$projectsOffer, $productsOffer],
            'count' => $count,
            'filterCount' => $filterCount,
            'limitCount' => $limitCount
        );
    }
}