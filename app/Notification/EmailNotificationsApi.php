<?php
/**
 * Created by IntelliJ IDEA.
 * User: dev
 * Date: 26.01.18
 * Time: 14:49
 */

namespace App\Notification;


use App\Mail\StatisticsMessage;
use App\Settings\Settings;
use App\User;
use Illuminate\Support\Facades\Mail;

class EmailNotificationsApi
{
    protected $user;
    protected $settings;

    public function __construct(User $user, Settings $settings)
    {
        $this->user = $user;
        $this->settings = $settings;
    }

    public function sendStatistics() {

        $users = $this->user->orderBy('id', 'desc')->get();

        $users->map(function($user) {
            $statisticsSetting = $this->settings->where('user_id', '=', $user->id)->first()->activity;

            switch($statisticsSetting) {
                case 'daily':
//                    Mail::to($user->email)->send(new StatisticsMessage($user, $statisticsSetting));
                    break;
                case 'weekly':
//                    Mail::to($user->email)->send(new StatisticsMessage($user, $statisticsSetting));
                    break;
            }
        });
    }
}