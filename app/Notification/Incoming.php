<?php

namespace App\Notification;

use App\Events\IncomingNotificationDeleted;
use App\Product\Product;
use App\Project\Project;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Incoming extends Model
{
    use Notifiable;
    protected $table = 'incoming_notifications';

    protected $fillable = [
        'type',
        'user_id',
        'target_user_id',
        'card_id',
        'value',
        'message',
        'share_id',
        'seen'
    ];

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'deleted' => IncomingNotificationDeleted::class,
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function card() {
        if($this->type == 'productShare') {
            return $this->belongsTo(Product::class);
        } else {
            return $this->belongsTo(Project::class);
        }
    }
}
