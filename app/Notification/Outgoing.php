<?php

namespace App\Notification;

use App\Events\OutgoingNotificationDeleted;
use App\Product\Product;
use App\Project\Project;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Outgoing extends Model
{
    use Notifiable;
    protected $table = 'outgoing_notifications';

    protected $fillable = [
        'type',
        'user_id',
        'target_user_id',
        'card_id',
        'value',
        'message',
        'share_id'
    ];

    protected $dispatchesEvents = [
        'deleted' => OutgoingNotificationDeleted::class
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function card() {
        if($this->type == 'productShare') {
            return $this->belongsTo(Product::class);
        } else {
            return $this->belongsTo(Project::class);
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function targetUser() {
        return $this->belongsTo(User::class);
    }
}
