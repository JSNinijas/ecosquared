<?php

namespace App\Share;

use Illuminate\Database\Eloquent\Model;

class Share extends Model
{
    protected $table = 'pending_products';

    protected $fillable = [
        'user_id',
        'target_user_id',
        'product_id',
        'user_transaction_id',
        'target_user_transaction_id',
        'amount',
        'message'
    ];
}