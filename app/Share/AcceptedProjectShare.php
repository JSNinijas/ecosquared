<?php

namespace App\Share;

use Illuminate\Database\Eloquent\Model;

class AcceptedProjectShare extends Model
{
//    protected $table = 'accepted_project_shares';

    protected $fillable = [
        'user_id',
        'target_user_id',
        'project_id',
        'amount',
        'message'
    ];
}
