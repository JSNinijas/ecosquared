<?php

namespace App\Share;

use App\Events\AddNewUserToAuthorFriendList;
use App\Events\AddNewUserToFriendList;
use App\Events\IncomingShareChanged;
use App\Events\OutgoingShareChanged;
use App\Events\ProductAccepted;
use App\Events\ProductRejected;
use App\Events\ProjectAccepted;
use App\Events\ProjectRejected;
use App\Events\ProjectShared;
use App\Mail\ShareMessage;
use App\Project\Project;
use App\User;
use App\Product\Product;
use App\Wallet\WalletApi;
use App\Events\ProductShared;
use App\Notification\Incoming;
use App\Notification\Outgoing;
use App\Events\UserBalanceUpdated;
use App\Notification\NotificationsApi;
use App\Events\IncomingNotificationDeleted;
use App\Events\OutgoingNotificationDeleted;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class ShareApi
{
    protected $user;
    protected $product;
    protected $project;
    protected $walletApi;
    protected $share;
    protected $projectShare;
    protected $notificationsApi;
    protected $acceptedShare;
    protected $acceptedProjectShare;
    protected $incoming;
    protected $outgoing;

    function __construct(User $user, Product $product, Project $project, WalletApi $walletApi, Share $share,
                         ProjectShare $projectShare, NotificationsApi $notificationsApi, AcceptedShare $acceptedShare,
                         AcceptedProjectShare $acceptedProjectShare, Incoming $incoming, Outgoing $outgoing)
    {
        $this->user = $user;
        $this->product = $product;
        $this->project = $project;
        $this->walletApi = $walletApi;
        $this->share = $share;
        $this->projectShare = $projectShare;
        $this->acceptedProjectShare = $acceptedProjectShare;
        $this->notificationsApi = $notificationsApi;
        $this->acceptedShare = $acceptedShare;
        $this->incoming = $incoming;
        $this->outgoing = $outgoing;
    }

    public function createShare(int $userId, int $targetUserId, int $productId,
                                int $amount, string $message = null, string $flag) {

        if(!($this->user->find($userId) && $this->user->find($targetUserId))) {
            return array('status' => false, 'error' => 'Invalid user id!');
        }
        if(!$this->product->find($productId)) {
            return array('status' => false, 'error' => 'Invalid product id!');
        }
        if($userId === $targetUserId) {
            return array('status' => false, 'error' => 'User can not share with himself!');
        }
        if($this->walletApi->fetchUserBalance($userId)['userBalance'] < $amount) {
            return array('status' => false, 'error' => 'Not enough credits to share product!');
        }
        if(!$message) {
            return array('status' => false, 'error' => 'The message field is required!');
        }

        if($this->share
            ->where('user_id', '=', $userId)
            ->where('target_user_id', '=', $targetUserId)
            ->where('product_id', '=', $productId)->first()) {

            return array('status' => false, 'error' => 'The same pending product exists!');
        }

        if($this->user->where('id', '=', $targetUserId)->first()->productsShared
            ->where('id', '=', $productId)->first()) {

            return array('status' => false, 'error' => 'Target user already has this product!');
        }

        $userTransaction = $this->walletApi
            ->createTransaction('default', 'pending', -$amount, $userId, 1);
        $targetUserTransaction = $this->walletApi
            ->createTransaction('default', 'pending', $amount, $targetUserId, 0);

        $share = $this->share->create([
            'user_id' => $userId,
            'target_user_id' => $targetUserId,
            'product_id' => $productId,
            'user_transaction_id' => $userTransaction['transaction']->id,
            'target_user_transaction_id' => $targetUserTransaction['transaction']->id,
            'amount' => $amount,
            'message' => $message
        ]);

        event(new ProductShared($share, $flag));

        $user = $this->user->find($userId);
        $targetUser = $this->user->find($targetUserId);
        $offers = $this->incoming
            ->where('target_user_id', '=', $targetUserId)
            ->whereIn('type', ['productShare', 'projectShare', 'userInvite', 'userProjectInvite'])
            ->count();
        $outgoingOffers = $this->outgoing
            ->where('user_id', '=', $userId)
            ->whereIn('type', ['productShare', 'projectShare', 'userInvite', 'userProjectInvite'])
            ->count();

        event(new IncomingShareChanged($targetUser, $offers));
        event(new OutgoingShareChanged($user, $outgoingOffers));


        return array('status' => true, 'share' => $share);
    }

    public function createProjectShare(int $userId, int $targetUserId, int $projectId, int $amount,
                                       string $message = null, string $flag) {

        if(!($this->user->find($userId) && $this->user->find($targetUserId))) {
            return array('status' => false, 'error' => 'Invalid user id!');
        }
        if(!$this->project->find($projectId)) {
            return array('status' => false, 'error' => 'Invalid project id!');
        }
        if($userId === $targetUserId) {
            return array('status' => false, 'error' => 'User can not share with himself!');
        }
        if($this->walletApi->fetchUserBalance($userId)['userBalance'] < $amount) {
            return array('status' => false, 'error' => 'Not enough credits to share project!');
        }
        if(!$message) {
            return array('status' => false, 'error' => 'The message field is required!');
        }

        if($this->projectShare
            ->where('user_id', '=', $userId)
            ->where('target_user_id', '=', $targetUserId)
            ->where('project_id', '=', $projectId)->first()) {

            return array('status' => false, 'error' => 'The same pending project exists!');
        }

        if($this->user->where('id', '=', $targetUserId)->first()->projectsShared
            ->where('id', '=', $projectId)->first()) {

            return array('status' => false, 'error' => 'Target user already has this project!');
        }

        $userTransaction = $this->walletApi
            ->createTransaction('default', 'pending', -$amount, $userId, 1);
        $targetUserTransaction = $this->walletApi
            ->createTransaction('default', 'pending', $amount, $targetUserId, 0);

        $projectShare = $this->projectShare->create([
            'user_id' => $userId,
            'target_user_id' => $targetUserId,
            'project_id' => $projectId,
            'user_transaction_id' => $userTransaction['transaction']->id,
            'target_user_transaction_id' => $targetUserTransaction['transaction']->id,
            'amount' => $amount,
            'message' => $message
        ]);

        event(new ProjectShared($projectShare, $flag));

        $user = $this->user->find($userId);
        $targetUser = $this->user->find($targetUserId);
        $offers = $this->incoming
            ->where('target_user_id', '=', $targetUserId)
            ->whereIn('type',['productShare', 'projectShare', 'userInvite', 'userProjectInvite'])
            ->count();
        $outgoingOffers = $this->outgoing
            ->where('user_id', '=', $userId)
            ->whereIn('type', ['productShare', 'projectShare', 'userInvite', 'userProjectInvite'])
            ->count();

        event(new IncomingShareChanged($targetUser, $offers));
        event(new OutgoingShareChanged($user, $outgoingOffers));

        return array('status' => true, 'share' => $projectShare);
    }

    /**
     * @param int $shareId
     * @return Share
     */
    public function getShare(int $shareId) {
        return $this->share->find($shareId);
    }

    /**
     * @param int $shareId
     * @return ProjectShare
     */
    public function getProjectShare(int $shareId) {
        return $this->projectShare->find($shareId);
    }

    public function acceptShare(int $notificationId, int $shareId)
    {
        $share = $this->share->find($shareId);
        $incomingNotification = $this->notificationsApi->getIncoming($notificationId);
        $outgoingNotification = $this->notificationsApi->getOutgoingByIds(
            $incomingNotification->type,
            $incomingNotification->user_id,
            $incomingNotification->target_user_id,
            $incomingNotification->card_id
        );

        $user = $this->user->find($share->user_id);
        $targetUser = $this->user->find($share->target_user_id);
        $product = $this->product->find($share->product_id);

        $isExistRelations = $user->users()
            ->where(['target_user_id' => $targetUser->id, 'user_id'=> $user->id])->first();
        if(!$isExistRelations){
            $user->users()->attach($targetUser->id);
            $targetUser->users()->attach($user->id);
            $user->activity()->attach($targetUser->id, ['activity' => Carbon::now()]);
        }

        $author = $this->user->find($product->user_id);
        $isExistAuthorRelations = $author->users()
            ->where(['target_user_id' => $targetUser->id, 'user_id' => $author->id])->first();
        if ($author->id != $user->id && !$isExistAuthorRelations) {
            $targetUser->users()->attach($product->user_id);
            $author->users()->attach($targetUser);
            $author->activity()->attach($targetUser->id, ['activity' => Carbon::now()]);
            $targetUser->activity()->attach($author->id, ['activity' => Carbon::now()]);
        }

        $product->activity()
            ->attach($targetUser->id, ['activity' => Carbon::now(), 'target_user_id' => $user->id]);

        $product = $this->product->find($share->product_id);
        $userTransaction = $this->walletApi->getTransactionById($share->user_transaction_id);
        $targetUserTransaction = $this->walletApi->getTransactionById($share->target_user_transaction_id);

        $targetUser->productsShared()->attach($product->id);

        $this->walletApi->updateTransaction($userTransaction->id, 1,
            'shared', 'default', -$share->amount, $user->id);


        $this->walletApi->updateTransaction($targetUserTransaction->id, 1,
            'received', 'default', $share->amount, $targetUser->id);
        event(new UserBalanceUpdated($this->walletApi->rebuildUserTotalBalance($targetUser->id)['userBalance']));

        $this->acceptedShare->create([
            'user_id' => $share->user_id,
            'target_user_id' => $share->target_user_id,
            'product_id' => $share->product_id,
            'amount' => $share->amount,
            'message' => $share->message
        ]);

        $share->delete();

        $sameShares = $this->share
            ->where('target_user_id', '=', $targetUser->id)
            ->where('product_id', '=', $product->id)->get();
        $sameShareIncoming = $this->incoming
            ->where('type', '=', 'productShare')
            ->where('target_user_id', '=', $targetUser->id)
            ->where('card_id', '=', $product->id)->get();
        $sameShareOutgoing = $this->outgoing
            ->where('type', '=', 'productShare')
            ->where('target_user_id', '=', $targetUser->id)
            ->where('card_id', '=', $product->id)->get();


        if($sameShares) {
            $sameShares->map(function ($share) {
                $userTransaction = $this->walletApi->getTransactionById($share->user_transaction_id);
                $targetUserTransaction = $this->walletApi->getTransactionById($share->target_user_transaction_id);

                $this->walletApi->updateTransaction($userTransaction->id, 1,
                    'received', 'default', $share->amount, $share->user_id);

                $this->walletApi->deleteTransaction($userTransaction->id);
                $this->walletApi->deleteTransaction($targetUserTransaction->id);

                $share->delete();
            });
        }

        $sameShareIncoming->map(function ($notification) {
            $notification->delete();
        });

        $sameShareOutgoing->map(function ($notification) {
            $notification->delete();
        });

        $author = $this->user->find($product->user_id);

        $offers = $this->incoming
            ->where('target_user_id', '=', $targetUser->id)
            ->whereIn('type',['productShare', 'projectShare'])
            ->count();
        $outgoingOffers = $this->outgoing
            ->where('user_id', '=', $user->id)
            ->whereIn('type', ['productShare', 'projectShare', 'userInvite', 'userProjectInvite'])
            ->count();

        event(new IncomingShareChanged($targetUser, $offers));
        event(new OutgoingShareChanged($user, $outgoingOffers));
        event(new AddNewUserToFriendList($user, $targetUser));
        event(new AddNewUserToAuthorFriendList($author, $targetUser));
        event(new IncomingNotificationDeleted($incomingNotification));
        event(new OutgoingNotificationDeleted($outgoingNotification));
        event(new UserBalanceUpdated($this->walletApi->rebuildUserTotalBalance($user->id)['userBalance']));
        event(new ProductAccepted($share));

        return $incomingNotification;
    }

    public function acceptProjectShare(int $notificationId, int $shareId)
    {
        $projectShare = $this->projectShare->find($shareId);
        $incomingNotification = $this->notificationsApi->getIncoming($notificationId);
        $outgoingNotification = $this->notificationsApi->getOutgoingByIds(
            $incomingNotification->type,
            $incomingNotification->user_id,
            $incomingNotification->target_user_id,
            $incomingNotification->card_id
        );

        $user = $this->user->find($projectShare->user_id);
        $targetUser = $this->user->find($projectShare->target_user_id);
        $project = $this->project->find($projectShare->project_id);

        $isExistRelations = $user->users()
            ->where(['target_user_id' => $targetUser->id, 'user_id'=> $user->id])->first();
        if(!$isExistRelations){
            $user->users()->attach($targetUser->id);
            $targetUser->users()->attach($user->id);
            $user->activity()->attach($targetUser->id, ['activity' => Carbon::now()]);
        }

        $author = $this->user->find($project->user_id);
        $isExistAuthorRelations = $author->users()
            ->where(['target_user_id' => $targetUser->id, 'user_id' =>$author->id])->first();
        if ($author->id != $user->id && !$isExistAuthorRelations) {
            $targetUser->users()->attach($project->user_id);
            $author->users()->attach($targetUser);
            $author->activity()->attach($targetUser->id, ['activity' => Carbon::now()]);
        }

//        $usersInProject = $project->users()->pluck('users.id');

//        dd($usersInProject);

//        $isExistProjectRelations = $project->users()->get();

        $project->activity()
            ->attach($targetUser->id, ['activity' => Carbon::now(), 'target_user_id' => $user->id]);

        $project = $this->project->find($projectShare->project_id);
        $userTransaction = $this->walletApi->getTransactionById($projectShare->user_transaction_id);
        $targetUserTransaction = $this->walletApi->getTransactionById($projectShare->target_user_transaction_id);

        $targetUser->projectsShared()->attach($project->id);


        $this->walletApi->updateTransaction($userTransaction->id, 1,
            'shared', 'default', -$projectShare->amount, $user->id);

        $this->walletApi->updateTransaction($targetUserTransaction->id, 1,
            'received', 'default', $projectShare->amount, $targetUser->id);

        event(new UserBalanceUpdated($this->walletApi->rebuildUserTotalBalance($targetUser->id)['userBalance']));

        $this->acceptedProjectShare->create([
            'user_id' => $projectShare->user_id,
            'target_user_id' => $projectShare->target_user_id,
            'project_id' => $projectShare->project_id,
            'amount' => $projectShare->amount,
            'message' => $projectShare->message
        ]);

        $projectShare->delete();

        $sameShares = $this->projectShare
            ->where('target_user_id', '=', $targetUser->id)
            ->where('project_id', '=', $project->id)->get();
        $sameShareIncoming = $this->incoming
            ->where('type', '=', 'projectShare')
            ->where('target_user_id', '=', $targetUser->id)
            ->where('card_id', '=', $project->id)->get();
        $sameShareOutgoing = $this->outgoing
            ->where('type', '=', 'projectShare')
            ->where('target_user_id', '=', $targetUser->id)
            ->where('card_id', '=', $project->id)->get();


        if($sameShares) {
            $sameShares->map(function ($share) {
                $userTransaction = $this->walletApi->getTransactionById($share->user_transaction_id);
                $targetUserTransaction = $this->walletApi->getTransactionById($share->target_user_transaction_id);

                $this->walletApi->updateTransaction($userTransaction->id, 1,
                    'received', 'default', $share->amount, $share->user_id);

                $this->walletApi->deleteTransaction($userTransaction->id);
                $this->walletApi->deleteTransaction($targetUserTransaction->id);

                $share->delete();
            });
        }

        $sameShareIncoming->map(function ($notification) {
            $notification->delete();
        });

        $sameShareOutgoing->map(function ($notification) {
            $notification->delete();
        });

        $author = $this->user->find($project->user_id);

        $offers = $this->incoming
            ->where('target_user_id', '=', $targetUser->id)
            ->whereIn('type',['productShare', 'projectShare', 'userInvite', 'userProjectInvite'])
            ->count();
        $outgoingOffers = $this->outgoing
            ->where('user_id', '=', $user->id)
            ->whereIn('type', ['productShare', 'projectShare', 'userInvite', 'userProjectInvite'])
            ->count();

        event(new IncomingShareChanged($targetUser, $offers));
        event(new OutgoingShareChanged($user, $outgoingOffers));
        event(new AddNewUserToFriendList($user, $targetUser));
        event(new AddNewUserToAuthorFriendList($author, $targetUser));
        event(new IncomingNotificationDeleted($incomingNotification));
        event(new OutgoingNotificationDeleted($outgoingNotification));
        event(new UserBalanceUpdated($this->walletApi->rebuildUserTotalBalance($user->id)['userBalance']));
        event(new ProjectAccepted($projectShare));

        return $incomingNotification;
    }

    public function rejectShare(int $notificationId, int $shareId) {
        $share = $this->share->find($shareId);
        $incomingNotification = $this->notificationsApi->getIncoming($notificationId);
        $outgoingNotification = $this->notificationsApi->getOutgoingByIds(
            $incomingNotification->type,
            $incomingNotification->user_id,
            $incomingNotification->target_user_id,
            $incomingNotification->card_id
        );

        $user = $this->user->find($share->user_id);
        $targetUser = $this->user->find($share->target_user_id);
        $userTransaction = $this->walletApi->getTransactionById($share->user_transaction_id);
        $targetUserTransaction = $this->walletApi->getTransactionById($share->target_user_transaction_id);

        $this->walletApi->updateTransaction($userTransaction->id, 1,
            'received', 'default', $share->amount, $share->user_id);

        $this->walletApi->deleteTransaction($userTransaction->id);
        $this->walletApi->deleteTransaction($targetUserTransaction->id);
        $incomingNotification->delete();
        $outgoingNotification->delete();

        $offers = $this->incoming
            ->where('target_user_id', '=', $targetUser->id)
            ->whereIn('type',['productShare', 'projectShare', 'userInvite', 'userProjectInvite'])
            ->count();
        $outgoingOffers = $this->outgoing
            ->where('user_id', '=', $user->id)
            ->whereIn('type', ['productShare', 'projectShare', 'userInvite', 'userProjectInvite'])
            ->count();

        event(new IncomingShareChanged($targetUser, $offers));
        event(new OutgoingShareChanged($user, $outgoingOffers));
        event(new IncomingNotificationDeleted($incomingNotification));
        event(new OutgoingNotificationDeleted($outgoingNotification));
        event(new UserBalanceUpdated($this->walletApi->rebuildUserTotalBalance($user->id)['userBalance']));
        event(new ProductRejected($share));

        $share->delete();

        return $incomingNotification;
    }

    public function rejectProjectShare(int $notificationId, int $shareId) {
        $share = $this->projectShare->find($shareId);
        $incomingNotification = $this->notificationsApi->getIncoming($notificationId);
        $outgoingNotification = $this->notificationsApi->getOutgoingByIds(
            $incomingNotification->type,
            $incomingNotification->user_id,
            $incomingNotification->target_user_id,
            $incomingNotification->card_id
        );

        $user = $this->user->find($share->user_id);
        $targetUser = $this->user->find($share->target_user_id);
        $userTransaction = $this->walletApi->getTransactionById($share->user_transaction_id);
        $targetUserTransaction = $this->walletApi->getTransactionById($share->target_user_transaction_id);

        $this->walletApi->updateTransaction($userTransaction->id, 1,
            'received', 'default', $share->amount, $share->user_id);

        $this->walletApi->deleteTransaction($userTransaction->id);
        $this->walletApi->deleteTransaction($targetUserTransaction->id);
        $incomingNotification->delete();
        $outgoingNotification->delete();

        $offers = $this->incoming
            ->where('target_user_id', '=', $targetUser->id)
            ->whereIn('type', ['productShare', 'projectShare', 'userInvite', 'userProjectInvite'])
            ->count();
        $outgoingOffers = $this->outgoing
            ->where('user_id', '=', $user->id)
            ->whereIn('type', ['productShare', 'projectShare', 'userInvite', 'userProjectInvite'])
            ->count();

        event(new IncomingShareChanged($targetUser, $offers));
        event(new OutgoingShareChanged($user, $outgoingOffers));
        event(new IncomingNotificationDeleted($incomingNotification));
        event(new OutgoingNotificationDeleted($outgoingNotification));
        event(new UserBalanceUpdated($this->walletApi->rebuildUserTotalBalance($user->id)['userBalance']));
        event(new ProjectRejected($share));

        $share->delete();

        return $incomingNotification;
    }
}