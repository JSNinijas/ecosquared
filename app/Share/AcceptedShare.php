<?php

namespace App\Share;

use Illuminate\Database\Eloquent\Model;

class AcceptedShare extends Model
{
    protected $table = 'accepted_product_shares';

    protected $fillable = [
        'user_id',
        'target_user_id',
        'product_id',
        'amount',
        'message'
    ];
}
