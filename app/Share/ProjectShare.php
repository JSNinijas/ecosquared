<?php

namespace App\Share;

use Illuminate\Database\Eloquent\Model;

class ProjectShare extends Model
{
    protected $table = 'pending_projects';

    protected $fillable = [
        'user_id',
        'target_user_id',
        'project_id',
        'user_transaction_id',
        'target_user_transaction_id',
        'amount',
        'message'
    ];
}
