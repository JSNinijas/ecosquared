<?php

namespace App\Chatwee\ChatweeSDK\Chatwee;

use Exception;

class ChatweeSsoManager {

	public static function registerUser($parameters) {
    	if(isSet($parameters["login"]) === false) {
    		throw new Exception("login parameter is required");
    	}

		$userId = ChatweeSsoUser::register(Array(
			"login" => $parameters["login"],
			"isAdmin" => isSet($parameters["isAdmin"]) === true ? $parameters["isAdmin"] : false,
			"avatar" => isSet($parameters["avatar"]) === true ? $parameters["avatar"] : ""
		));

		return $userId;
	}

    public static function loginUser($parameters) {
		if(isSet($parameters["userId"]) === false) {
			throw new Exception("userId parameter is required");
		}
    	if(self::isLogged() === true) {
    		self::logoutUser();
    	}

		$sessionId = ChatweeSsoUser::login(Array(
			"userId" => $parameters["userId"]
		));

		ChatweeSession::setSessionId($sessionId);

		return $sessionId;
    }

	public static function logoutUser() {
		if(self::isLogged() === false) {
			return false;
		}
		$sessionId = ChatweeSession::getSessionId();

		ChatweeSsoUser::removeSession(Array(
			"sessionId" => $sessionId
		));

		ChatweeSession::clearSessionId();
	}

    public static function editUser($parameters) {
    	if(isSet($parameters["login"]) === false) {
    		throw new Exception("login parameter is required");
    	}
		if(isSet($parameters["userId"]) === false) {
			throw new Exception("userId parameter is required");
		}

		$editParameters = Array(
			"userId" => $parameters["userId"],
			"login" => $parameters["login"],
			"avatar" => isSet($parameters["avatar"]) === true ? $parameters["avatar"] : ""
		);

		if(isSet($parameters["isAdmin"]) === true) {
			$editParameters["isAdmin"] = $parameters["isAdmin"];
		}

		ChatweeSsoUser::edit($editParameters);
    }

    private static function isLogged() {
    	return ChatweeSession::isSessionSet();
    }
}
