<?php

namespace App\Chatwee;

use App\User;
use Exception;
use App\Chatwee\ChatweeSDK\Chatwee\ChatweeConfiguration;
use App\Chatwee\ChatweeSDK\Chatwee\ChatweeSsoManager;

class Chatwee
{
    public function registerUser(User $user)
    {
        ChatweeConfiguration::setChatId(config('chatwee.account.chatId'));
        ChatweeConfiguration::setClientKey(config('chatwee.account.clientApiKey'));


        $user->roles->where('name', '=', 'admin')->first() ? $isAdmin = true : $isAdmin = false;

        $userId = ChatweeSsoManager::registerUser(Array(
            "login" => $user->name,
            "isAdmin" => $isAdmin,
            "avatar" => "storage/avatars/" . $user->id
        ));

        return $userId;
    }

    public function loginUser(User $user) {
        ChatweeConfiguration::setChatId(config('chatwee.account.chatId'));
        ChatweeConfiguration::setClientKey(config('chatwee.account.clientApiKey'));

        $sessionId = ChatweeSsoManager::loginUser(Array(
            "userId" => $user->chatwee_id
        ));

        dd($sessionId);
    }

    public function logoutUser() {
        ChatweeConfiguration::setChatId(config('chatwee.account.chatId'));
        ChatweeConfiguration::setClientKey(config('chatwee.account.clientApiKey'));

        ChatweeSsoManager::logoutUser();
    }

    public function editUser($user)
    {
        ChatweeConfiguration::setChatId(config('chatwee.account.chatId'));
        ChatweeConfiguration::setClientKey(config('chatwee.account.clientApiKey'));

        $user->roles->where('name', '=', 'admin')->first() ? $isAdmin = true : $isAdmin = false;

        ChatweeSsoManager::editUser(Array(
            "userId" => $user->chatwee_id,
            "login" => $user->name,
            "isAdmin" => $isAdmin,
            "avatar" => "storage/avatars/" . $user->id
        ));
    }
}
