<?php

namespace App\Settings;

use App\Notification\Incoming;
use App\Events\FetchNewIncoming;
use App\User;

class SettingsApi
{
    protected $settings;
    protected $projectSettings;
    protected $incoming;
    protected $user;

    public function __construct(Settings $settings, Incoming $incoming, ProjectSettings $projectSettings, User $user)
    {
        $this->settings = $settings;
        $this->incoming = $incoming;
        $this->projectSettings = $projectSettings;
        $this->user = $user;
    }

    public function getNotificationsSettings(int $userId) {
        $settings = $this->settings->where('user_id', '=', $userId)->first();

        if(!$settings) {
            $settings = $this->settings->create(['user_id' => $userId]);
            $settings = $this->settings->find($settings->id);
        }

        return array('status' => true, 'settings' => $settings);
    }

    /**
     * @param $userId
     * @param $shares
     * @param $push
     * @param $activity
     * @param $archive
     * @param $thanks
     * @param $thanksRemove
     * @param $productLikes
     * @param $productLikesRemove
     * @param $projectHappy
     * @param $projectHappyRemove
     * @param $productSupport
     * @param $productSupportRemove
     * @param $projectSupport
     * @param $projectSupportRemove
     * @return array
     */
    public function setNotificationsSettings($userId, $shares, $push, $activity, $archive,
                                             $thanks, $thanksRemove, $productLikes, $productLikesRemove,
                                             $projectHappy, $projectHappyRemove, $productSupport,
                                             $productSupportRemove, $projectSupport, $projectSupportRemove) {

        $settings = $this->settings->where('user_id', '=', $userId)->first();
        if (!$settings) {
            $settings = new $this->settings;
            $settings->user_id = $userId;
        }
        $settings->shares = $shares;
        $settings->shares = $shares;
        $settings->push = $push;
        $settings->activity = $activity;
        $settings->archive = $archive;
        $settings->thanks = $thanks;
        $settings->thanks_remove = $thanksRemove;
        $settings->product_likes = $productLikes;
        $settings->product_likes_remove = $productLikesRemove;
        $settings->project_happy = $projectHappy;
        $settings->project_happy_remove = $projectHappyRemove;
        $settings->product_support = $productSupport;
        $settings->product_support_remove = $productSupportRemove;
        $settings->project_support = $projectSupport;
        $settings->project_support_remove = $projectSupportRemove;

        $settings->save();

//        $notifications = $this->incoming->all();
        $notifications = $this->incoming->where('target_user_id', '=', $userId)->get();

        $notifications->map(function (Incoming $notification) use ($settings) {
            switch ($notification->type) {
                case 'thank':
                    $notification->on_off = $settings->thanks;
                    $notification->remove_on_read = $settings->thanks_remove;
                    break;
                case 'productLike':
                    $notification->on_off = $settings->product_likes;
                    $notification->remove_on_read = $settings->product_likes_remove;
                    break;
                case 'projectHappy':
                    $notification->on_off = $settings->project_happy;
                    $notification->remove_on_read = $settings->project_happy_remove;
                    break;
                case 'productSupported':
                    $notification->on_off = $settings->product_support;
                    $notification->remove_on_read = $settings->product_support_remove;
                    break;
                case 'projectSupported':
                    $notification->on_off = $settings->project_support;
                    $notification->remove_on_read = $settings->project_support_remove;
                    break;
                case 'projectThank':
                    $notification->on_off = $settings->thanks;
                    $notification->remove_on_read = $settings->thanks_remove;
                    break;
            }

            $notification->save();
        });
        $count =$this->incoming->where('target_user_id', '=', $userId)
            ->where('seen', '=', false)->count();
        $unseenSet= $this->incoming->where('target_user_id', '=', $userId)
            ->where('on_off', '=', 'off')->get();
        if($unseenSet->isNotEmpty() || $count<=0 ){
            $unseen = false;
            $count=0;
        }else{
            $unseen=true;
        }
        $user = $this->user->where('id', '=', $userId)->first();
        event(new FetchNewIncoming($user, array('count' => 0, 'unseen' => $unseen)));

        return array('status' => true, 'settings' => $settings);
    }

    public function getProjectSettings(int $projectId) {
        $settings = $this->projectSettings->where('project_id', '=', $projectId)->first();

        if (!$settings) {
            $settings = $this->projectSettings->create(['project_id' => $projectId]);
            $settings = $this->projectSettings->find($settings->id);
        }

        return array('status' => true, 'settings' => $settings);
    }

    public function setProjectSettings($projectId,
                                       $basicBalance,
                                       $ownershipType,
                                       $sqType,
                                       $ownershipSqBalance,
                                       $creditTargetType,
                                       $usershipType,
                                       $creditTargetBalance,
                                       $voteSq,
                                       $numberPercent,
                                       $ownershipUsershipBalance,
                                       $editorGovernance) {
        $settings = $this->projectSettings->where('project_id', '=', $projectId)->first();
        if(!$projectId) {
            return array('status' => false, 'error' => 'Use a valid project id!');
        }

        if (!$settings) {
            $settings = new $this->projectSettings;
            $settings->project_id = $projectId;
        }

        $settings->basic_balance = $basicBalance;
        $settings->ownership_type = $ownershipType;
        $settings->sq_type = $sqType;
        $settings->ownership_sq_balance = $ownershipSqBalance;
        $settings->credit_target_type = $creditTargetType;
        $settings->usership_type = $usershipType;
        $settings->credit_target_balance = $creditTargetBalance;
        $settings->vote_sq = $voteSq;
        $settings->number_percent = $numberPercent;
        $settings->ownership_usership_balance = $ownershipUsershipBalance;
        $settings->editor_governance = $editorGovernance;

        $settings->save();

        return array('status' => true, 'settings' => $settings);
    }
}