<?php

namespace App\Settings;

use Illuminate\Database\Eloquent\Model;

class ProjectSettings extends Model
{
    protected $table = 'project_settings';

    protected $fillable = [
        'project_id',
        'basic_balance',
        'ownership_type',
        'sq_type',
        'ownership_sq_balance',
        'credit_target_type',
        'usership_type',
        'credit_target_balance',
        'vote_sq',
        'number_percent',
        'ownership_usership_balance',
        'editor_governance'
    ];
}