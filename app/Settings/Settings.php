<?php

namespace App\Settings;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $table = 'emails';

    protected $fillable = [
        'user_id',
        'shares',
        'push',
        'activity',
        'archive',
        'thanks',
        'thanks_remove',
        'product_likes',
        'product_likes_remove',
        'project_happy',
        'project_happy_remove',
        'product_support',
        'product_support_remove',
        'project_support',
        'project_support_remove'
    ];
}
