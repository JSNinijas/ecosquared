<?php

namespace App\Console\Commands;

use App\Product\Product;
use App\Share\AcceptedShare;
use App\User;
use Illuminate\Console\Command;
use Carbon\Carbon;

class FillPivotTableToSortProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'activity:products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Filling pivot "product_activity" table for chronological sorting';

    protected $user;
    protected $product;
    protected $acceptedShare;

    /**
     * FillPivotTableToSortProducts constructor.
     * @param User $user
     * @param Product $product
     * @param AcceptedShare $acceptedShare
     */
    public function __construct(User $user, Product $product, AcceptedShare $acceptedShare)
    {
        parent::__construct();
        $this->user = $user;
        $this->product = $product;
        $this->acceptedShare = $acceptedShare;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = $this->user->all();

        $defaultUser = $this->user->first();

        $users->map(function ($user) use ($defaultUser){
            dump($user->id, '-----------------------------------------');
            $user->products()->get()->map(function ($product) use ($user, $defaultUser) {
                dump($product->id);
                $product->activity()->attach($user->id, ['activity' => Carbon::now(), 'target_user_id' => $defaultUser->id]);
            });
            $shares = $this->acceptedShare->where('target_user_id', '=', $user->id)->get();
            $shares->map(function ($share) use ($user, $defaultUser) {
                $product = $this->product->find($share->product_id);
                dump($product->id);
                $product->activity()
                    ->attach($user->id, ['activity' => Carbon::now(), 'target_user_id' => $defaultUser->id]);
            });
            dump('-----------------------------------------');
        });

        $this->info('Database is successfully updated.');
    }
}
