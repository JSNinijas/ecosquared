<?php

namespace App\Console\Commands;

use App\Settings\Settings;
use App\User;
use Illuminate\Console\Command;

class FillPivotTableToSort extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'activity:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Filling pivot "user_activity" table for chronological sorting';

    /**
     * @var
     */
    protected $settings;
    protected $user;

    /**
     * FillPivotTableToSort constructor.
     * @param User $user
     */
    public function __construct(Settings $settings, User $user)
    {
        parent::__construct();
        $this->settings = $settings;
        $this->user = $user;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = $this->user->all();

        $users->map(function ($user) {
            dump($user->id, '-----------------------------------------');
            $user->users()->get()->map(function ($targetUser) use ($user) {
                dump($targetUser->id);
                $targetUser->activity()->attach($user->id, ['activity' => \Carbon\Carbon::now()]);
            });
            dump('---------------------------------');
        });

        $this->info('Database is successfully updated.');
    }
}
