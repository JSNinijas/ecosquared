<?php

namespace App\Console\Commands;

use App\Notification\EmailNotificationsApi;
use Illuminate\Console\Command;

class EmailDistribution extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:statistics';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Distribution of email notifications.';

    protected $emailNotificationsApi;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(EmailNotificationsApi $emailNotificationsApi)
    {
        parent::__construct();
        $this->emailNotificationsApi = $emailNotificationsApi;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->emailNotificationsApi->sendStatistics();
    }
}
