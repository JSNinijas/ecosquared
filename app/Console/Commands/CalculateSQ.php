<?php

namespace App\Console\Commands;

use App\Services\SQ;
use Illuminate\Console\Command;

class CalculateSQ extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'calc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to calculate global SQ and store it to database';


    protected $sq;


    /**
     * Create a new command instance.
     * SQ $sq
     * @return void
     */
    public function __construct(SQ $sq)
    {
        parent::__construct();
        $this->sq = $sq;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->sq->updateUsersTable($this->sq->calculateGlobalSQ());
    }
}
