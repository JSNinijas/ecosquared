<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Wallet\AccountModel;

class UpdateCreateFeatureFillOnUserTabl extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:create_feature_fill';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update fill "create_feature" in user table';

    protected $user;
    protected $accountModel;

    /**
     * UpdateCreateFeatureFillOnUserTabl constructor.
     * @param User $user
     * @param AccountModel $accountModel
     */
    public function __construct(User $user, AccountModel $accountModel)
    {
        parent::__construct();
        $this->user = $user;
        $this->accountModel = $accountModel;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = $this->user->all();

        $users->map(function ($user){
            $sum = $this->accountModel->where('user_id', $user->id)
                ->where('transaction_type', 'bought')->sum('amount');

            if($sum >= 1000) {
                $user->create_feature = true;
                $user->save();
            }

        });

        $this->info('Database is successfully updated.');
    }
}
