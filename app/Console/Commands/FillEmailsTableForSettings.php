<?php

namespace App\Console\Commands;

use App\Settings\Settings;
use App\User;
use Illuminate\Console\Command;

class FillEmailsTableForSettings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fill:settings';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fill emails table for existing users';

    protected $settings;
    protected $user;

    /**
     * FillEmailsTableForSettings constructor.
     * @param Settings $settings
     * @param User $user
     */
    public function __construct(Settings $settings, User $user)
    {
        parent::__construct();
        $this->settings = $settings;
        $this->user = $user;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = $this->user->where('name', '!=', 'unknown')->get();
        $users->map(function ($user) {
            $this->settings->create(['user_id' => $user->id]);
        });

        $this->info('Database is successfully updated.');
    }
}
