<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restriction extends Model
{
    protected $table = 'global_restrictions';

    protected $fillable = [
        'share',
        'support'
    ];
}
