<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ShareMessage extends Mailable
{
    use Queueable, SerializesModels;

    public $userName, $userSurname, $targetUserName, $targetUserSurname;
    public $shareMessage, $productTitle, $amount, $author, $url;

    /**
     * ShareMessage constructor.
     * @param $userName
     * @param $userSurname
     * @param $targetUserName
     * @param $targetUserSurname
     * @param $shareMessage
     * @param $productTitle
     * @param $amount
     * @param $author
     * @param $url
     */
    public function __construct($userName, $userSurname, $targetUserName,
                                $targetUserSurname, $shareMessage, $productTitle, $amount, $author, $url)
    {
        $this->userName = $userName;
        $this->userSurname = $userSurname;
        $this->targetUserName = $targetUserName;
        $this->targetUserSurname = $targetUserSurname;
        $this->shareMessage = $shareMessage;
        $this->productTitle = $productTitle;
        $this->amount = $amount;
        $this->author = $author;
        $this->url = $url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('share@ecosquared.co.uk', 'Ecosquared')
            ->subject('Ecosquared Share')->view('emails.share');
    }
}
