<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RecoveryMessage extends Mailable
{
    use Queueable, SerializesModels;

    public $userName, $userSurname, $url;

    /**
     * RecoveryMessage constructor.
     * @param $userName
     * @param $userSurname
     * @param $url
     */
    public function __construct($userName, $userSurname, $url)
    {
        $this->userName = $userName;
        $this->userSurname = $userSurname;
        $this->url = $url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('recovery@ecosquared.co.uk', 'Ecosquared')
            ->subject('Password Recovery')
            ->view('emails.recovery');
    }
}
