<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RequestInviteMessage extends Mailable
{
    use Queueable, SerializesModels;

    public $email, $userMessage;

    /**
     * RequestInviteMessage constructor.
     * @param $email
     * @param $userMessage
     */
    public function __construct($email, $userMessage)
    {
        $this->email = $email;
        $this->userMessage = $userMessage;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.request');
    }
}
