<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InviteMessage extends Mailable
{
    use Queueable, SerializesModels;

    public $userName, $userSurname, $targetUserName, $targetUserSurname;
    public $inviteMessage, $productTitle, $amount, $url, $scheme, $author, $urlUnsubscribe;

    /**
     * InviteMessage constructor.
     * @param $userName
     * @param $userSurname
     * @param $targetUserName
     * @param $targetUserSurname
     * @param $inviteMessage
     * @param $productTitle
     * @param $amount
     * @param $url
     * @param $scheme
     * @param $author
     * @param $urlUnsubscribe
     */
    public function __construct($userName, $userSurname, $targetUserName,
                                $targetUserSurname, $inviteMessage, $productTitle, $amount, $url, $scheme, $author,$urlUnsubscribe)
    {
        $this->userName = $userName;
        $this->userSurname = $userSurname;
        $this->targetUserName = $targetUserName;
        $this->targetUserSurname = $targetUserSurname;
        $this->inviteMessage = $inviteMessage;
        $this->productTitle = $productTitle;
        $this->amount = $amount;
        $this->url = $url;
        $this->scheme = $scheme;
        $this->author = $author;
        $this->urlUnsubscribe = $urlUnsubscribe;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('invitation@ecosquared.co.uk', 'Ecosquared')
            ->subject('Ecosquared Invitation')->view('emails.invite');
    }
}
