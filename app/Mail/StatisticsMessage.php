<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class StatisticsMessage extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $statisticsSetting;

    /**
     * StatisticsMessage constructor.
     * @param User $user
     * @param string $statisticsSetting
     */
    public function __construct(User $user, string $statisticsSetting)
    {
        $this->user = $user;
        $this->statisticsSetting = $statisticsSetting;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('statistics@ecosquared.co.uk', 'Ecosquared')
            ->subject('Ecosquared Summary')->view('emails.statistics');
    }
}
