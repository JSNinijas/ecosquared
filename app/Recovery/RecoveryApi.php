<?php

namespace App\Recovery;



use App\Mail\RecoveryMessage;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;

class RecoveryApi
{
    protected $user;

    /**
     * RecoveryApi constructor.
     * @param User $user
     */
    function __construct(User $user)
    {
        $this->user = $user;
    }

    public function sendRecoveryMessage($email)
    {
        $user = $this->user->where('email', '=', $email)->first();
        if(!$user) {
            return array('status' => false, 'error' => 'Use valid email address!');
        }

        $token = md5($email);
        $url = URL::to('/') . '/#/recovery/' . $token;
        DB::table('password_resets')->insert(['email' => $email, 'token' => $token]);

        Mail::to($email)->send(new RecoveryMessage($user->name, $user->surname, $url));

        return array('status' => true, 'user' => $user);
    }

    public function checkRecoveryUser($hash) {
        $passwordReset = DB::table('password_resets')->where('token', '=', $hash )->first();

        if($passwordReset) {
            $user = $this->user->where('email', '=', $passwordReset->email)->first();
            return array('status' => true, 'user' => $user);
        } else {
            return array('status' => false, 'error' => 'You have used invalid link!');
        }
    }

    public function setPassword($email, $password) {

        $user = $this->user->where('email', '=', $email)->first();
        $user->password = bcrypt($password);
        $user->save();

        DB::table('password_resets')->where('email', '=', $email)->delete();

        return array('status' => true, 'user' => $user);
    }
}