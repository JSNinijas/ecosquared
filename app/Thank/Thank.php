<?php

namespace App\Thank;

use Illuminate\Database\Eloquent\Model;

class Thank extends Model
{
    protected $fillable = [
        'user_id',
        'target_user_id',
        'value',
        'message'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function targetUser() {
        return $this->belongsTo('App\User');
    }
}