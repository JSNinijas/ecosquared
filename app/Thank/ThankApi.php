<?php

namespace App\Thank;

use App\Events\TargetUserThankUpdated;
use App\Events\UserProjectThanked;
use App\Events\UserThanked;
use App\Project\Project;
use App\User;

class ThankApi
{

    /**
     * @var Thank
     * @var User
     */
    protected $thank;
    protected $user;
    protected $project;
    protected $projectThank;

    /**
     * ThankApi constructor.
     * @param Thank $thank
     * @param User $user
     * @param Project $project
     * @param ProjectThank $projectThank
     */
    public function __construct(Thank $thank, User $user, Project $project, ProjectThank $projectThank) {
        $this->thank = $thank;
        $this->user = $user;
        $this->project = $project;
        $this->projectThank = $projectThank;
    }

    /**
     * This method is used to create thank from one user to another
     * @param int $userId is id of user who thanks
     * @param int $targetUserId is id of user who receive thank
     * @param $value int is a thank value
     * @return array
     */
    public function createThank(int $userId, int $targetUserId, $value, $message = null)
    {
        if(!($this->user->find($userId) && $this->user->find($targetUserId))) {
            return array('status' => false, 'error' => 'Use a valid user id!');
        }

        if($userId == $targetUserId) {
            return array ('status' => false, 'error' => 'User can not thank himself!');
        }

        $thank =$this->thank->create([
            'user_id' => $userId,
            'target_user_id' => $targetUserId,
            'value' => $value,
            'message' => $message
        ]);

        $user = $this->user->find($userId);
        $targetUser = $this->user->find($targetUserId);

        $isExistRelations = $user->users()
            ->where(['target_user_id' => $targetUser->id, 'user_id'=> $user->id])->first();

        if(!$isExistRelations){
            $user->users()->attach($targetUser->id);
            $targetUser->users()->attach($user->id);
        }

        $targetUser->thanks = $user->getTotalThanksToUser($targetUser);
        $targetUser->update(['thanks' => $targetUser->thanks]);

        event(new UserThanked($thank));
        event(new TargetUserThankUpdated($user, $targetUser));

        $thank = $this->thank->orderBy('id', 'desc')->first();
        return array('status' => true, 'thank' => $thank);
    }

    public function createProjectThank(int $userId, int $targetUserId, $projectId, $value, $message = null)
    {
        if(!($this->user->find($userId) && $this->user->find($targetUserId))) {
            return array('status' => false, 'error' => 'Use a valid user id!');
        }

        if($userId == $targetUserId) {
            return array ('status' => false, 'error' => 'User can not thank himself!');
        }

        if(!$this->project->find($projectId)) {
            return array('status' => false, 'error' => 'Use a valid project id!');
        }

        $thank =$this->projectThank->create([
            'user_id' => $userId,
            'target_user_id' => $targetUserId,
            'project_id' => $projectId,
            'value' => $value,
            'message' => $message
        ])->load('project');

        $user = $this->user->find($userId);
        $targetUser = $this->user->find($targetUserId);
        $project = $this->project->find($projectId);

        $isExistRelations = $user->users()
            ->where(['target_user_id' => $targetUser->id, 'user_id'=> $user->id])->first();

        if(!$isExistRelations){
            $user->users()->attach($targetUser->id);
            $targetUser->users()->attach($user->id);
        }

        $targetUser->thanks = $user->getTotalThanksToUser($targetUser);
        $targetUser->project_thanks = $user->getTotalProjectThanksToUser($targetUser, $project);
        $targetUser->update(['project_thanks' => $targetUser->projectThanks, 'thanks' => $targetUser->thanks]);

        event(new UserProjectThanked($thank));
        event(new TargetUserThankUpdated($user, $targetUser));

        $thank = $this->projectThank->orderBy('id', 'desc')->first();
        return array('status' => true, 'thank' => $thank);
    }

    public function updateThank(int $id, int $userId, int $targetUserId, $value, $message = null) {

        if($userId == $targetUserId) {
            return array ('status' => false, 'error' => 'User can not thank himself!');
        }

        $thank = $this->thank->find($id);

        !$userId ? : $thank->user_id = $userId;
        !$targetUserId ? : $thank->target_user_id = $targetUserId;
        !$value ? : $thank->value = $value;
        $thank->message = $message;

        $thank->save();

        return array('status' => true, 'thank' => $thank);
    }

    /**
     * This method is used to return thank by thank id
     * @param int $thankId is id of thank in database table
     * @return array
     */
    public function getThankById(int $thankId)
    {
        if(!$this->thank->find($thankId)) {
            return array ('status' => false, 'error' => 'Use a valid thankId!');
        }

        $thank = $this->thank->find($thankId);

        return array('status' => true, 'thank' => $thank);
    }

    public function getProjectThankById(int $thankId)
    {
        if(!$this->projectThank->find($thankId)) {
            return array ('status' => false, 'error' => 'Use a valid thankId!');
        }

        $thank = $this->projectThank->find($thankId);

        return array('status' => true, 'thank' => $thank);
    }

    /**
     * This method is used to return all thanks one user gave an received
     * @param int $userId is id of user who thanks
     * @return array
     */
    public function getThanks(int $userId)
    {
        if(!$this->user->find($userId)) {
            return array('status' => false, 'error' => 'Use a valid userId!');
        }

        $thanksFrom = $this->thank->with('targetUser')->where('user_id', '=', $userId)
            ->get();

        $thanksTo = $this->thank->with('user')->where('target_user_id', '=', $userId)
            ->get();

        return array('status' => true, 'thanksFrom' => $thanksFrom, 'thanksTo' => $thanksTo);
    }

    public function getProjectThanks(int $userId)
    {
        if(!$this->user->find($userId)) {
            return array('status' => false, 'error' => 'Use a valid userId!');
        }

        $thanksFrom = $this->projectThank->with('targetUser')->where('user_id', '=', $userId)
            ->get();

        $thanksTo = $this->projectThank->with('user')->where('target_user_id', '=', $userId)
            ->get();

        return array('status' => true, 'thanksFrom' => $thanksFrom, 'thanksTo' => $thanksTo);
    }

    /**
     * @param int $thankId
     * @return array
     * @throws \Exception
     */
    public function deleteThank(int $thankId)
    {
        $thank = $this->thank->find($thankId);

        if(!$thank) {
            return array('status' => false, 'error' => 'Use a valid thank id!');
        }

        $thank->delete();

        return array('status' => true);
    }

    public function showProjectThanks(int $userId, int $projectId) {

        $user = $this->user->find($userId);
        $project = $this->project->find($projectId);
        $targetUser = $this->user->find($project->user_id);

        $thanks = $user->getTotalProjectThanksToUser($targetUser, $project);

        return array('status' => true, 'thanks' => $thanks);
    }
}