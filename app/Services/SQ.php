<?php

namespace App\Services;

use App\Project\Project;
use App\Thank\Thank;
use App\User;
use App\Thank\ProjectThank;
use Illuminate\Support\Facades\DB;

class SQ
{

    protected $thank;
    protected $user;

    public function __construct(Thank $thank, User $user, ProjectThank $projectThank)
    {
        $this->thank = $thank;
        $this->user = $user;
        $this->projectThank = $projectThank;
    }

    public function calculateGlobalSQ()
    {
        // Temporary fake global sq = 1.0

        $users = $this->user->all();
        $iteration = [];

        foreach($users as $user) {
            if($user->name != 'unknown') {
                $iteration[$user->id] = 1.0;
            }
        }

        return $iteration;

        // End of temporary block

        $precision = 0.00001;
        $dampingFactor = 0.85;
        $users = $this->user->setQuery(
            DB::table('users'));
        $thanks = $this->thank->all();
        $activeUsers = 0;


        $notCalculatedUsers  = clone($users);

        $usersQuery = $users
            ->whereIn('users.id', function ($query) {
                $query
                    ->select('users.id')
                    ->from('users')
                    ->join('project_thanks', 'users.id', '=', 'project_thanks.user_id');
//                    ->join('thanks', 'users.id', '=', 'thanks.user_id');
            });

        dd($usersQuery->get()->toArray());

        $notCalculatedUsersQuery = $notCalculatedUsers
            ->whereNotIn('users.id', function ($query) use ($projectId) {
                $query
                    ->select('users.id')
                    ->from('users')
                    ->join('project_thanks', 'users.id', '=', 'project_thanks.user_id')
                    ->where('project_thanks.project_id', '=', $projectId);
            });


        $usersIds = $usersQuery->pluck('users.id');
        $notCalculatedUsers = $notCalculatedUsersQuery->get();
        $users = $usersQuery->get();

        $notCalculatedUsers->map(function ($user) {
            $user->sq = 0.0;
            $user->save();
        });

//        dd($usersQuery->get()->toArray(), '--------------------------------', $notCalculatedUsersQuery->get()->toArray());

//        ---Special condition for  thanks to stripped-out users in sum of user
//          thanks (We don't consider thanks given to them---)

        $thanks = $this->projectThank
            ->where('project_id', '=', $projectId)
            ->whereIn('target_user_id', $usersIds)
            ->get();

        $precision = 0.00001;
        $dampingFactor = 0.85;
        $users = $this->user->all();
        $thanks = $this->thank->all();
        $activeUsers = 0;

        foreach ($users as $user) {
            $iteration[$user->id] = 1;

            if (($thanks->where('user_id', '=', $user->id)->sum('value')==0)) {
                continue;
            }

            $activeUsers++;
        }

        $i = 0;

        $precisionFlag = false;

        while (!$precisionFlag) {

            $previousIteration = $iteration;
            $i++;

            foreach ($users as $user) {
                if (($thanks->where('target_user_id', '=', $user->id)->sum('value')==0 &&
                    $thanks->where('user_id', '=', $user->id)->sum('value')==0)) {
                    continue;
                }

                $innerSum = 0;
                foreach ($users as $innerUser) {
                    $thanksOneToAnother = $thanks->where('target_user_id', '=', $user->id)
                        ->where('user_id', '=', $innerUser->id)->sum('value');

//                    dump($thanksOneToAnother);
//                    dd($user->id, $innerUser->id, $thanksOneToAnother);

                    if($user->id == $innerUser->id) {
                        continue;
                    }

//                    dd($user->id, $innerUser->id, $thanksOneToAnother, "kdfgjkfjgfkij");

                    $totalUserThanks = $thanks->where('user_id', '=', $innerUser->id)->sum('value');

//                    dd($totalUserThanks);
                    if($totalUserThanks == 0) {
//                        dd($innerUser);
//                        $innerSum = 5;
//                        foreach($iteration as $key => $userValue) {
//                            if($key == $innerUser->id) {
//                                continue;
//                            }
//                            $userValue += 1/($activeUsers-1);
//                            dump($activeUsers);
//                        }
//                        continue;
                        //$innerSum += $previousIteration[$innerUser->id];
//                        if($user->id>1) dd($user->id, $innerUser->id, $innerSum);
                    } else {
                        $innerSum += $thanksOneToAnother / $totalUserThanks * $previousIteration[$innerUser->id];
//                        dump($innerSum);
                    }

                }
                dump($innerSum);
                $iteration[$user->id] = /*(1 - $dampingFactor) + $dampingFactor * */ $innerSum;
            }

//            dump($iteration);
            $precisionFlag = true;
            foreach($iteration as $key => $sq) {
                if(abs($sq - $previousIteration[$key]) > $precision) {
                    $precisionFlag = false;
                    break;
                }
            }
        }

//        dd($i, $iteration, array_sum($iteration));
        $activeUsers = 0;
        foreach($iteration as $key => $value) {
            if ($thanks->where('user_id', '=', $key)->sum('value') !== 0) {
                $activeUsers++;
            }
        }

        foreach($iteration as $key => $userValue) {
            if (($thanks->where('user_id', '=', $key)->sum('value') == 0)) {
                $iteration[$key] = 0;
                foreach($iteration as $innerKey => $innerValue) {
                    if (($thanks->where('user_id', '=', $innerKey)->sum('value') != 0)) {
                        $iteration[$innerKey] += 1 / $activeUsers;
//                        dump($iteration[$key]);
                    }
                }
            }
        }

//        dd($iteration);
        return $iteration;
    }

    public function calculateClosedExclusiveSQ(int $projectId, $users) {

//
//      ---This block initializes input data for calculation---
//

        $dampingFactor = 0.85;
        $precision = 0.00000001;

//        dd($users->get()->toArray());

        $notCalculatedUsers  = clone($users);

        $usersQuery = $users
            ->whereIn('users.id', function ($query) use ($projectId) {
                $query
                    ->select('users.id')
                    ->from('users')
                    ->join('project_thanks', 'users.id', '=', 'project_thanks.user_id')
                    ->where('project_thanks.project_id', '=', $projectId);
            });

//        dd($usersQuery->get()->toArray());

        $notCalculatedUsersQuery = $notCalculatedUsers
            ->whereNotIn('users.id', function ($query) use ($projectId) {
                $query
                    ->select('users.id')
                    ->from('users')
                    ->join('project_thanks', 'users.id', '=', 'project_thanks.user_id')
                    ->where('project_thanks.project_id', '=', $projectId);
            });


        $usersIds = $usersQuery->pluck('users.id');
        $notCalculatedUsers = $notCalculatedUsersQuery->get();
        $users = $usersQuery->get();

//        dd($notCalculatedUsers);

        $notCalculatedUsers->map(function ($user) {
            $user->sq = 0.0;
            $user->save();
        });

//        dd($usersQuery->get()->toArray(), '--------------------------------', $notCalculatedUsersQuery->get()->toArray());

//        ---Special condition for  thanks to stripped-out users in sum of user
//          thanks (We don't consider thanks given to them---)

        $thanks = $this->projectThank
            ->where('project_id', '=', $projectId)
            ->whereIn('target_user_id', $usersIds)
            ->get();

//        dd($thanks->toArray());
//        dd($users->toArray());

        $activeUsers = 0;
        $iteration = [];

        foreach ($users as $user) {
            $iteration[$user->id] = 1.0;

//            dd($iteration[$user->id]);
//            dump(($thanks->where('user_id', '=', $user->id)->first()));
            if (!$thanks->where('user_id', '=', $user->id)->first()) {
//                dump($user->id);
                continue;
            }
            $activeUsers++;
        }

        if(count($iteration) === 1) {
            return $iteration;
        }

        $firstIteration = $iteration;

        $i = 0;
        $precisionFlag = false;

        while (!$precisionFlag) {

            $previousIteration = $iteration;
            $i++;
//            dump('---'.$i.'---');

            if($i > 99) {
                $iteration = $firstIteration;
                break;
            }
//
//          ---This block of code realizes David's initial SQ calculation formula in Opened/Exclusive case---
//

            foreach ($users as $user) {
//                if (($thanks->where('target_user_id', '=', $user->id)->sum('value')==0 &&
//                    $thanks->where('user_id', '=', $user->id)->sum('value')==0)) {
//                    continue;
//                }

                $innerSum = 0;
                foreach ($users as $innerUser) {
                    $thanksOneToAnother = $thanks->where('target_user_id', '=', $user->id)
                        ->where('user_id', '=', $innerUser->id)->sum('value');

//                    dump($thanksOneToAnother);
//                    dd($user->id, $innerUser->id, $thanksOneToAnother);

                    if($user->id == $innerUser->id) {
                        continue;
                    }

//                    dd($user->id, $innerUser->id, $thanksOneToAnother, "kdfgjkfjgfkij");

                    $totalUserThanks = $thanks->where('user_id', '=', $innerUser->id)->sum('value');

//                    dd($totalUserThanks);
                    if($totalUserThanks == 0) {
//                        dd($innerUser);
//                        $innerSum = 5;
//                        foreach($iteration as $key => $userValue) {
//                            if($key == $innerUser->id) {
//                                continue;
//                            }
//                            $userValue += 1/($activeUsers-1);
//                            dump($activeUsers);
//                        }
//                        continue;
                        //$innerSum += $previousIteration[$innerUser->id];
//                        if($user->id>1) dd($user->id, $innerUser->id, $innerSum);
                    } else {
                        $innerSum += $thanksOneToAnother / $totalUserThanks * $previousIteration[$innerUser->id];
//                        dump($innerSum);
                    }
                }

//                dump($innerSum);
//
//
//
                $iteration[$user->id] = /*(1 - $dampingFactor) + $dampingFactor * */ $innerSum;
            }

//            dump('-----------------------------------------------------------------');

//
//          ---This block checks the iterative convergence---
//
//            dump($iteration);
            $precisionFlag = true;
            foreach($iteration as $key => $sq) {
                if(abs($sq - $previousIteration[$key]) > $precision) {
                    $precisionFlag = false;
                    break;
                }
            }

            if($i === 1) {
                $firstIteration = $iteration;
            }

        }

//        dd($i, $iteration, array_sum($iteration));
        $activeUsers = 0;
        foreach($iteration as $key => $value) {
            if ($thanks->where('user_id', '=', $key)->sum('value') !== 0) {
                $activeUsers++;
            }
        }

        foreach($iteration as $key => $userValue) {
            if (($thanks->where('user_id', '=', $key)->sum('value') == 0)) {
                $iteration[$key] = 0;
                foreach($iteration as $innerKey => $innerValue) {
                    if (($thanks->where('user_id', '=', $innerKey)->sum('value') != 0)) {
                        $iteration[$innerKey] += 1 / $activeUsers;
//                        dump($iteration[$key]);
                    }
                }
            }
        }

//        ---This block is used to merge not calculated users to common array with calculated ones

        foreach($notCalculatedUsers as $user) {
            $iteration[$user->id] = 0.0;
        }
        return $iteration;
    }

    public function calculateOpenedExclusiveSQ(int $projectId, $users) {

    }

    public function updateUsersTable(array $usersSQ) {

        foreach($usersSQ as $id => $sq) {
            $user = $this->user->find($id);
            $user->sq = $sq;
            $user->save();
        }

        return $usersSQ;
    }
}