<?php

namespace App\Project;

use App\Events\ProjectCreated;
use App\Events\ProjectsChanged;
use App\Happy\Happy;
use App\Restriction;
use App\Share\AcceptedProjectShare;
use App\Share\ShareApi;
use App\Support\ProjectSupport;
use App\Support\Support;
use App\Support\SupportApi;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Services\Traits\EloquentBuilderTrait;

class ProjectApi
{
    use EloquentBuilderTrait;

    /**
     * @var Project
     */
    protected $project;
    protected $acceptedProjectShare;
    protected $supportApi;
    protected $user;
    protected $shareApi;
    protected $support;
    protected $happy;
    protected $restriction;
    protected $projectSupport;

    /**
     * ProjectApi constructor.
     * @param Project $project
     * @param AcceptedProjectShare $acceptedProjectShare
     * @param SupportApi $supportApi
     * @param User $user
     * @param ShareApi $shareApi
     * @param Support $support
     * @param ProjectSupport $projectSupport
     * @param Happy $happy
     * @param Restriction $restriction
     */
    public function __construct(Project $project, AcceptedProjectShare $acceptedProjectShare, SupportApi $supportApi,
                                User $user, ShareApi $shareApi, Support $support, ProjectSupport $projectSupport,
                                Happy $happy, Restriction $restriction)
    {
        $this->project = $project;
        $this->acceptedProjectShare = $acceptedProjectShare;
        $this->supportApi = $supportApi;
        $this->user = $user;
        $this->shareApi = $shareApi;
        $this->support = $support;
        $this->projectSupport = $projectSupport;
        $this->happy = $happy;
        $this->restriction = $restriction;
    }

    /**
     * @param $userId
     * @param $resourceOptions
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public function showProjectsList($userId, $resourceOptions)
    {
        $initialLimit = (int)$resourceOptions['limit'];
        $resourceOptions['limit'] = -1;

        $ownProjectsQuery = $this->project->setQuery(DB::table('projects')
            ->where('projects.user_id', '=', $userId)
            ->join('project_activity', 'projects.id', 'project_activity.project_id')
            ->where('project_activity.user_id', '=', Auth::user()->getAuthIdentifier())
            ->select('projects.*', 'project_activity.activity')
        );

        $applyQuery = $this->applyResourceOptions($ownProjectsQuery, $resourceOptions);

        $count0 = $applyQuery['count'];
        $limitCount0 = $applyQuery['limitCount'];

        $limit = $initialLimit - $limitCount0;
        if($limit < 0) $limit = 0;
//        $resourceOptions['limit'] = $limit;

        $query = $this->project->setQuery(DB::table('projects')
            ->join('shared_projects', 'projects.id', '=', 'shared_projects.project_id')
            ->where('shared_projects.user_id', '=', $userId)
            ->join('project_activity', 'projects.id', 'project_activity.project_id')
            ->where('project_activity.user_id', '=', Auth::user()->getAuthIdentifier())
            ->select('projects.*', 'project_activity.activity'));

        $applyQuery = $this->applyResourceOptions($query, $resourceOptions);

        $resourceOptions['limit'] = $initialLimit;
        $count = $applyQuery['count'] + $count0;
        $filterCount = $initialLimit;
        $limitCount = $applyQuery['limitCount'] + $limitCount0;

        $query = ($query->union($ownProjectsQuery))->with('user');

        $resourceOptions['limit'] = $initialLimit;
        $this->applyResourceOptions($query, $resourceOptions);

        $projects = $query->get();

        return array('projects' => $projects, 'count' => $count,
            'limitCount' => $limitCount, 'filterCount' => $filterCount);
    }

    /**
     * @param $userId
     * @param $authorizedUserId
     * @param $resourceOptions
     * @return array
     */
    public function showProjectsListToShare($userId, $authorizedUserId, $resourceOptions)
    {

        $initialLimit = (int)$resourceOptions['limit'];
        $resourceOptions['limit'] = -1;

        $targetUserOwnProjectsQuery = $this->project->setQuery(DB::table('projects')
            ->select('id')
            ->where('user_id', '=', $userId));

        $userOwnProjectsQuery = $this->project->setQuery(DB::table('projects')
            ->where('user_id', '=', $authorizedUserId)
            ->whereNotIn('projects.id', function ($query) use ($userId, $targetUserOwnProjectsQuery) {
                $query
                    ->select('projects.id')
                    ->from('projects')
                    ->join('shared_projects', 'projects.id', '=', 'shared_projects.project_id')
                    ->where('shared_projects.user_id', '=', $userId)
                    ->union($targetUserOwnProjectsQuery);
            }));

        $applyQuery = $this->applyResourceOptions($userOwnProjectsQuery, $resourceOptions);

        $count0 = $applyQuery['count'];
        $limitCount0 = $applyQuery['limitCount'];

        $limit = $initialLimit - $limitCount0;
        if($limit < 0) $limit = 0;
//        $resourceOptions['limit'] = $limit;

        $query = $this->project->setQuery(
            DB::table('projects')
                ->join('shared_projects', 'projects.id', '=', 'shared_projects.project_id')
                ->where('shared_projects.user_id', '=', $authorizedUserId)
                ->select('projects.*')
                ->whereNotIn('projects.id', function ($query) use ($userId, $targetUserOwnProjectsQuery) {
                    $query
                        ->select('projects.id')
                        ->from('projects')
                        ->join('shared_projects', 'projects.id', '=', 'shared_projects.project_id')
                        ->where('shared_projects.user_id', '=', $userId)
                        ->union($targetUserOwnProjectsQuery);
                }));

        $applyQuery = $this->applyResourceOptions($query, $resourceOptions);

        $resourceOptions['limit'] = $initialLimit;
        $count = $applyQuery['count'] + $count0;
        $filterCount = $initialLimit;
        $limitCount = $applyQuery['limitCount'] + $limitCount0;

        $query = ($query->union($userOwnProjectsQuery))->with('user');

        $resourceOptions['limit'] = $initialLimit;
        $this->applyResourceOptions($query, $resourceOptions);

        $projects = $query->get();

        return array('projects' => $projects, 'count' => $count,
            'limitCount' => $limitCount, 'filterCount' => $filterCount);
    }

    /**
     * @param $userId
     * @param $resourceOptions
     * @return array
     */
    public function showProjectsListToThank($userId, $resourceOptions)
    {
        $queryForShared = $this->project->setQuery(DB::table('projects')
            ->whereIn('projects.id', function($query) use ($userId) {
                $query
                    ->select('projects.id')
                    ->from('projects')
                    ->join('shared_projects', 'projects.id', '=', 'shared_projects.project_id')
                    ->where('shared_projects.user_id', '=', Auth::user()->getAuthIdentifier());
            })
            ->whereIn('projects.id', function($query) use ($userId) {
                $query
                    ->select('projects.id')
                    ->from('projects')
                    ->join('shared_projects', 'projects.id', '=', 'shared_projects.project_id')
                    ->where('shared_projects.user_id', '=', $userId);
            })
        )->pluck('projects.id')->toArray();

        $queryForAuthCreatedUserShared = $this->project->setQuery(DB::table('projects')
            ->where('user_id', '=', Auth::user()->getAuthIdentifier())
            ->whereIn('projects.id', function($query) use ($userId) {
                $query
                    ->select('projects.id')
                    ->from('projects')
                    ->join('accepted_project_shares', 'projects.id', '=', 'accepted_project_shares.project_id')
                    ->where('accepted_project_shares.target_user_id', '=', $userId);
            })
        )->pluck('projects.id')->toArray();

        $queryForUserCreatedAuthShared = $this->project->setQuery(DB::table('projects')
            ->where('user_id', '=', $userId)
            ->whereIn('projects.id', function($query) use ($userId) {
                $query
                    ->select('projects.id')
                    ->from('projects')
                    ->join('accepted_project_shares', 'projects.id', '=', 'accepted_project_shares.project_id')
                    ->where('accepted_project_shares.target_user_id', '=', Auth::user()->getAuthIdentifier());
            })
        )->pluck('projects.id')->toArray();

        $sharedProjectIds = array_merge($queryForShared, $queryForAuthCreatedUserShared, $queryForUserCreatedAuthShared);

        $query = $this->project->setQuery(
            DB::table('projects')->whereIn('id', $sharedProjectIds)
        );

        $applyQuery = $this->applyResourceOptions($query, $resourceOptions);

        $projects = $query->get();

        $count = $applyQuery['count'];
        $filterCount = $applyQuery['filterCount'];
        $limitCount = $applyQuery['limitCount'];

        return array('projects' => $projects, 'count' => $count, 'limitCount' => $limitCount, 'filterCount' => $filterCount);
    }

    /**
     * @param $userId
     * @param $resourceOptions
     * @return array
     */
    public function showProjectsListForUsership($userId, $resourceOptions)
    {
        $initialLimit = (int)$resourceOptions['limit'];
        $resourceOptions['limit'] = -1;

        $ownProjectsQuery = $this->project->setQuery(DB::table('projects')
            ->where('projects.user_id', '=', $userId)
            ->orWhereIn('projects.id', function ($query) use ($userId){
                $query
                    ->select('projects.id')
                    ->from('projects')
                    ->join('accepted_project_shares', 'accepted_project_shares.project_id', '=', 'projects.id')
                    ->where('accepted_project_shares.target_user_id', '=', $userId);
            })
        );

        $applyQuery = $this->applyResourceOptions($ownProjectsQuery, $resourceOptions);

        $count0 = $applyQuery['count'];
        $limitCount0 = $applyQuery['limitCount'];

        $limit = $initialLimit - $limitCount0;
        if($limit < 0) $limit = 0;
//        $resourceOptions['limit'] = $limit;

        $query = $this->project->setQuery(DB::table('projects')
            ->join('shared_projects', 'projects.id', '=', 'shared_projects.project_id')
            ->where('shared_projects.user_id', '=', $userId)
            ->select('projects.*')
        );

        $applyQuery = $this->applyResourceOptions($query, $resourceOptions);

        $resourceOptions['limit'] = $initialLimit;
        $count = $applyQuery['count'] + $count0;
        $filterCount = $initialLimit;
        $limitCount = $applyQuery['limitCount'] + $limitCount0;

        $query = ($query->union($ownProjectsQuery))->with('user');

        $resourceOptions['limit'] = $initialLimit;
        $this->applyResourceOptions($query, $resourceOptions);

        $projects = $query->get();

        return array('projects' => $projects, 'count' => $count,
            'limitCount' => $limitCount, 'filterCount' => $filterCount);
    }

    public function showUserProjectList ($userId, $resourceOptions) {

    }

    /**
     * @param $userId
     * @param $title
     * @param $description
     * @param $link
     * @param $creationDate
     * @param $completionDate
     * @param $type
     * @param $start
     * @param $end
     * @param $share
     * @param $support
     * @param $sqEnabled
     * @param $file
     * @param $author
     * @param $authorStatus
     * @return array
     */
    public function createProject($userId, $title, $description, $link, $creationDate,
                                  $completionDate, $type, $start, $end, $share, $support,
                                  $sqEnabled, $file, $author, $authorStatus)
    {

        if (!($title && $link)) {
            return array('status' => false, 'error' => 'Project must have title and link');
        }
        if ($this->project->where('link', '=', $link)->first()) {
            return array('status' => false, 'error' => 'Not unique link!');
        }

        $project = $this->project;
        $user = $this->user->find($userId);

        $project->user_id = $userId;
        $project->title = $title;
        $project->description = $description;
        $project->link = $link;
        $project->value = 0;
        $project->happy = 0;
        $authorStatus ? $project->author_status = $authorStatus : $project->author_status = 'me';
        if ($creationDate) $project->creation_date = Carbon::parse($creationDate);
        if($completionDate) $project->completion_date = Carbon::parse($completionDate);
        if ($type) $project->type = $type;
        $project->start = $start;
        $project->end = $end;
        if ($share === '0' || $share === '1') $project->share = $share;
        if ($support === '0' || $support == '1') $project->support = $support;
        if ($sqEnabled) $project->sq_enabled = $sqEnabled;
        $project->chronology = Carbon::now();
        $project->counter = 0;

        if($authorStatus === 'non-user') {
            $project->author = $author;
            $project->support = false;
        }

        $project->save();

        if ($file) {
            $path = $file->storeAs('public/projects', $project->id);
            Storage::setVisibility($path, 'public');
        } else {
            $path = 'public/projects/default';
        }
        $project->image = Storage::url($path);

        $project->save();

        $project->activity()->attach($userId, ['activity' => Carbon::now(), 'target_user_id' => $userId]);

        $project = $this->project->find($project->id);

        $todayDate = Carbon::now();
        $beginDate = Carbon::parse($project->creation_date);
        $endDate = Carbon::parse($project->completion_date);

        if($todayDate->timestamp < $beginDate->timestamp) $project->date_status = 'Begins';
        if($todayDate->timestamp > $beginDate->timestamp && $todayDate->timestamp < $endDate->timestamp)
            $project->date_status = 'Till';
        if($todayDate->timestamp > $endDate->timestamp) $project->date_status = 'Ended';

        $project->save();

        event(new ProjectCreated($project->load('user')));

        return array('status' => true, 'project' => $project);
    }

    /**
     * @param $id
     * @param $userId
     * @return Project
     */
    public function showProject($id, $userId) {

        $project = $this->project->where('id', '=', $id)->with('user')->first();
        $happiesSum = $project->usersHappy->unique()->count();

        $happiesSum !== 0 ? : $happiesSum = '0';
        $incomingShare = $this->acceptedProjectShare
            ->where('target_user_id', '=', $userId)
            ->where('project_id', '=', $project->id)->first();
        $outgoingShare = $this->acceptedProjectShare
            ->where('user_id', '=', $userId)
            ->where('project_id', '=', $project->id)->first();

        $lastSupport = $this->projectSupport
            ->where('user_id', '=', $userId)
            ->where('project_id', '=', $id)
            ->orderBy('id', 'desc')->first();

        $value = $project->total;

        if($lastSupport) $project->lastSupport = $lastSupport->amount;
        $project->happied = $happiesSum;

        $project->shared = $project->usersSharedWith->count() + 1;
        // + 1 means adding author of product to show correct info when self likes enabled
        $project->supportedByUser = $this->supportApi->projectSupportedByUser($userId, $project->id);
        $project->supportedByAllUsers = $this->supportApi->projectSupportedByAllUsers($project->id);
        $project->average = $value ? round($value/$happiesSum, 2) : '0';
        if($incomingShare) {
            $project->incomingShareUser = $this->user->find($incomingShare->user_id);
            $project->incomingShareAmount = $incomingShare->amount;
            $project->outgoingShareAmount = $this->acceptedProjectShare
                ->where('user_id', '=', $userId)
                ->where('project_id', '=', $project->id)
                ->sum('amount');
        }
        if($outgoingShare) {
            $project->outgoingShareAmount = $this->acceptedProjectShare
                ->where('user_id', '=', $userId)
                ->where('project_id', '=', $project->id)
                ->sum('amount');
        }

        $project->restrictions = $this->restriction->find(1);

        $project->owners = $project->owners()->get();

        return $project;
    }

    /**
     * @param $id
     * @param $title
     * @param $description
     * @param $link
     * @param $creationDate
     * @param $completionDate
     * @param $type
     * @param $start
     * @param $end
     * @param $share
     * @param $support
     * @param $sqEnabled
     * @param $file
     * @param $author
     * @param $authorStatus
     * @return array
     */
    public function updateProject($id, $title, $description, $link, $creationDate, $completionDate,
                                  $type, $start, $end, $share, $support, $sqEnabled, $file, $author,
                                  $authorStatus)
    {
        $project = $this->project->find($id);
        $user = $this->user->find($project->user_id);

        if($authorStatus === 'people' && $project->owners->isEmpty()) {
            return array('status' => false, 'error' => "You have to add authors!" );
        }

        if($authorStatus === 'project' && !$project->project_author) {
            return array('status' => false, 'error' => 'You have to add project as author!');
        }

        !$title ?: $project->title = $title;
        !$description ?: $project->description = $description;
        !$link ?: $project->link = $link;
        !$creationDate ?: $project->creation_date = Carbon::parse($creationDate);
        !$completionDate ?: $project->completion_date = Carbon::parse($completionDate);
        !$type ?: $project->type = $type;
        !$start ?: $project->start = $start;
        !$end ?: $project->end = $end;
        !$authorStatus ? $project->author_status = 'me' : $project->author_status = $authorStatus;

        $todayDate = Carbon::now();
        $beginDate = Carbon::parse($project->creation_date);
        $endDate = Carbon::parse($project->completion_date);

        if($todayDate->timestamp < $beginDate->timestamp) $project->date_status = 'Begins';
        if($todayDate->timestamp > $beginDate->timestamp && $todayDate->timestamp < $endDate->timestamp)
            $project->date_status = 'Till';
        if($todayDate->timestamp > $endDate->timestamp) $project->date_status = 'Ended';

        !($share === '0' || $share === '1') ?: $project->share = $share;
        !($support === '0' || $support === '1') ?: $project->support = $support;

        !$sqEnabled ?: $project->sq_enabled = $sqEnabled;

        if($authorStatus === 'non-user') {
            $project->author = $author;
            $project->support = false;
        } else {
            $project->author = null;
            $project->support = $support;
        }

        if ($file) {
            $path = $file->storeAs('public/projects', $id);
            Storage::setVisibility($path, 'public');
            $project->image = Storage::url($path);
        }

        if($authorStatus !== 'people') {
            if(!$project->owners->isEmpty()) {
                $project->owners->map(function($owner) use($project) {
                    $project->owners()->detach($owner->id);
                });
            }
        }

        if($authorStatus !== 'project') {
            if($project->project_author) {
                $project->project_author = null;
            }
        }

        $project->save();

        event(new ProjectsChanged($user, $project));

        return array('status' => true, 'project' => $project);
    }

    /**
     * @param int $projectId
     * @return array
     */
    public function getOwners(int $projectId) {

        $project = $this->project->find($projectId);

        if(!$project) {
            return array('status' => false, 'error' => 'Use a valid project id!');
        }

        $owners = $project->owners()->get();

        return array('status' => true, 'owners' => $owners);
    }

    /**
     * @param int $projectId
     * @param array $ownersId
     * @param string $authorStatus
     * @return array
     */
    public function addOwner(int $projectId, array $ownersId, string $authorStatus) {

        $project = $this->project->find($projectId);
        $owners = $this->user->whereIn('id', $ownersId)->get();

        if(!$project) {
            return array('status' => false, 'error' => 'Use a valid project id!');
        }

        if($owners->isEmpty()) {
            return array('status' => false, 'error' => 'Use a valid user id!');
        }

        $owners->map(function ($owner) use ($project) {
            $project->owners()->attach($owner->id);
        });

        $project->author_status = $authorStatus;
        $project->save();

        return array('status' => true, 'owners' => $owners);
    }

    /**
     * @param $projectId
     * @param $ownerId
     * @return array
     */
    public function deleteOwner($projectId, $ownerId) {
        $project = $this->project->find($projectId);
        $owner = $this->user->where('id', '=', $ownerId)->first();

        if(!$project) {
            return array('status' => false, 'error' => 'Use a valid project id!');
        }

        if(!$owner) {
            return array('status' => false, 'error' => 'Use a valid user id!');
        }

        $project->owners()->detach($ownerId);

        return array('status' => true, 'owner' => $owner);
    }

    /**
     * @param $projectId
     * @param $authorId
     * @param $authorStatus
     * @return array
     */
    public function addProjectAuthor($projectId, $authorId, $authorStatus) {
        $project = $this->project->find($projectId);
        $projectAuthor = $this->project->find($authorId);

        if(!($project && $projectAuthor)) {
            return array('status' => true, 'error' => 'Use a valid projectId!');
        }

        $project->project_author = $authorId;
        $project->author_status = $authorStatus;
        $project->save();

        return array('status' => true, 'projectAuthor' => $projectAuthor);
    }

    /**
     * @param $projectId
     * @return array
     */
    public function getProjectAuthor($projectId) {
        $project = $this->project->find($projectId);

        if(!$project) {
            return array('status' => false, 'error' => 'Use a valid project id!');
        }

        $projectAuthor = $this->project->find($project->project_author);


        return array('status' => true, 'projectAuthor' => $projectAuthor);
    }

    /**
     * @param $projectId
     * @param $authorId
     * @return array
     */
    public function deleteProjectAuthor($projectId, $authorId) {
        $project = $this->project->find($projectId);

        if(!$project) {
            return array('status' => false, 'error' => 'Use a valid project id!');
        }

        $author = $this->project->find($authorId);

        $project->project_author = null;
        $project->save();

        return array('status' => true, 'projectAuthor' => $author);
    }

    /**
     * @param $userId
     * @param $projectId
     * @param $resourceOptions
     * @return array
     */
    public function showPossibleProjectAuthorList($userId, $projectId, $resourceOptions) {
        $initialLimit = (int)$resourceOptions['limit'];
        $resourceOptions['limit'] = -1;

        $ownProjectsQuery = $this->project->setQuery(DB::table('projects')
            ->where('projects.user_id', '=', $userId)
            ->join('project_activity', 'projects.id', 'project_activity.project_id')
            ->where('project_activity.user_id', '=', Auth::user()->getAuthIdentifier())
            ->select('projects.*', 'project_activity.activity')
        )->whereNotIn('projects.id', [$projectId]);

        $applyQuery = $this->applyResourceOptions($ownProjectsQuery, $resourceOptions);

        $count0 = $applyQuery['count'];
        $limitCount0 = $applyQuery['limitCount'];

        $limit = $initialLimit - $limitCount0;
        if($limit < 0) $limit = 0;
//        $resourceOptions['limit'] = $limit;

        $query = $this->project->setQuery(DB::table('projects')
            ->join('shared_projects', 'projects.id', '=', 'shared_projects.project_id')
            ->where('shared_projects.user_id', '=', $userId)
            ->join('project_activity', 'projects.id', 'project_activity.project_id')
            ->where('project_activity.user_id', '=', Auth::user()->getAuthIdentifier())
            ->select('projects.*', 'project_activity.activity'))->whereNotIn('projects.id', [$projectId]);

        $applyQuery = $this->applyResourceOptions($query, $resourceOptions);

        $resourceOptions['limit'] = $initialLimit;
        $count = $applyQuery['count'] + $count0;
        $filterCount = $initialLimit;
        $limitCount = $applyQuery['limitCount'] + $limitCount0;

        $query = ($query->union($ownProjectsQuery))->with('user');

        $resourceOptions['limit'] = $initialLimit;
        $this->applyResourceOptions($query, $resourceOptions);

        $projects = $query->get();

        return array('projects' => $projects, 'count' => $count,
            'limitCount' => $limitCount, 'filterCount' => $filterCount);
    }

    /**
     * @param int $projectId
     * @return array
     */
    public function getEditors(int $projectId) {

        $project = $this->project->find($projectId);

        if(!$project) {
            return array('status' => false, 'error' => 'Use a valid project id!');
        }

        $editors = $project->editors()->get();

        return array('status' => true, 'editors' => $editors);
    }

    /**
     * @param int $projectId
     * @param int $userId
     * @return array
     */
    public function addEditor(int $projectId, int $userId) {

        $project = $this->project->find($projectId);
        $user = $this->user->find($userId);

        if(!$project) {
            return array('status' => false, 'error' => 'Use a valid project id!');
        }

        if(!$user) {
            return array('status' => false, 'error' => 'Use a valid user id!');
        }

        $project->editors()->attach($userId);

        return array('status' => true, 'editor' => $user);
    }

    public function getParents(int $projectId) {

        $project = $this->project->find($projectId);

        if(!$project) {
            return array('status' => false, 'error' => 'Use a valid project id!');
        }

        $parents = $project->parents()->get();

        return array('status' => true, 'parents' => $parents);
    }

    public function addParent(int $projectId, int $parentProjectId) {
        $project = $this->project->find($projectId);
        $parentProject = $this->project->find($parentProjectId);

        if(!$project || !$parentProject) {
            return array('status' => false, 'error' => 'Use a valid project id!');
        }

        $project->parents()->attach($parentProject);

        return array('status' => true, 'parent' => $parentProject);
    }

    public function getChildren(int $projectId) {

        $project = $this->project->find($projectId);

        if(!$project) {
            return array('status' => false, 'error' => 'Use a valid project id!');
        }

        $children = $project->children()->get();

        return array('status' => true, 'children' => $children);
    }

    public function addChild(int $projectId, int $childProjectId) {

        $project = $this->project->find($projectId);
        $childProject = $this->project->find($childProjectId);

        if(!$project || !$childProject) {
            return array('status' => false, 'error' => 'Use a valid project id!');
        }

        $project->children()->attach($childProjectId);

        return array('status' => true, 'child' => $childProject);
    }
}