<?php

namespace App\Project;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public $happyModel;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->happyModel = app()->make('App\Happy\Happy');
    }

    protected $fillable = [
        'title',
        'description',
        'image',
        'link',
        'creation_date',
        'completion_date',
        'type',
        'start',
        'end',
        'share',
        'support',
        'happy',
        'value',
        'sq_enabled',
        'counter',
        'author',
        'ended_date',
        'date_status',
        'chronology',
        'total'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function activity() {
        return $this
            ->belongsToMany('App\User', 'project_activity', 'project_id', 'user_id')
            ->withPivot('activity');
    }

    public function usersHappy() {
        return $this->belongsToMany('App\User', 'happies', 'project_id', 'user_id');
    }

    public function usersSharedWith() {
        return $this->belongsToMany('App\User', 'shared_projects',
            'project_id', 'user_id');
    }

    public function getHappyFromUser(User $user): int {
        $happyFromUser = $this->happyModel->where('user_id', '=', $user->id)
            ->where('project_id', '=', $this->id)
            ->orderBy('id', 'desc')->first();
        if($happyFromUser) {
            return $happyFromUser->value;
        } else {
            return 0;
        }
    }

    public function owners()
    {
        return $this
            ->belongsToMany(
                'App\User',
                'projects_users',
                'project_id',
                'user_id'
            );
    }

    public function editors() {
        return $this
            ->belongsToMany(
                'App\User',
                'editors',
                'project_id',
                'user_id'
            );
    }

    public function parents() {
        return $this
            ->belongsToMany(
                'App\Project\Project',
                'nested_projects',
                'child_id',
                'parent_id'
            );
    }

    public function children() {
        return $this
            ->belongsToMany(
                'App\Project\Project',
                'nested_projects',
                'parent_id',
                'child_id'
            );
    }
}
