<?php

namespace App\Listeners;

use App\Events\ProductShared;
use App\Events\ProjectShared;
use App\Mail\ShareMessage;
use App\Notification\NotificationsApi;
use App\Project\Project;
use App\Settings\Settings;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;

class CreateProjectShareNotifications
{
    public $notificationsApi;
    public $user;
    public $project;

    /**
     * CreateProjectShareNotifications constructor.
     * @param NotificationsApi $notificationsApi
     * @param User $user
     * @param Project $project
     */
    public function __construct(NotificationsApi $notificationsApi, User $user, Project $project)
    {
        $this->notificationsApi = $notificationsApi;
        $this->user = $user;
        $this->project = $project;
    }

    /**
     * Handle the event.
     *
     * @param  ProductShared  $event
     * @return void
     */
    public function handle(ProjectShared $event)
    {
        $type = 'projectShare';
        $userId = $event->projectShare->user_id;
        $targetUserId = $event->projectShare->target_user_id;
        $cardId = $event->projectShare->project_id;
        $value = $event->projectShare->amount;
        $message = $event->projectShare->message;
        $shareId = $event->projectShare->id;

        $incoming = $this->notificationsApi
            ->createIncomingNotification($type, $userId, $targetUserId, $value, $cardId, $message, $shareId);

        $outgoing = $this->notificationsApi
            ->createOutgoingNotification($type, $userId, $targetUserId, $value, $cardId, $message, $shareId);

        $user = $this->user->find($userId);
        $targetUser = $this->user->find($targetUserId);
        $product = $this->project->find($cardId);
        $author = $this->user->find($product->user_id);

        $settings = Settings::where('user_id',$targetUserId)->first();

        if(((isset($settings->id) && $settings->shares!=='off')||!isset($settings->id)) && !($targetUser->productsShared()->get()->isEmpty() && $targetUser->projectsShared()->get()->isEmpty())) {
            $url = URL::to('/') . '/#/login';
//            dd($url);
            Mail::to($targetUser->email)->send(new ShareMessage($user->name, $user->surname,
                $targetUser->name, $targetUser->surname, $message, $product->title, $value,
                $author->name . ' ' . $author->surname, $url));
        }
    }
}