<?php

namespace App\Listeners;

use App\Events\FetchNewIncoming;
use App\Events\IncomingNotificationCreated;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateUnseenAttributeAfterIncoming
{
    public $targetUser;

    /**
     * UpdateUnseenAttributeAfterIncoming constructor.
     * @param User $targetUser
     */
    public function __construct(User $targetUser)
    {
        $this->targetUser = $targetUser;
    }

    /**
     * Handle the event.
     *
     * @param  IncomingNotificationCreated  $event
     * @return void
     */
    public function handle(IncomingNotificationCreated $event)
    {
        $targetUser = $this->targetUser->find($event->incoming['target_user_id']);
        event(new FetchNewIncoming($targetUser, array('count' => 0, 'unseen' => true)));
    }
}
