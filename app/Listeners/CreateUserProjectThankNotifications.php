<?php

namespace App\Listeners;

use App\Events\UserProjectThanked;
use App\Notification\NotificationsApi;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateUserProjectThankNotifications
{

    public $notificationsApi;

    /**
     * CreateUserThankNotifications constructor.
     * @param NotificationsApi $notificationsApi
     */
    public function __construct(NotificationsApi $notificationsApi)
    {
        $this->notificationsApi = $notificationsApi;
    }

    /**
     * Handle the event.
     *
     * @param  UserProjectThanked  $event
     * @return void
     */
    public function handle(UserProjectThanked $event)
    {
        $type = 'projectThank';
        $userId = $event->projectThank->user_id;
        $targetUserId = $event->projectThank->target_user_id;
        $cardId = $event->projectThank->project_id;
        $value = $event->projectThank->value;
        $message = $event->projectThank->message;
        $shareId = null;

        $incoming = $this->notificationsApi
            ->createIncomingNotification($type, $userId, $targetUserId, $value, $cardId, $message, $shareId);
//        $outgoing = $this->notificationsApi
//            ->createOutgoingNotification($type, $userId, $targetUserId, $value, $cardId, $message, $shareId);
    }
}
