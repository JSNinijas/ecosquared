<?php

namespace App\Listeners;

use App\Events\ProductSupported;
use App\Events\ProjectRejected;
use App\Events\ProjectSupported;
use App\Notification\NotificationsApi;
use App\Product\Product;
use App\Project\Project;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateProjectSupportNotifications
{

    public $notificationsApi;
    public $project;

    /**
     * CreateProjectSupportNotifications constructor.
     * @param NotificationsApi $notificationsApi
     * @param Project $project
     */
    public function __construct(NotificationsApi $notificationsApi, Project $project)
    {
        $this->notificationsApi = $notificationsApi;
        $this->project = $project;
    }

    /**
     * @param ProjectSupported $event
     */
    public function handle(ProjectSupported $event)
    {
        $project = $this->project->find($event->support->project_id);

        $type = 'projectSupported';
        $userId = $event->support->user_id;
        $targetUserId = $project->user_id;
        $cardId = $event->support->project_id;
        $value = $event->support->amount;
        $message = $event->support->message;
        $shareId = null;

        $incoming = $this->notificationsApi
            ->createIncomingNotification($type, $userId, $targetUserId, $value, $cardId, $message, $shareId);
//        $outgoing = $this->notificationsApi
//            ->createOutgoingNotification($type, $userId, $targetUserId, $value, $cardId, $message, $shareId);
    }
}