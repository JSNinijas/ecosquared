<?php

namespace App\Listeners;

use App\Events\UserThanked;
use App\Notification\NotificationsApi;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateUserThankNotifications
{

    public $notificationsApi;

    /**
     * CreateUserThankNotifications constructor.
     * @param NotificationsApi $notificationsApi
     */
    public function __construct(NotificationsApi $notificationsApi)
    {
        $this->notificationsApi = $notificationsApi;
    }

    /**
     * Handle the event.
     *
     * @param  UserThanked  $event
     * @return void
     */
    public function handle(UserThanked $event)
    {
        $type = 'thank';
        $userId = $event->thank->user_id;
        $targetUserId = $event->thank->target_user_id;
        $cardId = null;
        $value = $event->thank->value;
        $message = $event->thank->message;
        $shareId = null;

        $incoming = $this->notificationsApi
            ->createIncomingNotification($type, $userId, $targetUserId, $value, $cardId, $message, $shareId);
//        $outgoing = $this->notificationsApi
//            ->createOutgoingNotification($type, $userId, $targetUserId, $value, $cardId, $message, $shareId);
    }
}
