<?php

namespace App\Listeners;

use App\Events\ProductShared;
use App\Events\ProjectShared;
use App\Product\Product;
use App\Project\Project;
use App\User;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateUserAfterProjectShareInteraction
{

    protected $user;
    protected $project;

    /**
     * UpdateUserAfterProjectShareInteraction constructor.
     * @param User $user
     * @param Project $project
     */
    public function __construct(User $user, Project $project)
    {
        $this->user = $user;
        $this->project = $project;
    }

    /**
     * @param ProjectShared $event
     */
    public function handle(ProjectShared $event)
    {
        $user = $this->user->where('id', '=', $event->projectShare->user_id)->first();
        $userCounter = $user->get()->toArray()[0]['counter'];
        $project = $this->project->where('id', '=', $event->projectShare->project_id)->first();
//        $projectCounter = $project->get()->toArray()[0]['counter'];
        $targetUser = $this->user->where('id', '=', $event->projectShare->target_user_id)->first();
        $targetUserCounter = $targetUser->get()->toArray()[0]['counter'];

//        $user->update(['activity' => Carbon::now(), 'counter' => $userCounter + 1]);
        if($event->flag === 'users') {
            $targetUser->activity()->detach($user->id);
            $targetUser->activity()->attach($user->id, ['activity' => Carbon::now()]);
        } elseif($event->flag === 'projects') {
            $project->activity()->detach($user->id);
            $project->activity()->attach($user->id, ['activity' => Carbon::now(), 'target_user_id' => $targetUser->id]);
        }
    }
}