<?php

namespace App\Listeners;

use App\Events\ProductAccepted;
use App\Events\ProjectAccepted;
use App\User;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateUserAfterProjectAcceptInteraction
{
    public $user;

    /**
     * UpdateUserAfterProjectAcceptInteraction constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Handle the event.
     *
     * @param  ProductAccepted  $event
     * @return void
     */
    public function handle(ProjectAccepted $event)
    {
        $user = $this->user->where('id', '=', $event->projectShare->target_user_id);
        $userCounter = $user->get()->toArray()[0]['counter'];

//        $user->update(['activity' => Carbon::now(), 'counter' => $userCounter + 1]);
    }
}