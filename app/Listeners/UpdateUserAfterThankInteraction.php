<?php

namespace App\Listeners;

use App\Events\UserThanked;
use App\User;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateUserAfterThankInteraction
{

    protected $user;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Handle the event.
     *
     * @param  UserThanked  $event
     * @return void
     */
    public function handle(UserThanked $event)
    {
        $user = $this->user->where('id', '=', $event->thank->user_id)->first();

        $targetUser = $this->user->where('id', '=', $event->thank->target_user_id)->first();

        $targetUser->activity()->detach($user->id);
        $targetUser->activity()->attach($user->id, ['activity' => Carbon::now()]);
    }
}