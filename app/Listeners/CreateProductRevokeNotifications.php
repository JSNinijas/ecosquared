<?php

namespace App\Listeners;

use App\Events\ProductRevoked;
use App\Notification\NotificationsApi;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateProductRevokeNotifications
{

    public $notificationsApi;

    /**
     * Create the event listener.
     *
     * @param NotificationsApi $notificationsApi
     * @return void
     */
    public function __construct(NotificationsApi $notificationsApi)
    {
        $this->notificationsApi = $notificationsApi;
    }

    /**
     * Handle the event.
     *
     * @param  ProductRevoked  $event
     * @return void
     */
    public function handle(ProductRevoked $event)
    {
        $type = 'productRevoke';
        $userId = $event->share->user_id;
        $targetUserId = $event->share->target_user_id;
        $cardId = $event->share->product_id;
        $value = $event->share->amount;
        $message = $event->share->message;
        $shareId = $event->share->id;

        $incoming = $this->notificationsApi
            ->createIncomingNotification($type, $userId, $targetUserId, $value, $cardId, $message, $shareId);
//        $outgoing = $this->notificationsApi
//            ->createOutgoingNotification($type, $userId, $targetUserId, $value, $cardId, $message, $shareId);
    }
}
