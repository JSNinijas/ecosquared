<?php

namespace App\Listeners;

use App\Console\Commands\EmailDistribution;
use App\Events\ProductShared;
use App\Mail\ShareMessage;
use App\Notification\NotificationsApi;
use App\Product\Product;
use App\Settings\Settings;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;

class CreateProductShareNotifications
{
    public $notificationsApi;
    public $user;
    public $product;

    /**
     * CreateProductShareNotifications constructor.
     * @param NotificationsApi $notificationsApi
     * @param User $user
     * @param Product $product
     */
    public function __construct(NotificationsApi $notificationsApi, User $user, Product $product)
    {
        $this->notificationsApi = $notificationsApi;
        $this->user = $user;
        $this->product = $product;
    }

    /**
     * Handle the event.
     *
     * @param  ProductShared $event
     * @return void
     */
    public function handle(ProductShared $event)
    {
        $type = 'productShare';
        $userId = $event->share->user_id;
        $targetUserId = $event->share->target_user_id;
        $cardId = $event->share->product_id;
        $value = $event->share->amount;
        $message = $event->share->message;
        $shareId = $event->share->id;

        $user = $this->user->find($userId);
        $targetUser = $this->user->find($targetUserId);
        $product = $this->product->find($cardId);
        $author = $this->user->find($product->user_id);

        if ($product->author !== null) {
            $author->name = $product->author;
            $author->surname = '';
        }

        $incoming = $this->notificationsApi
            ->createIncomingNotification($type, $userId, $targetUserId, $value, $cardId, $message, $shareId);

        $outgoing = $this->notificationsApi
            ->createOutgoingNotification($type, $userId, $targetUserId, $value, $cardId, $message, $shareId);

//        dd($targetUser->productsShared()->get()->isEmpty() && $targetUser->projectsShared()->get()->isEmpty());

        $settings = Settings::where('user_id', $targetUserId)->first();

        if (((isset($settings->id) && $settings->shares !== 'off') || !isset($settings->id)) && !($targetUser->productsShared()->get()->isEmpty() && $targetUser->projectsShared()->get()->isEmpty())) {
            $url = URL::to('/') . '/#/login';
//            dd($url);
            Mail::to($targetUser->email)->send(new ShareMessage($user->name, $user->surname,
                $targetUser->name, $targetUser->surname, $message, $product->title, $value,
                $author->name . ' ' . $author->surname, $url));
        }
    }
}