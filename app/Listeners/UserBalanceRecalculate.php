<?php

namespace App\Listeners;

use App\Events\TransactionCreated;
use App\Wallet\AccountModel;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Wallet\WalletApi;
use App\Events\UserBalanceUpdated;

class UserBalanceRecalculate
{
    public $walletApi;

    /**
     * UserBalanceRecalculate constructor.
     * @param WalletApi $walletApi
     */
    public function __construct(WalletApi $walletApi)
    {
        $this->walletApi = $walletApi;
    }

    /**
     * Handle the event.
     *
     * @param  TransactionCreated  $event
     * @return void
     */
    public function handle(TransactionCreated $event)
    {
        if ($event->transaction->transaction_status === 1) {
            $userTotal = $this->walletApi->updateUserTotalBalance($event->transaction->user_id, $event->transaction->amount);
            event(new UserBalanceUpdated($userTotal));
        }
    }
}
