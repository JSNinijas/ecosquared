<?php

namespace App\Listeners;

use App\Events\ProductSupported;
use App\Notification\NotificationsApi;
use App\Product\Product;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateProductSupportNotifications
{

    public $notificationsApi;
    public $product;

    /**
     * Create the event listener.
     *
     * @param Product $product
     * @param NotificationsApi $notificationsApi
     * @return void
     */
    public function __construct(NotificationsApi $notificationsApi, Product $product)
    {
        $this->notificationsApi = $notificationsApi;
        $this->product = $product;
    }

    /**
     * Handle the event.
     *
     * @param  ProductSupported  $event
     * @return void
     */
    public function handle(ProductSupported $event)
    {
        $product = $this->product->find($event->support->product_id);

        $type = 'productSupported';
        $userId = $event->support->user_id;
        $targetUserId = $product->user_id;
        $cardId = $event->support->product_id;
        $value = $event->support->amount;
        $message = $event->support->message;
        $shareId = null;

        $incoming = $this->notificationsApi
            ->createIncomingNotification($type, $userId, $targetUserId, $value, $cardId, $message, $shareId);
//        $outgoing = $this->notificationsApi
//            ->createOutgoingNotification($type, $userId, $targetUserId, $value, $cardId, $message, $shareId);
    }
}
