<?php

namespace App\Listeners;

use App\Events\ProductRejected;
use App\Product\Product;
use App\User;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateUserAfterRejectInteraction
{
    public $user;
    public $product;

    /**
     * Create the event listener.
     *
     * @param User $user
     * @param Product $product
     * @return void
     */
    public function __construct(User $user, Product $product)
    {
        $this->user = $user;
        $this->product = $product;
    }

    /**
     * Handle the event.
     *
     * @param  ProductRejected  $event
     * @return void
     */
    public function handle(ProductRejected $event)
    {
        $user = $this->user->where('id', '=', $event->share->target_user_id);
        $userCounter = $user->get()->toArray()[0]['counter'];
        $product = $this->product->where('id', '=', $event->share->product_id);
        $productCounter = $product->get()->toArray()[0]['counter'];

//        $user->update(['activity' => Carbon::now(), 'counter' => $userCounter + 1]);
        $product->update(['counter' => $productCounter - 1]);
    }
}
