<?php

namespace App\Listeners;

use App\Events\ProductLiked;
use App\Notification\NotificationsApi;
use App\Product\Product;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateProductLikeNotifications
{
    public $notificationsApi;
    public $product;

    /**
     * CreateProductLikeNotifications constructor.
     * @param NotificationsApi $notificationsApi
     * @param Product $product
     */
    public function __construct(NotificationsApi $notificationsApi, Product $product)
    {
        $this->notificationsApi = $notificationsApi;
        $this->product = $product;
    }

    /**
     * Handle the event.
     *
     * @param  ProductLiked  $event
     * @return void
     */
    public function handle(ProductLiked $event)
    {
        $product = $this->product->find($event->like->product_id);

        $type = 'productLike';
        $userId = $event->like->user_id;
        $targetUserId = $product->user_id;
        $cardId = $event->like->product_id;
        $value = $event->like->value;
        $message = $event->like->message;
        $shareId = null;

        $incoming = $this->notificationsApi
            ->createIncomingNotification($type, $userId, $targetUserId, $value, $cardId, $message, $shareId);
//        $outgoing = $this->notificationsApi
//            ->createOutgoingNotification($type, $userId, $targetUserId, $value, $cardId, $message, $shareId);
    }
}
