<?php

namespace App\Listeners;

use App\Events\ProjectHappied;
use App\Notification\NotificationsApi;
use App\Project\Project;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateProjectHappyNotifications
{
    public $notificationsApi;
    public $project;

    /**
     * CreateProjectHappyNotifications constructor.
     * @param NotificationsApi $notificationsApi
     * @param Project $project
     */
    public function __construct(NotificationsApi $notificationsApi, Project $project)
    {
        $this->notificationsApi = $notificationsApi;
        $this->project = $project;
    }

    /**
     * Handle the event.
     *
     * @param  ProjectHappied  $event
     * @return void
     */
    public function handle(ProjectHappied $event)
    {
        $project = $this->project->find($event->happy->project_id);

        $type = 'projectHappy';
        $userId = $event->happy->user_id;
        $targetUserId = $project->user_id;
        $cardId = $event->happy->project_id;
        $value = $event->happy->value;
        $message = $event->happy->message;
        $shareId = null;

        $incoming = $this->notificationsApi
            ->createIncomingNotification($type, $userId, $targetUserId, $value, $cardId, $message, $shareId);
//        $outgoing = $this->notificationsApi
//            ->createOutgoingNotification($type, $userId, $targetUserId, $value, $cardId, $message, $shareId);
    }
}