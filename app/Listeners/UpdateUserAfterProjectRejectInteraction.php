<?php

namespace App\Listeners;

use App\Events\ProductRejected;
use App\Events\ProjectRejected;
use App\Product\Product;
use App\Project\Project;
use App\User;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateUserAfterProjectRejectInteraction
{
    public $user;
    public $project;

    /**
     * Create the event listener.
     *
     * @param User $user
     * @param Product $product
     * @return void
     */
    public function __construct(User $user, Project $project)
    {
        $this->user = $user;
        $this->project = $project;
    }

    /**
     * @param ProjectRejected $event
     */
    public function handle(ProjectRejected $event)
    {
        $user = $this->user->where('id', '=', $event->projectShare->target_user_id);
        $userCounter = $user->get()->toArray()[0]['counter'];
        $project = $this->project->where('id', '=', $event->projectShare->project_id);
        $projectCounter = $project->get()->toArray()[0]['counter'];

//        $user->update(['activity' => Carbon::now(), 'counter' => $userCounter + 1]);
        $project->update(['counter' => $projectCounter - 1]);
    }
}