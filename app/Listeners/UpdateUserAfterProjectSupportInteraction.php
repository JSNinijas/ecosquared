<?php

namespace App\Listeners;

use App\Events\ProjectHappied;
use App\Events\ProjectSupported;
use App\Project\Project;
use App\User;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateUserAfterProjectSupportInteraction
{
    protected $user;
    protected $project;

    /**
     * UpdateUserAfterProjectSupportInteraction constructor.
     * @param User $user
     * @param Project $project
     */
    public function __construct(User $user, Project $project)
    {
        $this->user = $user;
        $this->project = $project;
    }

    /**
     * Handle the event.
     *
     * @param  ProjectSupported  $event
     * @return void
     */
    public function handle(ProjectSupported $event)
    {
        $user = $this->user->where('id', '=', $event->support->user_id)->first();
//        $userCounter = $user->get()->toArray()[0]['counter'];
        $project = $this->project->where('id', '=', $event->support->project_id)->first();
//        $productCounter = $project->get()->toArray()[0]['counter'];

//        $user->update(['activity' => Carbon::now(), 'counter' => $userCounter + 1]);

//        $product->update(['activity' => Carbon::now(), 'counter' => $productCounter + 1]);
        $project->activity()->detach($user->id);
        $project->activity()->attach($user->id, ['activity' => Carbon::now(), 'target_user_id' => $user->id]);
    }
}