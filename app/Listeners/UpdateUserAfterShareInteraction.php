<?php

namespace App\Listeners;

use App\Events\ProductShared;
use App\Product\Product;
use App\User;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateUserAfterShareInteraction
{

    protected $user;
    protected $product;

    /**
     * UpdateUserAfterShareInteraction constructor.
     * @param User $user
     * @param Product $product
     */
    public function __construct(User $user, Product $product)
    {
        $this->user = $user;
        $this->product = $product;
    }

    /**
     * Handle the event.
     *
     * @param  ProductShared  $event
     * @return void
     */
    public function handle(ProductShared $event)
    {
        $user = $this->user->where('id', '=', $event->share->user_id)->first();
//        $userCounter = $user->get()->toArray()[0]['counter'];
        $product = $this->product->where('id', '=', $event->share->product_id)->first();
//        $productCounter = $product->get()->toArray()[0]['counter'];
        $targetUser = $this->user->where('id', '=', $event->share->target_user_id)->first();
//        $targetUserCounter = $targetUser->get()->toArray()[0]['counter'];

//        $user->update(['activity' => Carbon::now(), 'counter' => $userCounter + 1]);
        if($event->flag === 'users') {
            $targetUser->activity()->detach($user->id);
            $targetUser->activity()->attach($user->id, ['activity' => Carbon::now()]);
        } elseif($event->flag === 'products') {
            $product->activity()->detach($user->id);
            $product->activity()->attach($user->id, ['activity' => Carbon::now(), 'target_user_id' => $targetUser->id]);
        }
    }
}
