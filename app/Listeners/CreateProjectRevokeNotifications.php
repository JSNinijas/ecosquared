<?php

namespace App\Listeners;

use App\Events\ProjectRevoked;
use App\Notification\NotificationsApi;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateProjectRevokeNotifications
{

    public $notificationsApi;

    /**
     * CreateProjectRevokeNotifications constructor.
     * @param NotificationsApi $notificationsApi
     */
    public function __construct(NotificationsApi $notificationsApi)
    {
        $this->notificationsApi = $notificationsApi;
    }

    /**
     * Handle the event.
     *
     * @param  ProjectRevoked  $event
     * @return void
     */
    public function handle(ProjectRevoked $event)
    {
        $type = 'projectRevoke';
        $userId = $event->projectShare->user_id;
        $targetUserId = $event->projectShare->target_user_id;
        $cardId = $event->projectShare->product_id;
        $value = $event->projectShare->amount;
        $message = $event->projectShare->message;
        $shareId = $event->projectShare->id;

        $incoming = $this->notificationsApi
            ->createIncomingNotification($type, $userId, $targetUserId, $value, $cardId, $message, $shareId);
//        $outgoing = $this->notificationsApi
//            ->createOutgoingNotification($type, $userId, $targetUserId, $value, $cardId, $message, $shareId);
    }
}