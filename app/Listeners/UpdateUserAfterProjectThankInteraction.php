<?php

namespace App\Listeners;

use App\Events\UserProjectThanked;
use App\User;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateUserAfterProjectThankInteraction
{

    protected $user;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Handle the event.
     *
     * @param  UserProjectThanked  $event
     * @return void
     */
    public function handle(UserProjectThanked $event)
    {
        $user = $this->user->where('id', '=', $event->projectThank->user_id)->first();

        $targetUser = $this->user->where('id', '=', $event->projectThank->target_user_id)->first();

        $targetUser->activity()->detach($user->id);
        $targetUser->activity()->attach($user->id, ['activity' => Carbon::now()]);
    }
}
