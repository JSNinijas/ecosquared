<?php

namespace App\Listeners;

use App\Events\ProductLiked;
use App\Events\ProductSupported;
use App\Product\Product;
use App\User;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateUserAfterSupportInteraction
{
    protected $user;
    protected $product;

    /**
     * UpdateUserAfterSupportInteraction constructor.
     * @param User $user
     * @param Product $product
     */
    public function __construct(User $user, Product $product)
    {
        $this->user = $user;
        $this->product = $product;
    }

    /**
     * Handle the event.
     *
     * @param  ProductLiked  $event
     * @return void
     */
    public function handle(ProductSupported $event)
    {
        $user = $this->user->where('id', '=', $event->support->user_id)->first();
        $userCounter = $user->get()->toArray()[0]['counter'];
        $product = $this->product->where('id', '=', $event->support->product_id)->first();
        $productCounter = $product->get()->toArray()[0]['counter'];

//        $user->update(['activity' => Carbon::now(), 'counter' => $userCounter + 1]);

//        $product->update(['activity' => Carbon::now(), 'counter' => $productCounter + 1]);
        $product->activity()->detach($user->id);
        $product->activity()->attach($user->id, ['activity' => Carbon::now(), 'target_user_id' => $user->id]);
    }
}