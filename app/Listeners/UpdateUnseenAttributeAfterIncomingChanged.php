<?php

namespace App\Listeners;

use App\Events\FetchNewIncoming;
use App\Events\IncomingNotificationChanged;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateUnseenAttributeAfterIncomingChanged
{
    public $targetUser;

    /**
     * UpdateUnseenAttributeAfterIncomingChanged constructor.
     * @param User $targetUser
     */
    public function __construct(User $targetUser)
    {
        $this->targetUser = $targetUser;
    }

    /**
     * Handle the event.
     *
     * @param  IncomingNotificationChanged  $event
     * @return void
     */
    public function handle(IncomingNotificationChanged $event)
    {
        $targetUser = $this->targetUser->find($event->incoming['target_user_id']);
        event(new FetchNewIncoming($targetUser, array('count' => 0, 'unseen' => false)));
    }
}