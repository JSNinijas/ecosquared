<?php

namespace App\Listeners;

use App\Events\OutgoingNotificationCreated;
use App\Events\UserInvited;
use App\Notification\NotificationsApi;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateUserInviteNotifications
{

    public $notificationsApi;
    /**
     * Create the event listener.
     *
     * @param NotificationsApi $notificationsApi
     * @return void
     */
    public function __construct(NotificationsApi $notificationsApi)
    {
        $this->notificationsApi = $notificationsApi;
    }

    /**
     * Handle the event.
     *
     * @param  UserInvited  $event
     * @return void
     */
    public function handle(UserInvited $event)
    {
        if($event->nonUser->card == 'product') {
            $type = 'userInvite';
            $userId = $event->nonUser->inviter_id;
            $targetUserId = $event->nonUser->user_id;
            $cardId = $event->nonUser->card_id;
            $value = $event->nonUser->amount;
            $message = $event->nonUser->message;
            $nonUserId = $event->nonUser->id;
        } else {
            $type = 'userProjectInvite';
            $userId = $event->nonUser->inviter_id;
            $targetUserId = $event->nonUser->user_id;
            $cardId = $event->nonUser->card_id;
            $value = $event->nonUser->amount;
            $message = $event->nonUser->message;
            $nonUserId = $event->nonUser->id;
        }

        $outgoing = $this->notificationsApi
            ->createOutgoingNotification($type, $userId, $targetUserId, $value, $cardId, $message, $nonUserId);
    }
}
