<?php

namespace App\Listeners;

use App\Events\ProductAccepted;
use App\User;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateUserAfterAcceptInteraction
{
    public $user;

    /**
     * Create the event listener.
     *
     * @param User $user
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Handle the event.
     *
     * @param  ProductAccepted  $event
     * @return void
     */
    public function handle(ProductAccepted $event)
    {
        $user = $this->user->where('id', '=', $event->share->target_user_id);
        $userCounter = $user->get()->toArray()[0]['counter'];

//        $user->update(['activity' => Carbon::now(), 'counter' => $userCounter + 1]);
    }
}
