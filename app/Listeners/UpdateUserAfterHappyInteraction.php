<?php

namespace App\Listeners;

use App\Events\ProjectHappied;
use App\Project\Project;
use App\User;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateUserAfterHappyInteraction
{
    protected $user;
    protected $project;

    /**
     * UpdateUserAfterHappyInteraction constructor.
     * @param User $user
     * @param Project $project
     */
    public function __construct(User $user, Project $project)
    {
        $this->user = $user;
        $this->project = $project;
    }

    /**
     * @param ProjectHappied $event
     */
    public function handle(ProjectHappied $event)
    {
        $user = $this->user->where('id', '=', $event->happy->user_id)->first();
        $userCounter = $user->get()->toArray()[0]['counter'];
        $project = $this->project->where('id', '=', $event->happy->project_id)->first();
//        $projectCounter = $project->get()->toArray()[0]['counter'];

//        $user->update(['activity' => Carbon::now(), 'counter' => $userCounter + 1]);

        $project->activity()->detach($user->id);
        $project->activity()->attach($user->id, ['activity' => Carbon::now(), 'target_user_id' => $user->id]);
    }
}