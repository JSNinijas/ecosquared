<?php

namespace App\Events;

use App\Share\Share;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class ProductShared
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $share, $flag;

    /**
     * ProductShared constructor.
     * @param Share $share
     * @param string $flag
     */
    public function __construct(Share $share, string $flag)
    {
        $this->share = $share;
        $this->flag = $flag;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}