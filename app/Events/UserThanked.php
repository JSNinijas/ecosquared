<?php

namespace App\Events;

use App\Thank\Thank;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserThanked
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $thank;

    /**
     * UserThanked constructor.
     * @param Thank $thank
     */
    public function __construct(Thank $thank)
    {
        $this->thank = $thank;
    }

    public function broadcastWith() {

    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('user.' . $this->thank->user_id);
    }
}
