<?php

namespace App\Events;

use App\Notification\Outgoing;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OutgoingNotificationDeleted implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $outgoing;
    public $id;

    /**
     * OutgoingNotificationDeleted constructor.
     * @param Outgoing $outgoing
     */
    public function __construct(Outgoing $outgoing)
    {
        $this->outgoing = $outgoing->id;
        $this->id = $outgoing->user_id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('outgoing_notification.' . $this->id);
    }
}
