<?php

namespace App\Events;

use App\Notification\Outgoing;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OutgoingNotificationCreated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $outgoing;

    /**
     * Create a new event instance.
     *
     * @param Outgoing $outgoing
     * @return void
     */
    public function __construct(Outgoing $outgoing)
    {
        $this->outgoing = $outgoing->toArray();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('outgoing_notification.' . $this->outgoing['user_id']);
    }
}