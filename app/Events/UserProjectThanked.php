<?php

namespace App\Events;

use App\Thank\ProjectThank;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserProjectThanked
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $projectThank;

    /**
     * UserThanked constructor.
     * @param ProjectThank $thank
     */
    public function __construct(ProjectThank $thank)
    {
        $this->projectThank = $thank;
    }

    public function broadcastWith() {

    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('user.' . $this->projectThank->user_id);
    }
}
