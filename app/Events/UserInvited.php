<?php

namespace App\Events;

use App\NonUser;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserInvited
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $nonUser;
    /**
     * Create a new event instance.
     *
     * @param NonUser $nonUser
     * @return void
     */
    public function __construct(NonUser $nonUser)
    {
        $this->nonUser = $nonUser;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
