<?php

namespace App\Events;

use App\Notification\Incoming;
use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class FetchNewIncoming implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $targetUser, $newIncoming;

    /**
     * FetchNewIncoming constructor.
     * @param array $newIncoming
     * @param User $targetUser
     */
    public function __construct(User $targetUser, array $newIncoming)
    {
        $this->newIncoming = $newIncoming;
        $this->targetUser = $targetUser;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('incoming_notification.' . $this->targetUser->id);
    }
}
