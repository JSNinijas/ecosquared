<?php

namespace App\Events;

use App\Support\ProjectSupport;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ProjectSupported
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $support;

    /**
     * ProjectSupported constructor.
     * @param ProjectSupport $support
     */
    public function __construct(ProjectSupport $support)
    {
        $this->support = $support;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}