<?php

namespace App\Events;

use App\Share\ProjectShare;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class ProjectAccepted
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $projectShare;

    /**
     * ProjectAccepted constructor.
     * @param ProjectShare $projectShare
     */
    public function __construct(ProjectShare $projectShare)
    {
        $this->projectShare = $projectShare;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}