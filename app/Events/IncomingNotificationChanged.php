<?php

namespace App\Events;

use App\Notification\Incoming;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class IncomingNotificationChanged implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $incoming;

    /**
     * IncomingNotificationChanged constructor.
     * @param Incoming $incoming
     */
    public function __construct(Incoming $incoming)
    {
        $this->incoming = $incoming;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('incoming_notification.' . $this->incoming->target_user_id);
    }
}
