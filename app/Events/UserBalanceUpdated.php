<?php

namespace App\Events;

use App\Wallet\UserTotalBalance;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserBalanceUpdated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $userTotalBalance;

    /**
     * UserBalanceUpdated constructor.
     * @param UserTotalBalance $userTotalBalance
     */
    public function __construct(UserTotalBalance $userTotalBalance)
    {
        $this->userTotalBalance = $userTotalBalance;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('user.' . $this->userTotalBalance->user_id);
    }
}
