<?php

namespace App\Events;

use App\Share\projectShare;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class ProjectRejected
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $projectShare;

    /**
     * ProjectRejected constructor.
     * @param projectShare $projectShare
     */
    public function __construct(projectShare $projectShare)
    {
        $this->projectShare = $projectShare;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}