<?php

namespace App\Events;

use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OutgoingShareChanged implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user, $outgoingOffers;

    /**
     * OutgoingShareChanged constructor.
     * @param User $user
     * @param int $outgoingOffers
     */
    public function __construct(User $user, int $outgoingOffers)
    {
        $this->user = $user;
        $this->outgoingOffers = $outgoingOffers;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('outgoing_notification.' . $this->user->id);
    }
}
