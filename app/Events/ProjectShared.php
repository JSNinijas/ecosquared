<?php

namespace App\Events;

use App\Share\ProjectShare;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class ProjectShared
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $projectShare, $flag;

    /**
     * ProjectShared constructor.
     * @param ProjectShare $projectShare
     * @param string $flag
     */
    public function __construct(ProjectShare $projectShare, string $flag)
    {
        $this->projectShare = $projectShare;
        $this->flag = $flag;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}