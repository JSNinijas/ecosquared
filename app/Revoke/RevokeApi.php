<?php

namespace App\Revoke;

use App\Events\IncomingNotificationDeleted;
use App\Events\IncomingShareChanged;
use App\Events\OutgoingNotificationDeleted;
use App\Events\OutgoingShareChanged;
use App\Events\UserBalanceUpdated;
use App\Notification\Incoming;
use App\Notification\NotificationsApi;
use App\Notification\Outgoing;
use App\Product\Product;
use App\Share\ProjectShare;
use App\Share\Share;
use App\User;
use App\Wallet\WalletApi;

class RevokeApi
{
    protected $user;
    protected $product;
    protected $share;
    protected $projectShare;
    protected $walletApi;
    protected $notificationsApi;
    protected $incoming;
    protected $outgoing;

    function __construct(Product $product, User $user, Share $share, ProjectShare $projectShare,
                         WalletApi $walletApi, NotificationsApi $notificationsApi,
                         Incoming $incoming, Outgoing $outgoing)
    {
        $this->user = $user;
        $this->product = $product;
        $this->share = $share;
        $this->walletApi = $walletApi;
        $this->notificationsApi = $notificationsApi;
        $this->projectShare = $projectShare;
        $this->incoming = $incoming;
        $this->outgoing = $outgoing;
    }

    /**
     * TODO: REFACTOR ME PLEASE
     * @param int $notificationId
     * @param int $shareId
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function revokeProductProject(int $notificationId, int $shareId) {

        $incomingNotification = $this->notificationsApi->getOutgoing($notificationId);
        if($incomingNotification->type == 'productShare') {
            $share = $this->share->find($shareId);
        } elseif($incomingNotification->type == 'projectShare') {
            $share = $this->projectShare->find($shareId);
        }

        if($incomingNotification->type == 'productShare') {
            $share = $this->share->find($shareId);
        } else {
            $share= $this->projectShare->find($shareId);
        }

        $outgoingNotification = $this->notificationsApi->getIncomingByIds(
            $incomingNotification->type,
            $incomingNotification->user_id,
            $incomingNotification->target_user_id,
            $incomingNotification->card_id
        )->first();

        $user = $this->user->find($share->user_id);
        $targetUser = $this->user->find($share->target_user_id);
        $userTransaction = $this->walletApi->getTransactionById($share->user_transaction_id);
        $targetUserTransaction = $this->walletApi->getTransactionById($share->target_user_transaction_id);

        $this->walletApi->updateTransaction($userTransaction->id, 1,
            'received', 'default', $share->amount, $share->user_id);

        $this->walletApi->deleteTransaction($userTransaction->id);
        $this->walletApi->deleteTransaction($targetUserTransaction->id);
        $incomingNotification->delete();
        $outgoingNotification->delete();
        $share->delete();

        $offers = $this->incoming
            ->where('target_user_id', '=', $targetUser->id)
            ->whereIn('type',['productShare', 'projectShare'])
            ->count();
        $outgoingOffers = $this->outgoing
            ->where('user_id', '=', $user->id)
            ->whereIn('type',['productShare', 'userInvite', 'projectShare', 'userProjectInvite'])
            ->count();


        event(new IncomingShareChanged($targetUser, $offers));
        event(new OutgoingShareChanged($user, $outgoingOffers));
        event(new IncomingNotificationDeleted($outgoingNotification));
        event(new OutgoingNotificationDeleted($incomingNotification));
        event(new UserBalanceUpdated($this->walletApi->rebuildUserTotalBalance($user->id)['userBalance']));

        return $outgoingNotification;
    }
}