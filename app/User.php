<?php

namespace App;

use App\Product\Product;
use App\Project\Project;
use App\Thank\Thank;
use App\Like\Like;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens, EntrustUserTrait;

    /**
     * @var mixed
     */
    public $thank, $acceptedProduct, $acceptedProject, $projectThank;


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->thank = app()->make('App\Thank\Thank');
        $this->acceptedProduct = app()->make('App\Share\AcceptedShare');
        $this->acceptedProject = app()->make('App\Share\AcceptedProjectShare');
        $this->projectThank = app()->make('App\Thank\ProjectThank');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname', 'description', 'email', 'active', 'create_feature'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function projects() {
        return $this->hasMany('App\Project\Project');
    }

    public function products() {
        return $this->hasMany('App\Product\Product');
    }

    public function users() {
        return $this
            ->belongsToMany('App\User', 'user_relations', 'user_id', 'target_user_id');
    }

    public function activity() {
        return $this
            ->belongsToMany('App\User', 'user_activity', 'target_user_id', 'user_id')
            ->withPivot('activity');
    }

    public function productsLiked() {
        return $this
            ->belongsToMany('App\Product\Product','likes', 'user_id', 'product_id')
            ->withTimestamps()->withPivot('value', 'message');
    }


    public function projectsHappied() {
        return $this
            ->belongsToMany('App\Project\Project','happies', 'user_id', 'project_id')
            ->withTimestamps()->withPivot('value', 'message');
    }

    /**
     * @param Product $product
     * @return mixed
     */
    public function getLikesByProduct(Product $product) {
        return $this->hasMany('App\Like\Like')
            ->where('product_id', $product->id)
            ->orderBy('id', 'desc')
            ->pluck('value')->first();
    }

    /**
     * @param Project $project
     * @return mixed
     */
    public function getHappyByProject(Project $project) {
        return $this->hasMany('App\Happy\Happy')
            ->where('project_id', $project->id)
            ->orderBy('id', 'desc')
            ->pluck('value')->first();
    }

    /**
     * @param Product $product
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getSharedCreditsByProduct(Product $product) {
        return $this->hasMany('App\Share\AcceptedShare')
            ->where('product_id', $product->id)->sum('amount');
    }

    /**
     * @param Project $project
     * @return mixed
     */
    public function getSharedCreditsByProject(Project $project) {
        return $this->hasMany('App\Share\AcceptedProjectShare')
            ->where('project_id', $project->id)->sum('amount');
    }

    /**
     * @param Product $product
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function getSupportCreditsByProduct(Product $product) {
        return $this->hasMany('App\Support\Support')
            ->where('product_id', $product->id)->sum('amount');
    }

    /**
     * @param Project $project
     * @return mixed
     */
    public function getSupportCreditsByProject(Project $project) {
        return $this->hasMany('App\Support\ProjectSupport')
            ->where('project_id', $project->id)->sum('amount');
    }

    /**
     * @param Product $product
     * @return mixed
     */
    public function getLastLikeComment(Product $product) {
        return $this->hasMany('App\Like\Like')
            ->where('product_id', $product->id)
            ->orderBy('id', 'desc')
            ->pluck('message')->first();
    }

    /**
     * @param Project $project
     * @return mixed
     */
    public function getLastHappyComment(Project $project) {
        return $this->hasMany('App\Happy\Happy')
            ->where('project_id', $project->id)
            ->orderBy('id', 'desc')
            ->pluck('message')->first();
    }

    /**
     * @param Product $product
     * @param User $user
     * @return mixed
     */
    public function getReceivedCredits(Product $product, User $user) {
        return $this->acceptedProduct->where('product_id', $product->id)
            ->where('target_user_id', $user->id)->sum('amount');
    }

    /**
     * @param Project $project
     * @param User $user
     * @return mixed
     */
    public function getProjectReceivedCredits(Project $project, User $user) {
        return $this->acceptedProject->where('project_id', $project->id)
            ->where('target_user_id', $user->id)->sum('amount');
    }

    /**
     * @param Product $product
     * @param User $user
     * @return mixed
     */
    public function getCreditsByProduct(Product $product, User $user){
        $received = $this->getReceivedCredits($product, $user);
        $shared = $this->getSharedCreditsByProduct($product);
        $supported = $this->getSupportCreditsByProduct($product);

        $sum = $shared + $supported - $received;
        return $sum;
    }

    /**
     * @param Project $project
     * @param User $user
     * @return mixed
     */
    public function getCreditsByProject(Project $project, User $user){
        $received = $this->getProjectReceivedCredits($project, $user);
        $shared = $this->getSharedCreditsByProject($project);
        $supported = $this->getSupportCreditsByProject($project);

        $sum = $shared + $supported - $received;
        return $sum;
    }

    public function productsShared() {
        return $this
            ->belongsToMany('App\Product\Product', 'shared_products', 'user_id', 'product_id')
            ->withTimestamps();
    }

    public function projectsShared() {
        return $this
            ->belongsToMany('App\Project\Project', 'shared_projects', 'user_id', 'project_id')
            ->withTimestamps();
    }

    public static function createFactory($fields) {
        $user = new User();
        $user->email = $fields['email'];
        $user->password = bcrypt($fields['password']);
        $user->api_token = str_random(16);
        $user->chronology = Carbon::now();
        $user->counter = 0;
        $user->active = true;
        $user->fill($fields);
        $user->save();

        return $user;
    }

    public function getTotalThanksFromUser(User $user): int {
        $nonProjectThank = $this->thank
            ->where('target_user_id', '=', $this->id)
            ->where('user_id', '=', $user->id)->sum('value');

        $projectThank = $this->projectThank
            ->where('target_user_id', '=', $this->id)
            ->where('user_id', '=', $user->id)
            ->sum('value');

        return $nonProjectThank + $projectThank;
    }

    public function getTotalProjectThanksFromUser(User $user, Project $project): int {
        return $this->projectThank
            ->where('target_user_id', '=', $this->id)
            ->where('user_id', '=', $user->id)
            ->where('project_id', '=', $project->id)
            ->sum('value');
    }

    public function getTotalThanksToUser(User $user): int {
        $nonProjectThank = $this->thank
            ->where('target_user_id', '=', $user->id)
            ->where('user_id', '=', $this->id)->sum('value');

        $projectThank = $this->projectThank
            ->where('target_user_id', '=', $user->id)
            ->where('user_id', '=', $this->id)
            ->sum('value');

        return $nonProjectThank + $projectThank;
    }

    public function
    getTotalProjectThanksToUser(User $user, Project $project): int {
        return $this->projectThank
            ->where('target_user_id', '=', $user->id)
            ->where('user_id', '=', $this->id)
            ->where('project_id', '=', $project->id)
            ->sum('value');
    }
 }