<?php

namespace App\Like;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $fillable = [
        'user_id',
        'product_id',
        'value',
        'message'
    ];
}
