<?php

namespace App\Like;

use App\Events\ProductLiked;
use App\Events\ProductsChanged;
use App\Product\Product;
use App\User;

class LikeApi
{
    /**
     * @var Like
     */
    protected $like;
    protected $user;
    protected $product;

    /**
     * LikeApi constructor.
     * @param Like $like
     * @param User $user
     * @param Product $product
     */
    public function __construct(Like $like, User $user, Product $product)
    {
        $this->like = $like;
        $this->user = $user;
        $this->product = $product;
    }

    public function createLike($userId, $productId, int $value, $message = null)
    {
        if (!($this->user->find($userId) && $this->product->find($productId))) {
            return array('status' => false, 'error' => 'Use valid user/product id!');
        }

//        if ($this->product->find($productId)->user_id === $userId) {
//            return array('status' => false, 'error' => 'Users can not like their own products');
//        }

        $user = $this->user->find($userId);

        $lastLike = $this->like
            ->where('user_id', '=', $userId)
            ->where('product_id', '=', $productId)
            ->orderBy('id', 'desc')->first();

        $lastLikeValue = null;
        if($lastLike) {
            $lastLikeValue = $lastLike->value;
        }

        $like = $this->like->create([
            'user_id' => $userId,
            'product_id' => $productId,
            'value' => $value,
            'message' => $message
        ]);


        $currentLikeValue = $this->product
            ->where('id', '=', $productId)->first()->value;
        if($lastLikeValue) {
            $newLikeValue = $currentLikeValue - $lastLikeValue + $value;
        } else {
            $newLikeValue = $currentLikeValue + $value;
        }

        $product = $this->product->where('id', '=', $productId)->first();
        $like->total = $newLikeValue;

        $product->value = $newLikeValue;
        $product->like = $value;
        $product->save();


        event(new ProductLiked($like));
        event(new ProductsChanged($user, $product));

        return array('status' => true, 'like' => $like);
    }

    public function rebuildLikeValues() {
        $products = $this->product->all();

        $products->map(function(Product $product) {
            $totalLikes = $this->like->where('product_id', '=', $product->id)->sum('value');
            $product->value = $totalLikes;
            $product->save();
        });
    }

    public function getLikeById(int $likeId)
    {
        $like = $this->like->find($likeId);
        if (!$like) {
            return array('status' => false, 'error' => 'Invalid like id!');
        }
        return array('status' => true, 'like' => $like);
    }

    public function deleteLike(int $likeId)
    {
        $like = $this->like->find($likeId);
        if (!$like) {
            return array('status' => false, 'error' => 'Invalid like id!');
        }

        $like->delete();

        return array('status' => true);
    }
}