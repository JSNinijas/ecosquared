<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\ProductShared' => [
            'App\Listeners\CreateProductShareNotifications',
            'App\Listeners\UpdateUserAfterShareInteraction',
        ],
        'App\Events\ProjectShared' => [
            'App\Listeners\CreateProjectShareNotifications',
            'App\Listeners\UpdateUserAfterProjectShareInteraction'
        ],
        'App\Events\UserInvited' => [
            'App\Listeners\CreateUserInviteNotifications',
        ],
        'App\Events\UserThanked' => [
            'App\Listeners\CreateUserThankNotifications',
            'App\Listeners\UpdateUserAfterThankInteraction'
        ],
        'App\Events\UserProjectThanked' => [
            'App\Listeners\CreateUserProjectThankNotifications',
            'App\Listeners\UpdateUserAfterProjectThankInteraction'
        ],
        'App\Events\ProductLiked' => [
            'App\Listeners\CreateProductLikeNotifications',
            'App\Listeners\UpdateUserAfterLikeInteraction',
        ],
        'App\Events\ProjectHappied' => [
            'App\Listeners\CreateProjectHappyNotifications',
            'App\Listeners\UpdateUserAfterHappyInteraction'
        ],
        'App\Events\ProductSupported' => [
            'App\Listeners\CreateProductSupportNotifications',
            'App\Listeners\UpdateUserAfterSupportInteraction'
        ],
        'App\Events\ProjectSupported' => [
            'App\Listeners\CreateProjectSupportNotifications',
            'App\Listeners\UpdateUserAfterProjectSupportInteraction'
        ],
        'App\Events\ProductRevoked' => [
            'App\Listeners\CreateProductRevokeNotifications',
        ],
        'App\Events\ProjectRevoked' => [
            'App\Listeners\CreateProjectRevokeNotifications',
        ],
        'App\Events\ProductAccepted' => [
            'App\Listeners\UpdateUserAfterAcceptInteraction',
        ],
        'App\Events\ProjectAccepted' => [
            'App\Listeners\UpdateUserAfterProjectAcceptInteraction',
        ],
        'App\Events\ProductRejected' => [
            'App\Listeners\UpdateUserAfterRejectInteraction'
        ],
        'App\Events\ProjectRejected' => [
            'App\Listeners\UpdateUserAfterProjectRejectInteraction'
        ],
        'App\Events\IncomingNotificationCreated' => [
            'App\Listeners\UpdateUnseenAttributeAfterIncoming'
        ],
        'App\Events\OutgoingNotificationCreated' => [],
        'App\Events\IncomingNotificationChanged' => [
            'App\Listeners\UpdateUnseenAttributeAfterIncomingChanged'
        ],
        'App\Events\IncomingShareChanged' => [],
        'App\Events\OutgoingShareChanged' => [],
        'App\Events\FetchNewIncoming' => [],
        'App\Events\TransactionCreated' => [
            'App\Listeners\UserBalanceRecalculate'
        ],
        'App\Events\UserBalanceUpdated' => [],
        'App\Events\ProductCreated' => [],
        'App\Events\ProjectCreated' => [],

        'App\Events\ProductsChanged' => [],
        'App\Events\ProjectsChanged' => [],
        'App\Events\UsersChanged' => [],
        'App\Events\AddNewUserToFriendList' => [],
        'App\Events\TargetUserThankUpdated' => [],
        'App\Events\AddNewUserToAuthorFriendList' => [],
        'App\Events\AuthSqChanged' => []
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
