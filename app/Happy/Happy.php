<?php

namespace App\Happy;

use Illuminate\Database\Eloquent\Model;

class Happy extends Model
{
    protected $fillable = [
        'user_id',
        'project_id',
        'value',
        'message'
    ];
}