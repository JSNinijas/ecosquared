<?php

namespace App\Happy;

use App\Events\ProjectHappied;
use App\Events\ProjectsChanged;
use App\Happy\Happy;
use App\Project\Project;
use App\User;

class HappyApi
{
    /**
     * @var \App\Happy\Happy
     */
    protected $happy;
    protected $user;
    protected $project;

    /**
     * HappyApi constructor.
     * @param Happy $happy
     * @param User $user
     * @param Project $project
     */
    public function __construct(Happy $happy, User $user, Project $project)
    {
        $this->happy = $happy;
        $this->user = $user;
        $this->project = $project;
    }

    public function createHappy($userId, $projectId, int $value, $message = null)
    {
        if (!($this->user->find($userId) && $this->project->find($projectId))) {
            return array('status' => false, 'error' => 'Use valid user/project id!');
        }

//        if ($this->project->find($projectId)->user_id === $userId) {
//            return array('status' => false, 'error' => 'Users can not happy their own projects');
//        }

        $user = $this->user->find($userId);
        $project = $this->project->find($projectId);

        $lastHappy = $this->happy
            ->where('user_id', '=', $userId)
            ->where('project_id', '=', $projectId)
            ->orderBy('id', 'desc')->first();

        $lastHappyValue = null;
        if($lastHappy) {
            $lastHappyValue = $lastHappy->value;
        }

        $happy = $this->happy->create([
            'user_id' => $userId,
            'project_id' => $projectId,
            'value' => $value,
            'message' => $message
        ]);


        $count = $project->usersSharedWith->count() + 1;
        $happiedCount = $project->usersHappy->unique()->count();

        $currentHappyValue = $this->project
            ->where('id', '=', $projectId)->first()->total;
        if($lastHappyValue) {
            $newHappyValue = $currentHappyValue - $lastHappyValue + $value;
        } else {
            $newHappyValue = $currentHappyValue + $value;
        }

        $project = $this->project->where('id', '=', $projectId)->first();
        $happy->total = $newHappyValue;

        $project->total = $newHappyValue;
        $project->value = round($newHappyValue / $happiedCount, 2);
        $project->happy = $value;
        $project->save();

        event(new ProjectHappied($happy));
        event(new ProjectsChanged($user, $project));

        return array('status' => true, 'happy' => $happy);
    }

    public function rebuildHappyValues() {
        $projects = $this->project->all();

        $projects->map(function(Project $project) {
            $totalHappy = $this->happy->where('project_id', '=', $project->id)->sum('value');
            $project->total = $totalHappy;
            $count = $project->usersSharedWith->count() + 1;
            $happiedCount = $project->usersHappy->unique()->count();
            $project->value = round($totalHappy / $happiedCount, 2);
            $project->save();
        });
    }

    public function getHappyById(int $happyId)
    {
        $happy = $this->happy->find($happyId);
        if (!$happy) {
            return array('status' => false, 'error' => 'Invalid happy id!');
        }
        return array('status' => true, 'happy' => $happy);
    }

    public function deleteHappy(int $happyId)
    {
        $happy = $this->happy->find($happyId);
        if (!$happy) {
            return array('status' => false, 'error' => 'Invalid happy id!');
        }

        $happy->delete();

        return array('status' => true);
    }


}