<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NonUser extends Model
{
    protected $fillable = [
        'user_id', 'card', 'email', 'inviter_id', 'transaction_id',
        'message', 'card_id', 'amount', 'encrypted_email', 'unsubscribe'
    ];
}