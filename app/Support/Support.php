<?php

namespace App\Support;

use Illuminate\Database\Eloquent\Model;

class Support extends Model
{
    protected $fillable = [
        'user_id',
        'target_user_id',
        'product_id',
        'amount',
        'message'
    ];
}