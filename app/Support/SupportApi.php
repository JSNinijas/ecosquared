<?php

namespace App\Support;

use App\Events\ProductSupported;
use App\Events\ProjectSupported;
use App\Product\Product;
use App\Project\Project;
use App\User;
use App\Wallet\WalletApi;

class SupportApi
{
    protected $support;
    protected $wallet;
    protected $user;
    protected $product;
    protected $project;
    protected $projectSupport;

    /**
     * SupportApi constructor.
     * @param WalletApi $wallet
     * @param User $user
     * @param Product $product
     * @param Support $support
     * @param Project $project
     * @param ProjectSupport $projectSupport
     */
    function __construct(WalletApi $wallet, User $user, Product $product,
                         Support $support, Project $project, ProjectSupport $projectSupport)
    {
        $this->wallet = $wallet;
        $this->user = $user;
        $this->product = $product;
        $this->support = $support;
        $this->project = $project;
        $this->projectSupport = $projectSupport;
    }

    /**
     * @param int $userId
     * @param int $targetUserId
     * @param int $productId
     * @param int $amount
     * @param string|null $message
     * @return array
     */
    public function createSupport(int $userId, int $targetUserId, int $productId, int $amount, string $message = null) {

        if($this->wallet->fetchUserBalance($userId)['userBalance'] < $amount) {
            return array('status' => false, 'error' => 'Not enough credits to support!');
        }
        if(!($this->user->find($userId) && $this->user->find($targetUserId))) {
            return array('status' => false, 'error' => 'Invalid user id!');
        }
        if(!$this->product->find($productId)) {
            return array('status' => false, 'error' => 'Invalid product id!');
        }

        $user = $this->user->find($userId);
        $targetUser = $this->user->find($targetUserId);

        $isExistRelations = $user->users()
            ->where(['target_user_id' => $targetUser->id, 'user_id'=> $user->id])->first();

        if(!$isExistRelations){
            $user->users()->attach($targetUser->id);
            $targetUser->users()->attach($user->id);
        }

        $support = $this->support->create([
            'user_id' => $userId,
            'target_user_id' => $targetUserId,
            'product_id' => $productId,
            'amount' => $amount,
            'message' => $message
        ]);

        $this->wallet
            ->createTransaction('default', 'shared', -$amount, $userId, 1);
        $this->wallet
            ->createTransaction('default', 'received', $amount, $targetUserId, 1);

        event(new ProductSupported($support));

        return array('status' => true, 'support' => $support);
    }

    /**
     * @param int $userId
     * @param int $targetUserId
     * @param int $projectId
     * @param int $amount
     * @param string|null $message
     * @return array
     */
    public function createProjectSupport(int $userId, int $targetUserId, int $projectId, int $amount, string $message = null) {

        if($this->wallet->fetchUserBalance($userId)['userBalance'] < $amount) {
            return array('status' => false, 'error' => 'Not enough credits to support!');
        }
        if(!($this->user->find($userId) && $this->user->find($targetUserId))) {
            return array('status' => false, 'error' => 'Invalid user id!');
        }
        if(!$this->project->find($projectId)) {
            return array('status' => false, 'error' => 'Invalid project id!');
        }

        $user = $this->user->find($userId);
        $targetUser = $this->user->find($targetUserId);

        $isExistRelations = $user->users()
            ->where(['target_user_id' => $targetUser->id, 'user_id'=> $user->id])->first();

        if(!$isExistRelations){
            $user->users()->attach($targetUser->id);
            $targetUser->users()->attach($user->id);
        }

        $support = $this->projectSupport->create([
            'user_id' => $userId,
            'target_user_id' => $targetUserId,
            'project_id' => $projectId,
            'amount' => $amount,
            'message' => $message
        ]);

        $this->wallet
            ->createTransaction('default', 'shared', -$amount, $userId, 1);
        $this->wallet
            ->createTransaction('default', 'received', $amount, $targetUserId, 1);

        event(new ProjectSupported($support));

        return array('status' => true, 'support' => $support);
    }

    public function deleteSupport($supportId) {

        $support = $this->support->find($supportId);
        $support->delete();

        return $support;
    }

    public function deleteProjectSupport($supportId) {

        $support = $this->projectSupport->find($supportId);
        $support->delete();

        return $support;
    }

    public function supportedByUser($userId, $productId) {

        $support = $this->support
            ->where('user_id', '=', $userId)
            ->where('product_id', '=', $productId)
            ->sum('amount');

        return $support;
    }

    public function projectSupportedByUser($userId, $projectId) {

        $support = $this->projectSupport
            ->where('user_id', '=', $userId)
            ->where('project_id', '=', $projectId)
            ->sum('amount');

        return $support;
    }

    public function supportedByAllUsers($productId) {

        $support = $this->support
            ->where('product_id', '=', $productId)
            ->sum('amount');
        return $support;
    }

    public function projectSupportedByAllUsers($projectId) {

        $support = $this->projectSupport
            ->where('project_id', '=', $projectId)
            ->sum('amount');
        return $support;
    }
}