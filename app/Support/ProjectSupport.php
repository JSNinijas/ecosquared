<?php

namespace App\Support;

use Illuminate\Database\Eloquent\Model;

class ProjectSupport extends Model
{
    protected $fillable = [
        'user_id',
        'target_user_id',
        'project_id',
        'amount',
        'message'
    ];
}