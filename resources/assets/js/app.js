
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import './bootstrap';
import Vue from 'vue';
import VueRouter from 'vue-router';
import VeeValidate from 'vee-validate';
import MainComponent from './components/Main.vue';
import LoginComponent from './components/Login';
import RecoveryComponent from './components/Recovery';
import RegistrationComponent from './components/RegistrationForm';
import UnsubscribeComponent from './components/Unsubscribe';
import ErrorsComponent from './components/Errors';
import InfoMessagesComponent from './components/InfoMessages';
import WelcomeComponent from './components/Welcome';
import AdminComponent from './components/Admin';
import AppComponent from './components/App';
import PassportComponent from './components/passport/Passport.vue';
import RequestInviteComponent from './components/RequestInvite';
import vModal from 'vue-js-modal';
import SummaryComponent from './components/Summary';
import EventBus from './services/EventBus';
import bFormSlider from 'vue-bootstrap-slider';

import VueMaterial from 'vue-material';
import 'vue-material/dist/vue-material.css';
import datePicker from 'vue-bootstrap-datetimepicker';
import 'bootstrap/dist/css/bootstrap.css';
import 'eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css';
import 'bootstrap-slider/dist/css/bootstrap-slider.css';
// import 'vis'

window.Vue = Vue;
/*window.EventBus = new Vue();*/

window.onRecaptchaloadCallback = () => {
    EventBus.$emit('recaptcha:loaded');
};

/** Define some routes */

var routes = [
    { path: '/passport', component: PassportComponent },
];

AdminComponent.registerRoutes(routes);
AppComponent.registerRoutes(routes);
LoginComponent.registerRoutes(routes);
RegistrationComponent.registerRoutes(routes);
UnsubscribeComponent.registerRoutes(routes);
RecoveryComponent.registerRoutes(routes);
ErrorsComponent.registerRoutes(routes);
InfoMessagesComponent.registerRoutes(routes);
RequestInviteComponent.registerRoutes(routes);
WelcomeComponent.registerRoutes(routes);
SummaryComponent.registerRoutes(routes);

routes.push({ path: '/', redirect: '/welcome' });
routes.push({ path: '/*', redirect: '/' });

/** Create the router instance and pass the `routes` option */

const router = new VueRouter({
    routes
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.use(VueRouter);
Vue.use(VeeValidate);
Vue.use(vModal);
Vue.use(VueMaterial);
Vue.use(datePicker);
Vue.use(bFormSlider);

Vue.material.registerTheme({
    default: {
        primary: 'black',
        accent: 'black'
    },
    green: {
        primary: 'green',
        accent: 'pink'
    },
    orange: {
        primary: 'orange',
        accent: 'green'
    },
    white: {
        primary: 'white',
        accent: 'green'
    },
})

Vue.material.registerTheme('black', {
    primary: 'green',
    accent: 'white',
    warn: 'red',
    background: 'black'
});

Vue.material.registerTheme('green', {
    primary: 'green',
    accent: 'pink',
    warn: 'red',
    background: 'white'
});

Vue.component('app-main', MainComponent);

const app = new Vue({
    el: '#app',
    router
});
