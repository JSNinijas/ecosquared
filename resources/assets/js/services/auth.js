import EventBus from './EventBus';
import Api from './api';
import {Observable} from 'rxjs';
class Auth {

    constructor() {
        this.user = this.getUserFromStorage();
        this.loginObservers = [];
        this.logoutObservers = [];
        EventBus.$on(['users:update', 'users:patch'], (user) => {
            if(user.id === this.getUser().id) {
                this.getUserFromApi().then(user => {
                    this.setUser(user);
                });
            }
        });
    }

    getUserFromApi() {
        return new Promise((resolve, reject) => {
            Api.call('/api/user', 'get').then(res => {
                resolve(res.data);
            }, reject);
        })
    }

    getUser() {
        return this.user ? this.user : this.getUserFromStorage();
    }

    setUser(user) {
        this.logoutObservers.forEach((observer) => {
            observer.next(this.user);
        });
        this.user = user;
        this.setUserToStorage(user);
        this.loginObservers.forEach((observer) => {
            observer.next(user);
        });
        return user;
    }

    login(email, password) {
        return new Promise((resolve, reject) => {
            window.axios.post('/login', {'email':email, 'password': password}).then((res) => {
                localStorage.clear();
                location.reload();
                this.setUser(res.data);
                EventBus.$emit('login:success', res.data);
                resolve(res.data);
            }, (x) => {
                reject(x.response.data);
            })
        })
    }

    logout() {
        localStorage.clear();
        window.location.reload();
    }

    getUserFromStorage() {
        return localStorage.getItem('User') ? JSON.parse(localStorage.getItem('User')) : false;
    }

    setUserToStorage(user) {
        return localStorage.setItem('User', JSON.stringify(user));
    }

    check() {
        return !!this.getUser();
    }

    hasRole(name) {
      return !!this.getUser()
      && this.getUser().roles
      && this.getUser().roles.find(r => r.name === name)
    }

    onLogin() {
        return new Observable((observer) => {
            this.loginObservers.push(observer);
        })
    }

    onLogout() {
        return new Observable((observer) => {
            this.logoutObservers.push(observer);
        })
    }
}

export default new Auth;
