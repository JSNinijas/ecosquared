const DEFAULT_APPEAR_ANIMATION = 'slide';
class ModalAnimationCoordinator {

    constructor() {
        this.nextAppearAnimation = DEFAULT_APPEAR_ANIMATION;
    }

    setNextAppearAnimation(name) {
        this.nextAppearAnimation = name;
    }

    getNextAppearAnimation() {
        return this.nextAppearAnimation;
    }

    resetAppearAnimation() {
        this.nextAppearAnimation = DEFAULT_APPEAR_ANIMATION;
    }
}

export default new ModalAnimationCoordinator;