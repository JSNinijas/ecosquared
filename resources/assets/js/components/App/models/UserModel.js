import Api from '../../../services/api';
import EventBus from '../../../services/EventBus';
import Resource from '../../../interfaces/Resource';

class User extends Resource {

    constructor() {
        super();
        this.basePath = '/api/app/';
        this.resourceName = 'users';
        this.transactionPath = '/api/payment/';
        this.data = null;
        this.length = null;
        this.balance = null;
        this.userTotalBalance = null;
        this.user = null;

        this.channels = [
            {
                name: 'user',
                events: [
                    {
                        name: 'UserBalanceUpdated',
                        prop: 'userTotalBalance',
                        handler: (e) => {
                            this.userTotalBalance = parseFloat(e.target.total_balance);
                        }
                    },
                    {
                        name: 'AuthSqChanged',
                        prop: 'user',
                        handler: (e) => {
                            this.user = e.target;
                        }
                    },
                    {
                        name: 'TargetUserThankUpdated',
                        prop: 'targetUser',
                        handler: (e) => {
                            let updatebleUser = _.find(this.data.users, i => i.id === e.target.id);
                            _.assign(updatebleUser, e.target);
                            EventBus.$emit('usersChanged', true)
                        }
                    },
                    {
                        name: 'AddNewUserToFriendList',
                        prop: 'targetUser',
                        handler: (e) => {
                            let checkNewUser = _.find(this.data.users, i => e.target.id !== i.id);
                            if(!checkNewUser) this.data.users.unshift(e.target);

                        }
                    },
                    {
                        name: 'AddNewUserToAuthorFriendList',
                        prop: 'targetUser',
                        handler: (e) => {
                            let checkNewUser = _.find(this.data.users, i => e.target.id !== i.id);
                            if (!checkNewUser) this.data.users.unshift(e.target);
                        }
                    },
                ]
            },
        ];

        this.listenSockets();
    }

    getBalance() {
        return new Promise((resolve, reject) => {
            if (this.userTotalBalance !== null) resolve(this.userTotalBalance);

            if(this.userTotalBalance === null){
                Api.call(this.basePath + 'total-balance', 'get').then((res) => {
                    this.userTotalBalance = parseFloat(res.data.userBalance);
                    resolve(this.userTotalBalance);
                }, (x) => {
                    reject(x.response);
                })
            }
        })
    }

    thanks(userId, targetUserId, value, message) {
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + 'thanks', 'post', {}, {userId, targetUserId, value, message}).then((res) => {
                EventBus.$emit('thanks:created', res.data);
                resolve(res.data);
            }, (x) => {
                reject(x.response.data);
            })
        })
    }

    changePass(oldPassword, newPassword){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + 'password', 'patch', {}, {oldPassword, newPassword}).then((res) => {
                resolve(res.data);
            }, (x) => {
                reject(x.response.data);
            })
        })
    }

    shareList(id, entity, limit, sort, filter_groups,){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + '/share/' + `${entity}/${id}`,
                'post', {_method: "GET"}, {sort, filter_groups, limit}).then((res) => {
                resolve(res.data);
            }, (x) => {
                reject(x.response.data);
            })
        })
    }

    fetchBalance() {
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + 'total-balance', 'get').then((res) => {
                this.balance = res.data.userBalance*1;
                resolve(this.balance);
            }, (x) => {
                reject(x.response.data);
            })
        })
    }

    fetchTransactions() {
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + 'total-balance/types', 'get').then((res) => {
                resolve(res.data);
            }, (x) => {
                reject(x.response.data);
            })
        })
    }

    buyCredits(email, credits){
        return new Promise((resolve, reject) => {
            Api.call(this.transactionPath + 'express-checkout', 'post', {}, {email, credits}).then((res) => {
                resolve(res.data);
            }, (x) => {
                reject(x.response.data);
            })
        })
    }

    getUserAfterBuyCredits(id) {
        return new Promise((resolve, reject) => {
            Api.call(this.transactionPath + `success/${id}`, 'get').then((res) => {
                resolve(res.data);
            }, (x) => {
                reject(x.response.data);
            })
        })
    }

    recovery(email) {
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + 'recovery', 'post', {}, {email}).then((res) => {
                resolve(res.data);
            }, (x) => {
                reject(x.response.data);
            })
        })
    }

    recoveryHash(hash) {
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + `recovery/${hash}`, 'get').then((res) => {
                resolve(res.data);
            }, (x) => {
                reject(x.response.data);
            })
        })
    }

    createNewPassword(email, password) {
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + `recovery/password`, 'post', {}, {email, password}).then((res) => {
                resolve(res.data);
            }, (x) => {
                reject(x.response.data);
            })
        })
    }

    sortByProjects(id, params={}){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + `project-thanks/projects/${id}`,
                'post', {_method: "GET"}, {sort: params.sort,filter_groups: params.filter_groups, limit: params.limit})
                .then(resp => {
                resolve(resp.data)
            }, rej => reject(rej.response.data))
        });
    }

    getThankByProject(projectId, userId){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + `project-thanks/thanks/${projectId}`, 'post', {_method: "GET"}, {userId}).then((res) => {
                resolve(res.data);
            }, (x) => {
                reject(x.response.data);
            })
        })
    }

    thanksByProject(userId, targetUserId, value, message, projectId){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + 'project-thanks', 'post', {}, {userId, targetUserId, value, message, projectId})
                .then((res) => {
                    EventBus.$emit('thanks:created', res.data);
                    resolve(res.data);
                }, (x) => {
                    reject(x.response.data);
                }
            )
        })
    }

    filterByEntityId(id, params={}) {
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + 'projects' + `/${id}` + '/usership', 'post', {_method: "GET"},
                {sort: params.sort, filter_groups: params.filter_groups, limit: params.limit}
            ).then(resp => {
                resolve(resp.data)
            }, rej => reject(rej.response.data))
        });
    }

    usershipKnown(id, params={}) {
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + 'projects' + `/${id}` + '/usership/known', 'post', {_method: "GET"},
                {sort: params.sort,  filter_groups: params.filter_groups, limit: params.limit}
            ).then(resp => {
                resolve(resp.data)
            }, rej => reject(rej.response.data))
        });
    }

    usershipUnknown(id, params={}) {
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + 'projects' + `/${id}` + '/usership/unknown', 'post', {_method: "GET"},
                {sort: params.sort,  filter_groups: params.filter_groups, limit: params.limit}
            ).then(resp => {
                resolve(resp.data)
            }, rej => reject(rej.response.data))
        });
    }
}

export default new User();