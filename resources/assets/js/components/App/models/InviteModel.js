import Api from '../../../services/api';
import EventBus from '../../../services/EventBus';
import Resource from '../../../interfaces/Resource';

'use strict';

class Invite extends Resource {

    constructor() {
        super();
        this.basePath = '/api/app/';
        this.resourceName = 'invite';
    }

    productsInvite(invite){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName, 'post', {}, this.processResource(invite, 'post')).then(resp => {
                resolve(resp)
            }, (rej) => {
                reject(rej.response);
            });
        });
    }

    projectsInvite(invite){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + '/project', 'post', {}, this.processResource(invite, 'post')).then(resp => {
                resolve(resp)
            }, (rej) => {
                reject(rej.response);
            });
        });
    }

    getByHash(hash){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + `/register/${hash}`, 'get', {}).then((res) => {
                resolve(res.data);
            }, (x) => {
                reject(x.response.data);
            })
        })
    }

    registration(data){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + `/register`, 'post', {}, data).then((res) => {
                resolve(res.data);
            }, (x) => {
                reject(x.response.data);
            })
        })
    }

    unsubscribe(hash){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + `/unsubscribe`, 'post', {}, {hash:hash}).then((res) => {
                resolve(res.data);
            }, (x) => {
                reject(x.response.data);
            })
        })
    }

    inviteRevoke(id){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + `/revoke/${id}`, 'post', {_method: 'PATCH'}, {})
                .then((res) => {
                    resolve(res.data);
                }, (x) => {
                reject(x.response.data);
            })
        })
    }

    request(email, message){
        return new Promise((resolve, reject) => {
            Api.call('/api/' + this.resourceName + '/request', 'post', {}, {email, message})
                .then((res) => {
                    resolve(res.data);
                }, (x) => {
                    reject(x.response.data);
                })
        })
    }
}

export default new Invite();

