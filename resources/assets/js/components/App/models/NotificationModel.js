import Api from '../../../services/api';
import Auth from '../../../services/auth';
import EventBus from '../../../services/EventBus';
import Resource from '../../../interfaces/Resource';

class Notification extends Resource {

    constructor() {
        super();
        this.basePath = '/api/app/';
        this.resourceName = 'notifications';
        this.notiPath = '/api/notification/';

        this.outgoing = [];
        this.incoming = [];

        this.newIncoming = null;
        this.offers = null;
        this.outgoingOffers = null;

        this.channels = [
            {
                name: 'incoming_notification',
                prop: 'incoming',
                events: [
                    {
                        name: 'IncomingNotificationCreated',
                        handler: (e) => {
                            let createdNotification = _.find(this.incoming, i => i.id === e.target.id);
                            if(!createdNotification){
                                this.incoming.unshift(e.target);
                            }
                        }
                    },
                    {
                        name: 'IncomingNotificationDeleted',
                        handler: (e) => {
                            _.remove(this.incoming, i => i.id === e.target);
                            this.incoming.splice(this.incoming.length);
                        }
                    },
                    {
                        name: 'IncomingNotificationChanged',
                        handler: (e) => {
                            let updatableNotification = _.find(this.incoming, i => i.id === e.target.id);
                            _.assign(updatableNotification, e.target);
                        }
                    },
                    {
                        name: 'FetchNewIncoming',
                        prop: 'newIncoming',
                        handler: (e) => {
                            this.newIncoming = e.target;
                        }
                    },
                    {
                        name: 'IncomingShareChanged',
                        prop: 'offers',
                        handler: (e) => {
                            this.offers = e.target;
                        }
                    },
                ]
            },
            {
                name: 'outgoing_notification',
                prop: 'outgoing',
                events: [
                    {
                        name: 'OutgoingNotificationCreated',
                        handler: (e) => {
                            let createdNotification = _.find(this.outgoing, i => i.id === e.target.id);
                            if(!createdNotification){
                                this.outgoing.unshift(e.target);
                            }
                        }
                    },
                    {
                        name: 'OutgoingNotificationDeleted',
                        handler: (e) => {
                            _.remove(this.outgoing, i => i.id === e.target);
                            this.outgoing.splice(this.outgoing.length);
                        }
                    },
                    {
                        name: 'OutgoingShareChanged',
                        prop: 'outgoingOffers',
                        handler: (e) => {
                            this.outgoingOffers = e.target;
                        }
                    }
                ]
            }
        ];
        this.listenSockets();
    }

    fetchIncoming(params={}){
        return this._fetch(params, 'incoming');
    }

    fetchOutgoing(params={}){
        return this._fetch(params, 'outgoing');
    }

    _fetch(params, entity) {
        return new Promise((resolve, reject) => {
            Api.call(`${this.basePath}${this.resourceName}/${entity}`, 'post',
                {_method: "GET"}, {filter_groups: params.sort, limit: params.limit}).then((res) => {
                this[entity] = res.data.notifications;
                resolve({
                    notifications: this[entity],
                    count: res.data.count,
                    filterCount: res.data.filterCount,
                    limitCount: res.data.limitCount,
                });
            }, (x) => {
                console.log('error', x);
                reject(x.response.data);
            })
        })
    }
    fetchUnseenIncoming(){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + '/incoming/count', 'get').then((res) => {
                this.newIncoming = res.data;
                this.offers = res.data.offers;
                this.outgoingOffers = res.data.outgoingOffers;
                resolve(this.newIncoming);
            }, (x) => {
                console.log('error', x);
                reject(x.response.data);
            })
        })
    }
    fetchById(type, id){
        return new Promise((resolve, reject) => {
            Api.call(this.notiPath + type + `/${id}`, 'get').then((res) => {
                resolve(res.data);
            }, (x) => {
                console.log('error', x);
                reject(x.response.data);
            })
        })
    }
    changeSettings(settings){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + '/settings', 'post', {_method: "PATCH"}, settings).then((res) => {
                resolve(res.data);
            }, (x) => {
                console.log('error', x);
                reject(x.response.data);
            })
        })
    }
    fetchSettings(settings){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + '/settings', 'get').then((res) => {
                resolve(res.data);
            }, (x) => {
                console.log('error', x);
                reject(x.response.data);
            })
        })
    }

    fetchOfferHistory(params={}){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + '/offer-history', 'post',
                {_method: "GET"}, {filter_groups: params.filter_groups, limit: params.limit, sort: params.sort})
                .then((res) => {
                resolve(res.data);
            }, (x) => {
                console.log('error', x);
                reject(x.response.data);
            })
        })
    }
}

export default new Notification();