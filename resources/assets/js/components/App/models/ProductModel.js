import Api from '../../../services/api';
import EventBus from '../../../services/EventBus';
import Resource from '../../../interfaces/Resource';

class Product extends Resource {

    constructor() {
        super();
        this.basePath = '/api/app/';
        this.resourceName = 'products';
        this.length = null;
        this.data = null;

        this.channels = [
            {
                name: 'product',
                prop: 'product',
                events: [
                    {
                        name: 'ProductsChanged',
                        handler: (e) => {
                            let changedProduct = _.find(this.data.products, i => i.id === e.target.id);
                            _.assign(changedProduct, e.target);
                            EventBus.$emit('productsChanged', true)
                        }
                    },
                    {
                        name: 'ProductCreated',
                        handler: (e) => {
                            let ifProduct = _.find(this.data.products, i => i.id === e.target.id);
                            if(!ifProduct) this.data.products.unshift(e.target);
                        }
                    },
                ]
            },
        ];

        this.listenSockets();
    }

    usership(sort, productId, limit, filter_groups){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + `/${productId}/users`,
                'post', {_method: "GET"}, {sort, limit, filter_groups})
                .then((res) => {
                    resolve(res.data);
                }, (x) => {
                    console.log('error', x);
                    reject(x.response.data);
                }
            )
        })
    }

    value(userId, productId, value, message) {
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + 'likes', 'post', {}, {userId, productId, value, message}).then((res) => {
                resolve(res.data);
            }, (x) => {
                console.log('error', x);
                reject(x.response.data);
            })
        })
    }
    support(targetUserId, productId, amount, message){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + '/' + 'support',
                'post', {}, {targetUserId, productId, amount, message})
                .then((res) => {
                    EventBus.$emit('support:created', res.data);
                    resolve(res.data);
            }, (x) => {
                console.log('error', x);
                reject(x.response.data);
            })
        })
    }
    share(targetUserId, productId, amount, message, entityFlag){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + '/' + 'share',
                'post', {}, {targetUserId, productId, amount, message, entityFlag})
                .then((res) => {
                    EventBus.$emit('share:created', res.data);
                    resolve(res.data);
                }, (x) => {
                    console.log('error', x);
                    reject(x.response.data);
                })
        })
    }
    shareList(id, entity, limit, sort, filter_groups){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + '/share' + `/${id}`, 'post',
                {_method: "GET"}, {sort, filter_groups, limit})
                .then((res) => {
                    resolve(res.data);
                }, (x) => {
                    reject(x.response.data);
                }
            )
        })
    }
    revoke(notificationId, shareId){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + '/revoke/' + shareId, 'patch', {}, {notificationId})
                .then(res => {
                    resolve(res.data);
            }, x => {
                reject(x.response.data);
            })
        })
    }
    accept(notificationId, shareId){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + '/share/accept/' + shareId, 'patch', {}, {notificationId})
                .then(res => {
                    resolve(res.data);
            }, x => {
                reject(x.response.data);
            })
        })
    }
    reject(notificationId, shareId){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + '/share/reject/' + shareId, 'patch', {}, {notificationId})
                .then(res => {
                    resolve(res.data);
            }, x => {
                reject(x.response.data);
            })
        })
    }
}

export default new Product();