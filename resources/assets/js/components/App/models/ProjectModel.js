import Resource from '../../../interfaces/Resource';
import Api from '../../../services/api';
import EventBus from '../../../services/EventBus';

class Project extends Resource {

    constructor() {
        super();
        this.basePath = '/api/app/';
        this.resourceName = 'projects';
        this.length = null;
        this.data = null;

        this.channels = [
            {
                name: 'project',
                prop: 'project',
                events: [
                    {
                        name: 'ProjectsChanged',
                        handler: (e) => {
                            let changedProject = _.find(this.data.projects, i => i.id === e.target.id);
                            _.assign(changedProject, e.target);
                            EventBus.$emit('projectsChanged', true)
                        }
                    },
                    {
                        name: 'ProjectCreated',
                        handler: (e) => {
                            let ifProject = _.find(this.data.projects, i => i.id === e.target.id);
                            if(!ifProject) this.data.projects.unshift(e.target);
                        }
                    },
                ]
            },
        ];

        this.listenSockets();
    }

    usership(sort,projectId, limit, filter_groups){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + `/${projectId}/users`,
                'post', {_method: "GET"}, {sort, limit, filter_groups})
                .then((res) => {
                    resolve(res.data);
                }, (x) => {
                    console.log('error', x);
                    reject(x.response.data);
                }
            )
        })
    }

    value(userId, projectId, value, message) {
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + 'happies', 'post', {}, {userId, projectId, value, message}).then((res) => {
                resolve(res.data);
            }, (x) => {
                console.log('error', x);
                reject(x.response.data);
            })
        })
    }

    support(targetUserId, projectId, amount, message){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + '/' + 'support',
                'post', {}, {targetUserId, projectId, amount, message})
                .then((res) => {
                    EventBus.$emit('support:created', res.data);
                    resolve(res.data);
                }, (x) => {
                    console.log('error', x);
                    reject(x.response.data);
                })
        })
    }

    share(targetUserId, projectId, amount, message, entityFlag){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + '/' + 'share',
                'post', {}, {targetUserId, projectId, amount, message, entityFlag})
                .then((res) => {
                    EventBus.$emit('share:created', res.data);
                    resolve(res.data);
                }, (x) => {
                    console.log('error', x);
                    reject(x.response.data);
                })
        })
    }

    shareList(id, entity, limit, sort, filter_groups){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + '/share' + `/${id}`, 'post',
                {_method: "GET"}, {sort, filter_groups, limit})
                .then((res) => {
                        resolve(res.data);
                    }, (x) => {
                        reject(x.response.data);
                    }
                )
        })
    }

    revoke(notificationId, shareId){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + '/revoke/' + shareId, 'patch', {}, {notificationId})
                .then(res => {
                    resolve(res.data);
                }, x => {
                    reject(x.response.data);
                })
        })
    }

    accept(notificationId, shareId){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + '/share/accept/' + shareId, 'patch', {}, {notificationId})
                .then(res => {
                    resolve(res.data);
                }, x => {
                    reject(x.response.data);
                })
        })
    }

    reject(notificationId, shareId){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + '/share/reject/' + shareId, 'patch', {}, {notificationId})
                .then(res => {
                    resolve(res.data);
                }, x => {
                    reject(x.response.data);
                })
        })
    }

    sortUsers(params={}){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + `/usership`, 'post', {_method: "GET"},
                {sort:params.sort,filter_groups: params.filter_groups, limit: params.limit,})
                .then(resp => {
                        resolve(resp.data)
                    }, rej => reject(rej.response.data)
                )
        });
    }

    //users as authors---------------------------------------------------------------------
    usersAsAuthorsList(id, params={}){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + `/${id}/owners/add`, 'post', {_method: "GET"},
                {filter_groups: params.filter_groups, limit: params.limit,}
            ).then(resp => {
                resolve(resp.data)
            }, rej => reject(rej.response.data))
        });
    }

    owners(id){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + `/${id}/owners`, 'post', {_method: "GET"}, {}).then(resp => {
                resolve(resp.data)
            }, rej => reject(rej.response.data))
        });
    }

    addOwners(projectId, ownersId, authorStatus){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + `/owners`, 'post', {}, {projectId, ownersId, authorStatus}).then(resp => {
                resolve(resp.data)
            }, rej => reject(rej.response.data))
        });
    }

    deleteOwners(projectId, ownerId){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + `/${projectId}/owners/${ownerId}`,
                'delete', {}, {}).then(resp => {
                resolve(resp.data)
            }, rej => reject(rej.response.data))
        });
    }

    //projects as authors------------------------------------------------------------
    projectsAsAuthorList(id, params={}){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + `/${id}/projects/add`, 'post', {_method: "GET"},
                {filter_groups: params.filter_groups, limit: params.limit,}
            ).then(resp => {
                resolve(resp.data)
            }, rej => reject(rej.response.data))
        });
    }

    fetchProjectAuthor(id){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + `/${id}/projects`, 'post', {_method: "GET"}, {}).then(resp => {
                resolve(resp.data)
            }, rej => reject(rej.response.data))
        });
    }

    addProjectAsAuthor(projectId, authorId, authorStatus){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + `/projects`, 'post', {}, {projectId, authorId, authorStatus}).then(resp => {
                resolve(resp.data)
            }, rej => reject(rej.response.data))
        });
    }

    deleteProjectAuthor(projectId, authorId){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + `/${projectId}/projects/${authorId}`,
                'delete', {}, {}).then(resp => {
                resolve(resp.data)
            }, rej => reject(rej.response.data))
        });
    }

}

export default new Project();