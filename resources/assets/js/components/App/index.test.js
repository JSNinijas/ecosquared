// import Vue from 'vue'
// import Auth from '../../services/auth';
// import Routes from './index'
// import LoginComponent from '../Login';
// import VeeValidate from 'vee-validate';
// import VueMaterial from 'vue-material';
// import VModal from 'vue-js-modal';
// import VueRouter from 'vue-router';
// import sinon from 'sinon';
//
//
// window.$ = require('jquery');
//
//
// describe('App Component ', () => {
//     Vue.use(VeeValidate);
//     Vue.use(VueMaterial);
//     Vue.use(VModal);
//     Vue.use(VueRouter);
//
//     var user = {name: 'name'};
//     var authStub = sinon.stub(Auth, 'getUser');
//     authStub
//         .onFirstCall()
//         .returns(false)
//         .returns(user);
//
//     var routes = [];
//
//     Routes.registerRoutes(routes);
//
//     const router = new VueRouter({routes});
//
//     const vm = new Vue({
//         el: document.createElement('div'),
//         template: '<div><router-view></router-view></div>',
//         router
//     }).$mount();
//
//     beforeEach(() => {
//         router.push(Routes.config().redirectPath);
//     });
//
//     // it('Should redirect to login when no auth', (done) => {
//     //     Vue.nextTick(() => {
//     //         expect(vm.$route.fullPath).toBe(LoginComponent.config().redirectPath);
//     //         done();
//     //     })
//     // });
//
//
//     // it('Should redirect to app when auth', (done) => {
//     //
//     //     Vue.nextTick(() => {
//     //         expect(vm.$children.length).toBeGreaterThan(0);
//     //         expect(vm.$route.fullPath).toBe(AppComponent.config().components.home.redirectPath);
//     //         done();
//     //     })
//     // });
//
//     // it('Should have initial data', (done) => {
//     //     Vue.nextTick(() => {
//     //         expect(typeof vm.$children[0].App).toBe('object');
//     //         expect(typeof vm.$children[0].Welcome).toBe('object');
//     //         done();
//     //     });
//     // });
// });
