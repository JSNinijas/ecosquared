// import Vue from 'vue'
// import UserInformationComponent from './UserInformationComponent.vue'
// import VueMaterial from 'vue-material';
// import VModal from 'vue-js-modal';
// import VueRouter from 'vue-router';
// import EventBus from '../../../services/EventBus'
//
// window.$ = require('jquery');
//
//
// describe('User Information Component ', () => {
//     Vue.use(VueMaterial);
//     Vue.use(VModal);
//     Vue.use(VueRouter);
//
//     it('has the correct default watch', () => {
//         expect(typeof UserInformationComponent.watch).toBe('object')
//     });
//
//     it('has the correct default components', () => {
//         expect(typeof UserInformationComponent.components).toBe('object');
//
//         let defaultComponents = UserInformationComponent.components;
//         expect(typeof defaultComponents.Modals).toBe('object');
//     });
//
//     it('has correct default methods', () => {
//         expect(typeof UserInformationComponent.methods).toBe('object');
//         let defaultMethods = UserInformationComponent.methods;
//         expect(typeof defaultMethods.init).toBe('function');
//     });
//
//     it('has a mounted hook', () => {
//         expect(typeof UserInformationComponent.mounted).toBe('function');
//     });
//
//
// });
