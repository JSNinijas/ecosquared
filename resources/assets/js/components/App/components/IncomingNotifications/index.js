import EntityComponent from '../../components/EntityComponent.vue';
import ActionComponent from '../../components/ActionComponent.vue';
import CreateComponent from '../../components/CreateComponent.vue';
import IncomingNotificationsComponent from './IncomingNotification';
import ShareSortingComponent from '../../components/ShareSortingComponent.vue';
import ProjectThankComponent from '../../components/ProjectThankComponent.vue';
import SortingActionComponent from '../../components/SortingActionComponent.vue';
import UserInformationComponent from '../../components/UserInformationComponent.vue';
import NotificationActionComponent from '../../components/NotificationActionComponent.vue';
export default {
    registerRoutes(routes){
        routes.children.push(
            {
                path: 'incoming-notifications',
                name: 'incomingNotification',
                component: IncomingNotificationsComponent,
                children: [
                    //-------------------show notification by id
                    {
                        path: ':entity/:id([0-9]*)',
                        name: 'incomingNotificationEntity',
                        component: EntityComponent,
                    },
                    //------------------thank like happy usership----------------------------
                    {
                        path: ':entity([users|products|projects]*)/:id([0-9]*)/:action',
                        name: 'incomingNotificationEntityAction',
                        component: ActionComponent,
                        children: [
                            {
                                path: 'limit=:limit',
                                name: 'incomingNotificationActionMore',
                                component: ActionComponent,
                                children: [
                                    {
                                        path: 'search=:query',
                                        name: 'incomingNotificationActionSearch',
                                        component: ActionComponent,
                                    }
                                ]
                            },

                        ]
                    },
                    //--------------------create component------------------------------
                    {
                        path: ':entity/:action([create]*)/:step([0-9]*)',
                        name: 'incomingNotificationCreate',
                        component: CreateComponent,
                    },
                    //------------------------unknown-----------------------
                    {
                        path: ':type([incoming|outgoing]*)/:id([0-9]*)/:action/:entity',
                        name: 'incomingNotificationAction',
                        component: NotificationActionComponent,
                    },
                    //---------------------------user-setting--buy balance-------------
                    {
                        path: ':type',
                        name: 'incomingNotificationUserInformation',
                        component: UserInformationComponent,
                    },
                    //----------------back share list -----------------------------
                    {
                        path: ':entity/:id([0-9]*)/:action/:sortEntity([a-zA-Z]*)/limit=:limit',
                        name: 'incomingNotificationShareSorting',
                        component: ShareSortingComponent,
                        children: [
                            {
                                path: 'search=:query',
                                name: 'incomingNotificationShareSortingSearch',
                                component: ShareSortingComponent,
                            }
                        ]
                    },
                    //--------------------share list action-------------------------
                    {
                        path: ':entity/:id([0-9]*)/:action/:sortEntity/:sortEntityId([0-9]*)',
                        name: 'incomingNotificationSortingAction',
                        component: SortingActionComponent,
                    },
                    //-----------------------thank by project--------------------------------
                    {
                        path: ':entity(users)/:id([0-9]*)/:action(thank)/:sortEntity(projects)/:projectId([0-9]*)',
                        name: 'incomingNotificationProjectThank',
                        component: ProjectThankComponent,
                    },
                ]
            },
        )
    }
}