import HomeComponent from '../../components/Home/Home.vue';
import EntityComponent from '../../components/EntityComponent.vue';
import InfoMessagesComponent from '../../../InfoMessages/InfoMessagesComponent.vue';
import AddItemsSortingListComponent from '../EditAddItemsListComponent.vue';
import ActionComponent from '../../components/ActionComponent.vue';
import ShareSortingComponent from '../../components/ShareSortingComponent.vue';
import SortingActionComponent from '../../components/SortingActionComponent.vue';
import ProjectThankComponent from '../../components/ProjectThankComponent.vue';
import CreateComponent from '../../components/CreateComponent.vue';
import EditComponent from '../../components/EditComponent.vue';
import InviteActionComponent from '../../components/InviteActionComponent.vue';
import InviteSortingComponent from '../../components/InviteSortingComponent.vue';
import UserInformationComponent from '../../components/UserInformationComponent.vue';
export default {
    registerRoutes(routes){
        routes.children.push(
            {
                path: 'home',
                name: 'home',
                component: HomeComponent,
                children: [
                    //----------------users project product ------------------------------------
                    {
                        path: ':entity/:id([0-9]*)/:projectId([0-9]*)?',
                        name: 'homeEntity',
                        component: EntityComponent,
                    },
                    //--------------------------unknown-----------------
                    {
                        path: 'messages/:message',
                        name: 'homeMessages',
                        component: InfoMessagesComponent,
                    },
                    //-----add owners----
                    {
                        path: ':entity(projects)/:id([0-9]*)/:action(add-items-sorting-list)/:sortEntity/limit=:limit',
                        name: 'homeAddItemsSortingList',
                        component: AddItemsSortingListComponent,
                        children: [
                            {
                                path: 'search=:query',
                                name: 'homeAddItemsSortingListSearch',
                                component: AddItemsSortingListComponent,
                            },
                        ]
                    },
                    //----------------thank--happy--like--usership-on-summary--------
                    {
                        path: ':entity/:id([0-9]*)/:action',
                        name: 'homeAction',
                        component: ActionComponent,
                        children: [
                            {
                                path: 'limit=:limit',
                                name: 'homeActionMore',
                                component: ActionComponent,
                                children: [
                                    {
                                        path: 'search=:query',
                                        name: 'homeActionSearch',
                                        component: ActionComponent,
                                    },
                                ]
                            },
                        ]
                    },
                    //----------------share-sorting-list---------------------------
                    {
                        path: ':entity/:id([0-9]*)/:action(share-sorting-list)/:sortEntity/limit=:limit',
                        name: 'homeShareSorting',
                        component: ShareSortingComponent,
                        children: [
                            {
                                path: 'search=:query',
                                name: 'homeShareSortingSearch',
                                component: ShareSortingComponent,
                            }
                        ]
                    },
                    //-----------------------share modal--------------------------------
                    {
                        path: ':entity/:id([0-9]*)/:action(share)/:sortEntity/:sortEntityId([0-9]*)',
                        name: 'homeSortingAction',
                        component: SortingActionComponent,
                    },
                    //-----------------------thank by project--------------------------------
                    {
                        path: ':entity(users)/:id([0-9]*)/:action(thank)/:sortEntity(projects)/:projectId([0-9]*)',
                        name: 'homeProjectThank',
                        component: ProjectThankComponent,
                    },
                    //----------------buy balance--settings--------------------
                    {
                        path: ':type',
                        name: 'homeUserInformation',
                        component: UserInformationComponent,
                    },
                    //-----------------create --------------------------
                    {
                        path: ':entity/:action([create]*)/:step([0-9]*)',
                        name: 'homeCreate',
                        component: CreateComponent,
                    },
                    //-------------------edit------------------------------
                    {
                        path: ':entity/:id([0-9]*)/:action(edit)/:step',
                        name: 'homeEdit',
                        component: EditComponent,
                    },
                    //----------------invite-list----------------------
                    {
                        path: 'invite-sorting-list/:entity/limit=:limit',
                        name: 'homeInviteSorting',
                        component: InviteSortingComponent,
                        children: [
                            {
                                path: 'search=:query',
                                name: 'homeInviteSortingSearch',
                                component: InviteSortingComponent,
                            }
                        ]
                    },
                    //-----------------------------invite -modal-----------------
                    {
                        path: 'invite-sorting-list/:entity/:id([0-9]*)/:action([a-zA-Z]*)',
                        name: 'homeInviteActionComponent',
                        component: InviteActionComponent,
                    },
                ]
            },
        )
    }
}