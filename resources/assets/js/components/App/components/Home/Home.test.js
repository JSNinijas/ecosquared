// import Vue from 'vue'
// import Home from './Home.vue';
// import Auth from '../../../../services/auth';
// import sinon from 'sinon';
//
// window.$ = require('jquery');
//
// describe('Home Component ', () => {
//
//     const user = {name: 'admin'};
//     sinon.stub(Auth, 'getUser').returns(user);
//
//     const vm = new Vue(Home).$mount();
//
//     it('Should render its content', (done) => {
//         Vue.nextTick(() => {
//             expect(vm.$el.textContent).toBeDefined();
//             done();
//         });
//     });
//
//     it('Should have user', (done) => {
//         Vue.nextTick(() => {
//             expect(vm.user).toEqual(user);
//             done();
//         })
//     });
//
//     it('Should have components', (done)=> {
//         Vue.nextTick(() => {
//             expect(typeof Home.components).toBe('object');
//             done();
//         })
//     });
//
// });
