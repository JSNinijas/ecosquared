import EntityListComponent from './EntityListComponent.vue';
import UsersPage from '../Users/User.vue';
import ProductsPage from '../Products/Products.vue';
import ProjectsPage from '../Projects/Projects.vue';
import EditComponent from '../../components/EditComponent.vue';
import EntityComponent from '../../components/EntityComponent.vue';
import ActionComponent from '../../components/ActionComponent.vue';
import CreateComponent from '../../components/CreateComponent.vue';
import EntityListFilterComponent from './EntityListFilterComponent.vue';
import FilterListComponent from '../../components/FilterListComponent.vue';
import AddItemsSortingListComponent from '../EditAddItemsListComponent.vue';
import ShareSortingComponent from '../../components/ShareSortingComponent.vue';
import ProjectThankComponent from '../../components/ProjectThankComponent.vue';
import SortingActionComponent from '../../components/SortingActionComponent.vue';
import UserInformationComponent from '../../components/UserInformationComponent.vue';
import ProjectsSortingListComponent from '../../components/ProjectsSortingListComponent.vue';

export default {
    registerRoutes(routes){
        routes.children.push(
            {
                path: 'page',
                name: 'item',
                component: EntityListComponent,
                children: [
                    {
                        path: ':entity/state/limit=:pageLimit|sortBy=:sortBy|direction=:direction|search=:search?',
                        name: 'entity',
                        components: {UsersPage, ProductsPage, ProjectsPage},
                        children: [
                            //------sort users by project------------------------
                            {
                                path: 'projects/:projectId([0-9]*)',
                                name: 'entityFilter',
                                components: {UsersPage, ProductsPage, ProjectsPage},
                            },
                            //------show card------------------------
                            {
                                path: ':id([0-9]*)',
                                name: 'itemEntity',
                                component: EntityComponent,
                                children: [
                                    {
                                        path: 'projects/:projectId([0-9]*)',
                                        name: 'itemEntityFiltered',
                                        component: EntityComponent,
                                    }
                                ]
                            },
                            //------show card------------------------
                            {
                                path: ':id([0-9]*)',
                                name: 'entityEntity',
                                component: EntityComponent,
                                children: [
                                    {
                                        path: 'projects/:projectId([0-9]*)',
                                        name: 'entityEntityFiltered',
                                        component: EntityComponent,
                                    }
                                ]
                            },
                            //------------card action-- usership -------------------------
                            {
                                path: ':id([0-9]*)/:action',
                                name: 'itemAction',
                                component: ActionComponent,
                                children: [
                                    {
                                        path: 'limit=:limit',
                                        name: 'itemActionMore',
                                        component: ActionComponent,
                                        children: [
                                            {
                                                path: 'search=:query',
                                                name: 'itemActionSearch',
                                                component: ActionComponent,
                                            }
                                        ]
                                    },
                                ]
                            },
                            //------------card action-- usership -------------------------
                            {
                                path: ':id([0-9]*)/:action',
                                name: 'entityAction',
                                component: ActionComponent,
                                children: [
                                    {
                                        path: 'limit=:limit',
                                        name: 'entityActionMore',
                                        component: ActionComponent,
                                        children: [
                                            {
                                                path: 'search=:query',
                                                name: 'entityActionSearch',
                                                component: ActionComponent,
                                            }
                                        ]
                                    },
                                ]
                            },
                            //-----------create-----------------------------
                            {
                                path: ':action([create]*)/:step([0-9]*)',
                                name: 'itemCreate',
                                component: CreateComponent,
                            },
                            //-----add owners----
                            {
                                path: ':id([0-9]*)/:action(add-items-sorting-list)/:sortEntity/limit=:limit',
                                name: 'itemAddItemsSortingList',
                                component: AddItemsSortingListComponent,
                                children: [
                                    {
                                        path: 'search=:query',
                                        name: 'itemAddItemsSortingListSearch',
                                        component: AddItemsSortingListComponent,
                                    },
                                ]
                            },
                            //------show filter users by projects------------------------
                            {
                                path: ':action(projects-sorting-list)/:sortEntity(projects)/limit=:limit([0-9]*)',
                                name: 'entityProjectsSortingList',
                                component: ProjectsSortingListComponent,
                                children: [
                                    {
                                        path: 'search=:query',
                                        name: 'entityProjectsSortingListSearch',
                                        component: ProjectsSortingListComponent,
                                    },
                                ]
                            },
                            //------show filter users by projects------------------------
                            {
                                path: ':action(projects-sorting-list)/:sortEntity(projects)/limit=:limit([0-9]*)',
                                name: 'itemProjectsSortingList',
                                component: ProjectsSortingListComponent,
                                children: [
                                    {
                                        path: 'search=:query',
                                        name: 'itemProjectsSortingListSearch',
                                        component: ProjectsSortingListComponent,
                                    },
                                ]
                            },
                            //-----------------share list---------------------
                            {
                                path: ':id([0-9]*)/:action/:sortEntity([a-zA-Z]*)/limit=:limit',
                                name: 'entityShareSorting',
                                component: ShareSortingComponent,
                                children: [
                                    {
                                        path: 'search=:query',
                                        name: 'entityShareSortingSearch',
                                        component: ShareSortingComponent,
                                    }
                                ]
                            },
                            //-----------------share list---------------------
                            {
                                path: ':id([0-9]*)/:action/:sortEntity([a-zA-Z]*)/limit=:limit',
                                name: 'itemShareSorting',
                                component: ShareSortingComponent,
                                children: [
                                    {
                                        path: 'search=:query',
                                        name: 'itemShareSortingSearch',
                                        component: ShareSortingComponent,
                                    }
                                ]
                            },
                            //-----------------------share action----------------------------------
                            {
                                path: ':id([0-9]*)/:action(share)/:sortEntity/:sortEntityId([0-9]*)',
                                name: 'itemSortingAction',
                                component: SortingActionComponent,
                            },
                            //-----------------------thank by project--------------------------------
                            {
                                path: ':id([0-9]*)/:action(thank)/:sortEntity(projects)/:projectId([0-9]*)',
                                name: 'itemProjectThank',
                                component: ProjectThankComponent,
                            },
                            //----------------------user info balance----------------------------
                            {
                                path: ':type',
                                name: 'entityUserInformation',
                                component: UserInformationComponent,
                            },
                            //---------------------edit ---------------------------------
                            {
                                path: ':id([0-9]*)/:action/:step([0-9]*)',
                                name: 'entityEdit',
                                component: EditComponent,
                            },
                            {
                                path: ':id([0-9]*)/:action/:step([0-9]*)',
                                name: 'itemEdit',
                                component: EditComponent,
                            },
                            //------------------------unknown-----------------------------
                            // {
                            //     path: ':action/:filterEntity',
                            //     name: 'entityFilterList',
                            //     component: FilterListComponent,
                            // },
                            //------------------------unknown-----------------------------
                            // {
                            //     path: ':filterEntity/:id([0-9]*)',
                            //     name: 'entityListById',
                            //     component: EntityListComponent,
                            // },
                        ],
                    },
                    //----------------------user info balance----------------------------
                    {
                        path: ':type',
                        name: 'itemUserInformation',
                        component: UserInformationComponent,
                    },
                ]
            },
        )
    }
}