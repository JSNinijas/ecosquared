import {Observable} from 'rxjs'
export default {

    observables: {},
    subscribers: {},

    emit(observableName, eventData) {
        return this.subscribers[observableName].forEach(sub => sub.next(eventData))
    },

    CreateObservableAndSubscribe(observableName, payload) {

        this.observables[observableName] = new Observable((subscriber => {

            if (!this.subscribers[observableName]) this.subscribers[observableName] = [];

            this.subscribers[observableName].push(subscriber);
        }));

        this.observables[observableName].subscribe(payload)

    }
}