const DEFAULT_STEP = false;
class BackButtonCoordinator {

    constructor() {
        this.nextStep = DEFAULT_STEP;
    }

    setNextStep() {
        this.nextStep = true;
    }

    getNextStep() {
        return this.nextStep;
    }

    resetStep() {
        this.nextStep = DEFAULT_STEP;
    }
}

export default new BackButtonCoordinator;