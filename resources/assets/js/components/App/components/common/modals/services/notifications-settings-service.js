const DEFAULT_SETTINGS = null;
const SETTINGS_MAP = [
    'sharePushType',
    'shareStatisticType',
    'archiveActivityType',
    'activityStatisticType',

    'thanksStatus',
    'productLikesStatus',
    'projectHappyStatus',
    'productSupportStatus',
    'projectSupportStatus',

    'thanksRemove',
    'productLikesRemove',
    'projectHappyRemove',
    'productSupportRemove',
    'projectSupportRemove',
];
class NotificationsSettingsService {

    constructor() {
        this.settings = DEFAULT_SETTINGS;
    }

    setSettings(item, value) {
        if(!value) this.settings = item;

        if(value) {
            _.each(SETTINGS_MAP, (i)=> {
                if (item === i) {
                    if (this.settings) {
                        _.set(this.settings, item, value);
                    }
                    if (!this.settings) {
                        this.settings = {};
                        _.set(this.settings, item, value);
                    }
                }
            });
        }
    }

    getSettings() {
        return this.settings;
    }

    resetSettings() {
        this.settings = DEFAULT_SETTINGS;
    }
}

export default new NotificationsSettingsService;