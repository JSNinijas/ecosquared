const LAST_VALID_STEP = 1;
class CreateItemCoordinator {
    constructor() {
        this.step = LAST_VALID_STEP
    }

    setValidStep(step) {
        this.step = step;
    }

    getValidStep() {
        return this.step;
    }

    resetValidStep() {
        this.step = LAST_VALID_STEP;
    }

}

export default new CreateItemCoordinator;