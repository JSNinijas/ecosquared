const DEFAULT_ROUTE = null;
class CloseButtonCoordinator {

    constructor() {
        this.closeButtonRoute = DEFAULT_ROUTE;
    }

    setRoute(route) {
        localStorage.setItem('close-route', JSON.stringify(route));
        this.closeButtonRoute = route;
    }

    getRoute() {
        return localStorage.getItem('close-route') ?
            JSON.parse(localStorage.getItem('close-route')) : this.closeButtonRoute;
    }

    resetRoute() {
        localStorage.setItem('close-route', JSON.stringify(DEFAULT_ROUTE));
        this.closeButtonRoute = DEFAULT_ROUTE;
    }
}

export default new CloseButtonCoordinator;