const DEFAULT_ITEM = null;
const ITEM_MAP = [
    'title','description','link', 'authorStatus', 'author', 'image', 'cImage', 'creationDate', 'completionDate',
    'type','start','end','share', 'support', 'ownershipBehaviourType', 'sQBehaviourType', 'usershipType',
    'creditTargetType', 'basicBalanceValue', 'ownershipSQBalanceValue', 'creditTargetValue',
    'editingBehaviourLeftValue', 'editingBehaviourRightValue', 'editorsGovernanceValue', 'ownershipUsershipBalanceValue'
];

class CreateEditItemService {

    constructor() {
        this.item = DEFAULT_ITEM;
    }

    setItem(item, value) {
        if(!value) this.item = item;

        if(value) {
            _.each(ITEM_MAP, (i)=>{
                if(item === i) {
                    if(this.item){
                        _.set(this.item, i, value);
                    }
                    if(!this.item){
                        this.item = {};
                        _.set(this.item, i, value);
                    }
                }
            })
        }
    }

    getItem() {
        return this.item;
    }

    resetItem() {
        this.item = DEFAULT_ITEM;
    }
}

export default new CreateEditItemService;