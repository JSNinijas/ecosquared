import OfferHistory from './OfferHistory.vue';
import UserInformationComponent from '../../components/UserInformationComponent.vue';
import NotificationsSettingsComponent from '../../components/NotificationsSettingsComponent.vue';
export default {
    registerRoutes(routes) {
        routes.children.push(
            {
                path: 'offer-history/:entity/limit=:pageLimit|sortBy=:sortBy|direction=:direction|search=:search?',
                component: OfferHistory,
                name: 'offerHistory',
                children: [
                    //-------------user-info-buy balance-----------
                    {
                        path: ':type',
                        name: 'offerHistoryUserInformation',
                        component: UserInformationComponent,
                    },
                    //--------------notification settings-------------------
                    {
                        path: ':type/:action',
                        name: 'offerHistoryNotificationsSettings',
                        component: NotificationsSettingsComponent,
                    },
                ]
            });
    }
}