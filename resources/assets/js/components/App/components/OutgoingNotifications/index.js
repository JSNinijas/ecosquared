import OutgoingNotifications from './OutgoingNotification.vue';
import CreateComponent from '../../components/CreateComponent.vue';
import EntityComponent from '../../components/EntityComponent.vue';
import ActionComponent from '../../components/ActionComponent.vue';
import ShareSortingComponent from '../../components/ShareSortingComponent.vue';
import SortingActionComponent from '../../components/SortingActionComponent.vue';
import UserInformationComponent from '../../components/UserInformationComponent.vue';
import NotificationActionComponent from '../../components/NotificationActionComponent.vue';

export default {
    registerRoutes(routes){
        routes.children.push(
            {
                path: 'outgoing-notifications',
                name: 'outgoingNotification',
                component: OutgoingNotifications,
                children: [
                    //----------get out notification by id-----------------
                    {
                        path: ':entity/:id([0-9]*)',
                        name: 'outgoingNotificationEntity',
                        component: EntityComponent,
                    },
                    //----------------revoke------------------------
                    {
                        path: ':type/:id([0-9]*)/:action',
                        name: 'outgoingNotificationAction',
                        component: NotificationActionComponent,
                    },
                    //----------------usership-------------------------------
                    {
                        path: ':entity/:id([0-9]*)/:action',
                        name: 'outgoingNotificationEntityAction',
                        component: ActionComponent,
                        children: [
                            {
                                path: 'limit/q=:limit',
                                name: 'outgoingNotificationActionMore',
                                component: ActionComponent,
                                children: [
                                    {
                                        path: 'search/q=:query',
                                        name: 'outgoingNotificationActionSearch',
                                        component: ActionComponent,
                                    }
                                ]
                            },
                        ]
                    },
                    //--------------------create component------------------------------
                    {
                        path: ':entity/:action([create]*)/:step([0-9]*)',
                        name: 'outgoingNotificationCreate',
                        component: CreateComponent,
                    },
                    //---------------------user settings-- buy balance----------------
                    {
                        path: ':type',
                        name: 'outgoingNotificationUserInformation',
                        component: UserInformationComponent,
                    },
                    //----------------back share list -----------------------------
                    {
                        path: ':entity/:id([0-9]*)/:action/:sortEntity([a-zA-Z]*)/limit/q=:limit',
                        name: 'outgoingNotificationShareSorting',
                        component: ShareSortingComponent,
                        children: [
                            {
                                path: 'search/q=:query',
                                name: 'outgoingNotificationShareSortingSearch',
                                component: ShareSortingComponent,
                            }
                        ]
                    },
                    //--------------------share list action-------------------------
                    {
                        path: ':entity/:id([0-9]*)/:action/:sortEntity/:sortEntityId([0-9]*)',
                        name: 'outgoingNotificationSortingAction',
                        component: SortingActionComponent,
                    },
                ]
            },
        )
    }
}