import MenuComponent from './Menu.vue';
import UserInformationComponent from '../../components/UserInformationComponent.vue';
import NotificationsSettingsComponent from '../../components/NotificationsSettingsComponent.vue';
export default {
    registerRoutes(routes) {
        routes.children.push(
            {
                path: 'menu',
                component: MenuComponent,
                name: 'menu',
                children: [
                    //-------------user-info-buy balance-----------
                    {
                        path: ':type',
                        name: 'menuUserInformation',
                        component: UserInformationComponent,
                    },
                    //--------------notification settings-------------------
                    {
                        path: ':type/:action',
                        name: 'menuNotificationsSettings',
                        component: NotificationsSettingsComponent,
                    },
                ]
            });
    }
}