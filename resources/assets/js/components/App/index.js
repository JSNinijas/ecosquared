import AppComponent from './App.vue';
import HomeComponent from './components/Home';
import MenuComponent from './components/Menu';
import OfferHistory from './components/OfferHistory';
import EntityListComponent from './components/EntityListComponent';
import IncomingNotificationsComponent from './components/IncomingNotifications';
import OutgoingNotificationsComponent from './components/OutgoingNotifications';

const MAIN_PATH = '/app/home';
const NAME = 'App';

export default {
    registerRoutes(routes){
        var appRoutes = {
            path: '/app',
            redirect: MAIN_PATH,
            component: AppComponent,
            children: []
        };
        HomeComponent.registerRoutes(appRoutes);
        IncomingNotificationsComponent.registerRoutes(appRoutes);
        OutgoingNotificationsComponent.registerRoutes(appRoutes);
        MenuComponent.registerRoutes(appRoutes);
        OfferHistory.registerRoutes(appRoutes);
        EntityListComponent.registerRoutes(appRoutes);
        routes.push(appRoutes);
    },
    config() {
        return {
            redirectPath: MAIN_PATH,
            name: NAME,
            components: {
                home: {
                    redirectPath: '/app/home',
                    name: 'Home'
                },
                menu: {
                    redirectPath: '/app/menu',
                    name: 'Menu'
                },
                settings: {
                    redirectPath: '/app/user-settings',
                },
                products: {
                    redirectPath: '/app/products'
                },
                projects: {
                    redirectPath: '/app/projects'
                },
                users: {
                    redirectPath: '/app/users'
                },
                balance: {
                    redirectPath: '/app/balance'
                },
                incoming: {
                    redirectPath: '/app/incoming-notifications',
                },
                outgoing: {
                    redirectPath: '/app/outgoing-notifications'
                }
            }
        }
    }
}