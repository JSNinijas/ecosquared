//
// import Vue from 'vue'
// import LoginComponent from './index'
// import VeeValidate from 'vee-validate';
// import VueMaterial from 'vue-material';
// import VModal from 'vue-js-modal';
// import VueRouter from 'vue-router';
// import Auth from '../../services/auth';
// import sinon from 'sinon';
// import Promise from 'promise';
// import AppComponent from '../../components/App';
//
// window.$ = require('jquery');
// window.Promise = Promise;
//
// describe('Login Component ', () => {
//     Vue.use(VeeValidate);
//     Vue.use(VueMaterial);
//     Vue.use(VModal);
//     Vue.use(VueRouter);
//
//     var routes = [];
//
//     LoginComponent.registerRoutes(routes);
//
//     const router = new VueRouter({routes});
//
//     const vm = new Vue({
//         el: document.createElement('div'),
//         template: '<div><router-view></router-view></div>',
//         router
//     }).$mount();
//
//     beforeEach(() => {
//         router.push(LoginComponent.config().redirectPath);
//     });
//
//     it('Should render route', (done) => {
//         Vue.nextTick(() => {
//             expect(vm.$children.length).toBe(1);
//             expect(vm.$route.fullPath).toBe(LoginComponent.config().redirectPath);
//             done();
//         })
//     });
//
//     it('Should have initial data', (done) => {
//         Vue.nextTick(()=>{
//             expect(vm.$children[0].email).toBe('');
//             expect(vm.$children[0].password).toBe('');
//             expect(typeof vm.$children[0].RequestInviteComponent).toBe('object');
//             expect(vm.$children[0].backendErrors).toEqual({});
//             done();
//         });
//     });
//
//     it('Should validate correct data', (done) => {
//         var cmp = vm.$children[0];
//         setTimeout(()=>{
//             Vue.set(cmp, 'email', 'sd@as.ss');
//             Vue.set(cmp, 'password', 'sd@as.ss');
//
//             Vue.nextTick( () => {
//                 expect(cmp.validate()).toEqual(true);
//                 done();
//             })
//         }, 500);
//     });
//
//     it('Should validate incorrect data', (done) => {
//         setTimeout(()=>{
//             var cmp = vm.$children[0];
//             Vue.set(cmp.$data, 'email', '');
//             Vue.set(cmp.$data, 'password', '');
//
//             Vue.nextTick(() => {
//                 expect(cmp.validate()).toEqual(false);
//                 done();
//             })
//         }, 500)
//     });
//
//     it('Should call the API and redirect on success after submit', (done) => {
//
//         var cmp = vm.$children[0];
//         setTimeout(() => {
//
//             Vue.set(cmp.$data, 'email', 'admin@admin.com');
//             Vue.set(cmp.$data, 'password', 'admin');
//
//             Vue.nextTick(() => {
//
//                 var loginStub = sinon.stub(Auth, 'login');
//
//                 loginStub
//                     .withArgs('admin@admin.com', 'admin')
//                     .callsFake(
//                         () => {
//                             return new Promise((resolve, reject) => {
//                                 resolve({});
//                             })
//                         }
//                     );
//
//                 var routerStub = sinon.stub(cmp.$router, 'push');
//                 routerStub
//                     .withArgs(AppComponent.config().redirectPath)
//                     .callsFake(() => {
//                         return 1;
//                     });
//
//                 cmp.submit();
//
//                 setTimeout(() => {
//                     expect(loginStub.called).toEqual(true);
//                     expect(routerStub.called).toEqual(true);
//                     done();
//                 });
//             })
//         }, 500)
//     });
//
//     it('Should add BE errors on submit if data is incorrect', () => {})
// });