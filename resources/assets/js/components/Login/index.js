import LoginComponent from './Login.vue';

const MAIN_PATH = '/login';
const NAME = 'LOG IN';

export default {
    registerRoutes(routes){
        routes.push({
            path: '/login',
            component: LoginComponent,
        })
    },
    config() {
        return {
            redirectPath: MAIN_PATH,
            name: NAME
        }
    }
}