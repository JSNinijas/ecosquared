import AdminComponent from './Admin.vue';
import AdminOverviewComponent from './components/Overview.vue';
import AdminProfileComponent from './components/Profile.vue';
import AdminItemComponent from './components/Items.vue';
import AdminTransactionComponent from './components/Transaction.vue'
import AdminTransactionFilterComponent from './components/TransactionFiltersComponent.vue'
import AdminItemEditForm from './components/AdminItemEditForm.vue'
import AdminItemCreateForm from './components/AdminItemCreateForm.vue'
import TransactionInformationComponent from './components/TransactionInformationComponent.vue'
import createNewTransaction from './components/common/CreateNewTransaction.vue'
import PayPalPavmentComonent from './components/PayPalPaymentConponent.vue'
import SettingsComponent from './components/Settings.vue';


const MAIN_PATH = '/admin/overview';
const NAME = 'Admin';

export default {
    registerRoutes(routes) {
        routes.push({
            path: '/admin',
            redirect: MAIN_PATH,
            component: AdminComponent,
            children: [
                {
                    path: 'overview',
                    name: 'overview',
                    component: AdminOverviewComponent
                },
                {
                    path: 'profile',
                    component: AdminProfileComponent
                },
                {
                    name: 'transactions',
                    path: 'transactions',
                    component: AdminTransactionComponent,
                    children: [
                        {
                            name: 'all-transactions',
                            path: 'all-transactions',
                            component: TransactionInformationComponent,
                            children: [
                                {
                                    name: 'createNewTransaction',
                                    path: 'create-new-transaction',
                                    component: createNewTransaction
                                }
                            ]
                        },
                        {
                            name: 'filters',
                            path: 'filters',
                            component: AdminTransactionFilterComponent
                        }

                    ]
                },
                {
                    name: 'payments',
                    path: 'payments',
                    component: PayPalPavmentComonent
                },
                {
                    name: 'settings',
                    path: 'settings',
                    component: SettingsComponent
                },
                {
                    name: 'items',
                    path: ':items',
                    component: AdminItemComponent
                }

            ]
        })
    },
    config() {
        return {
            redirectPath: MAIN_PATH,
            name: NAME,
            components: {
                overview: {
                    redirectPath: '/admin/overview',
                    name: 'Overview'
                },
                users: {
                    redirectPath: '/admin/users',
                    name: 'Users'
                },
                edit: {
                    redirectPath: '/admin/users/edit',
                    name: 'Edit'
                },
                create: {
                    redirectPath: '/admin/users/crete',
                    name: 'Create'
                },
                products: {
                    redirectPath: '/admin/products'
                },
                projects: {
                    redirectPath: '/admin/projects'
                }
            }
        }
    }
}