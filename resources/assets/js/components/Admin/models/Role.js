import Resource from '../../../interfaces/Resource';

class Role extends Resource {
    constructor () {
        super();
        this.basePath = '/api/admin/';
        this.resourceName = 'roles';
    }
}

export default new Role();
