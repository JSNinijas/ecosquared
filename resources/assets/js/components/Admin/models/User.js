
import Resource from '../../../interfaces/Resource';
import Api from '../../../services/api';
import EventBus from '../../../services/EventBus';
import Auth from '../../../services/auth';

class User extends Resource{

    constructor() {
        super();
        this.basePath = '/api/admin/';
        this.resourceName = 'users';
    }

    assignRoles(user, roles) {
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + '/' + user.id + '/assign-roles', 'post', {}, {roles}).then((res) => {
                EventBus.$emit(this.resourceName + ':update', res.data);
                resolve(res.data);
            }, (x) => {
                reject(x.response.data);
            })
        })
    }

    updateThanks(resource, userId, thankId) {
        return new Promise((resolve, reject) => {
            resource._method = 'PATCH';
            Api.call(this.basePath + this.resourceName + '/' + userId + '/thanks/' + thankId, 'post', {},
                this.processResource(resource, resource._method)).then((res) => {
                resolve(res.data);
            }, (x) => {
                reject(x.response.data);
            })
        })
    }

    getThanks(userId) {
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + '/' + userId + '/thanks', 'get', {}).then((res) => {
                resolve(res.data);
            }, (x) => {
                reject(x.response.data);
            })
        })
    }

    getTotalBalance(user) {
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + '/' + user.id + '/total-balance', 'get').then(resp => {
                resolve(resp.data)
            }, reject)
        })
    }

    getTransaction(user) {
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName  + '/tr', 'get').then(resp => {
                resolve(resp.data)
            }, reject)
        })
    }

    loginAs(user) {
        return new Promise((resolve, reject) => {
            Api.call(
              this.basePath + this.resourceName + '/' + user.id + '/login',
              'post'
            ).then((res) => {
              Auth.setUser(res.data);
              EventBus.$emit('redirect:home');
            })
        })
    }

    showPaymentStats(params={}) {
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + 'payment',
                'post',
                {_method: "GET"},
                {filter_groups: params.filter_groups}
            ).then(resp => {
                this.data = resp.data;
                resolve(this.data)
            }, rej => reject(rej.response.data))
        });
    }

    getBalance() {
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + 'total-balance', 'get', {}).then((res) => {
                resolve(res.data);
            }, (x) => {
                reject(x.response.data);
            })
        })
    }

}

export default new User();
