import Resource from '../../../interfaces/Resource';
import Api from '../../../services/api';
import EventBus from '../../../services/EventBus';
import Auth from '../../../services/auth';

class PayPal extends Resource{

    constructor() {
        super();
        this.basePath = '/api/payment/';
        this.resourceName = '';
    }

    getBalance() {
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + 'balance', 'get', {}).then((res) => {
                resolve(res.data);
            }, (x) => {
                reject(x.response.data);
            })
        })
    }

}


export default new PayPal();