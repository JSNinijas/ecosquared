import Resource from '../../../interfaces/Resource';
import Api from '../../../services/api';

class Product extends Resource {
    constructor () {
        super();
        this.basePath = '/api/admin/';
        this.resourceName = 'products';
    }

    enableDisableShareSupport(share, support) {
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + 'products/share-support', 'patch', {}, {share, support}).then((res) => {
                resolve(res.data);
            }, (x) => {
                reject(x.response.data);
            })
        })
    }

    getShareSupport() {
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + 'products/share-support', 'get', {}).then((res) => {
                resolve(res.data);
            }, (x) => {
                reject(x.response.data);
            })
        })
    }
}

export default new Product();
