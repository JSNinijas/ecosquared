import Resource from '../../../interfaces/Resource';

class Transaction extends Resource {

    constructor() {
        super();
        this.basePath = '/api/wallet/';
        this.resourceName = 'transactions';
    }
}
export default new Transaction();