import Resource from '../../../interfaces/Resource';

class Project extends Resource {
    constructor () {
        super();
        this.basePath = '/api/admin/';
        this.resourceName = 'projects';
    }
}

export default new Project();
