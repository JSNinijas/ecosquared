// import Vue from 'vue';
// import AdminComponent from './index';
// import LoginComponent from '../Login';
// import AppComponent from '../App'
// import VeeValidate from 'vee-validate';
// import VueMaterial from 'vue-material';
// import VModal from 'vue-js-modal';
// import VueRouter from 'vue-router';
// import Auth from '../../services/auth';
// import sinon from 'sinon';
//
// window.$ = require('jquery');
// window.EventBus = new Vue();
//
// describe('Admin Component', () => {
//     Vue.use(VeeValidate);
//     Vue.use(VueMaterial);
//     Vue.use(VModal);
//     Vue.use(VueRouter);
//
//     var user = {name: 'admin', email:'asdf@asdf.cc'};
//     var authStub = sinon.stub(Auth, 'getUser');
//     authStub
//         .onCall(0)
//         .returns(false)
//         .onCall(1)
//         .returns(user)
//         .returns(user)
//     ;
//
//
//     var routes = [];
//
//     AdminComponent.registerRoutes(routes);
//
//     const router = new VueRouter({routes});
//
//     const vm = new Vue({
//         el: document.createElement('div'),
//         template: '<div><router-view></router-view></div>',
//         router
//     }).$mount();
//
//     beforeEach(() => {
//         router.push(AdminComponent.config().redirectPath);
//     });

    // it('Shold render', (done) => {
    //   Vue.nextTick(() => {
    //     expect(vm.$children.length).toBeGreaterThan(0);
    //     done();
    //   })
    // })

    // it('Should redirect to login when no auth', (done) => {
    //     Vue.nextTick(() => {
    //         expect(vm.$route.fullPath).toBe(LoginComponent.config().redirectPath);
    //         done();
    //     })
    // });
    //
    // it('Should redirect to App when auth and non Admin', (done) => {
    //     Vue.nextTick(() => {
    //         expect(vm.$route.fullPath).toBe(AppComponent.config().redirectPath);
    //         done();
    //     })
    // });
// });
