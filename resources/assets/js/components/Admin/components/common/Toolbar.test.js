// import Vue from 'vue'
// import EventBus from '../../../../services/EventBus'
//
// import ToolComponent from './Toolbar.vue';
// import Auth from '../../../../services/auth';
// import WelcomeComponent from './../../../Welcome';
// import VueMaterial from 'vue-material';
// import VueRouter from 'vue-router';
// import sinon from 'sinon';
// import Promise from 'promise';
//
// window.$ = require('jquery');
// window.Promise = Promise;
//
// describe('Tool Component', () => {
//
//     Vue.use(VueMaterial);
//     Vue.use(VueRouter);
//
//     var stub = sinon.stub(Auth, 'logout');
//
//     const router = new VueRouter({routes:[]});
//
//     const Ctor = Vue.extend(ToolComponent);
//
//     stub.resolves(true);
//
//     const vm = new Ctor({router}).$mount();
//
//     afterAll(() => {
//         stub.restore();
//     });
//
//     it('Should toolbar have initial data', (done) =>  {
//         Vue.nextTick(()=>{
//             expect(typeof vm.Auth).toBe('object');
//             done();
//         });
//     });
//
//     it('Should toolbar logout method', (done) =>  {
//
//         expect(typeof vm.logout).toBe('function');
//         vm.logout();
//         Vue.nextTick(() => {
//             setTimeout( () => {
//                 expect(vm.$route.fullPath).toEqual(WelcomeComponent.config().redirectPath);
//                 done();
//             })
//         });
//
//     });
//
// });
