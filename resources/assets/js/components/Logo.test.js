
import Vue from 'vue'
import Logo from './Logo.vue'


import Promise from 'promise';


window.$ = require('jquery');
window.Promise = Promise;

describe('Logo Component ', () => {

    const Ctor = Vue.extend(Logo);
    const vm = new Ctor({ propsData: { src: 'foo'} }).$mount();

    it('Should have props', (done) => {
        Vue.nextTick(() => {
            expect(vm.src).toBe('foo');
            done();
        })
    });


    it('Should have computed props', (done) => {
        Vue.nextTick(() => {
            expect(vm.source).toBe('/pictures/foo');
            done();
        })
    });
});