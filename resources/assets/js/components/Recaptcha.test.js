// import Vue from 'vue';
// import Recaptcha from './Recaptcha.vue';
// import sinon from 'sinon';
// import EventBus from '../services/EventBus';
//
// window.$ = require('jquery');
//
// class fakeGrecaptca {
//     render(el, opts) {
//         $(el).append('<iframe src="google.com" style="width: 900px"></iframe>');
//     }
// }
//
// describe('Recaptcha Component ', () => {
//
//     var stub = sinon.stub(window, 'grecaptcha');
//
//
//     stub.value(new fakeGrecaptca());
//
//     var vm = new Vue(Recaptcha).$mount();
//     $(vm.$el).width(450);
//     //$(vm.$el).find('iframe').width = 900;
//
//     it('Should have initial data', () => {
//         expect(vm.rendered).toEqual(true);
//     });
//
//     describe('when grecaptcha not loaded', () => {
//         //var i = 0;
//         //var recap = new fakeGrecaptca;
//         // stub.get(() => {
//         //     return !(i++>0)? undefined : recap
//         // });
//
//         //var renderSpy = sinon.spy(recap, 'render');
//         // stub.value(new fakeGrecaptca());
//         //
//         // var vm = new Vue(Recaptcha).$mount();
//
//
//         //console.log(window.grecaptcha);
//
//         it('should wait grecaptcha to load', (done) => {
//             Vue.nextTick(() => {
//
//
//                 stub.value(new fakeGrecaptca());
//                 var vm = new Vue(Recaptcha).$mount();
//
//
//                 EventBus.$emit('recaptcha:loaded');
//
//
//                 expect(window.grecaptcha).toBeDefined();
//
//
//                 done()
//             })
//         });
//
//         stub.restore();
//         var vm = new Vue(Recaptcha).$mount();
//
//         //console.log(window.grecaptcha);
//
//         // it('should not trigger render on mount', () => {
//         //     expect(vm.rendered).toEqual(false);
//         // });
//
//
//
//         // stub.value(new fakeGrecaptca());
//         // var vm = new Vue(Recaptcha).$mount();
//
//
//
//     });
//
//
//     it('Should scale itself to viewport', (done) => {
//
//          Vue.nextTick(() => {
//              expect(vm.scale).toEqual($(vm.$el).width() / $(vm.$el).find('iframe').width());
//              done();
//          });
//      });
//
//
//     it('Should render its content', (done) => {
//         Vue.nextTick(() => {
//             expect(vm.rendered).toEqual(true);
//             done();
//         })
//     });
//
//
//     afterAll(() => {
//         stub.restore();
//     });
// });
