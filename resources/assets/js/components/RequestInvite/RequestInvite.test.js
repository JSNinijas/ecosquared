// import Vue from 'vue'
// import RequestInviteComponent from './index'
// import VeeValidate from 'vee-validate';
// import VueMaterial from 'vue-material';
// import VModal from 'vue-js-modal';
// import VueRouter from 'vue-router';
//
// window.$ = require('jquery');
//
// describe('Request Invite Component ', () => {
//     Vue.use(VeeValidate);
//     Vue.use(VueMaterial);
//     Vue.use(VModal);
//     Vue.use(VueRouter);
//
//     var routes = [];
//
//     RequestInviteComponent.registerRoutes(routes);
//
//     const router = new VueRouter({routes});
//
//     const vm = new Vue({
//         el: document.createElement('div'),
//         template: '<div><router-view></router-view></div>',
//         router
//     }).$mount();
//
//     beforeEach(() => {
//         router.push(RequestInviteComponent.config().redirectPath);
//     });
//
//     it('Should render route', (done) => {
//         Vue.nextTick(() => {
//             expect(vm.$children.length).toBe(1);
//             expect(vm.$route.fullPath).toBe(RequestInviteComponent.config().redirectPath);
//             done();
//         })
//     });
//
//     it('Should have initial data', (done) => {
//         Vue.nextTick(() => {
//             expect(vm.$children[0].email).toBe('');
//             expect(vm.$children[0].recaptchaResponse).toBe('');
//             expect(typeof vm.$children[0].LoginComponent).toBe('object');
//             expect(vm.$children[0].backendErrors).toEqual({});
//             done();
//         });
//     });
//
//     it('Should validate correct data', (done) => {
//         var cmp = vm.$children[0];
//         setTimeout(()=>{
//             Vue.set(cmp.$data, 'email', 'sd@as.ss');
//             Vue.set(cmp.$data, 'recaptchaResponse', 'dfrffdf')
//
//             Vue.nextTick( () => {
//                 expect(cmp.validate()).toEqual(true);
//                 done();
//             })
//         }, 500);
//     });
//
//     it('Should validate incorrect data', (done) => {
//         setTimeout(()=>{
//             var cmp = vm.$children[0];
//             Vue.set(cmp.$data, 'email', '');
//             Vue.set(cmp.$data, 'recaptchaResponse', '');
//
//             Vue.nextTick(() => {
//                 expect(cmp.validate()).toEqual(false);
//                 done();
//             })
//         }, 500)
//     });
// });