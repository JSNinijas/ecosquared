import RequestInviteComponent from './RequestInvite.vue';
import InfoMessagesComponent from '../InfoMessages/InfoMessagesComponent.vue';

const MAIN_PATH = '/request-invite';
const NAME = 'REQUEST INVITE';

export default {
    registerRoutes(routes){
        routes.push({
            path: '/request-invite',
            component: RequestInviteComponent,
            children: [
                {
                    path: `/messages/:message`,
                    name: 'messages',
                    component: InfoMessagesComponent,
                }
            ]
        })
    },
    config() {
        return {
            redirectPath: MAIN_PATH,
            name: NAME
        }
    }
}