// import Vue from 'vue'
// import WelcomeComponent from './index'
// import VueMaterial from 'vue-material';
// import VueRouter from 'vue-router';
// import EventBus from '../../services/EventBus';
//
// window.$ = require('jquery');
//
// describe('Welcome Component ', () => {
//     Vue.use(VueMaterial);
//     Vue.use(VueRouter);
//
//     var routes = [];
//
//     WelcomeComponent.registerRoutes(routes);
//
//     const router = new VueRouter({routes});
//
//     const vm = new Vue({
//         el: document.createElement('div'),
//         template: '<div><router-view></router-view></div>',
//         router
//     }).$mount();
//
//     beforeEach(() => {
//         router.push(WelcomeComponent.config().redirectPath);
//     });
//
//     it('Should render route', (done) => {
//         Vue.nextTick(() => {
//             expect(vm.$children.length).toBe(1);
//             expect(vm.$route.fullPath).toBe(WelcomeComponent.config().redirectPath);
//             done();
//         })
//     });
//
//     it('Should have initial data', (done) => {
//         Vue.nextTick(() => {
//             expect(typeof vm.$children[0].isLoggedIn).toBe('boolean');
//             done();
//         });
//     });
// });
