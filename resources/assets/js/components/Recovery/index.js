import RecoveryComponent from './Recovery.vue';
import ErrorsComponent from '../Errors/ErrorsComponent.vue';

const MAIN_PATH = '/recovery';
const NAME = 'RECOVERY';

export default {
    registerRoutes(routes){
        routes.push({
            path: '/recovery/:hash',
            name: NAME,
            component: RecoveryComponent,
            props: {default: true},
            children: [
                {
                    path: ':error',
                    name: 'recovery-errors',
                    component: ErrorsComponent,
                }
            ]
        })
    },
    config() {
        return {
            redirectPath: MAIN_PATH,
            name: NAME
        }
    }
}