import InfoMessagesComponent from './InfoMessagesComponent.vue';

const MAIN_PATH = '/messages';
const NAME = 'MESSAGES';

export default {
    registerRoutes(routes){
        routes.push({
            path: `${MAIN_PATH}/:message`,
            name: NAME,
            component: InfoMessagesComponent,
        })
    },
    config() {
        return {
            redirectPath: MAIN_PATH,
            name: NAME,
        }
    }
}