import ErrorsComponent from './ErrorsComponent.vue';

const MAIN_PATH = '/errors';
const NAME = 'ERRORS';

export default {
    registerRoutes(routes){
        routes.push({
            path: `${MAIN_PATH}/:error`,
            name: NAME,
            component: ErrorsComponent,
        })
    },
    config() {
        return {
            redirectPath: MAIN_PATH,
            name: NAME,
        }
    }
}