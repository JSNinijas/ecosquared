import UnsubscribeComponent from './Unscubscribe.vue';
import ErrorsComponent from '../Errors/ErrorsComponent.vue';

const MAIN_PATH = '/unsubscribe';
const NAME = 'unsubscribe';

export default {
    registerRoutes(routes){
        routes.push({
            path: '/unsubscribe/:hash',
            name: NAME,
            component: UnsubscribeComponent,
            props: {default: true},
            children: [
                {
                    path: ':error',
                    name: 'reg-errors',
                    component: ErrorsComponent,
                }
            ]
        })
    },
    config() {
        return {
            redirectPath: MAIN_PATH,
            name: NAME
        }
    }
}