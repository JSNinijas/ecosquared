import RegistrationComponent from './RegistrationForm.vue';
import ErrorsComponent from '../Errors/ErrorsComponent.vue';

const MAIN_PATH = '/registration';
const NAME = 'registration';

export default {
    registerRoutes(routes){
        routes.push({
            path: '/registration/:hash',
            name: NAME,
            component: RegistrationComponent,
            props: {default: true},
            children: [
                {
                    path: ':error',
                    name: 'reg-errors',
                    component: ErrorsComponent,
                }
            ]
        })
    },
    config() {
        return {
            redirectPath: MAIN_PATH,
            name: NAME
        }
    }
}