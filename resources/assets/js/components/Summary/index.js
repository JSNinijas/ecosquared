import SummaryComponent from './Summary.vue';

const MAIN_PATH = '/summary';
const NAME = 'Summary';

export default {
    registerRoutes(routes){
        routes.push({
            path: '/summary',
            component: SummaryComponent
        })
    },
    config() {
        return {
            redirectPath: MAIN_PATH,
            name: NAME
        }
    }
}