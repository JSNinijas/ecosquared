import Api from '../services/api';
import EventBus from '../services/EventBus';
import Auth from '../services/auth';
import { Observable } from 'rxjs/Observable';
import Vue from 'vue';

'use strict';

class Resource {

    constructor() {
        this.observers = [];
    }

    _registerListeners() {
        this.channels.forEach((channel) => {
            channel.events.forEach((event) => {
                window.Echo.channel(`${channel.name}.${Auth.getUser().id}`).listen(event.name, (e) => {
                    event.handler({name: event.name, target: e[event.prop || channel.prop]});
                    if (this.observers[event.prop || channel.prop]) {
                        this.observers[event.prop || channel.prop].forEach((ob) => {
                            ob.next(this[event.prop || channel.prop])
                        });
                    }
                });
            })
        });

        Auth.onLogout().subscribe((user) => {
            this._unregisterListeners(user)
        });
    }

    _unregisterListeners(user) {
        this.channels.forEach((channel) => {
            window.Echo.leave(`${channel.name}.${user.id}`);
        });

        Auth.onLogin().subscribe(this._registerListeners.bind(this));
    }

    listenSockets() {
        if (window.io && window.Echo) {
            if (!Auth.check()) {
                Auth.onLogin().subscribe(this._registerListeners.bind(this));
            } else {
                this._registerListeners();
            }
        }
    }

    getObservable(prop) {
        return new Observable((observer) => {
            if (this.observers[prop]) {
                this.observers[prop].push(observer);
            } else {
                this.observers[prop] = [observer];
            }
        })
    }

    sync(context, prop, source=null) {
        this.getObservable(source || prop).subscribe((entity) => {
            Vue.set(context, prop, entity);
        });
    }

    processResource(resource, method) {
        if (resource instanceof FormData) {
            resource.append('_method', method);
        }
        return resource;
    }

    all(params={}) {
        return new Promise((resolve, reject) => {
             Api.call(this.basePath + this.resourceName,
                 'post',
                 {_method: "GET"},
                 {
                     sort: params.sort,
                     filter_groups: params.filter_groups,
                     limit: params.limit,
                     page: params.page
                 }
             ).then(resp => {
                 this.data = resp.data;
                 resolve(this.data)
             }, rej => reject(rej.response.data))
        });
    }

    getCountItem() {
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + '/count',
                'post',
                {_method: "GET"}, {}
            ).then(resp => {
                resolve(resp.data)
            }, rej => reject(rej.response.data))
        });
    }

    findById(id, projectId) {
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + '/' + id, 'post', {_method: "GET"}, {projectId}).then(resp => {
                    resolve(resp.data)
                }, rej => reject(rej.response)
            )}
        )
    }

    update(resource, id) {
        return new Promise((resolve, reject) => {
            resource._method = 'PATCH';
            Api.call(this.basePath + this.resourceName + '/' + id, 'post', {},
                this.processResource(resource, resource._method)).then((res) => {
                EventBus.$emit(this.resourceName + ':update', res.data);
                resolve(res.data);
            }, (x) => {
                reject(x.response.data);
            })
        })
    }

    patch(id, resource) {
        return new Promise((resolve, reject) => {
            resource._method = 'PATCH';
            Api.call(
                this.basePath + this.resourceName + '/' + id,
                'post', {},
                this.processResource(resource, 'patch')
            ).then((res) => {
                EventBus.$emit(this.resourceName + ':patch', res.data);
                resolve(res.data);
            }, (x) => {
                reject(x.response.data);
            })
        })
    }

    destroy(resource) {
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + '/' + resource.id, 'delete').then((res) => {
                EventBus.$emit(this.resourceName + ':delete', res.data);
                resolve(res.data);
            }, (x) => {
                reject(x.response.data);
            })
        })
    }

    store(resource) {
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName, 'post', {},
                this.processResource(resource, 'post')).then((res) => {
                resolve(res.data);
            }, (x) => {
                reject(x.response.data);
            })
        })
    }

    arrayLength(params={}){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + this.resourceName + '/count',
                'post',
                {_method: "GET"},
                {sort: params.sort , filter_groups: params.filter_groups}
            ).then(resp => {
                this.length = resp.data;
                resolve(this.length)
            }, rej => reject(rej.response.data))
        });
    }

    shareInvite(invite){
        return new Promise((resolve, reject) => {
            Api.call(this.basePath + 'invite', 'post', {}, this.processResource(invite, 'post')).then(resp => {
                resolve(resp)
            }, (rej) => {
                reject(rej.response.data);
            });
        });
    }
}

export default Resource

