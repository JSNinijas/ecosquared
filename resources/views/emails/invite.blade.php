<table align="center" width="100%" id="invite" cellspacing="0" cellpadding="0" border="0"
       style="text-align: center;width: 100%;display: block;">
    <tbody style="width: 100%;display: inline-block;">
        <tr style="width: 100%;display: block;">
            <td align="center" style="text-align:center;width: 100%;display: block;max-width: 600px">
                <table align="center" cellpadding="0" cellspacing="0" border="0"
                       style="border-collapse: collapse;width: 100%;display: block;text-align: center;">
                    <tbody style="width: 100%;display: inline-block;max-width:600px;background: #eff3e8;">
                        <tr style="width: 100%;display: inline-block;">
                            <td align="center" style="padding:50px 0 40px;text-align:center;
                                                        width:100%;display: inline-block;">
                                <img style="display: inline-block;" width="50%" src="http://ecosquared.co.uk/img/logo-1x.png" />
                            </td>
                        </tr>
                        <tr style="width: 100%;display: inline-block;">
                            <td align="center" style="font-size: 16px;font-weight:400;display: inline-block;
                            padding: 0 15px 50px;color:#000;text-align:center;width:100%;">
                                A gift from <b>{{ $userName }} {{ $userSurname }}</b>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table align="center" cellpadding="0" cellspacing="0" border="0"
                       style="border-collapse: collapse;width: 100%;display: block;text-align: center;">
                    <tbody style="width: 100%;display: inline-block;max-width:600px;">
                        <tr style="width: 100%;display: inline-block;">
                            <td align="center" style="padding: 50px 0 10px;text-align:center;width: 100%;display: inline-block;">
                                <span style="font-size:18px;font-weight:400;color:#000;
                                            text-align:center;width: 100%;display: inline-block;">
                                    <b>{{ $productTitle }}</b> by <i>{{ $author }}</i>
                                </span>
                            </td>
                        </tr>
                        <tr style="width: 100%;display: block;">
                            <td align="center" style="width: 100%;line-height: 20px; padding: 10px 0;display: inline-block;">
                                <span style="width: 100%;font-weight: 600; font-size: 14px;color: #000;
                                            line-height: 20px;display: inline-block;">
                                    {{ $inviteMessage }}
                                </span>
                            </td>
                        </tr>
                        <tr style="width: 100%;text-align: center;display: block;">
                            <td align="center" style="text-align: center;width: 100%;display: inline-block;">
                                <a href="{{$url}}" style="
                                    position:relative;
                                    max-width:300px;
                                    padding:5px 15px;
                                    border-radius:20px;
                                    -webkit-box-shadow: 0 0 10px rgba(0,0,0,0.2);
                                    -moz-box-shadow: 0 0 10px rgba(0,0,0,0.2);
                                    box-shadow: 0 0 10px rgba(0,0,0,0.2);
                                    color:#fff;
                                    line-height:20px;
                                    font-size:14px;
                                    font-weight:600;
                                    background: #7ca016;
                                    border:none;
                                    margin: 0 auto 50px;
                                    display:block;
                                    text-decoration:none;
                                    cursor: pointer;
                                    text-align: center;">
                                    Collect and receive {{$amount}}cr (worth &#163;{{$amount/100}})
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table align="center" cellpadding="0" cellspacing="0" border="0"
                       style="border-collapse:collapse;text-align: center;display: block;">
                    <tbody style="width: 100%;display: inline-block;max-width: 600px;background:#ebebeb;">
                        <tr style="width: 100%;display: block;text-align: center;">
                            <td align="center" style="display: inline-block;font-size:14px;font-weight:400;
                                                            padding:30px 15px 10px;color: #000;">
                                Or copy and paste this link into your browser
                            </td>
                        </tr>
                        <tr style="width: 100%;display: block;">
                            <td align="center" style="font-size:14px;font-weight:400;
                                                text-align: center;width:100%;display: block;">
                                <span style="display: block;color: #2b2b2b;text-align: center;padding:0 15px 20px;">
                                    {{$url}}
                                </span>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table align="center" cellpadding="0" cellspacing="0" border="0"
                       style="border-collapse: collapse;text-align: center;margin: 0 auto;">
                    <tbody style="width: 100%;display: inline-block;max-width: 600px;">
                        <tr style="width: 100%;display: block;">
                            <td colspan="2" align="center" style="font-size:14px;font-weight:400;display: block;
                                                                padding:30px 0 10px;width:100%;">
                                <table align="center" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;
                                        display: block;width:80%">
                                    <tr style="width: 100%;display: block;">
                                        <td style="color:#000;text-align: center;width: 100%;display: block;">
                                            You are receiving this email because
                                            <b>{{ $userName }} {{ $userSurname }}</b>
                                            is a member of Ecosquared and chose to share with you <br>
                                            If you do not want to receive more email notification,
                                            visit to
                                            <a href="{{$urlUnsubscribe}}" target="_blank" style="color:#000;
                                                text-align: center;">unsubscribe</a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr style="width: 100%;display: block;">
                            <td style="font-size: 14px; font-weight: 400; padding-bottom: 20px;width:50%; float:left">
                                <table align="center" width="100%" cellpadding="0" cellspacing="0" border="0"
                                       style="border-collapse: collapse; text-align: center;display: block;">
                                    <tbody style="width: 100%;display: block;">
                                        <tr style="width: 100%;display: block;">
                                            <td align="center" style="width: 100%;display: block;">
                                                <a href="https://ecosquared.tech" target="_blank" style="color:#000;text-align: center;">
                                                    ecosquared.tech
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td style="font-size: 14px; font-weight: 400; padding-bottom: 20px;width:50%; float:right">
                                <table align="center" width="100%" cellpadding="0" cellspacing="0" border="0"
                                       style="border-collapse: collapse; text-align: center;display: block;">
                                    <tbody style="width: 100%;display: block;">
                                        <tr style="width: 100%;display: block;">
                                            <td align="center" style="width: 100%;display: block;">
                                                <a href="https://ecosquared.co.uk" target="_blank" style="color:#000;
                                                text-align: center;">
                                                    ecosquared.co.uk
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>