<table width="100%" cellspacing="0" cellpadding="0" border="0" style="text-align: center;">
    <tbody style="width: 100%;display: inline-block;">
    <tr style="width: 100%;display: block;">
        <td align="center" style="text-align: center;width: 100%;display: block;">
            <table width="600" cellpadding="0" cellspacing="0" border="0"
                   style="border-collapse: collapse; background: #eff3e8;display: block;margin: 0 auto;">
                <tbody style="display: block;width: 100%;">
                <tr style="width: 100%;display: block;">
                    <td align="center" style="font-size: 16px; font-weight: 800; padding: 50px 0;color: #000;
                                                text-align: center;width: 100%;display: block;">
                        You have request invite from <b>{{ $email }}</b>
                    </td>
                    <td align="center" style="font-size: 14px; font-weight: 400; padding: 20px 0;color: #000;
                                                text-align: center;width: 100%;display: block;font-style: italic">
                        {{ $userMessage }}
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>