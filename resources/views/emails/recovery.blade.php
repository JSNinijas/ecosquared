<table align="center" width="100%" id="invite" cellspacing="0" cellpadding="0" border="0"
       style="text-align: center;width: 100%;display: block;">
    <tbody style="width: 100%;display: inline-block;">
    <tr style="width: 100%;display: block;">
        <td align="center" style="text-align:center;width: 100%;display: block;max-width: 600px">
            <table align="center" cellpadding="0" cellspacing="0" border="0"
                   style="border-collapse: collapse;width: 100%;display: block;text-align: center;">
                <tbody style="width: 100%;display: inline-block;max-width:600px;background: #eff3e8;">
                <tr style="width: 100%;display: inline-block;">
                    <td align="center" style="padding:50px 0 40px;text-align:center;
                                                        width:100%;display: inline-block;">
                        <img style="display: inline-block;" width="50%" src="http://ecosquared.co.uk/img/logo-1x.png" />
                    </td>
                </tr>
                </tbody>
            </table>
            <table align="center" cellpadding="0" cellspacing="0" border="0"
                   style="border-collapse: collapse;width: 100%;display: block;text-align: center;">
                <tbody style="width: 100%;display: inline-block;max-width:600px;">
                <tr style="width: 100%;display: block;text-align: center;padding: 40px 0;">
                    <td align="center" style="text-align: center;width: 100%;display: inline-block;">
                        <a href="{{$url}}" style="
                                    position:relative;
                                    max-width:300px;
                                    padding:5px 15px;
                                    border-radius:20px;
                                    -webkit-box-shadow: 0 0 10px rgba(0,0,0,0.2);
                                    -moz-box-shadow: 0 0 10px rgba(0,0,0,0.2);
                                    box-shadow: 0 0 10px rgba(0,0,0,0.2);
                                    color:#fff;
                                    line-height:20px;
                                    font-size:14px;
                                    font-weight:600;
                                    background: #7ca016;
                                    border:none;
                                    margin: 0 auto 50px;
                                    display:block;
                                    text-decoration:none;
                                    cursor: pointer;
                                    text-align: center;">
                            Click here to recover password
                        </a>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>