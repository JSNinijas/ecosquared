<table align="center" width="100%" id="invite" cellspacing="0" cellpadding="0" border="0"
       style="text-align: center;width: 100%;display: block;">
    <tbody style="width: 100%;display: inline-block;">
    <tr style="width: 100%;display: block;">
        <td align="center" style="text-align:center;width: 100%;display: block;max-width: 600px">
            <table align="center" cellpadding="0" cellspacing="0" border="0"
                   style="border-collapse: collapse;width: 100%;display: block;text-align: center;">
                <tbody style="width: 100%;display: inline-block;max-width:600px;background: #eff3e8;">
                <tr style="width: 100%;display: inline-block;">
                    <td align="center" style="padding:50px 0 40px;text-align:center;
                                                        width:100%;display: inline-block;">
                        <img style="display: inline-block;" width="50%" src="http://ecosquared.co.uk/img/logo-1x.png" />
                    </td>
                </tr>
                <tr style="width: 100%;display: inline-block;">
                    <td align="center" style="font-size: 16px;line-height: 20px;font-weight:400;display: inline-block;
                            padding: 0 15px 50px;color:#000;text-align:center;width:100%;box-sizing:border-box">
                        <b class="animation1">Your {{ $statisticsSetting }} summary of activity.</b> <br>
                        <b>Your balance is 100cr (worth &#163;1)</b>
                    </td>
                </tr>
                </tbody>
            </table>
            <table align="center" cellpadding="0" cellspacing="0" border="0"
                   style="border-collapse: collapse;width: 100%;display: block;text-align: center;">
                <tbody style="width: 100%;display: inline-block;max-width:600px;">
                <tr style="width: 100%;display: block;">
                    <td align="center" style="width: 100%;line-height: 20px; padding: 10px 0;display: inline-block;">
                        <span style="width: 100%;font-weight: 600; font-size: 14px;color: #000;
                                    line-height: 20px;display: inline-block;">
                            You were offered 3 products from 2 people with total of 200cr (worth &#163;2).<br>
                            You accepted 2 products with total of 100cr (worth &#163;1) <br>
                            and still have 1 product incoming with total of 100cr (worth &#163;1)
                        </span>
                    </td>
                    <td align="center" style="width: 100%;line-height: 20px; padding: 10px 0;display: inline-block;">
                        <span style="width: 100%;font-weight: 600; font-size: 14px;color: #000;
                                    line-height: 20px;display: inline-block;">
                            Your 2 products were supported with total of 50cr (worth &#163;0.50)
                        </span>
                    </td>
                    <td align="center" style="width: 100%;line-height: 20px; padding: 10px 0;display: inline-block;">
                        <span style="width: 100%;font-weight: 600; font-size: 14px;color: #000;
                                    line-height: 20px;display: inline-block;">
                            You were thanked a total of 2345 by 7 people across 4 projects.
                        </span>
                    </td>
                </tr>
                </tbody>
            </table>
            <table align="center" cellpadding="0" cellspacing="0" border="0"
                   style="border-collapse:collapse;text-align: center;display: block;">
                <tbody style="width: 100%;display: inline-block;max-width: 600px;background:#ebebeb;">
                <tr style="width: 100%;display: block;text-align: center;">
                    <td align="center" style="width: 100%;line-height: 20px; padding: 10px 0;display: inline-block;">
                        <span style="width: 100%;font-weight: 600; font-size: 14px;color: #000;
                                    line-height: 20px;display: inline-block;">
                            You offered 3 products to 2 people with total of 200cr (worth &#163;2). <br>
                            They accepted 2 products with total of 100cr (worth &#163;1) <br>
                            and still have 1 product outgoing with total of 100cr (worth &#163;1)<br>
                        </span>
                    </td>
                    <td align="center" style="width: 100%;line-height: 20px; padding: 10px 0;display: inline-block;">
                        <span style="width: 100%;font-weight: 600; font-size: 14px;color: #000;
                                    line-height: 20px;display: inline-block;">
                            You supported 2 products with total of 50cr (worth &#163;0.50)
                        </span>
                    </td>
                    <td align="center" style="width: 100%;line-height: 20px; padding: 10px 0;display: inline-block;">
                        <span style="width: 100%;font-weight: 600; font-size: 14px;color: #000;
                                    line-height: 20px;display: inline-block;">
                            You thanked a total of 2345 by 7 people across 4 projects.
                        </span>
                    </td>
                </tr>
                </tbody>
            </table>
            <table align="center" cellpadding="0" cellspacing="0" border="0" width="100%"
                   style="border-collapse: collapse;text-align: center;margin: 0 auto;">
                <tbody style="width: 100%;display: inline-block;max-width: 600px;">
                <tr style="width: 100%;display: block;">
                    <td style="font-size: 14px; font-weight: 400; padding-bottom: 20px;width:50%; float:left">
                        <table align="center" width="100%" cellpadding="0" cellspacing="0" border="0"
                               style="border-collapse: collapse; text-align: center;display: block;">
                            <tbody style="width: 100%;display: block;">
                            <tr style="width: 100%;display: block;">
                                <td align="center" style="width: 100%;display: block;padding: 20px 0;">
                                    <a href="https://ecosquared.tech" target="_blank" style="color:#000;text-align: center;">
                                        ecosquared.tech
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                    <td style="font-size: 14px; font-weight: 400; padding-bottom: 20px;width:50%; float:right">
                        <table align="center" width="100%" cellpadding="0" cellspacing="0" border="0"
                               style="border-collapse: collapse; text-align: center;display: block;">
                            <tbody style="width: 100%;display: block;">
                            <tr style="width: 100%;display: block;">
                                <td align="center" style="width: 100%;display: block;padding: 20px 0;">
                                    <a href="https://ecosquared.co.uk" target="_blank" style="color:#000;
                                                text-align: center;">
                                        ecosquared.co.uk
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>