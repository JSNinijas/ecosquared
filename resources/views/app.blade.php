<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- RECAPTCHA KEY -->
    <meta name="recaptcha-key" content="{{ config('recaptcha.key') }}">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <title>{{ config('app.name', 'Laravel') }}</title>

    <script src="//{{ Request::getHost() }}:8443/socket.io/socket.io.js"></script>

    <link rel="apple-touch-icon" sizes="180x180" href="/pictures/favicons/apple-touch-icon.png?rand={{rand()}}">
    <link rel="icon" type="image/png" sizes="32x32" href="/pictures/favicons/favicon-32x32.png?rand={{rand()}}">
    <link rel="icon" type="image/png" sizes="16x16" href="/pictures/favicons/favicon-16x16.png?rand={{rand()}}">
    <link rel="manifest" href="/pictures/favicons/manifest.json?rand={{rand()}}">
    <link rel="mask-icon" href="/pictures/favicons/safari-pinned-tab.svg?rand={{rand()}}" color="#5bbad5">
    <meta name="theme-color" content="transparent">
    
    <!-- Styles -->

    <link href="{{ asset('css/app.css?v=1.4.12') }}" rel="stylesheet">

</head>
<body>
    <div id="app" class="app-viewport phone-viewport"><app-main></app-main></div>

    <!-- Scripts -->
    <script>
    function findGetParameter(parameterName) {
        var result = null,
            tmp = [];
        location.search
            .substr(1)
            .split("&")
            .forEach(function (item) {
              tmp = item.split("=");
              if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
            });
        return result;
    }

    if(findGetParameter('debug')) {
        window.onerror = function(msg, url, line, col, error) {
           var extra = !col ? '' : '\ncolumn: ' + col;
           extra += !error ? '' : '\nerror: ' + error;
           document.write("Error: " + msg + "\nurl: " + url + "\nline: " + line + extra);
        };
    }
    </script>
    <script src="{{ asset('js/app.js?v=1.4.12') }}"></script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onRecaptchaloadCallback&render=explicit&hl=en"></script>
    <script src="https://chatwee-api.com/v2/script/5aa665d1bd616df3696d0ae8.js"></script>
</body>
</html>
